import sys
import os
import fileinput

# python magica_voxel_prepare_frame_animation.py Capsule capsule 25

if len(sys.argv) > 3:
  path = sys.argv[1]
  filename = sys.argv[2]
  frames = int(sys.argv[3])
else:
  print('No arguments!')
  exit()

filepath = "./" + path + "/" + filename

print("FilePath {}".format(filepath))

for frame in range(0, frames):
  num = str(frame)
  if len(str(frame)) < 2:
    num = "0" + str(frame)
  
  text_to_search = filename + "-" + num
  text_to_replace = filename
  with fileinput.FileInput(filepath + "-" + num + ".obj", inplace=True) as file:
    for line in file:
      print(line.replace(text_to_search, text_to_replace), end='')
      
  print("Frame {}".format(num))
      
  if frame > 0:
    os.remove(filepath + "-" + num + ".mtl")
    os.remove(filepath + "-" + num + ".png")
    
os.rename(filepath + "-00.mtl", filepath + ".mtl")
os.rename(filepath + "-00.png", filepath + ".png")

text_to_search = filename + "-00"
text_to_replace = filename
with fileinput.FileInput(filepath + ".mtl", inplace=True) as file:
    for line in file:
      print(line.replace(text_to_search, text_to_replace), end='')
      
print("Done.")