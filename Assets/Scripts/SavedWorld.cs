﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public class SavedWorld
{
  public int lastId = 100000;

  public int resourcesInOrbitValue = -1;
  public float resourcesInOrbitTimer = -1f;

  public float checkEnemyWaveTimer = 0f;
  public float gameTimer = 0f;
  public float gameWithoutPauseTimer = 0f;
  public float taskWithoutPauseTimer = 0f;
  public float pauseTimer = 0f;

  public int lastEnemyWaveId = 0;
  public int lastLevelTaskId = 0;

  public Dictionary<int, int> hintCounter = new Dictionary<int, int>();
  public Dictionary<int, int> destroyedObjects = new Dictionary<int, int>();
  public Dictionary<int, List<int>> foundObjects = new Dictionary<int, List<int>>();

  public List<SavedWorldObject> objects = new List<SavedWorldObject>();

  public bool IsEmpty()
  {
    return objects.Count == 0;
  }

  public static void Write(SavedWorld world, string filename)
  {
    using (Stream stream = File.Open(filename + ".saved", FileMode.Create))
    {
      var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
      binaryFormatter.Serialize(stream, world);
    }
  }

  public static SavedWorld Read(string filename)
  {
    try
    {
      using (Stream stream = File.Open(filename + ".saved", FileMode.Open))
      {
        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        return (SavedWorld)binaryFormatter.Deserialize(stream);
      }
    }
    catch (Exception e)
    {
      return new SavedWorld();
    }
  }

  public override string ToString()
  {
    string temp = "";

    temp += "\n ***** SAVED WORLD *****";
    temp += "\n lastId=" + lastId;
    temp += "\n resourcesInOrbitValue=" + resourcesInOrbitValue;
    temp += "\n resourcesInOrbitTimer=" + resourcesInOrbitTimer;
    temp += "\n checkEnemyWaveTimer=" + checkEnemyWaveTimer;
    temp += "\n gameTimer=" + gameTimer;
    temp += "\n gameWithoutPauseTimer=" + gameWithoutPauseTimer;
    temp += "\n taskWithoutPauseTimer=" + taskWithoutPauseTimer;
    temp += "\n pauseTimer=" + pauseTimer;
    temp += "\n lastEnemyWaveId=" + lastEnemyWaveId;
    temp += "\n lastLevelTaskId=" + lastLevelTaskId;
    temp += "\n ***********************";

    return temp;
  }
}
