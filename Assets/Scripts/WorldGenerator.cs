﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using static MapObject;

public class WorldGenerator
{
  private const int TYPE_BLOCK_HERO = 1;
  private const int TYPE_BLOCK_PLAIN = 2;
  private const int TYPE_BLOCK_FOREST = 3;
  private const int TYPE_BLOCK_BASE = 4;
  private const int TYPE_BLOCK_FUEL = 5;
  private const int TYPE_BLOCK_BORDER = 7;

  private float ONE_BLOCK_SIZE = 0f;
  private const float ONE_TREE_SIZE = 5f;
  private const float ONE_STONES_SIZE = 25f;

  private int mapSizeX;
  private int mapSizeY;
  private int sizeX;
  private int sizeY;
  private int halfSizeX;
  private int halfSizeY;

  private float plainToForestRation = 0.5f;
  private float fuelRation = 1f;
  private float treeScaleFrom = 0.5f;
  private float treeScaleTo = 1.5f;
  private int maxFuelPointsByOneBase = 2;
  private int maxEnemyPointsByOneBase = 2;

  private int[,] blocksMap;

  private List<WorldObject> levelObjects;

  private EMapObjectType[] stoneTypes = new EMapObjectType[3] { EMapObjectType.TYPE_STONE_20, EMapObjectType.TYPE_STONE_40, EMapObjectType.TYPE_STONE_60 };

  protected void Reset()
  {
    blocksMap = new int[sizeX, sizeY];
    levelObjects = new List<WorldObject>();
  }

  public void SetMapSize(int mapSizeX, int mapSizeY, int mapBlockSize)
  {
    ONE_BLOCK_SIZE = mapBlockSize;

    if (mapSizeX % ONE_BLOCK_SIZE != 0 || mapSizeY % ONE_BLOCK_SIZE != 0)
    {
      throw new Exception("Map size must be a multiple of " + ONE_BLOCK_SIZE);
    }

    if (mapSizeX % (ONE_BLOCK_SIZE * 2) == 0 || mapSizeY % (ONE_BLOCK_SIZE * 2) == 0)
    {
      throw new Exception("Map size must be not a multiple of " + (ONE_BLOCK_SIZE * 2));
    }

    this.mapSizeX = mapSizeX;
    this.mapSizeY = mapSizeY;

    sizeX = Mathf.RoundToInt(mapSizeX / ONE_BLOCK_SIZE);
    sizeY = Mathf.RoundToInt(mapSizeY / ONE_BLOCK_SIZE);

    halfSizeX = Mathf.RoundToInt(sizeX / 2);
    halfSizeY = Mathf.RoundToInt(sizeY / 2);
  }

  public void SetTreeScale(float treeScaleFrom, float treeScaleTo)
  {
    this.treeScaleFrom = treeScaleFrom;
    this.treeScaleTo = treeScaleTo;
  }

  public void SetBlocksRatio(float plainToForestRation)
  {
    this.plainToForestRation = plainToForestRation;
  }

  public void SetFuelRatio(float fuelRation)
  {
    this.fuelRation = fuelRation;
  }

  public void Start(string filename)
  {
    System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
    stopWatch.Start();
    Debug.Log("Generate process is started...");
    Reset();
    Debug.Log("...Reset()");
    AddHero();
    Debug.Log("...AddHero()");
    CreateBlocksMap();
    Debug.Log("...CreateBlocksMap()");
    CreateTrees();
    Debug.Log("...CreateTrees()");
    AddBasePoints();
    Debug.Log("...AddBasePoints()");
    AddFuelPoints();
    Debug.Log("...AddFuelPoints()");
    CreateStones();
    Debug.Log("...CreateStones()");
    Utils.Print(blocksMap);
    SaveLevel(filename);
    Debug.Log("...SaveLevel()");
    Debug.Log("Generate process is finished.");
    stopWatch.Stop();
    TimeSpan ts = stopWatch.Elapsed;
    string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
        ts.Hours, ts.Minutes, ts.Seconds,
        ts.Milliseconds / 10);
    Debug.Log("RunTime " + elapsedTime);
  }

  protected void AddHero()
  {
    WorldObject levelObject = new WorldObject();
    levelObject.SetPosition(new Vector3(mapSizeX / 2, 0, -mapSizeY / 2));
    levelObject.type = (int)EMapObjectType.TYPE_HERO;
    levelObjects.Add(levelObject);
  }

  protected void CreateBlocksMap()
  {
    for (int x = 0; x < sizeX; x++)
    {
      for (int y = 0; y < sizeY; y++)
      {
        if (x == 0 || y == 0 || x == (sizeX - 1) || y == (sizeY - 1))
          blocksMap[x, y] = TYPE_BLOCK_BORDER;
        else
          blocksMap[x, y] = 0;
      }
    }

    for (int x = -1; x <= 1; x++)
    {
      for (int y = -1; y <= 1; y++)
      {
        blocksMap[halfSizeX + x, halfSizeY + y] = TYPE_BLOCK_PLAIN;
      }
    }

    blocksMap[halfSizeX, halfSizeY] = TYPE_BLOCK_HERO;

    for (int x = 0; x < sizeX; x++)
    {
      for (int y = 0; y < sizeY; y++)
      {
        if (blocksMap[x, y] == 0)
        {
          float random = UnityEngine.Random.Range(0f, 1f);
          if (random <= plainToForestRation)
          {
            blocksMap[x, y] = TYPE_BLOCK_PLAIN;
          }
          else
          {
            blocksMap[x, y] = TYPE_BLOCK_FOREST;
          }
        }
      }
    }
  }

  protected void CreateTrees()
  {
    int groupId = 0;
    float[] angles = new float[] { 0, 90, 180, 270 };
    for (int x = 0; x < sizeX; x++)
    {
      for (int y = 0; y < sizeY; y++)
      {
        if (blocksMap[x, y] == TYPE_BLOCK_FOREST)
        {
          groupId++;
          float fromX = x * ONE_BLOCK_SIZE;
          float fromY = y * ONE_BLOCK_SIZE;
          float groupX = x * ONE_BLOCK_SIZE + ONE_BLOCK_SIZE / 2;
          float groupY = y * ONE_BLOCK_SIZE + ONE_BLOCK_SIZE / 2;
          int treesXY = Mathf.RoundToInt(ONE_BLOCK_SIZE / ONE_TREE_SIZE);
          
          /*WorldObject worldObject = new WorldObject();
          worldObject.SetPosition(new Vector3(groupX, 0f, -groupY));
          worldObject.type = Const.TYPE_FOREST;
          worldObject.angle = angles[UnityEngine.Random.Range(0, angles.Length)];
          levelObjects.Add(worldObject);
          Debug.Log("+ forest");*/

          for (int tX = 0; tX < treesXY; tX++)
            for (int tY = 0; tY < treesXY; tY++)
            {
              float random = UnityEngine.Random.Range(0f, 1f);
              if (random > 0.4f)
              {
                WorldObject levelObject = new WorldObject();
                float randomX = UnityEngine.Random.Range(fromX + (tX * ONE_TREE_SIZE), fromX + ((tX + 1) * ONE_TREE_SIZE));
                float randomY = UnityEngine.Random.Range(fromY + (tY * ONE_TREE_SIZE), fromY + ((tY + 1) * ONE_TREE_SIZE));
                levelObject.SetPosition(new Vector3(randomX, 0f, -randomY));
                levelObject.type = (int)EMapObjectType.TYPE_TREE;
                levelObject.scaleXZ = UnityEngine.Random.Range(treeScaleFrom, treeScaleTo);
                levelObject.scaleY = UnityEngine.Random.Range(treeScaleFrom, treeScaleTo);
                levelObject.groupId = groupId;
                levelObject.SetGroupPosition(new Vector3(groupX, 0f, -groupY));
                levelObjects.Add(levelObject);
                Debug.Log("+ tree");
              }
            }
        }
      }
    }
  }

  protected void AddBasePoints()
  {
    int betweenSizeX = -1;
    int betweenSizeY = 0;

    for (int x = 0; x < sizeX; x++)
    {
      betweenSizeX *= -1;
      for (int y = 0; y < sizeY; y++)
      {
        betweenSizeY += 1;
        if (blocksMap[x, y] == TYPE_BLOCK_PLAIN)
        {
          if (betweenSizeX == 1 && betweenSizeY > 4)
          {
            betweenSizeY = 0;

            blocksMap[x, y] = TYPE_BLOCK_BASE;
          }
        }
      }
    }
  }

  protected void AddFuelPoints()
  {
    for (int x = 0; x < sizeX; x++)
    {
      for (int y = 0; y < sizeY; y++)
      {
        if (blocksMap[x, y] == TYPE_BLOCK_BASE)
        {
          AddFuelPointsForBaseWithCoords(x, y);
        }
      }
    }
  }

  protected void AddFuelPointsForBaseWithCoords(int baseX, int baseY)
  {
    int limit = maxFuelPointsByOneBase;
    for (int x = -2; x <= 2; x++)
    {
      for (int y = -2; y <= 2; y++)
      {
        if (limit == 0)
          break;

        int newX = baseX + x;
        int newY = baseY + y;

        if (newX < 0 || newX >= sizeX || newY < 0 || newY >= sizeY)
          continue;

        if (blocksMap[newX, newY] == TYPE_BLOCK_PLAIN)
        {
          float random = UnityEngine.Random.Range(0f, 1f);
          if (random <= fuelRation)
          {
            limit -= 1;
            blocksMap[newX, newY] = TYPE_BLOCK_FUEL;

            WorldObject levelObject = new WorldObject();
            levelObject.SetPosition(new Vector3(newX * ONE_BLOCK_SIZE + ONE_BLOCK_SIZE / 2, 0f, -(newY * ONE_BLOCK_SIZE + ONE_BLOCK_SIZE / 2)));
            levelObject.type = (int)EMapObjectType.TYPE_FUEL_SPAWN_POINT;
            levelObjects.Add(levelObject);

            Debug.Log("+ fuel");
          }
        }
      }
    }
  }

  protected void CreateStones()
  {
    for (int x = 0; x < sizeX; x++)
    {
      for (int y = 0; y < sizeY; y++)
      {
        if (blocksMap[x, y] == TYPE_BLOCK_PLAIN)
        {
          float fromX = x * ONE_BLOCK_SIZE;
          float fromY = y * ONE_BLOCK_SIZE;
          int stoneXY = Mathf.RoundToInt(ONE_BLOCK_SIZE / ONE_STONES_SIZE);
          for (int tX = 0; tX < stoneXY; tX++)
            for (int tY = 0; tY < stoneXY; tY++)
            {
              float random = UnityEngine.Random.Range(0f, 1f);
              if (random > 0.4f)
              {
                WorldObject levelObject = new WorldObject();
                float randomX = UnityEngine.Random.Range(fromX + (tX * ONE_STONES_SIZE), fromX + ((tX + 1) * ONE_STONES_SIZE));
                float randomY = UnityEngine.Random.Range(fromY + (tY * ONE_STONES_SIZE), fromY + ((tY + 1) * ONE_STONES_SIZE));
                levelObject.SetPosition(new Vector3(randomX, 0f, -randomY));
                levelObject.type = (int)stoneTypes[UnityEngine.Random.Range(0, 2)];
                levelObject.angle = UnityEngine.Random.Range(0, 360);
                levelObjects.Add(levelObject);
                Debug.Log("+ stone");
              }
            }
        }
      }
    }
  }

  public void SaveLevel(string filename)
  {
    World level = new World
    {
      sizeX = mapSizeX,
      sizeY = mapSizeY,
      objects = levelObjects
    };
    World.Write(level, filename);

    if (File.Exists(filename + ".saved"))
    {
      File.Delete(filename + ".saved");
    }
  }
}
