﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SateliteStrike : MapObject
{
  public ParticleSystem fire;
  private float firingTimer = 0;
  private float prepareTimer = 0;

  void Awake()
  {
    firingTimer = 0;
    prepareTimer = 0;
    Init();
    Stop();
    Ready();
    Utils.SetCollision4Particles(fire, Const.TAG_PLASMA);
  }

  void Update()
  {
    if (isPaused)
      return;
    
    if (prepareTimer > 0)
    {
      prepareTimer -= Time.deltaTime;
      if (prepareTimer <= 0)
      {
        prepareTimer = 0;
        fire.Play();
      }
      return;
    }

    if (firingTimer > 0)
    {
      firingTimer -= Time.deltaTime;
      if (firingTimer <= 0)
      {
        firingTimer = 0;
        Stop();
      }
    }
  }

  private void LateUpdate()
  {
    if (isPaused)
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return;
    }

    if (fire.isPlaying 
      && !audioObject.IsPlaying(0))
    {
      audioObject.Play(0);
    }
  }

  private void Stop()
  {
    fire.Stop();
    GoTo(Const.OUT_OF_WORLD_POSITION);
  }

  public void Fire(Vector3 position, float prepareDuration, float firingDuration)
  {
    GoTo(position);
    prepareTimer = prepareDuration;
    firingTimer = firingDuration;
  }

  public bool IsFiring()
  {
    return fire.isPlaying;
  }

  private void GoTo(Vector3 position)
  {
    transform.position = new Vector3(position.x, 0, position.z);
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);

    if (fire.isPlaying && value)
    {
      fire.Pause();
    }
    else if (fire.isPaused)
    {
      fire.Play();
    }
  }
}
