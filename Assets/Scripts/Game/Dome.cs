﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dome : MonoBehaviour
{
  BoxCollider boxCollider;

  void Start()
  {
    boxCollider = GetComponent<BoxCollider>();
  }

  public void SetSize(Vector3 size, float mapBlockSize)
  {
    boxCollider.size = new Vector3(size.x - mapBlockSize * 2, size.y, size.z - mapBlockSize * 2);
    transform.localPosition = new Vector3(size.x / 2f, 0, size.z / 2f);
  }

  private void OnTriggerExit(Collider other)
  {
    IMovingObject movingObject = other.gameObject.GetComponent<IMovingObject>();
    if (movingObject != null)
    {
      movingObject.BackTo();
    }
  }
}
