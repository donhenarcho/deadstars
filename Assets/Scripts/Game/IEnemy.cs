﻿using UnityEngine;

public interface IEnemy
{
  void Damage(GameObject other, EnemyPartTagEnum partTag, int damage, int partHp);
}
