﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGun
{
  void ChangePosition(float speed, bool firing);
  bool Use(float fuelLeft);
  void Grab();
  void Put();
  void On();
  void Off();
  void SetActive(bool isActive);
}
