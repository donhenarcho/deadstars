﻿public enum GolemStateEnum
{
  Ok,
  WithoutHandLeft,
  WithoutHandRight,
  WithoutHands,
  WithoutLegs,
  WithoutLegsWithoutHandLeft,
  WithoutLegsWithoutHandRight,
  WithoutLegsWithoutHands,
  Dead
}