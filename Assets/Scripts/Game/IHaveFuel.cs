﻿using UnityEngine;
using static MapObject;

public interface IHaveFuel
{
  int FuelIn(int value);
  int FuelReturn(int value);
  int FuelOut();
  int FuelOut(int value);
  bool IsFull();
  bool IsEmpty();
  void FillToMax();
  float GetFuelLeft();
  Vector3 GetPosition();
  GameObject GetGameObject();
  EMapObjectType GetMapObjectType();
}
