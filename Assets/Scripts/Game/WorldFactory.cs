﻿using System.Collections.Generic;
using UnityEngine;
using static MapObject;

public class WorldFactory : MonoBehaviour
{
  public Transform wrapper;

  public GameObject[] Prefabs;

  private VariableManager variableManager;

  private Dictionary<EMapObjectType, GameObject> prefabs;
  private Dictionary<EMapObjectType, Queue<GameObject>> queue;

  void Awake()
  {
    prefabs = new Dictionary<EMapObjectType, GameObject>();

    foreach (GameObject prefab in Prefabs)
    {
      MapObject mapObject = prefab.GetComponent<MapObject>();
      prefabs.Add(mapObject.GetUniqueType(), prefab);
    }

    queue = new Dictionary<EMapObjectType, Queue<GameObject>>();
  }

  public void Fill()
  {
    variableManager = FindObjectOfType<VariableManager>();

    Dictionary<int, int> factoryLimits = variableManager.GetFactoryLimits();

    int numTerraformers = 0;
    if (factoryLimits.ContainsKey((int)EMapObjectType.TYPE_TERRAFORMER) 
      && factoryLimits[(int)EMapObjectType.TYPE_TERRAFORMER] > 0)
    {
      for (int i = 0; i < 10 * numTerraformers; i++)
      {
        AddObject(NewObject(EMapObjectType.TYPE_TURRET, EMapObjectType.SUB_TYPE_TURRET_LEVEL_1));
        AddObject(NewObject(EMapObjectType.TYPE_TURRET, EMapObjectType.SUB_TYPE_TURRET_LEVEL_2));
        AddObject(NewObject(EMapObjectType.TYPE_TURRET, EMapObjectType.SUB_TYPE_TURRET_LEVEL_3));
        AddObject(NewObject(EMapObjectType.TYPE_TURRET, EMapObjectType.SUB_TYPE_TURRET_LEVEL_4));
        AddObject(NewObject(EMapObjectType.TYPE_TURRET, EMapObjectType.SUB_TYPE_TURRET_LEVEL_5));
      }
    }

    for (int i = 0; i < 40; i++)
    {
      AddObject(NewObject(EMapObjectType.TYPE_STONE_20));
      AddObject(NewObject(EMapObjectType.TYPE_STONE_40));
      AddObject(NewObject(EMapObjectType.TYPE_STONE_60));
    }

    foreach (KeyValuePair<int, int> keyValue in factoryLimits)
    {
      for (int i = 0; i < keyValue.Value; i++)
      {
        AddObject(NewObject((EMapObjectType)keyValue.Key));
      }
    }
  }

  public void ReturnObject(MapObject mapObject)
  {
    if (mapObject.GetMapObjectType() == EMapObjectType.TYPE_HERO)
    {
      return;
    }
    //Debug.Log("RETURN >>> " + mapObject.GetMapObjectType() + " >>> " + mapObject.GetMapObjectSubType());
    AddObject(mapObject.GetGameObject());
  }

  private GameObject NewObject(EMapObjectType objectType)
  {
    return NewObject(objectType, EMapObjectType.TYPE_NONE);
  }

  private GameObject NewObject(EMapObjectType objectType, EMapObjectType objectSubType)
  {
    EMapObjectType prefabKey = objectSubType != EMapObjectType.TYPE_NONE ? objectSubType : objectType;

    if (!prefabs.ContainsKey(prefabKey))
    {
      //Debug.LogError("PREFAB KEY NOT FOUND >>> " + prefabKey);
      return null;
    }

    GameObject gameObject = Instantiate(prefabs[prefabKey], Vector3.zero, Quaternion.identity);
    gameObject.GetComponent<MapObject>().EndedObjectInit();
    return gameObject;
  }

  private void AddObject(GameObject gameObject)
  {
    if (gameObject == null)
    {
      //Debug.LogError("ADDED GAME OBJECT IS NULL");
      return;
    }

    MapObject mapObject = gameObject.GetComponent<MapObject>();
    mapObject.Reset();
    gameObject.transform.parent = wrapper;

    CharacterController characterController = gameObject.GetComponent<CharacterController>();
    
    if (characterController != null)
    {
      characterController.enabled = false;
    }
    
    gameObject.transform.position = Const.OUT_OF_WORLD_POSITION;

    if (characterController != null)
    {
      characterController.enabled = true;
    }

    EMapObjectType objectType = mapObject.GetUniqueType();

    if (!queue.ContainsKey(objectType))
    {
      queue.Add(objectType, new Queue<GameObject>());
    }

    queue[objectType].Enqueue(gameObject);
  }

  public GameObject GetObject(EMapObjectType objectType)
  {
    GameObject gameObject = null;
    if (queue.ContainsKey(objectType) && queue[objectType].Count > 0)
    {
      //Debug.Log("WorldFactory.GetObject() >>> OLD >>> " + objectType);
      gameObject = queue[objectType].Dequeue();
      return gameObject;
    }
    //Debug.Log("WorldFactory.GetKamikaze() >>> NEW >>> " + objectType);
    AddObject(NewObject(objectType));

    if (queue.ContainsKey(objectType) && queue[objectType].Count > 0)
    {
      gameObject = queue[objectType].Dequeue();
    }
    
    return gameObject;
  }
}
