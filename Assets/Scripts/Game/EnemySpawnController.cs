﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpawnController
{
  WorldController worldController;
  int spawnPointBetweenDistance = 10;
  List<Vector3> spawnPoints;

  public void SetWorldController(WorldController worldController)
  {
    this.worldController = worldController;
  }

  public void SetMapSize(int mapSizeX, int mapSizeY)
  {
    spawnPoints = new List<Vector3>();
    float stepsForX = mapSizeX / spawnPointBetweenDistance;
    float stepsForY = mapSizeY / spawnPointBetweenDistance;

    for (int i = 1; i < stepsForX; i++)
    {
      for (int j = 1; j < stepsForY; j++)
      {
        if (i == 1 || j == 1 || i == (stepsForX - 1) || j == (stepsForY - 1))
        {
          Vector3 newPoint = new Vector3(i * spawnPointBetweenDistance, 0, -j * spawnPointBetweenDistance);
          spawnPoints.Add(newPoint);
          //Debug.Log("> " + newPoint);
        }
      }
    }

    //Debug.Log("Spawn Point Count = " + spawnPoints.Count);
  }

  public void Spawn(EnemyWave settings)
  {
    if (settings == null)
    {
      return;
    }

    if (settings.GetObjectValuesDict().Count == 0)
      return;

    Dictionary<int, int> counter = new Dictionary<int, int>(settings.GetObjectValuesDict());

    List<Vector3> points = spawnPoints.OrderBy(x => Random.value).ToList();

    int allTypes = counter.Count;
    foreach (Vector3 point in points)
    {
      foreach (KeyValuePair<int, int> keyValuePair in counter)
      {
        if (keyValuePair.Value > 0)
        {
          worldController.AddEnemy(keyValuePair.Key, point);
          counter[keyValuePair.Key] -= 1;
          if (counter[keyValuePair.Key] == 0)
          {
            allTypes -= 1;
          }
          break;
        }
      }

      if (allTypes == 0)
        break;
    }
  }
}
