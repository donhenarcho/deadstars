﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MapObject;

public interface IMapObject
{
  Vector3 GetPosition();
  bool IsHero();
  EMapObjectType GetMapObjectType();
}
