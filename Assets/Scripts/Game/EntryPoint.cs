﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
  void Start()
  {   
  }

  void Update()
  {
  }

  public Vector3 GetWorldCoords()
  {
    return transform.position;
  }

  public bool IsBeside(Vector3 target)
  {
    float distance = Vector3.Distance(GetWorldCoords(), target);
    return distance < 2;
  }
}
