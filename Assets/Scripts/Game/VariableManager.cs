﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using static MapObject;

public class VariableManager : MonoBehaviour
{
  public TextAsset defaultXml;
  public TextAsset[] levelXmls;
  public TextAsset[] levelMaps;

  LevelDefault levelDefault;
  List<Level> levels;
  Level level;

  void Awake()
  {
    ReadLevelDefault();
    ReadLevels();

    int currentLevelId = 1;
    if (PlayerPrefs.HasKey(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID))
    {
      currentLevelId = PlayerPrefs.GetInt(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID);
    }
    SetLevelId(currentLevelId);
  }

  private void ReadLevelDefault()
  {
    var serializer = new XmlSerializer(typeof(LevelDefault));
    using (var reader = new StringReader(defaultXml.text))
    {
      levelDefault = serializer.Deserialize(reader) as LevelDefault;
      levelDefault.InitAfterLoad();
      levelDefault.Print();
    }
  }

  private void ReadLevels()
  {
    levels = new List<Level>();

    var serializer = new XmlSerializer(typeof(Level));
    foreach (TextAsset textAsset in levelXmls)
    {
      using (var reader = new StringReader(textAsset.text))
      {
        Level level = serializer.Deserialize(reader) as Level;
        if (level != null)
        {
          level.InitAfterLoad();
          level.Print();
          levels.Add(level);
        }
      }
    }
  }

  private void SetLevelId(int levelId)
  {
    this.level = null;
    if (levelId > 0)
    {
      foreach (Level level in levels)
      {
        if (level.Id == levelId)
          this.level = level;
      }
    }
  }

  public List<Level> GetLevels()
  {
    return levels;
  }

  public Level GetCurrentLevel()
  {
    return level;
  }

  public Level GetNextLevel()
  {
    foreach (Level level in levels)
    {
      if (level.Id > this.level.Id)
        return level;
    }
    return null;
  }

  public LevelTask GetCurrentTask()
  {
    return GetCurrentLevel().GetCurrentTask();
  }

  public bool IsFirstLevel()
  {
    return GetCurrentLevel().Id == 1;
  }

  public bool IsFirstTaskInGame()
  {
    return GetCurrentLevel().Id == 1 && (GetCurrentTask() == null || (GetCurrentTask() != null && GetCurrentTask().Id == 1));
  }

  public string GetFilename()
  {
    if (level != null)
      return level.Filename;
    return levelDefault.Filename;
  }

  public World GetCurrentWorld()
  {
    if (level == null)
      return new World();

    return World.Read(levelMaps[level.Id - 1]);
  }

  /***********************************/
  /*************** MENU **************/
  /***********************************/

  public bool DoNeedToHideMenuButtons()
  {
    return level.HiddenMenu == 1;
  }

  public int[] GetHiddenMenuButtons()
  {
    return level.MenuHiddenButtons;
  }

  public int[] GetNewMenuButtons()
  {
    return level.MenuNewButtons;
  }

  /***********************************/
  /*** ENEMY SPAWN POINTS SETTINGS ***/
  /***********************************/

  public EnemyWave GetEnemyWave()
  {
    if (level != null)
      return level.GetEnemyWave();
    else
      return levelDefault.GetEnemyWave();
  }

  public void ReturnEnemyWave(EnemyWave wave)
  {
    if (level != null)
      level.ReturnEnemyWave(wave);
    else
      levelDefault.ReturnEnemyWave(wave);
  }

  public int GetDamage(EMapObjectType eMapObjectType)
  {
    int objectType = (int)eMapObjectType;

    if (level != null 
      && level.GetObjectSettingsDict().ContainsKey(objectType) 
      && level.GetObjectSettingsDict()[objectType].Damage > 0)
    {
      return level.GetObjectSettingsDict()[objectType].Damage;
    }

    if (levelDefault.GetObjectSettingsDict().ContainsKey(objectType)
      && levelDefault.GetObjectSettingsDict()[objectType].Damage > 0)
    {
      return levelDefault.GetObjectSettingsDict()[objectType].Damage;
    }

    Debug.LogError("DAMAGE NOT FOUND FOR: " + objectType);
    return 0;
  }

  public int GetHP(EMapObjectType eMapObjectType)
  {
    int objectType = (int)eMapObjectType;

    if (level != null
      && level.GetObjectSettingsDict().ContainsKey(objectType)
      && level.GetObjectSettingsDict()[objectType].HP > 0)
    {
      return level.GetObjectSettingsDict()[objectType].HP;
    }

    if (levelDefault.GetObjectSettingsDict().ContainsKey(objectType)
      && levelDefault.GetObjectSettingsDict()[objectType].HP > 0)
    {
      return levelDefault.GetObjectSettingsDict()[objectType].HP;
    }

    Debug.LogError("HP NOT FOUND FOR: " + objectType);
    return 0;
  }

  public float GetSpeed(EMapObjectType eMapObjectType)
  {
    int objectType = (int)eMapObjectType;

    if (level != null
      && level.GetObjectSettingsDict().ContainsKey(objectType)
      && level.GetObjectSettingsDict()[objectType].Speed > 0)
    {
      return level.GetObjectSettingsDict()[objectType].Speed;
    }

    if (levelDefault.GetObjectSettingsDict().ContainsKey(objectType)
      && levelDefault.GetObjectSettingsDict()[objectType].Speed > 0)
    {
      return levelDefault.GetObjectSettingsDict()[objectType].Speed;
    }

    Debug.LogError("SPEED NOT FOUND FOR: " + objectType);
    return 0;
  }

  public int GetSeeDistance(EMapObjectType eMapObjectType)
  {
    int objectType = (int)eMapObjectType;

    if (level != null
      && level.GetObjectSettingsDict().ContainsKey(objectType)
      && level.GetObjectSettingsDict()[objectType].SeeDistance > 0)
    {
      return level.GetObjectSettingsDict()[objectType].SeeDistance;
    }

    if (levelDefault.GetObjectSettingsDict().ContainsKey(objectType)
      && levelDefault.GetObjectSettingsDict()[objectType].SeeDistance > 0)
    {
      return levelDefault.GetObjectSettingsDict()[objectType].SeeDistance;
    }

    Debug.LogError("SEE DISTANCE NOT FOUND FOR: " + objectType);
    return 0;
  }

  public int GetTargetCost(EMapObjectType eMapObjectType, EMapObjectType eMapEnemyType)
  {
    int objectType = (int)eMapObjectType;
    int enemyType = (int)eMapEnemyType;

    if (enemyType == (int) EMapObjectType.TYPE_GOLEM)
    {
      if (level != null
      && level.GetTargetCosts4GolemDict().ContainsKey(objectType))
      {
        return level.GetTargetCosts4GolemDict()[objectType];
      }

      if (levelDefault.GetTargetCosts4GolemDict().ContainsKey(objectType))
      {
        return levelDefault.GetTargetCosts4GolemDict()[objectType];
      }
    }

    if (level != null
      && level.GetTargetCostsDict().ContainsKey(objectType))
    {
      return level.GetTargetCostsDict()[objectType];
    }

    if (levelDefault.GetTargetCostsDict().ContainsKey(objectType))
    {
      return levelDefault.GetTargetCostsDict()[objectType];
    }

    Debug.LogError("TARGET COST NOT FOUND FOR: " + objectType);
    return 0;
  }

  public Dictionary<int, int> GetFactoryLimits()
  {
    if (level != null)
    {
      return level.GetFactoryLimitsDict();
    }

    Debug.LogError("FACTORY LIMITS NOT FOUND");
    return null;
  }

  public int GetFactoryLimit(EMapObjectType eMapObjectType)
  {
    int objectType = (int)eMapObjectType;

    if (level != null
      && level.GetFactoryLimitsDict().ContainsKey(objectType)
      && level.GetFactoryLimitsDict()[objectType] > 0)
    {
      return level.GetFactoryLimitsDict()[objectType];
    }

    if (levelDefault.GetFactoryLimitsDict().ContainsKey(objectType)
      && levelDefault.GetFactoryLimitsDict()[objectType] > 0)
    {
      return levelDefault.GetFactoryLimitsDict()[objectType];
    }

    Debug.LogError("FACTORY LIMIT NOT FOUND FOR: " + objectType);
    return 0;
  }

  /***********************************/
  /********** RESOURCE_COST **********/
  /***********************************/

  public int GetResourceMax()
  {
    return levelDefault.ResourceMax;
  }

  public int GetResourceOnePartSize()
  {
    return levelDefault.ResourceOnePartSize;
  }

  public int GetResourceOnePartTime()
  {
    return levelDefault.ResourceOnePartTime;
  }

  public int GetResourceCost(EMapObjectType eMapObjectType)
  {
    int objectType = (int)eMapObjectType;

    if (levelDefault.GetResourceCostsDict().ContainsKey(objectType)
      && levelDefault.GetResourceCostsDict()[objectType] > 0)
    {
      return levelDefault.GetResourceCostsDict()[objectType];
    }

    Debug.LogError("RESOURCE COST NOT FOUND FOR: " + objectType);
    return 0;
  }

  public int GetSpeedHarvesterCleaner()
  {
    return levelDefault.SpeedHarvesterCleaner;
  }
  
  public int GetSpeedHarvesterFuelOut()
  {
    return levelDefault.SpeedHarvesterFuelOut;
  }
  
  public int GetSpeedTurretFuelOut()
  {
    return levelDefault.SpeedTurretFuelOut;
  }

  /***********************************/
  /********** SATELITE STRIKE ********/
  /***********************************/

  public float GetSateliteStrikeRecoveryTime()
  {
    return levelDefault.SateliteStrikeRecoveryTime;
  }

  public float GetSateliteStrikePrepareTime()
  {
    return levelDefault.SateliteStrikePrepareTime;
  }

  public float GetSateliteStrikeFiringTime()
  {
    return levelDefault.SateliteStrikeFiringTime;
  }

  /***********************************/
  /************** BEHAVIOR ***********/
  /***********************************/

  public Queue<ObjectAction> GetBehavior(int id)
  {
    return level.GetObjectActions(id).GetActions();
  }

  /***********************************/
  /**************** OTHER ************/
  /***********************************/

  public bool IsTargetOnlyHero()
  {
    if (level != null)
      return level.IsTargetOnlyHero();
    return levelDefault.IsTargetOnlyHero();
  }
}
