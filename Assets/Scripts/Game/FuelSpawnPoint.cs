﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class FuelSpawnPoint : SpawnPoint
{
  ParticleSystem fuel;

  protected int MAX_FUEL = 200;
  protected float distanceToBase = 0f;
  protected float delayTimer = 0f;

  void Update()
  {
    if (!isReady)
      return;

    if (!isInit)
    {
      isInit = true;
      SPAWN_DELAY = 3600;
      isInitSpawn = true;
      fuel = GetComponentInChildren<ParticleSystem>();
      gameObject.layer = Const.LAYER_FUEL_SPAWN_POINT;
      SpawnPointInit();
    }

    CheckDelay();

    delayTimer += Time.deltaTime;
    if (delayTimer < 5f)
      return;

    delayTimer = 0f;

    int particles = GetAliveParticles();
    //Debug.Log("particles=" + particles);
    if (particles < (MAX_FUEL * 0.2f) && timeSpawn > 10f)
    {
      //Debug.Log("< 20% " + particles);
      timeSpawn = 10f;
    }
  }

  protected int GetAliveParticles()
  {
    ParticleSystem.Particle[] particles = new ParticleSystem.Particle[fuel.particleCount];
    return fuel.GetParticles(particles);
  }

  protected override void Spawn()
  {
    if (levelController == null)
      return;

    fuel.Clear();
    fuel.Play();
    //Debug.Log("Added fuel");
  }

  public void SetDistanceToBase(float distance)
  {
    distanceToBase = distance;
  }

  public float GetDistanceToBase()
  {
    return distanceToBase;
  }

  public int GetFuelLeft()
  {
    return GetAliveParticles();
  }

  public float GetFuelLeftInPercents()
  {
    return GetAliveParticles() / (MAX_FUEL * 0.01f);
  }
}
