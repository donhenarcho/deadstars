﻿using UnityEngine;

public interface ISnowWave
{
  void Run();
  void ChangePosition(Vector3 vector);
}
