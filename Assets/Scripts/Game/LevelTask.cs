﻿using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using static MapObject;

public class LevelTask
{
  public const int TYPE_FIND_OBJECTS = 1;
  public const int SUBTYPE_FIND_OBJECTS__PYRAMID = 10;
  public const int SUBTYPE_FIND_OBJECTS__BLACK_BOX = 11;

  public const int TYPE_BUILD_OBJECTS = 2;
  public const int SUBTYPE_BUILD_OBJECTS__TERRAFORMER = 20;
  public const int SUBTYPE_BUILD_OBJECTS__HARVESTER = 21;
  public const int SUBTYPE_BUILD_OBJECTS__TURRET = 22;

  public const int TYPE_DEFEND_OBJECTS = 3;
  public const int SUBTYPE_DEFEND_OBJECTS__TERRAFORMER = 30;
  public const int SUBTYPE_DEFEND_OBJECTS__ASSISTANT = 31;
  public const int SUBTYPE_DEFEND_OBJECTS__PYRAMID = 32;
  public const int SUBTYPE_DEFEND_OBJECTS__HARVESTER = 33;
  public const int SUBTYPE_DEFEND_OBJECTS__ARK = 34;

  public const int TYPE_DESTROY_OBJECTS = 4;
  public const int SUBTYPE_DESTROY_OBJECTS__SCORPION = 40;
  public const int SUBTYPE_DESTROY_OBJECTS__GOLEM = 41;
  public const int SUBTYPE_DESTROY_OBJECTS__PYRAMID = 42;
  public const int SUBTYPE_DESTROY_OBJECTS__BIG_GOLEM = 43;

  public const int TYPE_UPGRADE_OBJECTS = 5;
  public const int SUBTYPE_UPGRADE_OBJECTS__TERRAFORMER = 50;

  public const int TYPE_COLLECTION = 6;
  public const int SUBTYPE_COLLECTION__FUEL = 60;

  [XmlAttribute("id")]
  public int Id;
  [XmlAttribute("type")]
  public int Type = 0;
  [XmlAttribute("subtype")]
  public int SubType = 0;
  [XmlAttribute("winValue")]
  public int WinValue = 0;
  [XmlAttribute("loseValue")]
  public int LoseValue = 0;
  [XmlAttribute("startDialogId")]
  public int StartDialogId = 0;
  [XmlAttribute("winDialogId")]
  public int WinDialogId = 0;
  [XmlAttribute("loseDialogId")]
  public int LoseDialogId = 0;
  [XmlAttribute("heroDeadDialogId")]
  public int HeroDeadDialogId = 0;
  [XmlAttribute("text")]
  public string text = "";

  private bool isEnded = false;

  public bool IsEnded()
  {
    return isEnded;
  }

  public void End()
  {
    isEnded = true;
  }

  public bool CheckWin(WorldController world)
  {
    switch (Type)
    {
      case TYPE_FIND_OBJECTS:
        if (SubType == SUBTYPE_FIND_OBJECTS__PYRAMID)
        {
          return world.GetFoundObjectCount((int)EMapObjectType.TYPE_PYRAMID) >= WinValue;
        }
        else if (SubType == SUBTYPE_FIND_OBJECTS__BLACK_BOX)
        {
          return world.GetFoundObjectCount((int)EMapObjectType.TYPE_BLACK_BOX) >= WinValue;
        }
        break;
      case TYPE_BUILD_OBJECTS:
        if (SubType == SUBTYPE_BUILD_OBJECTS__TERRAFORMER)
        {
          return world.GetTerraformersCount() >= WinValue;
        }
        else if (SubType == SUBTYPE_BUILD_OBJECTS__HARVESTER)
        {
          return world.GetHarvestersCount() >= WinValue;
        }
        else if (SubType == SUBTYPE_BUILD_OBJECTS__TURRET)
        {
          return world.GetMaxTurretUpgradeLevel() >= WinValue;
        }
        break;
      case TYPE_DEFEND_OBJECTS:
        if (SubType == SUBTYPE_DEFEND_OBJECTS__TERRAFORMER)
        {
          return world.GetTaskTimeWithoutPause() >= WinValue;
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__ASSISTANT)
        {
          return world.IsAssistantFinished();
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__PYRAMID)
        {
          return world.GetTaskTimeWithoutPause() >= WinValue;
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__HARVESTER)
        {
          return world.GetFinishedObjectCount((int)EMapObjectType.TYPE_HARVESTER) >= WinValue;
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__ARK)
        {
          return world.GetTaskTimeWithoutPause() >= WinValue;
        }
        break;
      case TYPE_DESTROY_OBJECTS:
        if (SubType == SUBTYPE_DESTROY_OBJECTS__SCORPION)
        {
          return world.GetDestroyedObjectCount((int)EMapObjectType.TYPE_SCORPION) >= WinValue;
        }
        else if (SubType == SUBTYPE_DESTROY_OBJECTS__GOLEM)
        {
          return world.GetDestroyedObjectCount((int)EMapObjectType.TYPE_GOLEM) >= WinValue;
        }
        else if (SubType == SUBTYPE_DESTROY_OBJECTS__PYRAMID)
        {
          return world.GetDestroyedObjectCount((int)EMapObjectType.TYPE_PYRAMID) >= WinValue;
        }
        else if (SubType == SUBTYPE_DESTROY_OBJECTS__BIG_GOLEM)
        {
          return world.GetDestroyedObjectCount((int)EMapObjectType.TYPE_BIG_GOLEM) >= WinValue;
        }
        break;
      case TYPE_UPGRADE_OBJECTS:
        if (SubType == SUBTYPE_UPGRADE_OBJECTS__TERRAFORMER)
        {
          return world.GetMaxTurretUpgradeLevel() >= WinValue;
        }
        break;
      case TYPE_COLLECTION:
        if (SubType == SUBTYPE_COLLECTION__FUEL)
        {
          return world.GetCurrentFuelValue() >= WinValue;
        }
        break;
    }

    return false;
  }

  public bool CheckLose(WorldController world)
  {
    switch (Type)
    {
      case TYPE_BUILD_OBJECTS:
        return false;
      case TYPE_DEFEND_OBJECTS:
        if (SubType == SUBTYPE_DEFEND_OBJECTS__TERRAFORMER)
        {
          return world.GetTerraformersCount() == 0;
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__ASSISTANT)
        {
          return world.GetAssistantsCount() == 0;
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__PYRAMID)
        {
          return world.GetPyramidsCount() <= LoseValue;
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__HARVESTER)
        {
          return world.GetDestroyedObjectCount((int)EMapObjectType.TYPE_HARVESTER) >= LoseValue;
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__ARK)
        {
          return world.GetDestroyedObjectCount((int)EMapObjectType.TYPE_ARK) >= LoseValue;
        }
        break;
    }

    return false;
  }

  public string GetProgress(WorldController world)
  {
    switch (Type)
    {
      case TYPE_FIND_OBJECTS:
        if (SubType == SUBTYPE_FIND_OBJECTS__PYRAMID)
        {
          return "[" + world.GetFoundObjectCount((int)EMapObjectType.TYPE_PYRAMID) + "/" + WinValue + "]";
        }
        else if (SubType == SUBTYPE_FIND_OBJECTS__BLACK_BOX)
        {
          return "[" + world.GetFoundObjectCount((int)EMapObjectType.TYPE_BLACK_BOX) + "/" + WinValue + "]";
        }
        break;
      case TYPE_BUILD_OBJECTS:
        if (SubType == SUBTYPE_BUILD_OBJECTS__TERRAFORMER)
        {
          return "[" + world.GetTerraformersCount() + "/" + WinValue + "]";
        }
        else if (SubType == SUBTYPE_BUILD_OBJECTS__HARVESTER)
        {
          return "[" + world.GetHarvestersCount() + "/" + WinValue + "]";
        }
        else if (SubType == SUBTYPE_BUILD_OBJECTS__TURRET)
        {
          return "[" + (world.GetMaxTurretUpgradeLevel() > 0 ? 1 : 0) + "/" + WinValue + "]";
        }
        break;
      case TYPE_DEFEND_OBJECTS:
        if (SubType == SUBTYPE_DEFEND_OBJECTS__TERRAFORMER)
        {
          if (world.GetTaskTimeWithoutPause() < WinValue)
            return "[" + Utils.SecToMinWithSec(WinValue - world.GetTaskTimeWithoutPause()) + "]";
          if (world.GetGameTimeWithoutPause() >= WinValue)
            return "[00:00]";
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__ASSISTANT)
        {
          return "[" + world.GetAssistantHp() + "/" + world.GetAssistantHpMax() + "]";
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__PYRAMID)
        {
          if (world.GetTaskTimeWithoutPause() < WinValue)
            return "[" + Utils.SecToMinWithSec(WinValue - world.GetTaskTimeWithoutPause()) + "]";
          if (world.GetGameTimeWithoutPause() >= WinValue)
            return "[00:00]";
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__HARVESTER)
        {
          return "[" + world.GetFinishedObjectCount((int)EMapObjectType.TYPE_HARVESTER) + "/" + WinValue + "]";
        }
        else if (SubType == SUBTYPE_DEFEND_OBJECTS__ARK)
        {
          if (world.GetTaskTimeWithoutPause() < WinValue)
            return "[" + Utils.SecToMinWithSec(WinValue - world.GetTaskTimeWithoutPause()) + "]";
          if (world.GetGameTimeWithoutPause() >= WinValue)
            return "[00:00]";
        }
        break;
      case TYPE_DESTROY_OBJECTS:
        if (SubType == SUBTYPE_DESTROY_OBJECTS__SCORPION)
        {
          return "[" + world.GetDestroyedObjectCount((int)EMapObjectType.TYPE_SCORPION) + "/" + WinValue + "]";
        }
        else if (SubType == SUBTYPE_DESTROY_OBJECTS__GOLEM)
        {
          return "[" + world.GetDestroyedObjectCount((int)EMapObjectType.TYPE_GOLEM) + "/" + WinValue + "]";
        }
        else if (SubType == SUBTYPE_DESTROY_OBJECTS__PYRAMID)
        {
          return "[" + world.GetDestroyedObjectCount((int)EMapObjectType.TYPE_PYRAMID) + "/" + WinValue + "]";
        }
        else if (SubType == SUBTYPE_DESTROY_OBJECTS__BIG_GOLEM)
        {
          return "[" + world.GetBigGolemHp() + "/" + world.GetBigGolemHpMax() + "]";
        }
        break;
      case TYPE_UPGRADE_OBJECTS:
        if (SubType == SUBTYPE_UPGRADE_OBJECTS__TERRAFORMER)
        {
          return "[" + world.GetMaxTurretUpgradeLevel() + "/" + WinValue + "]";
        }
        break;
      case TYPE_COLLECTION:
        if (SubType == SUBTYPE_COLLECTION__FUEL)
        {
          return "[" + world.GetCurrentFuelValue() + "/" + WinValue + "]";
        }
        break;
    }

    return "";
  }

  public int GetTaskType()
  {
    return Type;
  }

  public override string ToString()
  {
    string temp = "";
    temp += "\n *****TASK [" + Id + "]*****";
    temp += "\n Type=" + Type;
    temp += "\n SubType=" + SubType;
    temp += "\n WinValue=" + WinValue;
    temp += "\n LoseValue=" + LoseValue;
    temp += "\n StartDialogId=" + StartDialogId;
    temp += "\n WinDialogId=" + WinDialogId;
    temp += "\n LoseDialogId=" + LoseDialogId;
    temp += "\n text=" + text;
    temp += "\n isEnded=" + isEnded;
    return temp;
  }
}
