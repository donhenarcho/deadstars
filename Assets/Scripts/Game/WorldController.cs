﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using static MapObject;

public class WorldController : MonoBehaviour
{
  private GameController gameController;
  private VariableManager variableManager;
  private UI__Controller uiController;
  private WorldTerrain terrainController;
  private WorldCamera cameraController;
  private WorldFactory objectFactory;

  private EnemySpawnController enemySpawnController;

  public GameObject Stones;
  public GameObject Trees;
  public GameObject Monsters;
  public GameObject FuelSpawnPoints;
  public GameObject EnemySpawnPoints;
  public GameObject Transport;
  public GameObject AITransport;
  public GameObject Terraformers;
  public GameObject FoundObjects;
  public GameObject Turrets;
  public GameObject Other;
  public GameObject Spaceships;
  public GameObject SpacetruckContainers;

  private SateliteStrike sateliteStrike;

  private IHero hero;
  private IHero prevHero;
  private Girl girl;
  private MiniSpacetruck miniSpacetruck1;
  private MiniSpacetruck miniSpacetruck2;
  private MiniSpacetruck miniSpacetruck3;
  private MiniSpacetruck miniSpacetruck4;
  private int miniSpacetruckNum = 0;
  private EMapObjectType miniSpacetruckHaveObjectType = 0;

  private World world;
  private SavedWorld savedWorld;
  private int sizeX = 0;
  private int sizeY = 0;

  private IObjectInvisible lastObjectInvisible;

  private Terraformer currentTerraformer = null;
  private Terraformer prevTerraformer = null;
  private List<Terraformer> terraformers;
  private Dictionary<int, Terraformer> dictTerraformers;
  private List<Pyramid> pyramids;

  private int RESOUCES_IN_ORBIT_MAX = 10;
  private int NEXT_RESOUCES_IN_ORBIT_ONE_PART = 5;
  private float NEXT_RESOUCES_IN_ORBIT_DELAY = 10f;

  private int resourcesInOrbitValue = 0;
  private float resourcesInOrbitTimer = 0;

  private bool needAssistanMenuUpdateProgresses = false;

  private Dictionary<int, int> hintCounter = new Dictionary<int, int>();
  private float needShowHintTimer = 0;
  private int currentHintShow = 0;

  private float searchBaseNearbyTimer = 0;

  private Vector3 centerPoint = Vector3.zero;
  private float maxDistanceToCenter = 0;
  private float onePercentOfDistanceToCenter = 0;

  private Dictionary<int, AI__AttackTarget> potentialAttackTargets;

  private Queue<SavedWorldObject> queueToAdd = new Queue<SavedWorldObject>();

  private bool isLevelReady = false;
  private bool isGameStarting = false;
  private bool isLose = false;
  private bool isWin = false;

  private const int DELIVERY_TYPE_NEW_BASE = 0;
  private const int DELIVERY_TYPE_BASE_UPGRADE = 1;
  private const int DELIVERY_TYPE_HERO = 2;
  private int deliveryType = 0;

  Dictionary<int, GameObject> forests = new Dictionary<int, GameObject>();

  private bool isPaused = false;

  private float checkEnemyWaveTimer = 1f;
  private float gameTimer = 0f;
  private float gameWithoutPauseTimer = 0f;
  private float taskWithoutPauseTimer = 0f;
  private float pauseTimer = 0f;

  private Dictionary<int, int> destroyedObjects = new Dictionary<int, int>();
  private Dictionary<int, List<int>> foundObjects = new Dictionary<int, List<int>>();
  private Dictionary<int, List<int>> finishedObjects = new Dictionary<int, List<int>>();

  private float sateliteStrikeRecoveryTimer = 0f;
  private float updateMapTimer = 0f;

  void Start()
  {
    uiController = FindObjectOfType<UI__Controller>();
    gameController = FindObjectOfType<GameController>();
    variableManager = FindObjectOfType<VariableManager>();
    terrainController = FindObjectOfType<WorldTerrain>();
    cameraController = FindObjectOfType<WorldCamera>();
    objectFactory = FindObjectOfType<WorldFactory>();
    sateliteStrike = FindObjectOfType<SateliteStrike>();

    RESOUCES_IN_ORBIT_MAX = variableManager.GetResourceMax();
    NEXT_RESOUCES_IN_ORBIT_ONE_PART = variableManager.GetResourceOnePartSize();
    NEXT_RESOUCES_IN_ORBIT_DELAY = variableManager.GetResourceOnePartTime();

    foundObjects.Add((int)EMapObjectType.TYPE_PYRAMID, new List<int>());
    foundObjects.Add((int)EMapObjectType.TYPE_BLACK_BOX, new List<int>());

    finishedObjects.Add((int)EMapObjectType.TYPE_HARVESTER, new List<int>());

    enemySpawnController = new EnemySpawnController();
    enemySpawnController.SetWorldController(this);

    objectFactory.Fill();
  }

  void Update()
  {
    if (isLose || isWin)
    {
      if (Input.anyKeyDown)
      {
        GoToMainMenu();
      }
      return;
    }

    if (!isGameStarting)
      return;

    //*** TIMERS ***//
    gameTimer += Time.deltaTime;

    if (isPaused)
    {
      pauseTimer += Time.deltaTime;
    }
    else
    {
      gameWithoutPauseTimer += Time.deltaTime;
      taskWithoutPauseTimer += Time.deltaTime;

      if (uiController.IsShowDialog())
        taskWithoutPauseTimer = 0;

      if (sateliteStrikeRecoveryTimer > 0)
      {
        sateliteStrikeRecoveryTimer -= Time.deltaTime;
        if (sateliteStrikeRecoveryTimer <= 0)
          sateliteStrikeRecoveryTimer = 0f;
      }
    }
    //**************//

    if (Input.GetKeyDown(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_PAUSE)))
    {
      Pause();
    }

    if (Input.GetKeyDown(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_MAP)))
    {
      if (!uiController.IsShowAssistantMenu())
        ShowMap();
    }

    if (Input.GetKeyUp(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_MAP)))
    {
      if (uiController.IsShowMap())
        HideMap();
    }

    if (Input.GetKeyDown(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_MENU)))
    {
      ShowTerminalMenu();
    }

    if (Input.GetKeyUp(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_MENU)))
    {
      HideTerminalMenu();
    }

    if (currentHintShow == Const.KEYBOARD_HINT_WASD 
      && (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) 
      || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D)))
    {
      HintWASDHide();
    }

    if (currentHintShow == Const.MOUSE_HINT_FIRE 
      && Input.GetKeyDown(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_FIRE)))
    {
      HintFireHide();
    }

    CheckEnemyWave();

    SearchBaseNearby();
    SearchEnemiesNearHero();
    SearchFoundObjectsNearHero();

    if (!isPaused)
    {
      CheckResourcesInOrbitTimer();
    }

    CheckVisible();
    CheckNeedShowHint();
    UIUpdate();
  }

  private void UIUpdate()
  {
    if (needAssistanMenuUpdateProgresses)
    {
      needAssistanMenuUpdateProgresses = false;
      uiController.UpdateAssistanMenuProgresses();
    }

    if (updateMapTimer > 0f)
    {
      updateMapTimer -= Time.deltaTime;
      if (updateMapTimer <= 0f)
      {
        updateMapTimer = 0f;
        UpdateMap();
      }
    }

    if (Const.WITH_TIMER_MODE)
    {
      uiController.SetDebugLine(Utils.SecToMinWithSec(gameWithoutPauseTimer));
    }
  }

  public KeyValuePair<int, int> GetResourcesInOrbitValue()
  {
    return new KeyValuePair<int, int>(resourcesInOrbitValue, RESOUCES_IN_ORBIT_MAX);
  }

  public int GetTerraformersCount()
  {
    return terraformers.Count;
  }

  public int GetHarvestersCount()
  {
    int result = 0;
    foreach (Terraformer terraformer in terraformers)
    {
      result += terraformer.GetHarvestersCount();
    }
    return result;
  }

  public int GetMaxTurretUpgradeLevel()
  {
    int result = 0;
    foreach (Terraformer terraformer in terraformers)
    {
      if (result < terraformer.GetCurrentTurretUpgradeLevel())
        result = terraformer.GetCurrentTurretUpgradeLevel();
    }
    return result;
  }

  public int GetCurrentFuelValue()
  {
    int result = 0;
    foreach (Terraformer terraformer in terraformers)
    {
      result += (int) terraformer.GetCurrentFuelValue();
    }
    return result;
  }

  public string GetTaskProgress()
  {
    return variableManager.GetCurrentLevel().GetProgress(this);
  }

  public string GetResourcesInOrbitTimer()
  {
    return Utils.SecToMinWithSec(NEXT_RESOUCES_IN_ORBIT_DELAY - resourcesInOrbitTimer);
  }

  public string GetSateliteStrikeRecoveryTime()
  {
    return Utils.SecToMinWithSec(sateliteStrikeRecoveryTimer);
  }

  private void CheckResourcesInOrbitTimer()
  {
    if (resourcesInOrbitValue == RESOUCES_IN_ORBIT_MAX)
      return;

    resourcesInOrbitTimer += Time.deltaTime;
    if (resourcesInOrbitTimer > NEXT_RESOUCES_IN_ORBIT_DELAY)
    {
      resourcesInOrbitTimer = 0;
      resourcesInOrbitValue += NEXT_RESOUCES_IN_ORBIT_ONE_PART;
      if (resourcesInOrbitValue > RESOUCES_IN_ORBIT_MAX)
        resourcesInOrbitValue = RESOUCES_IN_ORBIT_MAX;
      needAssistanMenuUpdateProgresses = true;
    }
  }

  public int GetDestroyedObjectCount(int type)
  {
    if (destroyedObjects.ContainsKey(type))
      return destroyedObjects[type];
    return 0;
  }

  public int GetFoundObjectCount(int type)
  {
    if (foundObjects.ContainsKey(type))
      return foundObjects[type].Count;
    return 0;
  }

  public int GetFinishedObjectCount(int type)
  {
    if (finishedObjects.ContainsKey(type))
      return finishedObjects[type].Count;
    return 0;
  }

  public float GetGameTimeWithoutPause()
  {
    return gameWithoutPauseTimer;
  }

  public float GetTaskTimeWithoutPause()
  {
    return taskWithoutPauseTimer;
  }

  private void CheckVisible()
  {
    if (!IsLevelReady())
      return;

    if (hero == null)
      return;

    Vector3 heroPosition = hero.GetPosition();
    heroPosition.y += 2f;
    Vector3 dir = heroPosition - cameraController.GetCameraPosition();
    RaycastHit hit;

    if (Physics.Raycast(cameraController.GetCameraPosition(), dir, out hit, 1000, Const.LAYER_MASK_FOR_HIDDEN_OBJECTS))
    {
      //Debug.Log(hit.collider.name + ", " + hit.collider.tag);
      IObjectInvisible objectInvisible = hit.collider.gameObject.GetComponent<IObjectInvisible>();

      if (lastObjectInvisible != null && (objectInvisible == null || !objectInvisible.Equals(lastObjectInvisible)))
      {
        lastObjectInvisible.SetAlpha(1f);
        lastObjectInvisible = null;
      }

      if (hit.collider.gameObject.GetComponentInParent<MapObject>().GetId() == hero.GetId())
      {
        if (lastObjectInvisible != null)
        {
          lastObjectInvisible.SetAlpha(1f);
          lastObjectInvisible = null;
        }
        return;
      }

      if (objectInvisible != null && (lastObjectInvisible == null || !objectInvisible.Equals(lastObjectInvisible)))
      {
        lastObjectInvisible = objectInvisible;
        objectInvisible.SetAlpha(0.5f);
      }
    }
  }

  private void LateUpdate()
  {
    if (!isLevelReady)
      return;

    if (queueToAdd.Count > 0)
      AddObjectToWorld(queueToAdd.Dequeue());
  }

  public void LevelDestroy()
  {
    isGameStarting = false;
    isLevelReady = false;

    foreach (Transform obj in Trees.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in Stones.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in Transport.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in AITransport.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in Monsters.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in FuelSpawnPoints.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in EnemySpawnPoints.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in Terraformers.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in Turrets.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in Other.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in Spaceships.transform)
    {
      Destroy(obj.gameObject);
    }
    foreach (Transform obj in SpacetruckContainers.transform)
    {
      Destroy(obj.gameObject);
    }
    if (hero != null)
    {
      Destroy(hero.GetGameObject());
    }
  }

  public void Play()
  {
    LevelCreate();
  }

  public void LevelCreate()
  {
    Debug.Log("World creating...");
    isLevelReady = false;
    isGameStarting = false;
    isLose = false;
    isWin = false;

    resourcesInOrbitTimer = 0;
    resourcesInOrbitValue = RESOUCES_IN_ORBIT_MAX;

    LevelDestroy();

    hintCounter = new Dictionary<int, int>();

    terraformers = new List<Terraformer>();
    dictTerraformers = new Dictionary<int, Terraformer>();
    pyramids = new List<Pyramid>();
    potentialAttackTargets = new Dictionary<int, AI__AttackTarget>();

    world = variableManager.GetCurrentWorld();
    sizeX = world.sizeX;
    sizeY = world.sizeY;

    enemySpawnController.SetMapSize(sizeX, sizeY);

    // отключаем загрузку сохраненных данных
    savedWorld = new SavedWorld(); //SavedWorld.Read(variableManager.GetFilename());
    if (!savedWorld.IsEmpty())
    {
      resourcesInOrbitTimer = savedWorld.resourcesInOrbitTimer;
      resourcesInOrbitValue = savedWorld.resourcesInOrbitValue;

      checkEnemyWaveTimer = savedWorld.checkEnemyWaveTimer;
      gameTimer = savedWorld.gameTimer;
      gameWithoutPauseTimer = savedWorld.gameWithoutPauseTimer;
      taskWithoutPauseTimer = savedWorld.taskWithoutPauseTimer;
      pauseTimer = savedWorld.pauseTimer;

      hintCounter = savedWorld.hintCounter;
      foundObjects = savedWorld.foundObjects;
      destroyedObjects = savedWorld.destroyedObjects;

      variableManager.GetCurrentLevel().ApplySavedWorld(savedWorld);
    }

    terrainController.SetSize(sizeX, sizeY, Const.TERRAIN_HEIGHT, 2f);

    centerPoint = new Vector3(sizeX / 2, 0f, -sizeY / 2);
    maxDistanceToCenter = Vector3.Distance(Vector3.zero, centerPoint);
    onePercentOfDistanceToCenter = maxDistanceToCenter / 100f;
    //Debug.Log("maxDistanceToCenter=" + maxDistanceToCenter);
    //Debug.Log("onePercentOfDistanceToCenter=" + onePercentOfDistanceToCenter);

    StartCoroutine(AddObjectsToWorld(0.1f));
  }

  private IEnumerator AddObjectsToWorld(float waitTime)
  {
    yield return new WaitForSeconds(waitTime);

    System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
    stopWatch.Start();

    miniSpacetruck1 = AddMiniSpacetruck(Const.OUT_OF_WORLD_POSITION);
    miniSpacetruck2 = AddMiniSpacetruck(Const.OUT_OF_WORLD_POSITION);
    miniSpacetruck3 = AddMiniSpacetruck(Const.OUT_OF_WORLD_POSITION);
    miniSpacetruck4 = AddMiniSpacetruck(Const.OUT_OF_WORLD_POSITION);

    bool isSaved = !savedWorld.IsEmpty();

    Debug.Log("isSaved=" + isSaved);
    //Debug.Log("savedWorld.lastId=" + savedWorld.lastId);
    //Debug.Log("savedWorld.resourcesInOrbitValue=" + savedWorld.resourcesInOrbitValue);
    //Debug.Log("savedWorld.resourcesInOrbitTimer=" + savedWorld.resourcesInOrbitTimer);

    forests.Clear();

    foreach (WorldObject worldObject in world.objects)
    {
      if (isSaved && (worldObject.type == (int)EMapObjectType.TYPE_STONE_20 || worldObject.type == (int)EMapObjectType.TYPE_STONE_40
        || worldObject.type == (int)EMapObjectType.TYPE_STONE_60 || worldObject.type == (int)EMapObjectType.TYPE_HERO))
        continue;

      //if (worldObject.type == Const.TYPE_TREE)
        //continue;

      /*if (worldObject.type == (int)EMapObjectType.TYPE_TREE)
      {
        if (!forests.ContainsKey(worldObject.groupId))
        {
          GameObject gameObject = Instantiate(PrefabForestWrapper, worldObject.GetGroupPosition(), Quaternion.identity);
          gameObject.transform.parent = GetWrapperByType(EMapObjectType.TYPE_FOREST).transform;
          forests.Add(worldObject.groupId, gameObject);
          //Debug.Log("+ forestId=" + worldObject.groupId);
        }
      }*/

      SavedWorldObject savedWorldObject = SavedWorldObject.GetSavedWorldObject(worldObject);
      if (savedWorldObject.type != (int)EMapObjectType.TYPE_TREE
        && savedWorldObject.type != (int)EMapObjectType.TYPE_STONE_20
        && savedWorldObject.type != (int)EMapObjectType.TYPE_STONE_40
        && savedWorldObject.type != (int)EMapObjectType.TYPE_STONE_60
        && savedWorldObject.type != (int)EMapObjectType.TYPE_FUEL_SPAWN_POINT 
        && savedWorldObject.id == 0)
        savedWorldObject.id = savedWorld.lastId++;
      AddObjectToWorld(savedWorldObject);
    }

    if (!isSaved)
    {
      Queue<Vector3> coords = GetRandomPositions(50f, 5f);
      foreach (LevelObject levelObject in variableManager.GetCurrentLevel().ObjectList)
      {
        SavedWorldObject savedWorldObject = SavedWorldObject.GetSavedWorldObject(new WorldObject(levelObject));
        savedWorldObject.id = savedWorld.lastId++;

        if (savedWorldObject.IsZeroPosition())
        {
          Vector3 coord = coords.Dequeue();
          savedWorldObject.SetPosition(coord);
          //Debug.Log("> point=" + coord);
        }

        AddObjectToWorld(savedWorldObject);
      }
    }

    foreach (SavedWorldObject savedWorldObject in savedWorld.objects)
    {
      AddObjectToWorld(savedWorldObject);
    }

    stopWatch.Stop();
    TimeSpan ts = stopWatch.Elapsed;
    string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
        ts.Hours, ts.Minutes, ts.Seconds,
        ts.Milliseconds / 10);
    Debug.Log("World is ready " + elapsedTime);

    /*if (!File.Exists(variableManager.GetFilename() + ".saved"))
    {
      StartCoroutine(SaveWorldWithDelay());
      Debug.Log("First save for new world.");
    }*/

    StartCoroutine(LevelReady());
  }

  private Queue<Vector3> GetRandomPositions(float size, float border)
  {
    List<Vector3> coords = new List<Vector3>();

    int maxX = Mathf.RoundToInt(sizeX / size) - 1;
    int maxY = Mathf.RoundToInt(sizeY / size) - 1;

    int centerX = Mathf.RoundToInt(maxX / 2);
    int centerY = Mathf.RoundToInt(maxY / 2);

    for (int x = 1; x < maxX; x++)
    {
      for (int y = 1; y < maxY; y++)
      {
        if (x == centerX && y == centerY)
          continue;

        float fromX = x * size;
        float fromY = y * size;
        float randomX = UnityEngine.Random.Range(fromX + border, fromX + size - border);
        float randomY = UnityEngine.Random.Range(fromY + border, fromY + size - border);
        Vector3 coord = new Vector3(randomX, 0f, -randomY);
        coords.Add(coord);
        //Debug.Log("+ point=" + coord);
      }
    }

    Utils.Shuffle(coords);
    return new Queue<Vector3>(coords);
  }

  private IEnumerator LevelReady()
  {
    yield return new WaitForSeconds(2f);
    uiController.LoadingHide();
    isLevelReady = true;

    isGameStarting = true;

    StartCoroutine(RunNextLevelTask());
    StartCoroutine(CheckWinLose());

    if (!savedWorld.IsEmpty())
      ShowTaskInfo();
  }

  public bool ShowDialog(LevelDialog dialog)
  {
    Debug.Log("ShowDialog() >>> " + dialog);
    if (dialog != null)
    {
      Pause();
      uiController.ClearHints();
      HideTaskInfo();
      uiController.HideMap();
      uiController.HideAssistantMenu();
      uiController.ShowDialog(dialog);
      return true;
    }
    return false;
  }

  public void HideDialog()
  {
    Debug.Log("HideDialog()");

    uiController.HideDialog();
    if (variableManager.GetCurrentLevel().IsEnded())
    {
      if (variableManager.GetCurrentLevel().Result())
      {
        StartCoroutine(Win());
      }
      else
      {
        StartCoroutine(Lose());
      }
    }
    else
    {
      Pause();
      StartCoroutine(RunNextLevelTask());
    }

    taskWithoutPauseTimer = 0;
    ShowTaskInfo();

    if (variableManager.IsFirstTaskInGame())
    {
      HintWASDShow();
    }
  }

  private void HintWASDShow()
  {
    if (!gameController.IsJoystickControl() 
      && GetHintFromCounter(Const.KEYBOARD_HINT_WASD) == 0)
    {
      if (uiController.WasdHintShow(UI__WasdHint.STATE_BUTTON_ALL))
      {
        AddHintToCounter(Const.KEYBOARD_HINT_WASD);
        uiController.SetBottomHint("KEYBOARD_HINT_WASD");
        currentHintShow = Const.KEYBOARD_HINT_WASD;
      }
      return;
    }
  }

  private void HintWASDHide()
  {
    if (currentHintShow == Const.KEYBOARD_HINT_WASD)
    {
      uiController.WasdHintHide();
      uiController.ClearBottomHint();
      currentHintShow = 0;
      HintKeyboardShow(Const.KEYBOARD_HINT_TERMINAL_MENU, "HINT_OPEN_TERMINAL_MENU");
    }
  }

  private void HintKeyboardShow(int hintCode, string labelCode)
  {
    if (!gameController.IsJoystickControl()
      && GetHintFromCounter(hintCode) == 0)
    {
      if (uiController.KeyboardHintShow(UI__WasdHint.STATE_BUTTON_ALL))
      {
        if (uiController.IsShowAssistantMenu())
          HideTerminalMenu();

        AddHintToCounter(hintCode);
        uiController.SetBottomHint(labelCode);
        currentHintShow = hintCode;
      }
      return;
    }
  }

  private void HintKeyboardHide(int hintCode)
  {
    if (currentHintShow == hintCode)
    {
      uiController.KeyboardHintHide();
      uiController.ClearBottomHint();
      currentHintShow = 0;
    }
  }

  private void HintFireShow()
  {
    if (!gameController.IsJoystickControl() 
      && GetHintFromCounter(Const.MOUSE_HINT_FIRE) == 0)
    {
      if (uiController.MouseHintShow(UI__MouseHint.STATE_BUTTON_LEFT))
      {
        AddHintToCounter(Const.MOUSE_HINT_FIRE);
        uiController.SetBottomHint("MOUSE_HINT_FIRE");
        currentHintShow = Const.MOUSE_HINT_FIRE;
      }
      return;
    }
  }

  private void HintFireHide()
  {
    if (currentHintShow == Const.MOUSE_HINT_FIRE)
    {
      uiController.MouseHintHide();
      uiController.ClearBottomHint();
      currentHintShow = 0;
    }
  }

  private void HintTransportUseShow()
  {
    if (!variableManager.IsFirstLevel())
      return;

    if (gameController.IsJoystickControl() 
      && GetHintFromCounter(Const.JOYSTICK_HINT_USE_TRANSPORT) == 0)
    {
      if (uiController.JoystickHintShow(UI__JoystickHint.STATE_BUTTON_A))
      {
        AddHintToCounter(Const.JOYSTICK_HINT_USE_TRANSPORT);
        currentHintShow = Const.JOYSTICK_HINT_USE_TRANSPORT;
      }
    }
    else if (!gameController.IsJoystickControl() 
      && GetHintFromCounter(Const.MOUSE_HINT_USE_TRANSPORT) == 0)
    {
      if (uiController.MouseHintShow(UI__MouseHint.STATE_BUTTON_RIGHT))
      {
        AddHintToCounter(Const.MOUSE_HINT_USE_TRANSPORT);
        uiController.SetBottomHint("MOUSE_HINT_USE_TRANSPORT");
        currentHintShow = Const.MOUSE_HINT_USE_TRANSPORT;
      }
    }
  }

  private void HintTransportUseHide()
  {
    uiController.MouseHintHide();
    uiController.ClearBottomHint();
    currentHintShow = 0;
  }

  public void ShowTaskInfo()
  {
    uiController.ShowTaskInfo(variableManager.GetCurrentLevel().GetCurrentTask());
  }

  public void HideTaskInfo()
  {
    uiController.HideTaskInfo();
  }
     
  private void CheckEnemyWave()
  {
    if (isPaused)
      return;

    if (checkEnemyWaveTimer == 0f)
      return;

    checkEnemyWaveTimer -= Time.deltaTime;
    if (checkEnemyWaveTimer <= 0)
    {
      checkEnemyWaveTimer = 0f;
      AddEnemyWave();
    }
  }

  private void AddEnemyWave()
  {
    if (variableManager.GetCurrentLevel().GetCurrentTask() == null)
    {
      checkEnemyWaveTimer = 2f;
      return;
    }

    EnemyWave enemyWave = variableManager.GetEnemyWave();
    if (enemyWave != null)
    {
      //Debug.Log("enemyWave.WaitTaskId -> " + enemyWave.WaitTaskId);
      //if (variableManager.GetCurrentLevel().GetCurrentTask() != null)
      //Debug.Log("variableManager.GetCurrentLevel().GetCurrentTask().Id -> " + variableManager.GetCurrentLevel().GetCurrentTask().Id);

      if (enemyWave.WaitTaskId > 0 
        && enemyWave.WaitTaskId > variableManager.GetCurrentLevel().GetCurrentTask().Id)
      {
        checkEnemyWaveTimer = 2f;
        variableManager.ReturnEnemyWave(enemyWave);
        return;
      }

      if (enemyWave.OnlyTaskId > 0 
        && enemyWave.OnlyTaskId != variableManager.GetCurrentLevel().GetCurrentTask().Id)
      {
        checkEnemyWaveTimer = 2f;
        return;
      }

      enemySpawnController.Spawn(enemyWave);
      checkEnemyWaveTimer = enemyWave.PauseAfter;

      if (variableManager.IsFirstLevel())
        StartCoroutine(CheckMapHint());
    }
  }

  private IEnumerator CheckMapHint()
  {
    yield return new WaitForSeconds(1f);
    //Debug.Log("CheckMapHint() --> " + Monsters.transform.childCount);
    if (Monsters.transform.childCount > 0)
    {
      HintKeyboardShow(Const.KEYBOARD_HINT_MAP, "KEYBOARD_HINT_MAP");
    }
  }

  public void SetHero(IHero iHero)
  {
    prevHero = hero;
    hero = iHero;

    if (prevHero != null)
      UpdateAttackTarget(prevHero.GetId(), hero.GetId());

    cameraController.SetTarget(hero);

    if (currentHintShow == Const.MOUSE_HINT_USE_TRANSPORT)
    {
      HintTransportUseHide();
    }

    if (variableManager.GetCurrentLevel().Id == 2 
      && hero.GetMapObjectType() == EMapObjectType.TYPE_BMP)
    {
      HintFireShow();
    }
  }

  public void UpdateAttackTarget(int oldId, int newId)
  {
    foreach (Transform obj in Monsters.transform)
    {
      AI__Enemy enemy = obj.gameObject.GetComponent<AI__Enemy>();
      if (enemy != null && enemy.CheckAttackTarget(oldId))
      {
        enemy.SetAttackTarget(potentialAttackTargets[newId]);
      }
    }
  }

  public GameObject AddObjectToWorld(SavedWorldObject savedWorldObject)
  {
    int type = savedWorldObject.subType > 0 ? savedWorldObject.subType : savedWorldObject.type;
    EMapObjectType eMapObjectType = (EMapObjectType)type;

    GameObject parent = GetWrapperByType(eMapObjectType);
    /*if (eMapObjectType == EMapObjectType.TYPE_TREE)
    {
      //Debug.Log("find forestId=" + savedWorldObject.groupId);
      parent = forests[savedWorldObject.groupId];
    }*/

    GameObject gameObject = objectFactory.GetObject(eMapObjectType);

    MapObject mapObject = gameObject.GetComponent<MapObject>();
    mapObject.SetSavedWorldObject(savedWorldObject);
    gameObject.transform.parent = parent.transform;

    if (eMapObjectType == EMapObjectType.TYPE_TERRAFORMER)
    {
      terraformers.Add(gameObject.GetComponent<Terraformer>());
      dictTerraformers.Add(mapObject.GetId(), gameObject.GetComponent<Terraformer>());
      //Debug.Log("Added terraformer with id=" + mapObject.GetId());
    }
    else if (eMapObjectType == EMapObjectType.TYPE_PYRAMID)
    {
      pyramids.Add(gameObject.GetComponent<Pyramid>());
    }
    else if (eMapObjectType == EMapObjectType.TYPE_HARVESTER || eMapObjectType == EMapObjectType.TYPE_TURRET 
      || eMapObjectType == EMapObjectType.SUB_TYPE_TURRET_LEVEL_1 || eMapObjectType == EMapObjectType.SUB_TYPE_TURRET_LEVEL_2 
      || eMapObjectType == EMapObjectType.SUB_TYPE_TURRET_LEVEL_3 || eMapObjectType == EMapObjectType.SUB_TYPE_TURRET_LEVEL_4 
      || eMapObjectType == EMapObjectType.SUB_TYPE_TURRET_LEVEL_5)
    {
      //Debug.Log("Added harvester with ownerId=" + mapObject.GetOwnerId());
      if (mapObject.GetOwnerId() > 0)
      {
        dictTerraformers[mapObject.GetOwnerId()].AddObject(mapObject);
      }
      //savedWorldObject.Print();
    }
    else if (eMapObjectType == EMapObjectType.TYPE_KAMIKAZE || eMapObjectType == EMapObjectType.TYPE_SCORPION 
      || eMapObjectType == EMapObjectType.TYPE_GOLEM || eMapObjectType == EMapObjectType.TYPE_CYCLOP)
    {
      //Debug.Log("Added enemy with ownerId=" + mapObject.GetOwnerId());
      if (savedWorldObject.attackTargetId > 0)
      {
        //Debug.Log("Set attack target [id="+ savedWorldObject.attackTargetId + "] for enemy");
        if (potentialAttackTargets.ContainsKey(savedWorldObject.attackTargetId))
        {
          gameObject.GetComponent<AI__Enemy>().SetAttackTarget(potentialAttackTargets[savedWorldObject.attackTargetId]);
        }
        else
        {
          Debug.LogError("Not found for enemy target with ID=" + savedWorldObject.attackTargetId);
        }
      }
    }

    if (mapObject.IsTarget())
    {
      //Debug.Log("Add potential target with ID=" + mapObject.GetId());
      potentialAttackTargets.Add(mapObject.GetId(), mapObject.GetComponent<AI__AttackTarget>());
    }

    mapObject.Ready();

    if (mapObject.GetMapObjectType() == EMapObjectType.TYPE_HERO)
    {
      girl = gameObject.GetComponent<Girl>();
      SetHero(girl);
    }

    updateMapTimer = 1f;

    if (savedWorldObject.behaviorId > 0)
    {
      mapObject.SetBehavior(variableManager.GetBehavior(savedWorldObject.behaviorId));
    }

    return gameObject;
  }

  public int AddTerraformer(Vector3 position)
  {
    SavedWorldObject savedWorldObject = new SavedWorldObject();
    savedWorldObject.id = savedWorld.lastId++;
    savedWorldObject.SetPosition(position);
    savedWorldObject.type = (int)EMapObjectType.TYPE_TERRAFORMER;
    savedWorldObject.angle = 180f;
    queueToAdd.Enqueue(savedWorldObject);
    return savedWorldObject.id;
  }

  public int AddPyramid(Vector3 position)
  {
    SavedWorldObject savedWorldObject = new SavedWorldObject();
    savedWorldObject.id = savedWorld.lastId++;
    savedWorldObject.SetPosition(position);
    savedWorldObject.type = (int)EMapObjectType.TYPE_PYRAMID;
    savedWorldObject.angle = 180f;
    queueToAdd.Enqueue(savedWorldObject);
    return savedWorldObject.id;
  }

  public int AddHarvester(Vector3 position, int ownerId)
  {
    SavedWorldObject savedWorldObject = new SavedWorldObject();
    savedWorldObject.id = savedWorld.lastId++;
    savedWorldObject.SetPosition(position);
    savedWorldObject.type = (int)EMapObjectType.TYPE_HARVESTER;
    savedWorldObject.ownerId = ownerId;
    queueToAdd.Enqueue(savedWorldObject);
    return savedWorldObject.id;
  }

  public int AddTurret(Vector3 position, int upgradeLevel, int ownerId)
  {
    SavedWorldObject savedWorldObject = new SavedWorldObject();
    savedWorldObject.id = savedWorld.lastId++;
    savedWorldObject.SetPosition(position);
    savedWorldObject.type = (int)EMapObjectType.TYPE_TURRET;
    savedWorldObject.ownerId = ownerId;

    switch (upgradeLevel)
    {
      case 1:
        savedWorldObject.subType = (int)EMapObjectType.SUB_TYPE_TURRET_LEVEL_1;
        break;
      case 2:
        savedWorldObject.subType = (int)EMapObjectType.SUB_TYPE_TURRET_LEVEL_2;
        break;
      case 3:
        savedWorldObject.subType = (int)EMapObjectType.SUB_TYPE_TURRET_LEVEL_3;
        break;
      case 4:
        savedWorldObject.subType = (int)EMapObjectType.SUB_TYPE_TURRET_LEVEL_4;
        break;
      case 5:
        savedWorldObject.subType = (int)EMapObjectType.SUB_TYPE_TURRET_LEVEL_5;
        break;
    }

    queueToAdd.Enqueue(savedWorldObject);
    return savedWorldObject.id;
  }

  public int AddEnemy(int objectType, Vector3 position)
  {
    Debug.Log("AddEnemy() >>> " + objectType);
    SavedWorldObject savedWorldObject = new SavedWorldObject();
    savedWorldObject.id = savedWorld.lastId++;
    savedWorldObject.SetPosition(position);
    savedWorldObject.type = objectType;
    queueToAdd.Enqueue(savedWorldObject);
    return savedWorldObject.id;
  }

  public int AddTransport(EMapObjectType objectType, Vector3 position)
  {
    SavedWorldObject savedWorldObject = new SavedWorldObject();
    savedWorldObject.id = savedWorld.lastId++;
    savedWorldObject.SetPosition(position);
    savedWorldObject.type = (int)objectType;
    savedWorldObject.angle = 270f;
    queueToAdd.Enqueue(savedWorldObject);
    return savedWorldObject.id;
  }

  public int AddTransportNow(EMapObjectType objectType, Vector3 position)
  {
    SavedWorldObject savedWorldObject = new SavedWorldObject();
    savedWorldObject.id = savedWorld.lastId++;
    savedWorldObject.SetPosition(position);
    savedWorldObject.type = (int)objectType;
    savedWorldObject.angle = 270f;
    AddObjectToWorld(savedWorldObject);
    return savedWorldObject.id;
  }

  public MiniSpacetruck AddMiniSpacetruck(Vector3 position)
  {
    position.y = 1000f;
    SavedWorldObject savedWorldObject = new SavedWorldObject();
    savedWorldObject.SetPosition(position);
    savedWorldObject.type = (int)EMapObjectType.TYPE_MINI_SPACETRUCK;
    savedWorldObject.angle = 270f;

    return AddObjectToWorld(savedWorldObject).GetComponent<MiniSpacetruck>();
  }

  public GameObject AddSpacetruckContainer(Vector3 position)
  {
    SavedWorldObject savedWorldObject = new SavedWorldObject();
    savedWorldObject.SetPosition(position);
    savedWorldObject.type = (int)EMapObjectType.TYPE_SPACETRUCK_CONTAINER;
    savedWorldObject.angle = 270f;

    return AddObjectToWorld(savedWorldObject);
  }

  public void OnTapObject(MapObject _mapObject)
  {
  }

  public bool AddObjectToCurrentBase(EMapObjectType typeObject)
  {
    if (OneSpacetruckIsStarted())
      return false;

    Vector3 position = currentTerraformer.GetPositionForMiniSpacetruck();

    if (position == Vector3.zero)
    {
      uiController.ToastError("WARNING_SPACETRUCK_DELIVERY_IS_NOT_POSSIBLE");
      return false;
    }

    switch (typeObject)
    {
      case EMapObjectType.TYPE_TURRET:
        if (currentTerraformer.GetCurrentTurretUpgradeLevelForUpdate() >= Const.MAX_BASE_UPGRADE_LEVEL)
        {
          uiController.ToastError("WARNING_TURRET_UPGRADE_LEVEL_IS_MAXIMUM");
          return false;
        }
        break;
      case EMapObjectType.TYPE_TERRAFORMER_UPGRADE_SHIELD:
        if (currentTerraformer.GetCurrentShieldUpgradeLevel() >= Const.MAX_BASE_UPGRADE_LEVEL)
        {
          uiController.ToastError("WARNING_TURRET_UPGRADE_LEVEL_IS_MAXIMUM");
          return false;
        }
        break;
      case EMapObjectType.TYPE_TERRAFORMER_UPGRADE_WEAPON:
        if (currentTerraformer.GetCurrentWeaponUpgradeLevel() >= Const.MAX_BASE_UPGRADE_LEVEL)
        {
          uiController.ToastError("WARNING_TURRET_UPGRADE_LEVEL_IS_MAXIMUM");
          return false;
        }
        break;
    }

    bool result = miniSpacetruckGoTo(position, miniSpacetruck1, 0f);

    if (result)
    {
      prevTerraformer = currentTerraformer;
      deliveryType = DELIVERY_TYPE_BASE_UPGRADE;
      miniSpacetruckHaveObjectType = typeObject;
      miniSpacetruckNum = 1;
      resourcesInOrbitValue -= variableManager.GetResourceCost(typeObject);
    }

    return true;
  }

  public bool AddObjectToHero(EMapObjectType typeObject)
  {
    if (OneSpacetruckIsStarted())
      return false;

    Vector3 position = Terraformer.GetPositionForMiniSpacetruck(hero.GetPosition());

    if (position == Vector3.zero)
    {
      uiController.ToastError("WARNING_SPACETRUCK_DELIVERY_IS_NOT_POSSIBLE");
      return false;
    }

    if (miniSpacetruck1.IsStarted())
      return false;

    bool result = miniSpacetruckGoTo(position, miniSpacetruck1, 0f);

    if (result)
    {
      deliveryType = DELIVERY_TYPE_HERO;
      miniSpacetruckHaveObjectType = typeObject;
      miniSpacetruckNum = 1;
      resourcesInOrbitValue -= variableManager.GetResourceCost(typeObject);
    }

    return true;
  }

  public bool OneSpacetruckIsStarted()
  {
    if (miniSpacetruck1.IsStarted() || miniSpacetruck2.IsStarted()
      || miniSpacetruck3.IsStarted() || miniSpacetruck4.IsStarted())
    {
      uiController.ToastError("WARNING_SPACETRUCK_DELIVERY_IS_NOT_POSSIBLE_ONE_STARTED");
      return true;
    }

    return false;
  }

  public bool AddBaseTo()
  {
    if (OneSpacetruckIsStarted())
      return false;

    Vector3 position = hero.GetPosition();

    Terraformer terraformer = null;
    Collider[] hitColliders = Physics.OverlapSphere(position, Terraformer.MIN_DISTANCE_BETWEEN_TWO_TERRAFORMERS, Const.LAYER_MASK_BUILDING_ONLY);
    foreach (var hitCollider in hitColliders)
    {
      terraformer = hitCollider.GetComponent<Terraformer>();
      if (terraformer != null)
      {
        uiController.ToastError("WARNING_NEXT_TO_ANOTHER_TERRAFORMER");
        return false;
      }
    }

    FuelSpawnPoint fuelSpawnPoint = null;
    hitColliders = Physics.OverlapSphere(position, 30f, Const.LAYER_MASK_FUEL_SPAWN_POINT_ONLY);
    foreach (var hitCollider in hitColliders)
    {
      fuelSpawnPoint = hitCollider.GetComponent<FuelSpawnPoint>();
      if (fuelSpawnPoint != null)
      {
        uiController.ToastError("WARNING_NEXT_TO_FUEL_SPAWN_POINT");
        return false;
      }
    }

    AI__Enemy enemy = null;
    hitColliders = Physics.OverlapSphere(position, 50f, Const.LAYER_MASK_ENEMY_ONLY);
    foreach (var hitCollider in hitColliders)
    {
      enemy = hitCollider.GetComponent<AI__Enemy>();
      if (enemy != null)
      {
        uiController.ToastError("WARNING_SPACETRUCK_DELIVERY_IS_NOT_POSSIBLE_FOR_ENEMY");
        return false;
      }
    }

    Vector3[] points = new Vector3[4]; 
    points[0] = Terraformer.GetPositionForMiniSpacetruck(position, 8, 0);
    points[1] = Terraformer.GetPositionForMiniSpacetruck(position, 8, 1);
    points[2] = Terraformer.GetPositionForMiniSpacetruck(position, 8, 2);
    points[3] = Terraformer.GetPositionForMiniSpacetruck(position, 8, 3);

    Vector3 freePoint1 = Vector3.zero;
    Vector3 freePoint2 = Vector3.zero;

    for (int i = 0; i < 4;  i++)
    {
      Vector3 point = Terraformer.GetPositionForMiniSpacetruck(position, 8, i);
      if (Terraformer.IsFreePoint(point, 5f))
      {
        if (freePoint1 == Vector3.zero)
        {
          freePoint1 = point;
        }
        else
        {
          freePoint2 = point;
          break;
        }
      }
    }

    if (freePoint1 == Vector3.zero || freePoint2 == Vector3.zero)
    {
      uiController.ToastError("WARNING_SPACETRUCK_DELIVERY_IS_NOT_POSSIBLE");
      return false;
    }

    bool result1 = miniSpacetruckGoTo(freePoint1, miniSpacetruck1, 0f);
    bool result2 = miniSpacetruckGoTo(freePoint2, miniSpacetruck2, 2.4f);

    bool result = result1 && result2;

    if (result)
    {
      deliveryType = DELIVERY_TYPE_NEW_BASE;
      miniSpacetruckNum = 2;
      resourcesInOrbitValue -= variableManager.GetResourceCost(EMapObjectType.TYPE_TERRAFORMER);
    }

    return true;
  }

  private bool miniSpacetruckGoTo(Vector3 position, MiniSpacetruck spacetruck, float delay)
  {
    if (spacetruck.IsStarted())
      return false;

    StartCoroutine(goToWithDelay(spacetruck, position, delay));
    return true;
  }

  private IEnumerator goToWithDelay(MiniSpacetruck spacetruck, Vector3 position, float waitTime)
  {
    yield return new WaitForSeconds(waitTime);

    if (isPaused)
    {
      float delta = waitTime - pauseTimer;
      StartCoroutine(goToWithDelay(spacetruck, position, delta > 0f ? delta : 0f));
    }
    else
    {
      spacetruck.GoTo(position);
    }
  }

  public void MiniSpacetruckFinish()
  {
    miniSpacetruckNum -= 1;
    //Debug.Log("miniSpacetruckNum=" + miniSpacetruckNum);
    if (miniSpacetruckNum == 0)
    {
      MiniSpacetrucksFinish();
    }
  }

  public void MiniSpacetrucksFinish()
  {
    if (deliveryType == DELIVERY_TYPE_BASE_UPGRADE)
    {
      AddObjectToCurrentBase();
    }
    else if (deliveryType == DELIVERY_TYPE_HERO)
    {
      AddObjectToHero();
    }
    else if (deliveryType == DELIVERY_TYPE_NEW_BASE)
    {
      AddBaseManager();
    }
  }

  private void AddObjectToHero()
  {
    foreach (Transform obj in SpacetruckContainers.transform)
    {
      Vector3 position = obj.position;
      AddTransport(miniSpacetruckHaveObjectType, position);
      Destroy(obj.gameObject);
    }
  }

  private void AddObjectToCurrentBase()
  {
    foreach (Transform obj in SpacetruckContainers.transform)
    {
      Vector3 position = obj.position;

      switch (miniSpacetruckHaveObjectType)
      {
        case EMapObjectType.TYPE_TURRET:
          int upgradeLevel = prevTerraformer.GetCurrentTurretUpgradeLevelForUpdate();
          prevTerraformer.DestroyAllTurrets();
          switch (upgradeLevel)
          {
            case 0:
              for (int i = 0; i < 4; i++)
              {
                AddTurret(prevTerraformer.GetPositionForTurretByBase(i), 1, prevTerraformer.GetId());
              }
              break;
            case 1:
              for (int i = 0; i < 8; i++)
              {
                AddTurret(prevTerraformer.GetPositionForTurretByBase(i), 2, prevTerraformer.GetId());
              }
              break;
            case 2:
              for (int i = 0; i < 8; i++)
              {
                AddTurret(prevTerraformer.GetPositionForTurretByBase(i), 3, prevTerraformer.GetId());
              }
              break;
            case 3:
              for (int i = 0; i < 8; i++)
              {
                AddTurret(prevTerraformer.GetPositionForTurretByBase(i), 4, prevTerraformer.GetId());
              }
              break;
            case 4:
              for (int i = 0; i < 8; i++)
              {
                AddTurret(prevTerraformer.GetPositionForTurretByBase(i), 5, prevTerraformer.GetId());
              }
              break;
          }
          break;
        case EMapObjectType.TYPE_TERRAFORMER_UPGRADE_SHIELD:
          prevTerraformer.AddObject(miniSpacetruckHaveObjectType);
          break;
        case EMapObjectType.TYPE_TERRAFORMER_UPGRADE_WEAPON:
          prevTerraformer.AddObject(miniSpacetruckHaveObjectType);
          break;
        case EMapObjectType.TYPE_HARVESTER:
          AddHarvester(position, prevTerraformer.GetId());
          break;
      }

      Destroy(obj.gameObject);
    }

    needAssistanMenuUpdateProgresses = true;
  }

  private void AddBaseManager()
  {
    int index = 0;
    int newTerraformerId = 0;

    foreach (Transform obj in SpacetruckContainers.transform)
    {
      Vector3 position = obj.position;
      
      switch (index)
      {
        case 0:
          newTerraformerId = AddTerraformer(position);
          break;
        case 1:
          AddHarvester(position, newTerraformerId);
          break;
      }

      Destroy(obj.gameObject);
      index++;
    }

    needAssistanMenuUpdateProgresses = true;
  }

  //***********************************************************************************
  //***********************************************************************************

  public void ShowTerminalMenu()
  {
    if (isPaused)
      return;

    if (variableManager.IsFirstTaskInGame()
      && GetHintFromCounter(Const.KEYBOARD_HINT_TERMINAL_MENU) == 0
      && currentHintShow != Const.KEYBOARD_HINT_TERMINAL_MENU)
      return;

    if (currentHintShow == Const.MOUSE_HINT_FIRE
      || currentHintShow == Const.MOUSE_HINT_USE_TRANSPORT
      || currentHintShow == Const.KEYBOARD_HINT_WASD
      || currentHintShow == Const.KEYBOARD_HINT_MAP)
      return;

    if (uiController.IsShowMap())
      return;

    if (uiController.IsShowAssistantMenu())
      return;

    if (variableManager.IsFirstLevel())
    {
      uiController.RunAssistantMenuHelpMode();
    }

    uiController.ShowAssistantMenu();

    HintKeyboardHide(Const.KEYBOARD_HINT_TERMINAL_MENU);
    hero.Connect();
    HideTaskInfo();
  }

  public void HideTerminalMenu()
  {
    if (!uiController.IsShowAssistantMenu())
      return;

    uiController.HideAssistantMenu();
    hero.Disconnect();
    ShowTaskInfo();
  }

  public bool IsLevelReady()
  {
    return isLevelReady;
  }

  public bool IsGameStarting()
  {
    return isGameStarting;
  }

  public int GetAssistantsCount()
  {
    AI__Assistant[] items = FindObjectsOfType<AI__Assistant>();
    int result = 0;
    foreach (AI__Assistant item in items)
      if (!item.IsDead())
        result++;
    return result;
  }

  public int GetPyramidsCount()
  {
    Pyramid[] items = FindObjectsOfType<Pyramid>();
    int result = 0;
    foreach (Pyramid item in items)
      if (!item.IsDead())
        result++;
    return result;
  }

  public int[] GetPyramidsFuel()
  {
    Pyramid[] items = FindObjectsOfType<Pyramid>();
    int[] result = new int[items.Length];
    int i = 0;
    foreach (Pyramid item in items)
    {
      if (!item.IsDead())
      {
        result[i] = item.GetHP();
      }
      i++;
    }
    return result;
  }

  public bool IsAssistantFinished()
  {
    AI__Assistant[] items = FindObjectsOfType<AI__Assistant>();
    bool result = false;
    foreach (AI__Assistant item in items)
      if (item.IsFinished())
        result = true;
    return result;
  }

  public int GetAssistantHp()
  {
    AI__Assistant[] items = FindObjectsOfType<AI__Assistant>();
    int result = 0;
    foreach (AI__Assistant item in items)
      if (!item.IsDead())
        result += (int) item.GetCurrentFuelValue();
    return result;
  }

  public int GetAssistantHpMax()
  {
    return variableManager.GetHP(MapObject.EMapObjectType.TYPE_ASSISTANT);
  }

  public int GetBigGolemHp()
  {
    AI__BigGolem[] items = FindObjectsOfType<AI__BigGolem>();
    int result = 0;
    foreach (AI__BigGolem item in items)
      if (!item.IsDead())
        result += (int)item.GetHP();
    return result;
  }

  public int GetBigGolemHpMax()
  {
    return variableManager.GetHP(MapObject.EMapObjectType.TYPE_BIG_GOLEM);
  }

  protected void SearchBaseNearby()
  {
    searchBaseNearbyTimer += Time.deltaTime;
    if (searchBaseNearbyTimer >= 2f)
    {
      searchBaseNearbyTimer = 0;

      currentTerraformer = null;
      foreach (Terraformer terraformer in terraformers)
        if (Vector3.Distance(terraformer.GetPosition(), hero.GetPosition()) <= Terraformer.FOR_FRIENDLY_OBJECTS_DISTANCE)
        {
          currentTerraformer = terraformer;
          break;
        }
    }
  }

  public List<MapObject> GetHeroBesideFoundObjects()
  {
    List<MapObject> objects = new List<MapObject>();
    Collider[] hitColliders = Physics.OverlapSphere(hero.GetPosition(), Const.BESIDE_OBJECTS_FOR_FIND_DISTANCE, Const.LAYER_MASK_FOUND_OBJECT_ONLY);
    foreach (var hitCollider in hitColliders)
    {
      MapObject mapObject = hitCollider.GetComponent<MapObject>();
      if (mapObject != null)
      {
        objects.Add(mapObject);
      }
    }
    return objects;
  }

  protected void SearchFoundObjectsNearHero()
  {
    if (hero == null)
      return;

    List<MapObject> mapObjects = GetHeroBesideFoundObjects();
    if (mapObjects.Count > 0)
    {
      foreach (MapObject mapObject in mapObjects)
      {
        if (!foundObjects[mapObject.GetTypeAsInt()].Contains(mapObject.GetId()))
        {
          foundObjects[mapObject.GetTypeAsInt()].Add(mapObject.GetId());
          mapObject.OnFound();
        }
      }
    }
  }

  public void IAmFinished(MapObject mapObject)
  {
    if (!finishedObjects[mapObject.GetTypeAsInt()].Contains(mapObject.GetId()))
    {
      finishedObjects[mapObject.GetTypeAsInt()].Add(mapObject.GetId());
    }
  }

  public List<AI__Enemy> GetHeroBesideEnemiesOffCameraView()
  {
    List<AI__Enemy> objects = new List<AI__Enemy>();
    Collider[] hitColliders = Physics.OverlapSphere(hero.GetPosition(), Const.BESIDE_ENEMIES_DISTANCE, Const.LAYER_MASK_ENEMY_ONLY);
    foreach (var hitCollider in hitColliders)
    {
      AI__Enemy enemy = hitCollider.GetComponent<AI__Enemy>();
      if (enemy != null && !enemy.IsVisibleOnCameraView())
      {
        objects.Add(enemy);
      }
    }
    return objects;
  }

  protected void SearchEnemiesNearHero()
  {
    if (hero == null)
      return;

    List<AI__Enemy> enemies = GetHeroBesideEnemiesOffCameraView();
    if (enemies.Count > 0)
    {
      List<Vector3> directions = new List<Vector3>();
      Vector3 direction;
      foreach (AI__Enemy enemy in enemies)
      {
        direction = (enemy.GetPosition() - hero.GetPosition()).normalized;
        directions.Add(direction);
        //Debug.Log(">>> " + direction);
      }
      uiController.ShowPointers(directions, hero.GetMapObjectType());
    }
    else
    {
      uiController.HidePointers();
    }

    uiController.MovePointers(hero.GetPosition());
  }

  public bool СheckAvailabilityOfBaseNearby()
  {
    if (terraformers.Count == 0)
    {
      uiController.ToastError("WARNING_HAVE_NOT_FOUNDED_ANY_BASE");
      return false;
    }

    if (currentTerraformer == null)
    {
      uiController.ToastError("WARNING_BASE_OUT_OF_RANGE");
      return false;
    }

    return true;
  }

  public bool СheckResourcesInOrbit(int cost)
  {
    if (resourcesInOrbitValue < cost)
    {
      uiController.ToastError("WARNING_INSUFFICIENT_RESOURCES_IN_ORBIT");
      return false;
    }

    return true;
  }

  public Terraformer GetCurrentBase()
  {
    return currentTerraformer;
  }

  public void ShowMap()
  {
    if (isPaused)
      return;

    if (uiController.IsShowMap())
      return;

    if (currentHintShow != 0 
      && currentHintShow != Const.KEYBOARD_HINT_MAP)
      return;

    if (currentHintShow == Const.KEYBOARD_HINT_MAP)
    {
      HintKeyboardHide(Const.KEYBOARD_HINT_MAP);
    }

    HideTaskInfo();

    List<IMapObject> mapObjects = new List<IMapObject>();
    mapObjects.Add(hero.GetMapObject());
    mapObjects.AddRange(terraformers);
    foreach (Transform obj in Transport.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      if (hero.GetId() != obj.GetComponent<MapObject>().GetId())
        mapObjects.Add(obj.GetComponent<MapObject>());
    }
    foreach (Transform obj in AITransport.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      mapObjects.Add(obj.GetComponent<MapObject>());
    }
    foreach (Transform obj in Monsters.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      mapObjects.Add(obj.GetComponent<MapObject>());
    }
    foreach (Transform obj in FoundObjects.transform)
    {
      mapObjects.Add(obj.GetComponent<MapObject>());
    }
    uiController.ShowMap(mapObjects, sizeX);
  }

  public void UpdateMap()
  {
    if (isPaused)
      return;

    if (!uiController.IsShowMap())
      return;

    List<IMapObject> mapObjects = new List<IMapObject>();
    mapObjects.Add(hero.GetMapObject());
    mapObjects.AddRange(terraformers);
    foreach (Transform obj in Transport.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      if (hero.GetId() != obj.GetComponent<MapObject>().GetId())
        mapObjects.Add(obj.GetComponent<MapObject>());
    }
    foreach (Transform obj in AITransport.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      mapObjects.Add(obj.GetComponent<MapObject>());
    }
    foreach (Transform obj in Monsters.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      mapObjects.Add(obj.GetComponent<MapObject>());
    }
    foreach (Transform obj in FoundObjects.transform)
    {
      mapObjects.Add(obj.GetComponent<MapObject>());
    }

    uiController.UpdateMap(mapObjects);
  }

  public void HideMap()
  {
    uiController.HideMap();
    ShowTaskInfo();
  }

  /****************************************HINT****************************************/
  /************************************************************************************/

  public List<MapObject> GetHeroBesideObjectsForHint()
  {
    List<MapObject> objects = new List<MapObject>();
    if (hero == null)
      return objects;

    Collider[] hitColliders = Physics.OverlapSphere(hero.GetPosition(), Const.BESIDE_OBJECTS_FOR_USE_DISTANCE, Const.LAYER_MASK_TRANSPORT_ONLY);
    foreach (var hitCollider in hitColliders)
    {
      if (hitCollider.gameObject.Equals(hero.GetGameObject()))
        continue;

      MapObject mapObject = hitCollider.GetComponent<MapObject>();
      if (mapObject != null)
      {
        objects.Add(mapObject);
      }
    }
    return objects;
  }

  public void KillAllObjectsHere(Vector3 position, float radius)
  {
    position.y = 0f;
    Collider[] hitColliders = Physics.OverlapSphere(position, radius);
    foreach (var hitCollider in hitColliders)
    {
      MapObject mapObject = hitCollider.GetComponent<MapObject>();
      if (mapObject != null)
      {
        mapObject.TrueKill();
      }
    }

  }

  protected void CheckNeedShowHint()
  {
    needShowHintTimer += Time.deltaTime;
    if (needShowHintTimer > 0.5f)
    {
      needShowHintTimer = 0;
      List<MapObject> heroBesideObjects = GetHeroBesideObjectsForHint();

      uiController.UsableObjectMarkerHide();
      foreach (MapObject mapObject in heroBesideObjects)
      {
        uiController.UsableObjectMarkerShow(mapObject.GetPosition());
        HintTransportUseShow();
        break;
      }
    }
  }

  protected void AddHintToCounter(int hintId)
  {
    if (hintCounter.ContainsKey(hintId))
    {
      hintCounter[hintId] += 1;
    }
    else
    {
      hintCounter.Add(hintId, 1);
    }
  }

  protected void SetMaxHintToCounter(int hintId)
  {
    if (hintCounter.ContainsKey(hintId))
    {
      hintCounter[hintId] = Const.MAX_HINT_SHOW_TIMES;
    }
    else
    {
      hintCounter.Add(hintId, Const.MAX_HINT_SHOW_TIMES);
    }
  }

  protected int GetHintFromCounter(int hintId)
  {
    if (hintCounter.ContainsKey(hintId))
    {
      return hintCounter[hintId];
    }
    return 0;
  }

  /************************************************************************************/
  /************************************************************************************/

  protected List<SavedWorldObject> GetSavedWorldObjects()
  {
    List<SavedWorldObject> savedWorldObjects = new List<SavedWorldObject>();

    foreach (Transform obj in Terraformers.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      savedWorldObjects.Add(obj.GetComponent<Terraformer>().GetSavedWorldObject());
    }

    foreach (Transform obj in FoundObjects.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      savedWorldObjects.Add(obj.GetComponent<Pyramid>().GetSavedWorldObject());
    }

    foreach (Transform obj in Turrets.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      savedWorldObjects.Add(obj.GetComponent<Building>().GetSavedWorldObject());
    }

    foreach (Transform obj in AITransport.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      savedWorldObjects.Add(obj.GetComponent<AI__Transport>().GetSavedWorldObject());
    }

    foreach (Transform obj in Transport.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      savedWorldObjects.Add(obj.GetComponent<Transport>().GetSavedWorldObject());
    }

    foreach (Transform obj in Stones.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      savedWorldObjects.Add(obj.GetComponent<Stone>().GetSavedWorldObject());
    }

    // this order is very important! this girl 1
    savedWorldObjects.Add(girl.GetSavedWorldObject());

    // this order is very important! this monsters 2
    foreach (Transform obj in Monsters.transform)
    {
      if (obj.GetComponent<MapObject>().IsDead())
        continue;
      savedWorldObjects.Add(obj.GetComponent<AI__Enemy>().GetSavedWorldObject());
    }

    return savedWorldObjects;
  }

  public void SaveWorld()
  {
    StartCoroutine(SaveWorldWithDelay());
  }

  private IEnumerator SaveWorldWithDelay()
  {
    yield return new WaitForSeconds(1f);
    savedWorld.objects = GetSavedWorldObjects();
    
    savedWorld.resourcesInOrbitValue = resourcesInOrbitValue;
    savedWorld.resourcesInOrbitTimer = resourcesInOrbitTimer;
    
    savedWorld.checkEnemyWaveTimer = checkEnemyWaveTimer;
    savedWorld.gameTimer = gameTimer;
    savedWorld.gameWithoutPauseTimer = gameWithoutPauseTimer;
    savedWorld.taskWithoutPauseTimer = taskWithoutPauseTimer;
    savedWorld.pauseTimer = pauseTimer;

    savedWorld.hintCounter = hintCounter;
    savedWorld.foundObjects = foundObjects;
    savedWorld.destroyedObjects = destroyedObjects;

    savedWorld.lastEnemyWaveId = variableManager.GetCurrentLevel().GetLastEnemyWaveId();
    savedWorld.lastLevelTaskId = variableManager.GetCurrentLevel().GetLastLevelTaskId();
    
    SavedWorld.Write(savedWorld, variableManager.GetFilename());
    uiController.ToastInfo("INFO_WORLD_SAVED");
  }

  public void LoadWorld()
  {
    LevelCreate();
  }

  public void GoToMainMenu()
  {
    PlayerPrefs.SetInt(Const.PLAYER_PREFS_AFTER_WIN, 1);
    SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
  }

  private GameObject GetWrapperByType(EMapObjectType mapObjectType)
  {
    switch (mapObjectType)
    {
      case EMapObjectType.TYPE_STONE_20:
      case EMapObjectType.TYPE_STONE_40:
      case EMapObjectType.TYPE_STONE_60:
        return Stones;
      case EMapObjectType.TYPE_TREE:
      case EMapObjectType.TYPE_FOREST:
        return Trees;
      case EMapObjectType.TYPE_SCORPION:
      case EMapObjectType.TYPE_GOLEM:
      case EMapObjectType.TYPE_KAMIKAZE:
      case EMapObjectType.TYPE_CYCLOP:
      case EMapObjectType.TYPE_CENTAUR:
      case EMapObjectType.TYPE_DRAGONFLY:
      case EMapObjectType.TYPE_FLY:
      case EMapObjectType.TYPE_BIG_GOLEM:
        return Monsters;
      case EMapObjectType.TYPE_FUEL_SPAWN_POINT:
        return FuelSpawnPoints;
      case EMapObjectType.TYPE_BIKE:
      case EMapObjectType.TYPE_TANK:
      case EMapObjectType.TYPE_CANNON:
      case EMapObjectType.TYPE_BMP:
      case EMapObjectType.TYPE_STORM:
      case EMapObjectType.TYPE_FIGHTER:
        return Transport;
      case EMapObjectType.TYPE_HARVESTER:
      case EMapObjectType.TYPE_ASSISTANT:
        return AITransport;
      case EMapObjectType.TYPE_TERRAFORMER:
        return Terraformers;
      case EMapObjectType.TYPE_PYRAMID:
      case EMapObjectType.TYPE_BLACK_BOX:
        return FoundObjects;
      case EMapObjectType.TYPE_TURRET:
      case EMapObjectType.SUB_TYPE_TURRET_LEVEL_1:
      case EMapObjectType.SUB_TYPE_TURRET_LEVEL_2:
      case EMapObjectType.SUB_TYPE_TURRET_LEVEL_3:
      case EMapObjectType.SUB_TYPE_TURRET_LEVEL_4:
      case EMapObjectType.SUB_TYPE_TURRET_LEVEL_5:
        return Turrets;
      case EMapObjectType.TYPE_MINI_SPACETRUCK:
      case EMapObjectType.TYPE_SPACESHIP:
      case EMapObjectType.TYPE_SPACETRUCK:
        return Spaceships;
      case EMapObjectType.TYPE_SPACETRUCK_CONTAINER:
        return SpacetruckContainers;
      default:
        return Other;
    }
  }

  public void MyHpIsZero(MapObject mapObject)
  {
  }

  public void IAmWaitingDeath(MapObject mapObject)
  {
    if (!destroyedObjects.ContainsKey(mapObject.GetTypeAsInt()))
      destroyedObjects.Add(mapObject.GetTypeAsInt(), 0);
    destroyedObjects[mapObject.GetTypeAsInt()] += 1;
  }

  public void IAmDead(MapObject mapObject)
  {
    //Debug.Log("IAmDead >>> " + mapObject.GetMapObjectType() + " >>> " + mapObject.GetMapObjectSubType());
    if (mapObject.GetMapObjectType() == EMapObjectType.TYPE_TERRAFORMER)
    {
      if (currentTerraformer != null && currentTerraformer.GetId() == mapObject.GetId())
      {
        if (uiController.IsShowAssistantMenu())
        {
          uiController.SwitchAssistantMenu();
        }
        currentTerraformer = null;
      }
      //Debug.Log("IAmDead >>> REMOVE TERRAFORMER FROM LIST >>> " + mapObject.GetId());
      //Debug.Log("SIZE BEFORE >>> " + terraformers.Count);
      dictTerraformers.Remove(mapObject.GetId());
      terraformers.Remove(mapObject.GetComponent<Terraformer>());
      //Debug.Log("SIZE AFTER >>> " + terraformers.Count);
      needAssistanMenuUpdateProgresses = true;
    }
    else if (mapObject.GetMapObjectType() == EMapObjectType.TYPE_HARVESTER 
      || mapObject.GetMapObjectType() == EMapObjectType.TYPE_TURRET)
    {
      Terraformer terraformer = GetTerraformerById(mapObject.GetOwnerId());
      if (terraformer != null)
      {
        terraformer.RemoveObject(mapObject);
        needAssistanMenuUpdateProgresses = true;
      }
    }

    objectFactory.ReturnObject(mapObject);
  }

  protected Terraformer GetTerraformerById(int id)
  {
    foreach (Terraformer terraformer in terraformers)
      if (terraformer.GetId() == id)
        return terraformer;
    return null;
  }

  protected IEnumerator CheckWinLose()
  {
    yield return new WaitForSeconds(1f);

    //Debug.Log("CheckWinLose()");
    if (girl.IsDead())
    {
      LevelTask task = variableManager.GetCurrentLevel().GetCurrentTask();
      if (task != null && !task.IsEnded())
      {
        bool result = ShowDialog(variableManager.GetCurrentLevel().GetHeroDeadDialog());
        variableManager.GetCurrentLevel().EndTask(false);
        if (!result)
        {
          StartCoroutine(Lose());
        }
      }
    }
    else if (variableManager.GetCurrentLevel() != null)
    {
      LevelTask task = variableManager.GetCurrentLevel().GetCurrentTask();
      if (task != null && !task.IsEnded())
      {
        if (task.CheckWin(this))
        {
          taskWithoutPauseTimer = 0f;
          bool result = ShowDialog(variableManager.GetCurrentLevel().GetWinDialog());
          variableManager.GetCurrentLevel().EndTask(true);
          Debug.Log("WIN() >>> " + result);
          if (!result)
          {
            if (variableManager.GetCurrentLevel().IsEnded())
            {
              PauseAfterEnded();
              StartCoroutine(Win());
            }
            else
            {
              StartCoroutine(RunNextLevelTask());
            }
          }
        }
        else if (task.CheckLose(this))
        {
          bool result = ShowDialog(variableManager.GetCurrentLevel().GetLoseDialog());
          variableManager.GetCurrentLevel().EndTask(false);
          Debug.Log("END() >>> " + result);
          if (!result)
          {
            PauseAfterEnded();
            StartCoroutine(Lose());
          }
        }
      }
    }

    if (isGameStarting)
      StartCoroutine(CheckWinLose());
  }

  private IEnumerator RunNextLevelTask()
  {
    yield return new WaitForSeconds(1f);
    LevelTask task = variableManager.GetCurrentLevel().GetNextTask();
    //Debug.Log("RunNextLevelTask() = " + task);
    if (task != null)
    {
      ShowDialog(variableManager.GetCurrentLevel().GetStartDialog());
    }
  }

  private IEnumerator Win()
  {
    yield return new WaitForSeconds(1f);
    Debug.Log("YOU_WIN");
    isWin = true;
    isGameStarting = false;

    Level nextLevel = variableManager.GetNextLevel();
    if (nextLevel != null)
    {
      PlayerPrefs.SetInt(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID, nextLevel.Id);
    }

    if (variableManager.GetCurrentLevel().Id == Const.LAST_LEVEL_ID)
    {
      PlayerPrefs.SetInt(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID, Const.LAST_LEVEL_ID + 1);
    }

    if (uiController.IsShowAssistantMenu())
    {
      uiController.SwitchAssistantMenu();
    }
    uiController.SetBottomHint("YOU_WIN");
  }

  private IEnumerator Lose()
  {
    yield return new WaitForSeconds(1f);
    Debug.Log("YOU_LOSE");
    isLose = true;
    isGameStarting = false;
    if (uiController.IsShowAssistantMenu())
    {
      uiController.SwitchAssistantMenu();
    }
    uiController.SetBottomHint("YOU_LOSE");
  }

  public bool Pause()
  {
    if (uiController.IsShowDialog())
      return isPaused;

    isPaused = !isPaused;
    pauseTimer = 0f;

    MapObject[] objects = FindObjectsOfType<MapObject>();
    foreach (MapObject mapObject in objects)
      mapObject.SetPause(isPaused);

    if (isPaused)
    {
      if (uiController.IsShowAssistantMenu())
        HideTerminalMenu();

      if (uiController.IsShowMap())
        HideMap();
    }

    CheckPauseForUI();

    return isPaused;
  }

  private void CheckPauseForUI()
  {
    uiController.ClearHints();
    if (isPaused)
    {
      uiController.SetMiddleHint("HINT_PAUSE_OFF");
      HideTaskInfo();
    }
    else
    {
      uiController.SetTopHintToRight("HINT_PAUSE_ON");
      ShowTaskInfo();
    }
  }

  public bool PauseAfterEnded()
  {
    isPaused = !isPaused;
    pauseTimer = 0f;

    MapObject[] objects = FindObjectsOfType<MapObject>();
    foreach (MapObject mapObject in objects)
      mapObject.SetPause(isPaused);

    if (isPaused)
    {
      if (uiController.IsShowAssistantMenu())
        HideTerminalMenu();

      if (uiController.IsShowMap())
        HideMap();
    }

    uiController.ClearHints();

    return isPaused;
  }

  public Terraformer GetNearbyTerraformer(Terraformer fromTerraformer)
  {
    Terraformer nearbyTerraformer = null;
    float minDistance = float.MaxValue;
    
    foreach (Terraformer terraformer in terraformers)
    {
      if (terraformer.GetId() == fromTerraformer.GetId() 
        || terraformer.IsDead())
        continue;

      float distance = Vector3.Distance(fromTerraformer.GetPosition(), terraformer.GetPosition());
      if (minDistance > distance)
      {
        minDistance = distance;
        nearbyTerraformer = terraformer;
      }
    }

    return nearbyTerraformer;
  }

  public bool Strike()
  {
    if (sateliteStrikeRecoveryTimer > 0)
    {
      uiController.ToastError("WARNING_SATELITE_STRIKE_IS_RECOVERING");
      return false;
    }

    if (sateliteStrike.IsFiring())
    {
      uiController.ToastError("WARNING_SATELITE_STRIKE_IS_FIRING");
      return false;
    }

    sateliteStrike.Fire(hero.GetPosition(), 
      variableManager.GetSateliteStrikePrepareTime(), 
      variableManager.GetSateliteStrikeFiringTime());

    sateliteStrikeRecoveryTimer = variableManager.GetSateliteStrikeRecoveryTime();
    return true;
  }

  public Dictionary<int, float> GetTerraformerEffects(MapObject mapObject)
  {
    foreach (Terraformer terraformer in terraformers)
      if (Vector3.Distance(terraformer.GetPosition(), mapObject.GetPosition()) <= Terraformer.FOR_FRIENDLY_OBJECTS_DISTANCE)
        return terraformer.GetEffects();

    Dictionary<int, float> effects = new Dictionary<int, float>();
    effects.Add(Terraformer.EFFECT_SHIELD, 0);
    effects.Add(Terraformer.EFFECT_WEAPON, 0);
    return effects;
  }
}
