﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class GolemBlood : MonoBehaviour
{
  ParticleSystem particleSystem;

  void Start()
  {
    particleSystem = GetComponentInChildren<ParticleSystem>();
    Utils.SetCollision4Particles(particleSystem, Const.TAG_SNOW_BLOOD);
  }

  public void Run()
  {
    particleSystem.Play();
  }

  public void SetPause(bool value)
  {
    if (!particleSystem.IsAlive())
      return;

    if (value)
    {
      particleSystem.Pause();
    }
    else
    {
      particleSystem.Play();
    }
  }
}
