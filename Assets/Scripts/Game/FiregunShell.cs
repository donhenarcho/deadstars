﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiregunShell : MonoBehaviour
{
  public static float MAX_DISTANCE_TO_TARGET = 1000f;

  public enum FiregunShellState
  {
    Idle,
    Start,
    Fly,
    End,
    Dead
  }

  public ParticleSystem fire;
  public GameObject FiregunShellFirePrefab;

  GameObject owner;
  Firegun firegun;
  Vector3 startDirection;
  AI__Enemy target;
  FiregunShellState currentState;
  float timer;
  float speedMove = 40f;
  float speedRotate = 4f;

  bool isInit = false;
  bool isPaused = false;

  public void Init()
  {
    currentState = FiregunShellState.Idle;
    isInit = true;
  }

  void Update()
  {
    if (!isInit)
      return;

    if (isPaused)
      return;

    switch (currentState)
    {
      case FiregunShellState.Idle:
        timer += Time.deltaTime;
        if (timer > 1f)
        {
          timer = 0f;
          //SearchTarget();
          currentState = FiregunShellState.End;
        }
        break;
      case FiregunShellState.Start:
        timer += Time.deltaTime;
        gameObject.transform.Translate(transform.up * speedMove * Time.deltaTime);
        if (timer > 0.2f)
        {
          timer = 0f;
          currentState = FiregunShellState.Fly;
        }
        break;
      case FiregunShellState.Fly:
        //gameObject.transform.Translate(transform.up * speedMove * Time.deltaTime);
        if (target != null)
        {
          Vector3 targetPosition = target.GetPosition();
          Vector3 direction = targetPosition - transform.position;
          transform.up = Vector3.Slerp(transform.up, direction, speedRotate * Time.deltaTime);
          if (transform.position.y <= 0f)
          {
            currentState = FiregunShellState.End;
          }
        }
        else
        {
          timer += Time.deltaTime;
          if (timer > 1f)
          {
            currentState = FiregunShellState.End;
          }
        }
        break;
      case FiregunShellState.End:
        fire.Stop();
        currentState = FiregunShellState.Dead;
        firegun.OnShellDead(this);
        Boom();
        Destroy(gameObject);
        break;
    }
  }

  void Boom()
  {
    Vector3 firePosition = transform.position;
    firePosition.y = 0.1f;
    Instantiate(FiregunShellFirePrefab, firePosition, Quaternion.identity);
  }

  public void SetOwner(GameObject gameObject)
  {
    owner = gameObject;
  }

  public void SetFiregun(Firegun firegun)
  {
    this.firegun = firegun;
  }

  void SearchTarget()
  {
    Dictionary<AI__Enemy, float> targets = new Dictionary<AI__Enemy, float>();
    AI__Enemy bestTarget = null;
    float bestCost = 0f;

    Collider[] hitColliders = Physics.OverlapSphere(transform.position, MAX_DISTANCE_TO_TARGET, Const.LAYER_MASK_ENEMY_ONLY);
    foreach (var hitCollider in hitColliders)
    {
      AI__Enemy target = hitCollider.GetComponent<AI__Enemy>();
      if (target != null)
      {
        float cost = 1000000f;
        cost -= target.GetHP();
        cost -= Vector3.Distance(transform.position, target.GetPosition());
        if (cost > bestCost)
        {
          bestCost = cost;
          bestTarget = target;
        }
      }
    }

    this.target = bestTarget;
  }

  public void SetPause(bool value)
  {
    isPaused = value;
    if (value)
    {
      fire.Pause();
    }
    else
    {
      fire.Play();
    }
  }
}
