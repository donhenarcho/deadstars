﻿using UnityEngine;

public interface IMovingObject
{
  void BackTo();
}
