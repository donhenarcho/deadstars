﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInvisible : MonoBehaviour, IObjectInvisible
{
  void Start()
  {
        
  }

  void Update()
  {
        
  }

  public void SetAlpha(float alpha)
  {
    //Debug.Log("SetAlpha()=" + alpha);
    MeshRenderer[] meshRenderers = this.GetComponentsInChildren<MeshRenderer>();
    foreach (MeshRenderer renderer in meshRenderers)
    {
      if (renderer.GetComponent<ObjectInvisibleMeshRendererSkip>() != null)
        continue;
      renderer.material.color = new Color(1.0f, 1.0f, 1.0f, alpha);
    }
  }
}
