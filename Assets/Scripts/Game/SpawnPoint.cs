﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpawnPoint : MapObject
{
  public GameObject markerForEditor;
  protected float SPAWN_DELAY = 6f;
  protected float SPAWN_DELAY_RANDOM_PART = 0f;
  protected float timeSpawn = 0;
  protected float delayAfterSpawn = 0;
  protected bool isInitSpawn = false;

  protected Vector3 areaSize = Vector3.zero;

  protected void SpawnPointInit()
  {
    //Debug.Log("SpawnPointInit()");
    Init();
    if (levelController != null)
    {
      Destroy(markerForEditor);
      BoxCollider boxCollider = GetComponent<BoxCollider>();
      areaSize = boxCollider.size;
      boxCollider.size = new Vector3(0.01f, 0.01f, 0.01f);
      timeSpawn = SPAWN_DELAY;
      if (isInitSpawn)
      {
        Spawn();
      }
    }
  }

  protected bool CheckDelay()
  {
    if (timeSpawn > 0)
    {
      timeSpawn -= Time.deltaTime;
      if (timeSpawn <= 0)
      {
        timeSpawn = SPAWN_DELAY;
        Spawn();
        return false;
      }
      return true;
    }
    return false;
  }

  protected virtual void Spawn()
  {
    //Debug.Log("No added scorpion");
  }

  public override bool IsVisibleOnGameMap()
  {
    return false;
  }

  public Vector3 GetAreaSize()
  {
    return areaSize;
  }
}
