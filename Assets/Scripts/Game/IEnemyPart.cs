﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyPart
{
  EnemyPartTagEnum GetTag();
  void SetEnemyController(IEnemy golemController);

  void BoxCollider(bool enabled);
  void Show();
  void Hide();

  bool IsBody();
  bool IsHead();
}
