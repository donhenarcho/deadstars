﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class ObjectActions
{
  [XmlAttribute("id")]
  public int Id;

  [XmlElement("ObjectAction")]
  public ObjectAction[] ObjectActionArray;
  private Queue<ObjectAction> ObjectActionQueue;

  public void InitAfterLoad()
  {
    ObjectActionQueue = new Queue<ObjectAction>();
    //Utils.Reverse(ObjectActionArray);
    foreach (ObjectAction objectAction in ObjectActionArray)
    {
      ObjectActionQueue.Enqueue(objectAction);
    }
  }

  public Queue<ObjectAction> GetActions()
  {
    return ObjectActionQueue;
  }
}
