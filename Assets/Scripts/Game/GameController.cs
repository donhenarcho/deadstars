﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
  private WorldController levelController;
  private UI__Controller uiController;

  private bool isInit = false;

  private int typeControl = Const.TYPE_CONTROL_KEYBOARD_WITH_MOUSE;

  void Start()
  {
    //QualitySettings.vSyncCount = 1;
    //Application.targetFrameRate = 30;

    levelController = FindObjectOfType<WorldController>();
    uiController = FindObjectOfType<UI__Controller>();

    LoadSettings();
  }

  void Update()
  {
    if (!isInit)
    {
      isInit = true;
      levelController.Play();
    }

    /*if (Input.GetKeyDown(KeyCode.F3))
    {
      ScreenCapture.CaptureScreenshot("screenshot_" + (int)(Time.time * 1000) + ".png");
    }*/

    /*foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
    {
      if (Input.GetKeyDown(kcode))
        Debug.Log("KeyCode down: " + kcode);
    }*/
  }

  private void LoadSettings()
  {
    if (PlayerPrefs.HasKey(Const.PLAYER_PREFS_JOYSTICK_IS_ON))
    {
      if (PlayerPrefs.GetInt(Const.PLAYER_PREFS_JOYSTICK_IS_ON) == 1)
        typeControl = Const.TYPE_CONTROL_JOYSTICK;
      else
        typeControl = Const.TYPE_CONTROL_KEYBOARD_WITH_MOUSE;
    }
  }

  public void PlayerStopped()
  {
  }

  public int GetTypeControl()
  {
    return typeControl;
  }

  public bool IsJoystickControl()
  {
    return typeControl == Const.TYPE_CONTROL_JOYSTICK;
  }

  public void SwitchTypeControl()
  {
    typeControl = typeControl == Const.TYPE_CONTROL_KEYBOARD_WITH_MOUSE ? Const.TYPE_CONTROL_JOYSTICK : Const.TYPE_CONTROL_KEYBOARD_WITH_MOUSE;
  }

  public KeyCode GetKeyCodeForAction(int actionId)
  {
    switch (actionId)
    {
      case Const.CONTROL_ACTION_FIRE:
        if (IsJoystickControl())
          return KeyCode.JoystickButton0;
        else
          return KeyCode.Mouse0;
      case Const.CONTROL_ACTION_USE:
        if (IsJoystickControl())
          return KeyCode.JoystickButton1;
        else
          return KeyCode.Mouse1;
      case Const.CONTROL_ACTION_MENU:
        if (IsJoystickControl())
          return KeyCode.JoystickButton1;
        else
          return KeyCode.Q;
      case Const.CONTROL_ACTION_MAP:
        if (IsJoystickControl())
          return KeyCode.JoystickButton1;
        else
          return KeyCode.E;
      case Const.CONTROL_ACTION_ESCAPE:
        if (IsJoystickControl())
          return KeyCode.Escape;
        else
          return KeyCode.Escape;
      case Const.CONTROL_ACTION_RESTART:
        if (IsJoystickControl())
          return KeyCode.JoystickButton7;
        else
          return KeyCode.F1;
      case Const.CONTROL_ACTION_PAUSE:
        if (IsJoystickControl())
          return KeyCode.F3;
        else
          return KeyCode.F3;
    }

    return KeyCode.None;
  }
}
