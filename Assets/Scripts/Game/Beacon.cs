﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beacon : MonoBehaviour
{
  public GameObject marker;
  private Material material;
  private float blinkingTimer = 0f;
  private bool emit = false;
  private bool isBlinking = true;

  void Start()
  {
    material = marker.GetComponentInChildren<Renderer>().material;
  }

  void Update()
  {
    if (isBlinking)
      blinking();
  }

  private void blinking()
  {
    if (blinkingTimer >= 1f)
    {
      emit = !emit;
      if (emit)
        material.EnableKeyword("_EMISSION");
      else
        material.DisableKeyword("_EMISSION");
      blinkingTimer = 0f;
    }

    blinkingTimer += Time.deltaTime;
  }

  public void SetBlinking(bool isBlinking)
  {
    this.isBlinking = isBlinking;
  }
}
