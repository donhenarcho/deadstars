﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationObject : MonoBehaviour
{
  protected int state = 0;
  protected int prevState = 0;
  protected int numStates = 0;
  protected Transform currentTransform = null;
  protected bool isInit = false;
  protected bool isPlay = false;
  protected bool isPaused = false;
  protected float ANIMATION_DELAY = 0.2f;
  protected float animationTimer = 0;
  
  protected int animationType = 0;
  protected int[] animationStates;
  protected int currentAnimationStateIndex = 0;

  void Update()
  {
    if (!isInit || !isPlay)
      return;

    if (isPaused)
      return;

    CheckAnimation();
  }

  private void LateUpdate()
  {
    if (!isInit)
    {
      isInit = true;
      numStates = transform.childCount;
    }
  }

  public void SetAnimationDelay(float delay)
  {
    ANIMATION_DELAY = delay;
  }

  public void CheckAnimation()
  {
    animationTimer += Time.deltaTime;
    if (animationTimer >= ANIMATION_DELAY)
    {
      animationTimer = 0;
      Next();
    }
  }

  public void SetState(
      int _state
    )
  {
    if (state == _state)
      return;

    prevState = state;
    state = _state;

    int num = 0;
    foreach (Transform obj in transform)
    {
      if (state == num)
      {
        obj.gameObject.SetActive(true);
        currentTransform = obj;
      }
      else
      {
        obj.gameObject.SetActive(false);
      }
      ++num;
    }
  }

  public int GetState()
  {
    return state;
  }

  public void SetAnimationWithStates(int[] states)
  {
    animationType = 1;
    this.animationStates = states;
    currentAnimationStateIndex = 0;
  }

  public void Stop()
  {
    isPlay = false;
  }

  public void Play()
  {
    isPlay = true;
  }

  public void Next()
  {
    int _state = state;

    if (animationType == 0)
    {
      _state = state + 1;
      if (_state >= numStates)
        _state = 0;
    }
    else
    {
      currentAnimationStateIndex++;
      if (currentAnimationStateIndex > animationStates.Length - 1)
        currentAnimationStateIndex = 0;

      _state = animationStates[currentAnimationStateIndex];
    }

    SetState(_state);
  }

  public void Prev()
  {
    int _state = state - 1;
    if (_state == -1)
      _state = numStates;

    SetState(_state);
  }

  public void SetPause(bool value)
  {
    isPaused = value;
  }
}
