﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowWave : MonoBehaviour, ISnowWave
{
  private ParticleSystem particleSystem;

  void Start()
  {
    particleSystem = GetComponent<ParticleSystem>();
    Utils.SetCollision4Particles(particleSystem, Const.TAG_SNOW_WAVE);
  }

  public void Run()
  {
    particleSystem.Play();
  }

  public void ChangePosition(Vector3 vector)
  {
    this.gameObject.transform.localPosition = vector;
  }

  public void SetPause(bool value)
  {
    if (!particleSystem.IsAlive())
      return;

    if (value)
    {
      particleSystem.Pause();
    }
    else
    {
      particleSystem.Play();
    }
  }
}
