﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObjectInvisible
{
  void SetAlpha(float alpha);
}
