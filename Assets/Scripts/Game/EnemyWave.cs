﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class EnemyWave
{
  [XmlAttribute("id")]
  public int Id;
  [XmlAttribute("waitTaskId")]
  public int WaitTaskId;
  [XmlAttribute("onlyTaskId")]
  public int OnlyTaskId;
  [XmlAttribute("pauseAfter")]
  public int PauseAfter;

  [XmlElement("ObjectValue")]
  public ObjectValue[] ObjectValues;
  private Dictionary<int, int> ObjectValuesDict;

  public void InitAfterLoad()
  {
    ObjectValuesDict = new Dictionary<int, int>();
    if (ObjectValues != null)
    {
      foreach (ObjectValue objectValue in ObjectValues)
        ObjectValuesDict.Add(objectValue.ObjectType, objectValue.Value);
    }
  }

  public Dictionary<int, int> GetObjectValuesDict()
  {
    return ObjectValuesDict;
  }

  public override string ToString()
  {
    string temp = "";
    temp += "\n *****ENEMY_WAVE [" + Id + "]*****";
    temp += "\n WaitTaskId=" + WaitTaskId;
    temp += "\n PauseAfter=" + PauseAfter;
    foreach (KeyValuePair<int, int> keyValuePair in ObjectValuesDict)
    {
      temp += "\n ObjectValue=" + keyValuePair.Key + " >>> " + keyValuePair.Value;
    }
    return temp;
  }
}
