﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapObject : MonoBehaviour, IMapObject, AI__MoveTarget
{
  public enum EMapObjectType
  {
    TYPE_NONE = 0,
    TYPE_RANDOM = 999,

    TYPE_STONE_60 = 10,
    TYPE_STONE_40 = 11,
    TYPE_STONE_20 = 12,

    TYPE_HERO = 3,
    TYPE_ASSISTANT = 31,

    TYPE_ENEMY = 40,
    TYPE_GOLEM = 41,
    TYPE_SCORPION = 42,
    TYPE_KAMIKAZE = 43,
    TYPE_CYCLOP = 44,
    TYPE_CENTAUR = 45,
    TYPE_DRAGONFLY = 46,
    TYPE_FLY = 47,
    TYPE_BIG_GOLEM = 48,

    TYPE_TRANSPORT = 50,
    TYPE_HARVESTER = 51,
    TYPE_CANNON = 54,
    TYPE_TANK = 55,
    TYPE_BIKE = 56,
    TYPE_BMP = 57,
    TYPE_STORM = 58,
    TYPE_FIGHTER = 59,

    TYPE_SATELITE_STRIKE = 60,

    TYPE_SPACESHIP = 90,
    TYPE_SPACETRUCK = 91,
    TYPE_MINI_SPACETRUCK = 92,
    TYPE_SPACETRUCK_CONTAINER = 93,
    TYPE_ARK = 94,
    TYPE_BLACK_BOX = 95,

    TYPE_BUILDING = 200,
    TYPE_TERRAFORMER = 202,
    TYPE_TERRAFORMER_UPGRADE_SHIELD = 2021,
    TYPE_TERRAFORMER_UPGRADE_WEAPON = 2022,
    TYPE_TURRET = 203,
    SUB_TYPE_TURRET_LEVEL_1 = 2031,
    SUB_TYPE_TURRET_LEVEL_2 = 2032,
    SUB_TYPE_TURRET_LEVEL_3 = 2033,
    SUB_TYPE_TURRET_LEVEL_4 = 2034,
    SUB_TYPE_TURRET_LEVEL_5 = 2035,
    TYPE_PYRAMID = 204,

    TYPE_FUEL_SPAWN_POINT = 301,

    TYPE_TREE = 401,
    TYPE_FOREST = 402,

    TYPE_FIRE = 501
  }

  public EMapObjectType Type = EMapObjectType.TYPE_NONE;
  public EMapObjectType SubType = EMapObjectType.TYPE_NONE;
  public int ObjectRadius = 0;

  protected GameController gameController;
  protected WorldController levelController;
  protected VariableManager variableManager;

  protected AudioObject audioObject;

  protected int id = 0;
  protected int ownerId = 0;

  protected bool isDead = false;
  protected bool isInit = false;
  protected bool isReady = false;

  protected bool isPaused = false;

  public bool withGravity = true;
  public bool isFlyingObject = false;

  protected Queue<ObjectAction> behavior;
  protected ObjectAction currentObjectAction;
  protected float waitNextObjectActionTimer = 0f;

  protected Dictionary<int, float> effects = new Dictionary<int, float>();
  protected float updateEffectsTimer = 0f;
  protected const float UPDATE_EFFECTS_DELAY = 2f;

  public bool IsFlyingObject()
  {
    return isFlyingObject;
  }

  protected void Init()
  {
    gameController = FindObjectOfType<GameController>();
    levelController = FindObjectOfType<WorldController>();
    variableManager = FindObjectOfType<VariableManager>();
    audioObject = GetComponent<AudioObject>();
  }

  public virtual void EndedObjectInit()
  {

  }

  private void OnMouseDown()
  {
    if (EventSystem.current.IsPointerOverGameObject())
    {
      return;
    }

    if (levelController != null)
      levelController.OnTapObject(this);
  }

  public virtual void Beside(
      GameObject _monster
    )
  {

  }

  public int GetObjectRadius()
  {
    return ObjectRadius;
  }

  public virtual bool IsTarget()
  {
    return false;
  }

  public virtual bool IsVisibleOnGameMap()
  {
    return true;
  }

  public virtual bool IsEnemy()
  {
    return false;
  }

  public Vector3 GetPosition()
  {
    return transform.position;
  }

  public void SetPosition(Vector3 position)
  {
    if (isFlyingObject)
    {
      position.y = Const.FLYHEIGHT;
    }

    CharacterController characterController = GetComponent<CharacterController>();
    if (characterController != null)
      characterController.enabled = false;
    this.transform.position = position;
    if (characterController != null)
      characterController.enabled = true;
  }

  public GameObject GetGameObject()
  {
    return transform.gameObject;
  }

  public Transform GetTransform()
  {
    return transform;
  }

  public MapObject GetMapObject()
  {
    return this;
  }

  public void SetEnabledForAllColliders(bool enabled)
  {
    foreach (Collider collider in GetComponentsInChildren<Collider>())
      collider.enabled = enabled;
  }

  protected void SetEnabledForRenderers(Transform transform, bool enabled)
  {
    Renderer[] renderers = transform.GetComponentsInChildren<Renderer>();
    foreach (Renderer renderer in renderers)
      renderer.enabled = enabled;
  }

  public virtual bool IsHero()
  {
    return false;
  }

  public void SetId(int id)
  {
    this.id = id;
  }

  public int GetId()
  {
    return id;
  }

  public void SetOwnerId(int ownerId)
  {
    this.ownerId = ownerId;
  }

  public int GetOwnerId()
  {
    return ownerId;
  }

  public EMapObjectType GetMapObjectType()
  {
    return Type;
  }

  public EMapObjectType GetMapObjectSubType()
  {
    return SubType;
  }

  public EMapObjectType GetUniqueType()
  {
    return SubType != EMapObjectType.TYPE_NONE ? SubType : Type;
  }

  public int GetTypeAsInt()
  {
    return (int)Type;
  }

  public int GetSubTypeAsInt()
  {
    return (int)SubType;
  }

  public virtual SavedWorldObject GetSavedWorldObject()
  {
    SavedWorldObject savedLevelObject = new SavedWorldObject();
    savedLevelObject.id = GetId();
    savedLevelObject.ownerId = GetOwnerId();
    savedLevelObject.SetPosition(transform.position);
    savedLevelObject.type = GetTypeAsInt();
    savedLevelObject.subType = GetSubTypeAsInt();
    savedLevelObject.angle = transform.localEulerAngles.y;
    return savedLevelObject;
  }

  public virtual SavedWorldObject SetSavedWorldObject(SavedWorldObject savedLevelObject)
  {
    SetId(savedLevelObject.id);
    SetOwnerId(savedLevelObject.ownerId);
    SetPosition(savedLevelObject.GetPosition());
    Type = (EMapObjectType) savedLevelObject.type;
    SubType = (EMapObjectType) savedLevelObject.subType;
    transform.Rotate(new Vector3(0, savedLevelObject.angle, 0));
    transform.localScale = new Vector3(
      transform.localScale.x * savedLevelObject.scaleXZ,
      transform.localScale.y * savedLevelObject.scaleY,
      transform.localScale.z * savedLevelObject.scaleXZ);
    return savedLevelObject;
  }

  public virtual void Ready()
  {
    isDead = false;
    isReady = true;
  }

  public bool IsReady()
  {
    return isReady;
  }

  public virtual bool IsDead()
  {
    return isDead;
  }

  public virtual void Reset()
  {
    isDead = true;
    isReady = false;
    id = 0;
    ownerId = 0;
  }

  protected void MyHpIsZero()
  {
    levelController.MyHpIsZero(this);
  }

  protected void IAmWaitingDeath()
  {
    levelController.IAmWaitingDeath(this);
  }

  protected void IAmDead()
  {
    levelController.IAmDead(this);
  }

  public void Kill()
  {
    IAmDead();
  }

  public virtual void TrueKill()
  {
  }

  public virtual void SetPause(bool value)
  {
    this.isPaused = value;
  }

  public virtual void OnFound()
  {
  }

  public void SetBehavior(Queue<ObjectAction> behavior)
  {
    Debug.Log("SetBehavior() >>> " + behavior.Count);
    this.behavior = behavior;
  }

  protected virtual void DoIt(string operationName)
  {

  }

  protected int GetDamage4Enemy(GameObject other)
  {
    int damage = 0;
    switch (other.tag)
    {
      case Const.TAG_SNOW_WAVE:
        damage = variableManager.GetDamage(EMapObjectType.TYPE_GOLEM);
        break;
      case Const.TAG_SNOW_WAVE_BIG:
      case Const.TAG_SNOW_BLOOD:
        damage = variableManager.GetDamage(EMapObjectType.TYPE_BIG_GOLEM);
        break;
      case Const.TAG_SNOW_WAVE_SMALL:
        damage = variableManager.GetDamage(EMapObjectType.TYPE_KAMIKAZE);
        break;
      case Const.TAG_FIRE:
      case Const.TAG_PLASMA:
        ParticleSystem particleSystem = other.GetComponent<ParticleSystem>();
        if (particleSystem != null)
        {
          damage = particleSystem.GetSafeCollisionEventSize();
        }
        break;
    }

    if (effects.ContainsKey(Terraformer.EFFECT_WEAPON))
      damage *= Mathf.RoundToInt(1.0f + effects[Terraformer.EFFECT_WEAPON] * 0.05f);

    return damage;
  }

  protected int GetDamage4Units(GameObject other)
  {
    int damage = 0;
    switch (other.tag)
    {
      case Const.TAG_SNOW_WAVE:
        damage = variableManager.GetDamage(EMapObjectType.TYPE_GOLEM);
        break;
      case Const.TAG_SNOW_WAVE_SMALL:
        damage = variableManager.GetDamage(EMapObjectType.TYPE_KAMIKAZE);
        break;
      case Const.TAG_SNOW_WAVE_BIG:
      case Const.TAG_SNOW_BLOOD:
        damage = variableManager.GetDamage(EMapObjectType.TYPE_BIG_GOLEM);
        break;
      case Const.TAG_CYCLOP_FIRE:
        damage = variableManager.GetDamage(EMapObjectType.TYPE_CYCLOP);
        break;
      case Const.TAG_CENTAUR_ARROW:
        damage = variableManager.GetDamage(EMapObjectType.TYPE_CENTAUR);
        break;
      case Const.TAG_DRAGONFLY_ARROW:
        damage = variableManager.GetDamage(EMapObjectType.TYPE_DRAGONFLY);
        break;
      case Const.TAG_FIRE:
      case Const.TAG_PLASMA:
        ParticleSystem particleSystem = other.GetComponent<ParticleSystem>();
        if (particleSystem != null)
        {
          damage = particleSystem.GetSafeCollisionEventSize();
        }
        break;
    }

    if (effects.ContainsKey(Terraformer.EFFECT_SHIELD))
      damage *= Mathf.RoundToInt(1.0f - effects[Terraformer.EFFECT_SHIELD] * 0.05f);

    return damage;
  }

  protected void AddEffects(Dictionary<int, float> effects)
  {
    foreach (KeyValuePair<int, float> keyValuePair in effects)
    {
      if (this.effects.ContainsKey(keyValuePair.Key))
        this.effects[keyValuePair.Key] = keyValuePair.Value;
      else
        this.effects.Add(keyValuePair.Key, keyValuePair.Value);
    }

    OnUpdatedEffects();
  }

  protected void UpdateEffects()
  {
    if (levelController == null)
      return;

    AddEffects(levelController.GetTerraformerEffects(this));

    if (effects[Terraformer.EFFECT_SHIELD] > 0)
    {
      Debug.Log("EFFECT_SHIELD >>> " + effects[Terraformer.EFFECT_SHIELD] + " [" + GetMapObjectType() + "]");
    }
  }

  protected virtual void CheckUpdateEffects()
  {
    updateEffectsTimer += Time.deltaTime;
    if (updateEffectsTimer >= UPDATE_EFFECTS_DELAY)
    {
      updateEffectsTimer = 0f;
      UpdateEffects();
    }
  }

  protected virtual void OnUpdatedEffects()
  {

  }
}
