﻿using System.Collections.Generic;
using UnityEngine;
using static MapObject;

public interface IHero : AI__MoveTarget
{
  Transform GetTransform();
  void SetPosition(Vector3 position);
  MapObject GetMapObject();
  bool IsDead();
  bool IsAvailableForAttack();
  void On();
  void Off();
  void NeedOn();
  void NeedOff();
  EMapObjectType GetHeroType();
  EMapObjectType GetMapObjectType();
  void NeedDeactivate();
  void Connect();
  void Disconnect();
  GameObject GetGameObject();
}
