﻿using System;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class LevelDialogMessage
{
  public int Id = 0;
  public int AuthorId = 0;

  public LevelDialogMessage(int id, int authorId)
  {
    this.Id = id;
    this.AuthorId = authorId;
  }

  public string GetTextCode(int levelId, int dialogId)
  {
    return String.Format("LEVEL_{0}_DIALOG_{1}_MESSAGE_{2}", levelId, dialogId, Id);
  }

  public override string ToString()
  {
    string temp = "";
    temp += "\n *****MESSAGE [" + Id + "]*****";
    temp += "\n AuthorId=" + AuthorId;
    return temp;
  }
}
