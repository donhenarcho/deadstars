﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotating : MonoBehaviour
{
  public float Speed = 20f;
  public RotateTypeEnum RotateType;
  private bool isOn = false;

  void Update()
  {
    if (!isOn)
      return;

    if (RotateType == RotateTypeEnum.x)
      transform.Rotate(Speed * Time.deltaTime, 0, 0);
    else if (RotateType == RotateTypeEnum.y)
      transform.Rotate(0, Speed * Time.deltaTime, 0);
    else if (RotateType == RotateTypeEnum.z)
      transform.Rotate(0, 0, Speed * Time.deltaTime);
  }

  public void On()
  {
    isOn = true;
  }

  public void Off()
  {
    isOn = false;
  }

  public void Switch(bool value)
  {
    isOn = value;
  }
}
