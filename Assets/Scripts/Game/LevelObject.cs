﻿using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class LevelObject
{
  [XmlAttribute("type")]
  public int Type = 0;
  [XmlAttribute("posX")]
  public float PosX = 0;
  [XmlAttribute("posY")]
  public float PosY = 0;
  [XmlAttribute("posZ")]
  public float PosZ = 0;
  [XmlAttribute("angle")]
  public float Angle = 0;
  [XmlAttribute("behaviorId")]
  public int BehaviorId = 0;

  public Vector3 GetPosition()
  {
    return new Vector3(PosX, PosY, PosZ);
  }

  public override string ToString()
  {
    string temp = "";
    temp += "\n *****OBJECT [" + Type + "]*****";
    temp += "\n PosX=" + PosX;
    temp += "\n PosY=" + PosY;
    temp += "\n PosZ=" + PosZ;
    temp += "\n Angle=" + Angle;
    temp += "\n BehaviorId=" + BehaviorId;
    return temp;
  }
}
