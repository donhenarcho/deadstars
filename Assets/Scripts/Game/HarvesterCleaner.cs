﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarvesterCleaner : MonoBehaviour
{
  public AI__Harvester harvester;
  private bool isLastFullState = false;
  private float fullStateTimer = 0f;

  void Start()
  {
    transform.gameObject.layer = Const.LAYER_FUEL_CLEANER;
  }

  void Update()
  {
  }

  void LateUpdate()
  {
    fullStateTimer += Time.deltaTime;
    if (fullStateTimer <= 1f)
      return;

    fullStateTimer = 0f;

    if (!harvester.IsFull() && isLastFullState)
    {
      isLastFullState = false;
      GetComponent<BoxCollider>().enabled = true;
      return;
    }
  }

  private void OnParticleCollision(GameObject other)
  {
    //Debug.Log("HarvesterCleaner.OnParticleCollision()");
    if (other.tag.Equals(Const.TAG_FUEL))
    {
      if (harvester.IsFull() && !isLastFullState)
      {
        isLastFullState = true;
        GetComponent<BoxCollider>().enabled = false;
        return;
      }

      ParticleSystem particleSystem = other.GetComponent<ParticleSystem>();
      if (particleSystem != null)
      {
        ParticleSystem.Particle[] m_Particles;
        m_Particles = new ParticleSystem.Particle[particleSystem.main.maxParticles];

        List<ParticleCollisionEvent> events;
        events = new List<ParticleCollisionEvent>();

        ParticlePhysicsExtensions.GetCollisionEvents(other.GetComponent<ParticleSystem>(), gameObject, events);

        foreach (ParticleCollisionEvent coll in events)
        {
          if (coll.intersection != Vector3.zero)
          {
            int numParticlesAlive = particleSystem.GetParticles(m_Particles);

            // Check only the particles that are alive
            for (int i = 0; i < numParticlesAlive; i++)
            {

              //If the collision was close enough to the particle position, destroy it
              if (Vector3.Magnitude(m_Particles[i].position - coll.intersection) < 1f)
              {
                m_Particles[i].remainingLifetime = -1; //Kills the particle
                particleSystem.SetParticles(m_Particles); // Update particle system
                harvester.AddFuelFromCleaner();
                break;
              }
            }
          }
        }
      }
      return;
    }
  }
}
