﻿using System;
using System.Collections;
using UnityEngine;

public class EnemyPart : MonoBehaviour, IEnemyPart
{
  public static int ENEMY_PART_STATE_100 = 0;
  public static int ENEMY_PART_STATE_80 = 1;
  public static int ENEMY_PART_STATE_60 = 2;
  public static int ENEMY_PART_STATE_40 = 3;
  public static int ENEMY_PART_STATE_20 = 4;
  public static int ENEMY_PART_STATE_0 = 5;

  public EnemyPartTagEnum Tag;

  private GameObject part;
  private Color partDefaultColor;
  private Renderer[] renderers;

  private IEnemy enemyController;

  protected int state = 0;
  protected int prevState = 0;
  protected int numStates = 0;
  protected Transform currentTransform = null;

  protected bool isInit = false;

  public EnemyPartTagEnum GetTag()
  {
    return Tag;
  }

  void Awake()
  {
    renderers = GetComponentsInChildren<Renderer>();
    partDefaultColor = renderers[0].material.color;

    this.gameObject.layer = Const.LAYER_AFRAID_OF_FIRE;
  }

  void Update()
  {

  }

  private void LateUpdate()
  {
    if (!isInit)
    {
      isInit = true;
      numStates = transform.childCount;
      SetState(ENEMY_PART_STATE_100);
    }
  }

  private void OnParticleCollision(GameObject other)
  {
    if (!other.tag.Equals(Const.TAG_FIRE) 
      && !other.tag.Equals(Const.TAG_PLASMA))
      return;

    ParticleSystem particleSystem = other.GetComponent<ParticleSystem>();
    if (particleSystem != null)
    {
      int damage = particleSystem.GetSafeCollisionEventSize();
      enemyController.Damage(other, Tag, damage, 0);
      if (gameObject.activeSelf)
        StartCoroutine(blink());
    }
  }

  private IEnumerator blink()
  {
    foreach (Renderer renderer in renderers)
      renderer.material.color = Color.red;
    yield return new WaitForSeconds(0.1f);
    foreach (Renderer renderer in renderers)
      renderer.material.color = partDefaultColor;
  }

  public void SetEnemyController(IEnemy enemyController)
  {
    this.enemyController = enemyController;
  }

  public bool IsBody()
  {
    return this.Tag == EnemyPartTagEnum.Body;
  }

  public bool IsHead()
  {
    return this.Tag == EnemyPartTagEnum.Head;
  }

  public void BoxCollider(bool enabled)
  {
    GetComponent<BoxCollider>().enabled = enabled;
  }

  public void Show()
  {
    foreach (Renderer renderer in renderers)
      renderer.enabled = true;
  }

  public void Hide()
  {
    foreach (Renderer renderer in renderers)
      renderer.enabled = false;
  }

  public void SetState(int _state)
  {
    if (state == _state)
      return;

    prevState = state;
    state = _state;

    int num = 0;
    foreach (Transform obj in transform)
    {
      if (state == num)
      {
        obj.gameObject.SetActive(true);
        currentTransform = obj;
      }
      else
      {
        obj.gameObject.SetActive(false);
      }
      ++num;
    }
  }
}
