﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioObject : MonoBehaviour
{
  public AudioSource[] audioSources;
  protected bool[] paused;
  protected float[] volumes;

  protected AudioController audioController;

  void Awake()
  {
    audioController = FindObjectOfType<AudioController>();
    paused = new bool[audioSources.Length];
    foreach (AudioSource audioSource in audioSources)
    {
      audioSource.volume = audioSource.volume * audioController.GetSoundVolume();
    }
  }

  void Update()
  {
        
  }

  public bool IsPlaying(int sourceIndex)
  {
    return audioSources[sourceIndex].isPlaying;
  }

  public bool IsPaused(int sourceIndex)
  {
    return paused[sourceIndex];
  }

  public void Play(int sourceIndex)
  {
    Play(sourceIndex, false);
  }

  public void Play(int sourceIndex, bool loop)
  {
    //Debug.Log("AudioObject.Play()=" + sourceIndex);
    audioSources[sourceIndex].loop = loop;
    audioSources[sourceIndex].Play();
    paused[sourceIndex] = false;
  }

  public void Pause(int sourceIndex)
  {
    audioSources[sourceIndex].Pause();
    paused[sourceIndex] = true;
  }

  public void UnPause(int sourceIndex)
  {
    audioSources[sourceIndex].UnPause();
    paused[sourceIndex] = false;
  }

  public void Stop(int sourceIndex)
  {
    //Debug.Log("AudioObject.Stop()=" + sourceIndex);
    audioSources[sourceIndex].Stop();
  }

  public void SetPause4All(bool value)
  {
    for (int i = 0; i < audioSources.Length; i++)
    {
      if (value)
      {
        if (IsPlaying(i))
        {
          Pause(i);
        }
      }
      else
      {
        if (IsPaused(i))
        {
          UnPause(i);
        }
      }
    }
  }
}
