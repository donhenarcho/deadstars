﻿using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class ObjectValue
{
  [XmlAttribute("objectType")]
  public int ObjectType;
  [XmlAttribute("value")]
  public int Value;
}
