﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssistantHead : AnimationObject
{
  protected float speed0 = 5.0f;
  protected float speed1 = 0.5f;
  protected float timer = 0;
  protected bool isOn = false;

  void Start()
  {
    On();
  }

  void Update()
  {
    if (!isOn)
      return;

    CheckGo();
  }

  protected void CheckGo()
  {
    if (timer > 0)
    {
      timer -= Time.deltaTime;
      if (timer <= 0)
      {
        Next();
        timer = state == 0 ? speed0 : speed1;
      }
    }
    return;
  }

  public void On()
  {
    SetState(0);
    timer = speed0;
    isOn = true;
  }

  public void Off()
  {
    isOn = false;
    timer = 0;
    SetState(1);
  }
}
