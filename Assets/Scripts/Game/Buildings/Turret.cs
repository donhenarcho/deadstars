﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : Building, IShellOwner
{
  public int upgradeLevel = 1;

  public Transform ShellPoint;
  public GameObject ShellPrefab;
  private List<GameObject> shells;

  protected Cabine cabine;
  protected ParticleSystem[] fires;
  protected float WORKING_DISTANCE = 20f;
  protected float WORKING_SEARCH_ENEMIES_TIME = 0.1f;
  protected float workingSearchEnemiesTimer = 0;
  protected float searchFlyingTargetTimer = 0;

  protected float rotateSpeed = 12.0f;

  protected List<AI__Enemy> enemies;
  protected AI__Enemy currentTarget;
  protected AI__Enemy currentFlyingTarget;

  protected bool isFiringLast = false;
  protected bool isFirePlay = false;

  protected int fuelOutSpeed = 1;

  public override void EndedObjectInit()
  {
    BuildingInit();
    FUEL_OUT_FOR_WORKING_DELAY = 1.0f;
    fuelOutSpeed = variableManager.GetSpeedTurretFuelOut();
    cabine = GetComponentInChildren<Cabine>();
    fires = cabine.GetComponentsInChildren<ParticleSystem>();
    FireStop();
    shells = new List<GameObject>();

    isInit = true;
  }

  public override int Working()
  {
    if (terraformer == null)
    {
      GetTerraformer();
      return 0;
    }

    if (isPaused)
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return 0;
    }

    CheckUpdateEffects();

    CheckOnOffMarker();

    if (terraformer.IsEmpty())
    {
      if (isFirePlay)
      {
        FireStop();
        currentTarget = null;
      }
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return 0;
    }

    bool isFiring = false;

    if (currentTarget != null && currentTarget.IsAlive())
    {
      float targetDist = Vector3.Distance(transform.position, currentTarget.transform.position);
      if (targetDist > WORKING_DISTANCE)
      {
        currentTarget = null;
      }
      else
      {
        isFiring = true;
        RotateToObject(currentTarget.gameObject);
      }
    }

    if (currentTarget != null && !currentTarget.IsAlive())
    {
      currentTarget = null;
      workingSearchEnemiesTimer = WORKING_SEARCH_ENEMIES_TIME;
    }

    if (currentTarget == null)
    {
      workingSearchEnemiesTimer += Time.deltaTime;
      if (workingSearchEnemiesTimer >= WORKING_SEARCH_ENEMIES_TIME)
      {
        workingSearchEnemiesTimer = 0;
        SearchTarget();
      }
    }

    if (upgradeLevel == Const.MAX_BASE_UPGRADE_LEVEL)
    {
      if (currentFlyingTarget != null && currentFlyingTarget.IsAlive())
      {
        if (shells.Count == 0)
        {
          if (Vector3.Distance(transform.position, currentFlyingTarget.GetPosition()) > WORKING_DISTANCE * 2f)
          {
            currentFlyingTarget = null;
          }
          else
          {
            FireShell();
          }
        }
      }
      else
      {
        searchFlyingTargetTimer -= Time.deltaTime;
        if (searchFlyingTargetTimer <= 0f)
        {
          searchFlyingTargetTimer = 1f;
          SearchFlyingTarget();
        }
      }
    }

    if (isFiring && !isFiringLast)
    {
      isFiringLast = isFiring;
      FirePlay();
    } 
    else if (!isFiring && isFiringLast)
    {
      isFiringLast = isFiring;
      FireStop();
    }

    if (isFiring)
    {
      workingFuelTimer += Time.deltaTime;
      if (workingFuelTimer >= FUEL_OUT_FOR_WORKING_DELAY)
      {
        workingFuelTimer = 0;
        if (terraformer.IsEmpty())
        {
          FireStop();
          currentTarget = null;
        }
        terraformer.FuelOut(fuelOutSpeed);
        AfterWorking();
      }

      if (!audioObject.IsPlaying(0))
        audioObject.Play(0, true);
    }
    else
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
    }

    return 1;
  }

  public void OnTerraformerDying()
  {
    die();
  }

  protected void SearchTarget()
  {
    enemies = GetBesideEnemies(false);
    if (enemies.Count > 0)
    {
      foreach (AI__Enemy enemy in enemies)
      {
        if (enemy.IsAlive())
        {
          currentTarget = enemy;
          break;
        }
      }
    }
  }

  protected void SearchFlyingTarget()
  {
    enemies = GetBesideEnemies(true);
    if (enemies.Count > 0)
    {
      foreach (AI__Enemy enemy in enemies)
      {
        if (enemy.IsAlive())
        {
          currentFlyingTarget = enemy;
          break;
        }
      }
    }
  }

  protected List<AI__Enemy> GetBesideEnemies(bool isFlying)
  {
    List<AI__Enemy> objects = new List<AI__Enemy>();
    Vector3 point = transform.position;
    if (isFlying)
    {
      point.y += Const.FLYHEIGHT;
    }

    Collider[] hitColliders = Physics.OverlapSphere(point, WORKING_DISTANCE);
    foreach (var hitCollider in hitColliders)
    {
      AI__Enemy enemy = hitCollider.GetComponent<AI__Enemy>();
      if (enemy != null)
      {
        if (enemy.IsFlyingObject() == isFlying)
          objects.Add(enemy);
      }
    }
    return objects;
  }

  protected void RotateTo(
      Vector3 _direction
    )
  {
    Quaternion direction = Quaternion.LookRotation(_direction);
    cabine.transform.rotation = Quaternion.Lerp(
      cabine.transform.rotation,
      direction,
      rotateSpeed * Time.deltaTime);
  }

  protected void RotateToObject(
      GameObject _object
    )
  {
    Quaternion direction = Quaternion.LookRotation(_object.transform.position - cabine.transform.position);
    cabine.transform.rotation = Quaternion.Lerp(
      cabine.transform.rotation,
      direction,
      rotateSpeed * Time.deltaTime);
  }

  protected void FirePlay()
  {
    foreach (ParticleSystem fire in fires)
      fire.Play();
    isFirePlay = true;
  }

  protected void FirePause()
  {
    foreach (ParticleSystem fire in fires)
      fire.Pause();
  }

  protected void FireStop()
  {
    foreach (ParticleSystem fire in fires)
      fire.Stop();
    isFirePlay = false;
  }

  public int GetUpgradeLevel()
  {
    return upgradeLevel;
  }

  public override float GetCost(EMapObjectType enemyType)
  {
    return variableManager.GetTargetCost(GetMapObjectType(), enemyType);
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);

    if (value)
    {
      FirePause();
    }
    else if (isFirePlay)
    {
      FirePlay();
    }

    foreach (GameObject gameObject in shells)
    {
      if (gameObject != null)
        gameObject.GetComponent<TurretShell>().SetPause(value);
    }
  }

  public void OnShellDead(GameObject shell)
  {
    shells.Remove(shell);
  }

  private void FireShell()
  {
    GameObject gameObject = Instantiate(ShellPrefab, ShellPoint.position, Quaternion.identity);
    gameObject.GetComponent<TurretShell>().Shot(currentFlyingTarget, this);
    shells.Add(gameObject);

    terraformer.FuelOut(5);
  }
}
