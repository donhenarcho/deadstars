﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Building : MapObject, IBuilding, AI__AttackTarget
{
  public Transform Main;
  public ParticleSystem bricks;
  
  protected Shield shield;

  protected float DEAD_DURATION = 10f;

  protected float FUEL_OUT_FOR_WORKING_DELAY = 0f;
  protected float workingFuelTimer = 0;
  protected float checkOnOffMarkerTimer = 0;

  protected OnOffMarker onOffMarker;

  protected Terraformer terraformer;

  protected float deadTimer = 0;

  protected FuelMarker fuelMarker;
  protected int FUEL_MAX = 0;
  protected int fuel = 0;
  protected float fuelTimer = 0;

  protected void BuildingInit()
  {
    Init();
    onOffMarker = GetComponentInChildren<OnOffMarker>();
    shield = GetComponentInChildren<Shield>();

    transform.gameObject.layer = Const.LAYER_BUILDING;
    FUEL_MAX = variableManager.GetHP(GetMapObjectType());
  }

  protected void GetTerraformer()
  {
    Collider[] hitColliders = Physics.OverlapSphere(transform.position, 30, Const.LAYER_MASK_BUILDING_ONLY);
    foreach (var hitCollider in hitColliders)
    {
      Terraformer terraformer = hitCollider.GetComponent<Terraformer>();
      if (terraformer != null)
      {
        this.terraformer = terraformer;
        break;
      }
    }

    if (this.terraformer == null)
      Debug.LogError("No or more than one terraformer nearby!");
  }

  void Update()
  {
    if (!isReady)
      return;

    if (CheckDead())
      return;

    Working();
  }

  public virtual int Working()
  {
    if (terraformer == null)
    {
      GetTerraformer();
    }

    CheckOnOffMarker();

    if (terraformer != null)
    {
      workingFuelTimer += Time.deltaTime;
      if (workingFuelTimer >= FUEL_OUT_FOR_WORKING_DELAY)
      {
        workingFuelTimer = 0;
        terraformer.FuelOut(1);
        AfterWorking();
        return 1;
      }
    }
    return 0;
  }

  public virtual void AfterWorking()
  {
  }

  protected void CheckOnOffMarker()
  {
    checkOnOffMarkerTimer += Time.deltaTime;
    if (checkOnOffMarkerTimer >= 1f)
    {
      checkOnOffMarkerTimer = 0;
      RepaintOnOffMarker();
    }
  }

  protected void RepaintOnOffMarker()
  {
    if (terraformer == null 
      || onOffMarker == null)
      return;

    if (terraformer.IsEmpty())
      onOffMarker.Off();
    else
      onOffMarker.On();
  }

  public virtual float GetCost(EMapObjectType enemyType)
  {
    return variableManager.GetTargetCost(GetMapObjectType(), enemyType);
  }

  public bool IsAvailableForAttack()
  {
    return !IsDead();
  }

  protected virtual void beforeDie()
  {

  }

  protected virtual void die()
  {
    beforeDie();
    isDead = true;
    SetEnabledForAllColliders(false);
    SetEnabledForRenderers(Main, false);
    bricks.Play();
    deadTimer = DEAD_DURATION;
    afterDie();
  }

  protected virtual void afterDie()
  {

  }

  protected virtual bool CheckDead()
  {
    if (deadTimer > 0)
    {
      deadTimer -= Time.deltaTime;
      if (deadTimer <= 0)
      {
        deadTimer = 0;
        IAmDead();
        return false;
      }
      return true;
    }
    return false;
  }

  public virtual void Attack(Vector3 enemyPosition, int damage)
  {
    if (terraformer == null || terraformer.IsEmpty())
    {
      die();
    }
    else
    {
      terraformer.FuelOut(damage);
      shield.Play(enemyPosition, GetObjectRadius());
    }
  }

  private void OnParticleCollision(GameObject other)
  {
    if (IsDead())
      return;

    Attack(other.transform.position, GetDamage4Units(other));
  }

  public override bool IsTarget()
  {
    return true;
  }

  public override void Reset()
  {
    base.Reset();
    SetEnabledForAllColliders(true);
    SetEnabledForRenderers(Main, true);
  }
}
