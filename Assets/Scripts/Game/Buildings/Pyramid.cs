﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pyramid : MapObject, AI__AttackTarget
{
  public Transform Main;
  public ParticleSystem bricks;

  protected float DEAD_DURATION = 10f;
  protected float deadTimer = 0;

  private int hp = 0;

  protected Renderer[] renderers;
  private Color partDefaultColor;

  public override void EndedObjectInit()
  {
    Init();
    Main.gameObject.layer = Const.LAYER_INVISIBLE_OBJECT;
    renderers = GetComponentsInChildren<Renderer>();
    partDefaultColor = renderers[0].material.color;
    transform.gameObject.layer = Const.LAYER_FOUND_OBJECT;
    hp = variableManager.GetHP(GetMapObjectType());
  }

  void Update()
  {
    if (!isReady)
      return;

    if (CheckDead())
      return;
  }

  public override void OnFound()
  {
  }

  protected virtual void die()
  {
    isDead = true;
    SetEnabledForAllColliders(false);
    SetEnabledForRenderers(Main, false);
    bricks.Play();
    IAmWaitingDeath();
    deadTimer = DEAD_DURATION;
  }

  protected virtual bool CheckDead()
  {
    if (deadTimer > 0)
    {
      deadTimer -= Time.deltaTime;
      if (deadTimer <= 0)
      {
        deadTimer = 0;
        IAmDead();
        return false;
      }
      return true;
    }
    return false;
  }

  private void OnParticleCollision(GameObject other)
  {
    if (IsDead())
      return;

    Attack(Vector3.zero, GetDamage4Units(other));
  }

  private IEnumerator blink()
  {
    foreach (Renderer renderer in renderers)
      renderer.material.color = Color.red;
    yield return new WaitForSeconds(0.1f);
    foreach (Renderer renderer in renderers)
      renderer.material.color = partDefaultColor;
  }

  public float GetCost(EMapObjectType enemyType)
  {
    return variableManager.GetTargetCost(GetMapObjectType(), enemyType);
  }

  public bool IsAvailableForAttack()
  {
    return true;
  }

  public void Attack(Vector3 enemyPosition, int damage)
  {
    Debug.Log("hp >>> " + hp);
    hp -= damage;
    if (hp > 0)
    {
      StartCoroutine(blink());
    }
    else
    {
      die();
    }
  }

  public int GetHP()
  {
    return hp;
  }
}
