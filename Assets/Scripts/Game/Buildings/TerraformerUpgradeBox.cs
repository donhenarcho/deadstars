﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerraformerUpgradeBox : MonoBehaviour
{
  public Transform Levels;

  public void SetLevel(int value)
  {
    gameObject.SetActive(value > 0);

    int i = 0;
    foreach(Transform Level in Levels)
    {
      Level.gameObject.SetActive(i < value);
      i++;
    }
  }
}
