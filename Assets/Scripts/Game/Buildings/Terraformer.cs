﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Terraformer : Building, IHaveFuel
{
  public static int EFFECT_SHIELD = 101;
  public static int EFFECT_WEAPON = 102;

  public Transform pointsForTurretsWrapper;
  public Transform pointsForContainersWrapper;

  protected Vector3[] pointsForTurrets;
  protected Vector3[] pointsForContainers;

  protected ParticleSystem smoke;
  protected const int TERRAFORMING_SCORE_BY_ONE_WORK = 1;
  protected int terraformingScore = 0;

  public override void EndedObjectInit()
  {
    //Debug.Log("Terraformer.TerraformerInit()");

    FUEL_OUT_FOR_WORKING_DELAY = 10f;
    BuildingInit();

    Main.gameObject.layer = Const.LAYER_INVISIBLE_OBJECT;

    smoke = Main.GetComponentInChildren<ParticleSystem>();

    fuelMarker = GetComponentInChildren<FuelMarker>();

    FUEL_OUT_FOR_WORKING_DELAY = 10f;

    pointsForTurrets = new Vector3[8];
    int index = 0;
    foreach (Transform point in pointsForTurretsWrapper)
    {
      pointsForTurrets[index] = point.position;
      index++;
    }

    pointsForContainers = new Vector3[4];
    index = 0;
    foreach (Transform point in pointsForContainersWrapper)
    {
      pointsForContainers[index] = point.position;
      index++;
    }

    BuildingInit();

    fuel = (int) (FUEL_MAX * 0.1f);

    repaintFuelMarker();

    isInit = true;
  }

  void Update()
  {
    if (levelController == null 
      && FUEL_MAX == 0)
    {
      FUEL_MAX = 10000;
      fuel = FUEL_MAX;
      fuelMarker = GetComponentInChildren<FuelMarker>();
      repaintFuelMarker();
      return;
    }

    if (!isReady || !isInit)
      return;

    if (CheckDead())
      return;

    if (isDead)
      return;

    Working();
    BaseManagerUpdate();
  }

  public override int Working()
  {
    //Debug.Log("Storage::Working()");
    if (fuel <= 0)
    {
      fuel = 0;
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      if (!smoke.isStopped)
        smoke.Stop();
      return 0;
    }

    if (isPaused)
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return 0;
    }

    CheckUpdateEffects();

    if (!audioObject.IsPlaying(0))
      audioObject.Play(0, true);

    workingFuelTimer += Time.deltaTime;
    if (workingFuelTimer >= FUEL_OUT_FOR_WORKING_DELAY)
    {
      workingFuelTimer = 0;
      fuel = fuel > 0 ? fuel - 1 : 0;
      AfterWorking();
      return 1;
    }
    return 0;
  }

  public override void AfterWorking()
  {
    if (isDead)
      return;

    if (IsEmpty())
    {
      smoke.Stop();
    }
    else
    {
      if (smoke.isStopped)
        smoke.Play();

      terraformingScore += TERRAFORMING_SCORE_BY_ONE_WORK;
    }

    repaintFuelMarker();
  }

  public int FuelIn(int value)
  {
    if (IsFull() || value == 0)
      return value;

    //Debug.Log("Storage::FuelIn() + " + value);

    fuel += value;

    int fuelReturn = 0;
    if (fuel > FUEL_MAX)
    {
      fuelReturn = fuel - FUEL_MAX;
      fuel = FUEL_MAX;
    }

    AfterFuelIn();

    return fuelReturn;
  }

  public int FuelReturn(int value)
  {
    if (IsFull() || value == 0)
      return value;

    //Debug.Log("Storage::FuelReturn() + " + value);

    fuel += value;

    int fuelReturn = 0;
    if (fuel > FUEL_MAX)
    {
      fuelReturn = fuel - FUEL_MAX;
      fuel = FUEL_MAX;
    }

    AfterFuelIn();

    return fuelReturn;
  }

  public int FuelOut()
  {
    //Debug.Log("Storage::FuelOut()");
    if (fuel <= 0)
    {
      fuel = 0;
      return 0;
    }

    fuelTimer += Time.deltaTime;
    if (fuelTimer >= 1)
    {
      fuelTimer = 0;
      fuel = fuel > 0 ? fuel - 1 : 0;
      AfterFuelOut();
      return 1;
    }
    return 0;
  }

  public int FuelOut(int value)
  {
    //Debug.Log("Storage::FuelOut(int value)");
    if (fuel <= 0)
    {
      fuel = 0;
      return 0;
    }

    fuel = fuel > 0 ? fuel - value : 0;
    if (fuel < 0)
    {
      value += fuel;
      fuel = 0;
    }

    AfterFuelOut();
    return value;
  }

  public void FillToMax()
  {
    fuel = FUEL_MAX;
  }

  public bool IsFull()
  {
    return fuel == FUEL_MAX;
  }

  public bool IsEmpty()
  {
    return fuel == 0;
  }

  public float GetFuelLeft()
  {
    return fuel / (FUEL_MAX / 100f);
  }

  public float GetCurrentFuelValue()
  {
    return fuel;
  }

  protected void repaintFuelMarker()
  {
    if (fuelMarker == null)
      return;

    fuelMarker.Repaint(fuel / (float)FUEL_MAX);
    fuelMarker.SetBlinking(IsEmpty());
  }

  public void AfterFuelIn()
  {
    repaintFuelMarker();
  }

  public void AfterFuelOut()
  {
    repaintFuelMarker();
  }

  public int PickUpTerraformingScore()
  {
    int result = terraformingScore;
    terraformingScore = 0;
    return result;
  }

  public override SavedWorldObject GetSavedWorldObject()
  {
    SavedWorldObject savedLevelObject = base.GetSavedWorldObject();
    savedLevelObject.fuel = this.fuel;
    return savedLevelObject;
  }

  public override SavedWorldObject SetSavedWorldObject(SavedWorldObject savedLevelObject)
  {
    savedLevelObject = base.SetSavedWorldObject(savedLevelObject);
    if (savedLevelObject.fuel >= 0)
      this.fuel = savedLevelObject.fuel;
    return savedLevelObject;
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    if (value)
    {
      smoke.Pause();
    }
    else if (!IsEmpty())
    {
      smoke.Play();
    }
  }

  //***************************************************************
  //***************************************************************

  public const float HARVESTER_TO_FUEL_STORAGE_DISTANCE = 20.0f;
  public const float FOR_FRIENDLY_OBJECTS_DISTANCE = 50f;
  public const float ENEMY_DETECT_DISTANCE = 50f;
  public const float MIN_DISTANCE_BETWEEN_TWO_TERRAFORMERS = 100f;

  public TerraformerUpgradeBox UpgradeBoxShield;
  public TerraformerUpgradeBox UpgradeBoxWeapon;

  private List<AI__Harvester> harvesters = new List<AI__Harvester>();
  private List<Turret> turrets = new List<Turret>();
  private List<FuelSpawnPoint> fuelSpawnPoints = new List<FuelSpawnPoint>();
  private Dictionary<FuelSpawnPoint, List<AI__Harvester>> fuelSpawnPointWithHarvesters = new Dictionary<FuelSpawnPoint, List<AI__Harvester>>();
  private Dictionary<AI__Harvester, FuelSpawnPoint> harvesterWithFuelSpawnPoint = new Dictionary<AI__Harvester, FuelSpawnPoint>();

  private float pingHarvestersTimer = 0;
  private float fuelOutObjectsNearbyTimer = 0;
  private int currentTurretUpgradeLevel = 0;
  private int currentShieldUpgradeLevel = 0;
  private int currentWeaponUpgradeLevel = 0;

  private void BaseManagerInit()
  {
    UpdateUpgradeBoxes();
    StartCoroutine(SearchFuelSpawnPoints());
  }

  private void UpdateUpgradeBoxes()
  {
    UpgradeBoxShield.SetLevel(currentShieldUpgradeLevel);
    UpgradeBoxWeapon.SetLevel(currentWeaponUpgradeLevel);
  }

  public void BaseManagerUpdate()
  {
    if (isPaused)
      return;
    
    pingHarvestersTimer += Time.deltaTime;
    if (pingHarvestersTimer >= 1f)
    {
      pingHarvestersTimer = 0;
      PingHarvesters();
    }

    CheckFuelOutObjectsNearby();
  }

  public void AddObject(MapObject mapObject)
  {
    switch (mapObject.Type)
    {
      case EMapObjectType.TYPE_HARVESTER:
        harvesters.Add(mapObject.GetComponent<AI__Harvester>());
        break;
      case EMapObjectType.TYPE_TURRET:
        turrets.Add(mapObject.GetComponent<Turret>());
        break;
      case EMapObjectType.TYPE_TERRAFORMER_UPGRADE_SHIELD:
        currentShieldUpgradeLevel++;
        UpdateUpgradeBoxes();
        break;
      case EMapObjectType.TYPE_TERRAFORMER_UPGRADE_WEAPON:
        currentWeaponUpgradeLevel++;
        UpdateUpgradeBoxes();
        break;
    }

    currentTurretUpgradeLevel = 0;
    if (turrets.Count > 0)
    {
      currentTurretUpgradeLevel = turrets[0].upgradeLevel;
    }

    mapObject.SetOwnerId(GetId());
  }

  public void AddObject(MapObject.EMapObjectType objectType)
  {
    switch (objectType)
    {
      case EMapObjectType.TYPE_TERRAFORMER_UPGRADE_SHIELD:
        currentShieldUpgradeLevel++;
        UpdateUpgradeBoxes();
        break;
      case EMapObjectType.TYPE_TERRAFORMER_UPGRADE_WEAPON:
        currentWeaponUpgradeLevel++;
        UpdateUpgradeBoxes();
        break;
    }
  }

  public void RemoveObject(MapObject mapObject)
  {
    switch (mapObject.Type)
    {
      case EMapObjectType.TYPE_HARVESTER:
        harvesters.Remove(mapObject.GetComponent<AI__Harvester>());
        //Debug.Log("Remove harvester >>> " + harvesters.Count);
        break;
      case EMapObjectType.TYPE_TURRET:
        turrets.Remove(mapObject.GetComponent<Turret>());
        //Debug.Log("Remove turrets >>> " + turrets.Count);
        break;
    }

    currentTurretUpgradeLevel = 0;
    if (turrets.Count > 0)
    {
      currentTurretUpgradeLevel = turrets[0].upgradeLevel;
    }
  }

  private IEnumerator SearchFuelSpawnPoints()
  {
    yield return new WaitForSeconds(2f);

    fuelSpawnPoints.Clear();
    Collider[] hitColliders = Physics.OverlapSphere(transform.position, 200f, Const.LAYER_MASK_FUEL_SPAWN_POINT_ONLY);
    foreach (var hitCollider in hitColliders)
    {
      FuelSpawnPoint point = hitCollider.GetComponent<FuelSpawnPoint>();
      if (point != null)
      {
        point.SetDistanceToBase(Vector3.Distance(transform.position, point.GetPosition()));
        fuelSpawnPoints.Add(point);
      }
    }

    //Debug.Log("SearchFuelSpawnPoints(2)=" + fuelSpawnPoints.Count);
  }

  private FuelSpawnPoint GetBestPoint(AI__Harvester harvester)
  {
    Dictionary<FuelSpawnPoint, float> fuelSpawnPointWithRating = new Dictionary<FuelSpawnPoint, float>();

    foreach (FuelSpawnPoint fuelSpawnPoint in fuelSpawnPoints)
    {
      fuelSpawnPointWithRating.Add(fuelSpawnPoint, 0f);
      fuelSpawnPointWithRating[fuelSpawnPoint] -= Vector3.Distance(fuelSpawnPoint.GetPosition(), harvester.GetPosition());
      fuelSpawnPointWithRating[fuelSpawnPoint] += fuelSpawnPoint.GetFuelLeft();
      if (fuelSpawnPointWithHarvesters.ContainsKey(fuelSpawnPoint))
        fuelSpawnPointWithRating[fuelSpawnPoint] -= fuelSpawnPointWithHarvesters[fuelSpawnPoint].Count * 200f;

    }

    FuelSpawnPoint bestFuelSpawnPoint = null;

    foreach (KeyValuePair<FuelSpawnPoint, float> candidate in fuelSpawnPointWithRating)
    {
      //Debug.Log("point=" + candidate.Key.GetPosition() + " [" + candidate.Value + "]");
      if (bestFuelSpawnPoint == null || candidate.Value > fuelSpawnPointWithRating[bestFuelSpawnPoint])
        bestFuelSpawnPoint = candidate.Key;
    }

    return bestFuelSpawnPoint;
  }

  private void PingHarvesters()
  {
    //Debug.Log("PingHarvesters()");
    if (fuelSpawnPoints.Count == 0)
    {
      Debug.LogError("No fuel fields found near the terraformer!");
      return;
    }

    foreach (AI__Harvester harvester in harvesters)
    {
      if (harvester.GetFuelLeft() >= 25f
        && Vector3.Distance(harvester.GetPosition(), GetPosition()) <= HARVESTER_TO_FUEL_STORAGE_DISTANCE)
      {
        //Debug.Log("Harvester.FuelOut()1");
        if (!IsFull())
        {
          //Debug.Log("Harvester.FuelOut()2");
          harvester.FuelIn(FuelIn(harvester.FuelOut(variableManager.GetSpeedHarvesterFuelOut())));
        }
      }
      else if ((harvester.IsFull() || harvester.GetFuelLeft() <= 10f) && !harvester.IsPathEnded())
      {
        //Debug.Log("Harvester.ResetPath()");
        harvester.ResetPath();
        harvester.SetMoveTarget(this, 8f);
      }
      else if ((harvester.IsFull() || harvester.GetFuelLeft() <= 10f) && !harvester.HaveMoveTarget())
      {
        //Debug.Log("Harvester.SetMoveTarget() BACK");
        harvester.SetMoveTarget(this, 8f);
      }
      else if (!harvester.IsFull() && harvester.IsPathEnded() && !harvester.HaveMoveTarget())
      {
        FuelSpawnPoint fuelSpawnPoint = GetBestPoint(harvester);
        //Debug.Log("Harvester.SetFuelSpawnPoint() = " + fuelSpawnPoint);
        updateHarvesterWithFuelSpawnPoint(harvester, fuelSpawnPoint);
        if (fuelSpawnPoint != null)
        {
          //Debug.Log("fuelSpawnPoint=" + fuelSpawnPoint.GetPosition());
          harvester.SetPath(Utils.GetPathRandom(
          harvesterWithFuelSpawnPoint[harvester].GetPosition(),
          harvesterWithFuelSpawnPoint[harvester].GetAreaSize().x / 2f, 5f));
        }
      }
    }
  }

  private void updateHarvesterWithFuelSpawnPoint(AI__Harvester harvester, FuelSpawnPoint fuelSpawnPoint)
  {
    if (harvesterWithFuelSpawnPoint.ContainsKey(harvester))
    {
      if (fuelSpawnPointWithHarvesters[harvesterWithFuelSpawnPoint[harvester]].Contains(harvester))
      {
        fuelSpawnPointWithHarvesters[harvesterWithFuelSpawnPoint[harvester]].Remove(harvester);
      }
    }
    harvesterWithFuelSpawnPoint.Remove(harvester);

    if (fuelSpawnPoint == null)
      return;

    if (harvesterWithFuelSpawnPoint.ContainsKey(harvester))
    {
      harvesterWithFuelSpawnPoint[harvester] = fuelSpawnPoint;
    }
    else
    {
      harvesterWithFuelSpawnPoint.Add(harvester, fuelSpawnPoint);
    }

    if (!fuelSpawnPointWithHarvesters.ContainsKey(fuelSpawnPoint))
    {
      fuelSpawnPointWithHarvesters.Add(fuelSpawnPoint, new List<AI__Harvester>());
      fuelSpawnPointWithHarvesters[fuelSpawnPoint].Add(harvester);
    }
    else if (!fuelSpawnPointWithHarvesters[fuelSpawnPoint].Contains(harvester))
    {
      fuelSpawnPointWithHarvesters[fuelSpawnPoint].Add(harvester);
    }
  }

  public Terraformer GetTerraformer()
  {
    return this;
  }

  public int GetHarvestersCount()
  {
    return harvesters.Count;
  }

  public void DestroyAllTurrets()
  {
    List<Turret> temp = new List<Turret>();
    temp.AddRange(turrets);
    foreach (Turret turret in temp)
    {
      turret.Kill();
    }
    temp.Clear();
    turrets.Clear();
  }

  public int GetCurrentTurretUpgradeLevel()
  {
    return currentTurretUpgradeLevel;
  }

  public int GetCurrentTurretUpgradeLevelForUpdate()
  {
    if ((currentTurretUpgradeLevel == 1 && turrets.Count < 4)
      || (currentTurretUpgradeLevel > 1 && turrets.Count < 8))
      return currentTurretUpgradeLevel - 1;
    return currentTurretUpgradeLevel;
  }

  public int GetCurrentShieldUpgradeLevel()
  {
    return currentShieldUpgradeLevel;
  }

  public int GetCurrentWeaponUpgradeLevel()
  {
    return currentWeaponUpgradeLevel;
  }

  protected List<IHaveFuel> GetHaveFuelObjects()
  {
    List<IHaveFuel> objects = new List<IHaveFuel>();
    Collider[] hitColliders = Physics.OverlapSphere(transform.position, FOR_FRIENDLY_OBJECTS_DISTANCE);
    foreach (var hitCollider in hitColliders)
    {
      IHaveFuel iHaveFuel = hitCollider.GetComponent<IHaveFuel>();
      if (iHaveFuel != null)
      {
        if (iHaveFuel.GetMapObjectType() != EMapObjectType.TYPE_HARVESTER
          || iHaveFuel.GetFuelLeft() < 20f)
        {
          //Debug.Log("---->" + iHaveFuel.GetPosition() + "/" + iHaveFuel.GetGameObject());
          objects.Add(iHaveFuel);
        }
      }
    }

    //Debug.Log("[IHaveFuel] objects.Count=" + objects.Count);

    return objects;
  }

  protected void CheckFuelOutObjectsNearby()
  {
    if (GetFuelLeft() <= 10f)
      return;

    fuelOutObjectsNearbyTimer += Time.deltaTime;
    if (fuelOutObjectsNearbyTimer >= 1)
    {
      fuelOutObjectsNearbyTimer = 0;
      FuelOutObjectsNearby();
    }
  }

  protected void FuelOutObjectsNearby()
  {
    List<IHaveFuel> objects = GetHaveFuelObjects();
    foreach (IHaveFuel iHaveFuel in objects)
    {
      if (!iHaveFuel.IsFull())
      {
        FuelReturn(iHaveFuel.FuelIn(FuelOut(10)));
      }
    }
  }

  public List<AI__Enemy> GetEnemyBeside()
  {
    List<AI__Enemy> objects = new List<AI__Enemy>();
    Collider[] hitColliders = Physics.OverlapSphere(transform.position, ENEMY_DETECT_DISTANCE);
    foreach (var hitCollider in hitColliders)
    {
      AI__Enemy enemy = hitCollider.GetComponent<AI__Enemy>();
      if (enemy != null)
      {
        objects.Add(enemy);
      }
    }
    return objects;
  }

  public bool IsHero()
  {
    return false;
  }

  public Vector3 GetPositionForMiniSpacetruck()
  {
    return GetPositionForMiniSpacetruck(transform.position);
  }

  public static Vector3 GetPositionForMiniSpacetruck(Vector3 position)
  {
    Vector3 point = Vector3.zero;
    for (int i = 0; i < 4; i++)
    {
      point = GetPositionForMiniSpacetruck(position, 12f, i);
      if (IsFreePoint(point, 6f))
        return point;
    }

    return Vector3.zero;
  }

  public static Vector3 GetPositionForMiniSpacetruck(Vector3 position, float distance, int num)
  {
    Vector3 point = position;

    switch (num)
    {
      case 0:
        point.x += -distance;
        point.z += distance;
        break;
      case 1:
        point.x += distance;
        point.z += distance;
        break;
      case 2:
        point.x += -distance;
        point.z += -distance;
        break;
      case 3:
        point.x += distance;
        point.z += -distance;
        break;
    }

    return point;
  }

  public static bool IsFreePoint(Vector3 point, float radius)
  {
    bool result = true;

    Collider[] hitColliders = Physics.OverlapSphere(point, radius);
    foreach (var hitCollider in hitColliders)
    {
      MapObject mapObject = hitCollider.GetComponent<MapObject>();
      if (mapObject != null)
        result = false;
    }

    return result;
  }

  public Vector3 GetPositionForTurretByBase(int num)
  {
    return GetPositionForTurretByBase(transform.position, num);
  }

  public static Vector3 GetPositionForTurretByBase(Vector3 position, int num)
  {
    float distance = 20f;

    Vector3 point = position;

    switch (num)
    {
      case 0:
        point.x += -distance;
        break;
      case 1:
        point.x += distance;
        break;
      case 2:
        point.z += -distance;
        break;
      case 3:
        point.z += distance;
        break;
      case 4:
        point.x += -distance;
        point.z += -distance;
        break;
      case 5:
        point.x += distance;
        point.z += distance;
        break;
      case 6:
        point.x += distance;
        point.z += -distance;
        break;
      case 7:
        point.x += -distance;
        point.z += distance;
        break;
    }

    return point;
  }

  public override void Attack(Vector3 enemyPosition, int damage)
  {
    //Debug.Log("Building.Attack()=" + damage);
    if (IsEmpty())
    {
      die();
      //Debug.Log("Building.Attack() >>> die()");
    }
    else
    {
      FuelOut(damage);
      shield.Play(enemyPosition, GetObjectRadius());
      //Debug.Log("Building.Attack() >>> FuelOut()");
    }
  }

  protected override void afterDie()
  {
    if (harvesters.Count > 0)
    {
      Terraformer nearbyTerraformer = levelController.GetNearbyTerraformer(this);
      if (nearbyTerraformer != null)
      {
        foreach (AI__Harvester harvester in harvesters)
        {
          if (harvester.IsDead())
            continue;

          nearbyTerraformer.AddObject(harvester.GetMapObject());
        }
      }
    }

    foreach (Turret turret in turrets)
      turret.OnTerraformerDying();
  }

  public override void Reset()
  {
    base.Reset();
    fuel = (int)(FUEL_MAX * 0.1f);
    repaintFuelMarker();

    harvesters.Clear();
    turrets.Clear();
    fuelSpawnPoints.Clear();
    fuelSpawnPointWithHarvesters.Clear();
    harvesterWithFuelSpawnPoint.Clear();

    pingHarvestersTimer = 0;
    fuelOutObjectsNearbyTimer = 0;
    currentTurretUpgradeLevel = 0;
}

  public override void Ready()
  {
    base.Ready();
    BaseManagerInit();
  }

  public Dictionary<int, float> GetEffects()
  {
    Dictionary<int, float> effects = new Dictionary<int, float>();

    effects.Add(Terraformer.EFFECT_SHIELD, GetCurrentShieldUpgradeLevel());
    effects.Add(Terraformer.EFFECT_WEAPON, GetCurrentWeaponUpgradeLevel());

    return effects;
  }

  protected override void CheckUpdateEffects()
  {
    updateEffectsTimer += Time.deltaTime;
    if (updateEffectsTimer >= UPDATE_EFFECTS_DELAY)
    {
      updateEffectsTimer = 0f;
      AddEffects(GetEffects());
    }
  }
}