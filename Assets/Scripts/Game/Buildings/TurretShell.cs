﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretShell : MonoBehaviour
{
  public ParticleSystem tail;
  public ParticleSystem explosion;

  protected AudioObject audioObject;

  AI__Enemy target;
  bool isShoted = false;
  bool isBoomed = false;
  float upTimer = 0.1f;
  float timer = 2f;
  float speedMove = 40f;
  float speedRotate = 4f;
  bool isPaused = false;
  IShellOwner owner;

  void Awake()
  {
    audioObject = GetComponent<AudioObject>();
    Utils.SetCollision4Particles(explosion, Const.TAG_FIRE);
  }

  void Update()
  {
    if (isPaused || isBoomed)
      return;

    if (isShoted)
    {
      if (upTimer > 0f)
      {
        upTimer -= Time.deltaTime;
        gameObject.transform.Translate(transform.up * speedMove * Time.deltaTime);
      }
      else
      {
        if (target != null && target.IsAlive())
        {
          gameObject.transform.Translate(transform.up * speedMove * Time.deltaTime);
          Vector3 direction = target.GetPosition() - transform.position;
          transform.up = Vector3.Slerp(transform.up, direction, speedRotate * Time.deltaTime);
        }
        else
        {
          StartCoroutine(Die(0f));
        }
      }

      Collider[] hitColliders = Physics.OverlapSphere(transform.position, 2f, Const.LAYER_MASK_ENEMY_ONLY);
      if (hitColliders.Length > 0)
      {
        Boom();
        StartCoroutine(Die(2f));
        return;
      }

      timer -= Time.deltaTime;
      if (timer <= 0)
      {
        StartCoroutine(Die(0f));
      }
    }
  }

  void Boom()
  {
    isBoomed = true;
    GetComponent<Renderer>().enabled = false;
    tail.Stop();
    explosion.Play();
    audioObject.Play(1);
  }

  IEnumerator Die(float delay)
  {
    yield return new WaitForSeconds(delay);

    tail.Stop();
    GetComponent<Renderer>().enabled = false;
    isShoted = false;
    if (owner != null)
      owner.OnShellDead(gameObject);
    Destroy(gameObject);
  }

  public void Shot(AI__Enemy enemy, IShellOwner owner)
  {
    target = enemy;
    this.owner = owner;
    isShoted = true;
    audioObject.Play(0);
  }

  public void SetPause(bool value)
  {
    isPaused = value;
    if (value)
    {
      tail.Pause();
      explosion.Pause();
    }
    else
    {
      tail.Play();
      explosion.Play();
    }
    audioObject.SetPause4All(value);
  }
}
