﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class LevelDialog
{
  [XmlAttribute("id")]
  public int Id = 0;
  [XmlAttribute("levelId")]
  public int LevelId = 0;

  public string Messages;

  private Queue<LevelDialogMessage> MessageQueue = new Queue<LevelDialogMessage>();

  public void InitAfterLoad()
  {
    string[] messages = Messages.Split(',');
    MessageQueue = new Queue<LevelDialogMessage>();

    if (Messages.Length > 0)
    {
      int id = 1;
      foreach (string message in messages)
      {
        MessageQueue.Enqueue(new LevelDialogMessage(id, Int16.Parse(message)));
        id++;
      }
    }
  }

  public LevelDialogMessage GetMessage()
  {
    if (MessageQueue == null || MessageQueue.Count == 0)
      return null;

    return MessageQueue.Dequeue();
  }

  public bool IsEnd()
  {
    return MessageQueue.Count == 0;
  }

  public override string ToString()
  {
    string temp = "";
    temp += "\n *****DIALOG [" + Id + "]*****";
    return temp;
  }
}
