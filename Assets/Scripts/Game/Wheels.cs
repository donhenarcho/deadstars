﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MapObject;

public class Wheels : MonoBehaviour, IWheels
{
  public ParticleSystem snow;
  protected float rps = 40; // revolutions per second

  protected bool isRun = false;

  protected IWheel[] wheels;

  void Start()
  {
    wheels = GetComponentsInChildren<IWheel>();
  }

  void Update()
  {
  }

  void OnTriggerEnter(Collider collider)
  {
    if (!isRun)
      return;

    //Debug.Log("OnTriggerEnter");
    MapObject mapObject = collider.gameObject.GetComponent<MapObject>();
    if (mapObject != null)
    {
      //Debug.Log("mapObject.type=" + mapObject.GetMapObjectType());
      switch (mapObject.GetMapObjectType())
      {
        case EMapObjectType.TYPE_TREE:
          Tree tree = mapObject.gameObject.GetComponent<Tree>();
          tree.Boom(transform.forward);
          break;
        case EMapObjectType.TYPE_STONE_20:
        case EMapObjectType.TYPE_STONE_40:
        case EMapObjectType.TYPE_STONE_60:
          Stone stone = mapObject.gameObject.GetComponent<Stone>();
          stone.Boom();
          break;
      }
    }
  }

  public void Go(float rps)
  {
    if (this.rps == rps && isRun)
      return;

    this.rps = rps;
    Go();
  }

  public void Go()
  {
    if (isRun)
      return;

    isRun = true;
    StateChanged();
    if (snow != null)
      snow.Play();
  }

  public void Stop()
  {
    if (!isRun)
      return;

    isRun = false;
    StateChanged();
    if (snow != null)
      snow.Stop();
  }

  private void StateChanged()
  {
    foreach (IWheel wheel in wheels)
    {
      if (isRun)
        wheel.Go(rps, false);
      else
        wheel.Stop();
    }
  }

  public bool IsGo()
  {
    return isRun;
  }
}
