﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackBox : MapObject
{
  public GameObject Lamp;

  private Material material;

  private float blinkingTimer = 0f;
  private bool emit = false;
  private bool isBlinking = false;

  public override void EndedObjectInit()
  {
    Init();
    isBlinking = true;
    material = Lamp.GetComponentInChildren<Renderer>().material;
    transform.gameObject.layer = Const.LAYER_FOUND_OBJECT;
  }

  void Update()
  {
    if (isBlinking)
      blinking();
  }

  private void blinking()
  {
    if (blinkingTimer >= .4f)
    {
      emit = !emit;
      if (emit)
        material.EnableKeyword("_EMISSION");
      else
        material.DisableKeyword("_EMISSION");
      blinkingTimer = 0f;
    }

    blinkingTimer += Time.deltaTime;
  }

  public override void OnFound()
  {
    isDead = true;
    IAmDead();
    gameObject.SetActive(false);
  }
}
