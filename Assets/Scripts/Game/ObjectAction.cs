﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class ObjectAction
{
  public const string TYPE_GOTO = "goto"; // команда двигаться в точку
  public const string TYPE_WAIT = "wait"; // команда ждать столько то секунд
  public const string TYPE_DOIT = "doit"; // команда выполнить процедуру с заданным именем
  public const string TYPE_PUTO = "puto"; // команда поставить объект в точку

  [XmlAttribute("type")]
  public string Type;

  [XmlAttribute("value1")]
  public string Value1;

  [XmlAttribute("value2")]
  public string Value2;

  public Vector3 GetPoint()
  {
    return new Vector3(float.Parse(Value1), 0f, float.Parse(Value2));
  }

  public float GetDelay()
  {
    return float.Parse(Value1);
  }

  public string GetOperationName()
  {
    return Value1;
  }

  public override string ToString()
  {
    string temp = "";

    temp += "\n *****OBJECT ACTION [" + Type + "]*****";
    temp += "\n Value1=" + Value1;
    temp += "\n Value2=" + Value2;
    
    return temp;
  }

  public void Print()
  {
    Debug.Log(ToString());
  }
}
