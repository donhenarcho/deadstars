﻿using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class ObjectSettings
{
  [XmlAttribute("objectType")]
  public int ObjectType;
  [XmlAttribute("hp")]
  public int HP;
  [XmlAttribute("damage")]
  public int Damage;
  [XmlAttribute("speed")]
  public float Speed;
  [XmlAttribute("seeDistance")]
  public int SeeDistance;

  public override string ToString()
  {
    string temp = "";
    temp += "\n *****OBJECT SETTINGS [" + ObjectType + "]*****";
    temp += "\n HP=" + HP;
    temp += "\n Damage=" + Damage;
    temp += "\n Speed=" + Speed;
    temp += "\n SeeDistance=" + SeeDistance;
    
    return temp;
  }
}
