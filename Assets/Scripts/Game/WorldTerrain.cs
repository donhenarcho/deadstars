﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldTerrain : MonoBehaviour
{
  private Terrain terrain;
  private Dome dome;

  private int grassDensity = 2048;
  private int patchDetail = 16;
  
  void Start()
  {
    terrain = GetComponent<Terrain>();
    terrain.gameObject.layer = Const.LAYER_TERRAIN;
    terrain.terrainData.size = Vector3.zero;
    terrain.gameObject.transform.position = Vector3.zero;

    dome = GetComponentInChildren<Dome>();
  }

  void Update()
  {
  }

  public void SetSize(float width, float length, float height, float mapBlockSize)
  {
    terrain.terrainData.size = new Vector3(width, height, length);
    terrain.gameObject.transform.position = new Vector3(
      terrain.gameObject.transform.position.x, 
      terrain.gameObject.transform.position.y, 
      -length);

    dome.SetSize(terrain.terrainData.size, mapBlockSize);

    //ClearGrass();
    //grassDensity = (int)width * 2;
    //AddGrass();
  }

  public void ClearGrass()
  {
    terrain.terrainData.SetDetailResolution(2048, patchDetail);

    int[,] newMap = new int[2048, 2048];

    for (int i = 0; i < 2048; i++)
    {
      for (int j = 0; j < 2048; j++)
      {
          newMap[i, j] = 0;
      }
    }
    terrain.terrainData.SetDetailLayer(0, 0, 0, newMap);
  }

  public void AddGrass()
  {
    terrain.terrainData.SetDetailResolution(grassDensity, patchDetail);

    int[,] newMap = new int[grassDensity, grassDensity];

    for (int i = 0; i < grassDensity; i++)
    {
      for (int j = 0; j < grassDensity; j++)
      {
        float height = terrain.terrainData.GetHeight(i, j);
        if (height < 10.0f)
        {
          newMap[i, j] = 6;
        }
        else
        {
          newMap[i, j] = 0;
        }
      }
    }
    terrain.terrainData.SetDetailLayer(0, 0, 0, newMap);
  }
}
