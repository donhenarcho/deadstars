﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : Transport, IShellOwner
{
  public Transform Tower;
  public Transform ShellPoints;
  public GameObject ShellPrefab;
  private List<Transform> points;
  private List<GameObject> shells;
  Cabine cabine;

  protected float AUTO_FIRING_DELAY = 1f;
  protected float autoFiringTimer = 0;

  protected float WORKING_DISTANCE = 100f;
  protected float WORKING_SEARCH_ENEMIES_TIME = 0.1f;
  protected float workingSearchEnemiesTimer = 0;

  protected List<AI__Enemy> enemies;
  protected AI__Enemy currentTarget;

  public override void EndedObjectInit()
  {
    TransportInit();
    wheels = GetComponentInChildren<IWheels>();
    cabine = GetComponentInChildren<Cabine>();

    points = new List<Transform>();
    foreach (Transform shellPoint in ShellPoints)
    {
      points.Add(shellPoint);
    }

    shells = new List<GameObject>();

    rotateSpeed = 6.0f;
    walkSpeed = 0f;
    currentMoveSpeed = walkSpeed;
    repaintFuelMarker();
    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    if (CheckOnOff())
      return;
    
    if (IsHero())
    {
      RaycastHit hit;
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      if (Physics.Raycast(ray, out hit, 1000, Const.LAYER_MASK_TERRAIN_ONLY))
      {
        Vector3 point = hit.point;
        point.y = cabine.transform.position.y;
        Vector3 direction = point - cabine.transform.position;
        cabine.transform.forward = direction;
      }

      StandartButtonsControll();
    }
    else
    {
      AutoWorking();
    }
  }

  private void LateUpdate()
  {
    if (!isReady)
      return;

    if (isPaused)
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return;
    }

    if (isDead || IsEmpty())
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return;
    }
  }

  protected override void ActionDown()
  {
    if (IsEmpty())
      return;

    Fire();
  }

  protected void Fire()
  {
    if (IsEmpty())
      return;

    Transform point = points[Random.Range(0, points.Count)];
    GameObject gameObject = Instantiate(ShellPrefab, point.position, Quaternion.identity);
    gameObject.GetComponent<CannonShell>().Shot(Tower.transform.forward, this);
    shells.Add(gameObject);

    FuelOut(5);
    StartCoroutine(TowerMove());
    audioObject.Play(1);
  }

  protected IEnumerator TowerMove()
  {
    Tower.localPosition = new Vector3(0f, -1f, -1f);
    yield return new WaitForSeconds(0.1f);
    Tower.localPosition = new Vector3(0f, 0f, 0f);
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    foreach (GameObject gameObject in shells)
    {
      if (gameObject != null)
        gameObject.GetComponent<CannonShell>().SetPause(value);
    }
  }

  public void OnShellDead(GameObject shell)
  {
    shells.Remove(shell);
  }

  protected List<AI__Enemy> GetBesideEnemies()
  {
    List<AI__Enemy> objects = new List<AI__Enemy>();
    Collider[] hitColliders = Physics.OverlapSphere(transform.position, WORKING_DISTANCE);
    foreach (var hitCollider in hitColliders)
    {
      AI__Enemy enemy = hitCollider.GetComponent<AI__Enemy>();
      if (enemy != null)
      {
        if (!enemy.IsFlyingObject())
          objects.Add(enemy);
      }
    }
    return objects;
  }

  protected void RotateToObject(
      GameObject _object
    )
  {
    Vector3 point = _object.transform.position;
    point.y = cabine.transform.position.y;
    Quaternion direction = Quaternion.LookRotation(point - cabine.transform.position);
    cabine.transform.rotation = Quaternion.Lerp(
      cabine.transform.rotation,
      direction,
      rotateSpeed * Time.deltaTime);
  }

  public void AutoWorking()
  {
    bool isFiring = false;

    if (currentTarget != null && currentTarget.IsAlive())
    {
      float targetDist = Vector3.Distance(transform.position, currentTarget.transform.position);
      if (targetDist > WORKING_DISTANCE)
      {
        currentTarget = null;
      }
      else
      {
        isFiring = true;
        RotateToObject(currentTarget.gameObject);
      }
    }

    if (currentTarget != null && !currentTarget.IsAlive())
    {
      currentTarget = null;
      workingSearchEnemiesTimer = WORKING_SEARCH_ENEMIES_TIME;
    }

    if (currentTarget == null)
    {
      workingSearchEnemiesTimer += Time.deltaTime;
      if (workingSearchEnemiesTimer >= WORKING_SEARCH_ENEMIES_TIME)
      {
        workingSearchEnemiesTimer = 0;
        enemies = GetBesideEnemies();

        if (enemies.Count > 0)
        {
          foreach (AI__Enemy enemy in enemies)
          {
            if (enemy.IsAlive())
            {
              currentTarget = enemy;
              autoFiringTimer = AUTO_FIRING_DELAY;
              break;
            }
          }
        }
      }
    }

    if (isFiring)
    {
      autoFiringTimer -= Time.deltaTime;
      if (autoFiringTimer <= 0f)
      {
        autoFiringTimer = AUTO_FIRING_DELAY;
        Fire();
      }
    }
  }
}
