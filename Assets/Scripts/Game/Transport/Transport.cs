﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transport : MapObject, IHero, IUsable, ITransport, IHaveFuel, IMovingObject, AI__AttackTarget
{
  public Transform Main;
  public ParticleSystem bricks;
  
  protected Shield shield;
  protected UI__FuelInMarker fuelInMarker;

  protected float DEAD_DURATION = 10f;
  protected float FUEL_OUT_FOR_MOVING_DELAY = 10f;

  protected FuelMarker fuelMarker;
  protected int FUEL_MAX = 0;
  protected int fuel = 0;
  protected float fuelTimer = 0;

  protected float rps = 40; // revolutions per second

  protected float walkSpeed = 6.0f;
  protected float rotateSpeed = 10.0f;
  protected float currentMoveSpeed = 0;

  protected bool moving = false;
  protected bool prevMoving = true;
  protected bool isOn = false;

  protected float timerOn = 0;
  protected float timerOff = 0;

  protected CharacterController characterController;

  protected Vector2 nextMapCoords = Vector2.zero;
  protected Vector3 nextWorldCoords = Vector3.zero;
  protected List<Vector2> pathToNextMapCoords = new List<Vector2>();

  protected IWheels wheels;
  protected Girl driver;

  protected EntryPoint[] entryPoints;

  protected bool needDeactivate = false;
  protected bool isReverse = false;

  protected float firingFuelTimer = 0;

  protected bool isBackTo = false;
  protected Vector3 isBackToDirection;
  protected float isBackToTimer = 0;

  protected float moveStopTimer = 0;
  protected float deadTimer = 0;

  protected bool isConnected = false;
  protected bool isFiring = false;

  protected bool isContollBlocked = false;

  protected UI__EffectMarker effectMarker;

  protected void TransportInit()
  {
    Init();

    effectMarker = GetComponentInChildren<UI__EffectMarker>();
    effectMarker.SetValue(0);

    levelController = FindObjectOfType<WorldController>();
    characterController = GetComponent<CharacterController>();
    fuelMarker = GetComponentInChildren<FuelMarker>();
    entryPoints = GetComponentsInChildren<EntryPoint>();
    wheels = GetComponentInChildren<IWheels>();
    shield = GetComponentInChildren<Shield>();
    fuelInMarker = GetComponentInChildren<UI__FuelInMarker>();
    fuelInMarker.Off();

    transform.gameObject.layer = Const.LAYER_TRANSPORT;
    Main.gameObject.layer = Const.LAYER_INVISIBLE_OBJECT;

    walkSpeed = variableManager.GetSpeed(GetMapObjectType());
    FUEL_MAX = variableManager.GetHP(GetMapObjectType());
  }

  private void LateUpdate()
  {
    if (!isReady)
      return;

    if (isPaused)
    {
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      return;
    }

    if (isDead || !isOn || IsEmpty())
    {
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      return;
    }

    if (wheels != null)
    {
      if (moving && !wheels.IsGo())
      {
        wheels.Go(rps);
      }
      else
      {
        wheels.Stop();
      }
    }
  }

  protected bool MainCheck()
  {
    CheckOnOff();

    if (!isOn)
      return true;

    /*if (isConnected)
      return true;*/

    return false;
  }

  protected virtual bool StandartButtonsControll()
  {
    if (isConnected || isContollBlocked)
      return true;

    if (Input.GetKeyDown(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_USE)))
    {
      UseObject();
      return true;
    }

    if (Input.GetKeyDown(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_FIRE)))
    {
      ActionDown();
      return true;
    }

    if (Input.GetKeyUp(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_FIRE)))
    {
      ActionUp();
      return true;
    }

    if (Input.GetKey(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_FIRE)))
    {
      ActionHold();
      return true;
    }

    return false;
  }

  protected List<IHaveFuel> GetBesideHaveFuelObjects()
  {
    List<IHaveFuel> objects = new List<IHaveFuel>();
    Collider[] hitColliders = Physics.OverlapSphere(transform.position, 4f);
    foreach (var hitCollider in hitColliders)
    {
      if (hitCollider.gameObject.Equals(this.gameObject))
        continue;

      IHaveFuel iHaveFuel = hitCollider.GetComponent<IHaveFuel>();
      if (iHaveFuel != null)
      {
        objects.Add(iHaveFuel);
      }
    }
    return objects;
  }

  protected virtual void ActionHold()
  {
    //Debug.Log("Transport.ActionHold()");
  }

  protected virtual void ActionDown()
  {
    //Debug.Log("Transport.ActionDown()");
  }

  protected virtual void ActionUp()
  {
    //Debug.Log("Transport.ActionUp()");
  }

  protected virtual void StandartMoveControll()
  {
    Vector3 movement = Vector3.zero;

    if (CheckBackTo())
    {
      movement = GetMovementFromDirection(movement, isBackToDirection);
      ApplyMovement(movement);
      return;
    }

    if (IsEmpty() || isContollBlocked)
    {
      ApplyMovement(movement);
      return;
    }

    Vector3 distance = Vector3.zero;
    float dltH = Input.GetAxis("Horizontal");
    float dltV = Input.GetAxis("Vertical");
    distance = Quaternion.Euler(0, 45, 0) * (new Vector3(dltH, 0, dltV));

    distance.y = 0;
    if (distance != Vector3.zero)
      RotateTo(distance);

    /*if (type == Const.TYPE_TANK)
    {
      float angleToTarget = Vector3.Angle(transform.forward, distance);
      if (angleToTarget > 20f)
        currentMoveSpeed = 0;
      else
        currentMoveSpeed = walkSpeed;
    }*/

    movement = GetMovementFromDirection(movement, distance);

    if (isReverse)
    {
      movement.x *= -1;
      movement.z *= -1;
    }

    ApplyMovement(movement);
  }

  private Vector3 GetMovementFromDirection(Vector3 movement, Vector3 direction)
  {
    direction.y = 0;

    if (isFlyingObject)
    {
      direction.y = transform.position.y < Const.FLYHEIGHT ? Const.FLYHEIGHT : 0;
    }

    if (direction != Vector3.zero)
    {
      Vector3 rotateDirection = direction;
      rotateDirection.y = 0f;
      RotateTo(rotateDirection);
    }
      
    direction.Normalize();
    movement = direction * currentMoveSpeed;
    movement = Vector3.ClampMagnitude(movement, currentMoveSpeed);
    return movement;
  }

  protected virtual void StandartOnlyRotateControll()
  {
    if (IsEmpty())
    {
      return;
    }

    Vector3 distance = Vector3.zero;
    if (gameController.GetTypeControl() == Const.TYPE_CONTROL_JOYSTICK)
    {
      float dltH = Input.GetAxis("Horizontal");
      float dltV = Input.GetAxis("Vertical");
      distance = Quaternion.Euler(0, 45, 0) * (new Vector3(dltH, 0, dltV));
    }

    distance.y = 0;
    if (distance != Vector3.zero)
      RotateTo(distance);
  }

  protected void ApplyMovement(Vector3 movement)
  {
    if (withGravity)
    {
      movement.y = Const.WORLD_GRAVITY * 50 * Time.deltaTime;
    }

    movement *= Time.deltaTime;
    characterController.Move(movement);

    if (characterController.velocity != Vector3.zero)
    {
      moving = true;
      moveStopTimer = .2f;
    }

    if (moveStopTimer > 0)
    {
      moveStopTimer -= Time.deltaTime;
      if (moveStopTimer <= 0)
      {
        moving = false;
        moveStopTimer = 0;
      }
    }
  }

  public virtual bool IsAvailableForAttack()
  {
    return !IsDead();
  }

  protected virtual void RotateTo(
      Vector3 _direction
    )
  {
    Quaternion direction = Quaternion.LookRotation(_direction);
    transform.rotation = Quaternion.Lerp(
      transform.rotation,
      direction,
      rotateSpeed * Time.deltaTime);
  }

  protected void FastRotateToObject(
      GameObject _object
    )
  {
    Vector3 direction = _object.transform.position - transform.position;
    direction.y = 0;
    transform.rotation = Quaternion.LookRotation(direction);
  }

  public void NeedOn()
  {
    timerOn = 5;
  }

  public void NeedOff()
  {
    timerOff = 5;
  }

  public virtual void On()
  {
    isOn = true;
  }

  public virtual void Off()
  {
    isOn = false;
  }

  protected bool CheckOnOff()
  {
    if (timerOn > 0)
    {
      timerOn -= 1;
      if (timerOn == 0)
        On();
      return true;
    }
    else if (timerOff > 0)
    {
      timerOff -= 1;
      if (timerOff == 0)
        Off();
      return true;
    }
    return false;
  }

  public EMapObjectType GetHeroType()
  {
    return GetMapObjectType();
  }

  public override bool IsHero()
  {
    return driver != null;
  }

  public virtual void Kick()
  {
    throw new System.NotImplementedException();
  }

  public void NeedDeactivate()
  {
    needDeactivate = true;
  }

  public bool IsBeside(Vector3 target)
  {
    float distance = Vector3.Distance(transform.position, target);
    return distance <= Const.BESIDE_OBJECTS_FOR_USE_DISTANCE;
  }

  public virtual bool In(Girl girl)
  {
    Debug.Log("In()");
    if (!IsBeside(girl.GetPosition()))
      return false;

    driver = girl;

    levelController.SetHero(this);

    driver.Off();
    driver.NeedDeactivate();

    NeedOn();

    AfterIn();

    //Debug.Log("In()-2");

    return true;
  }

  protected virtual void AfterIn()
  {

  }

  public virtual bool Out()
  {
    Debug.Log("Out()");
    return OutDefault();
  }

  protected virtual bool OutDefault()
  {
    Debug.Log("OutDefault()");
    driver.SetPosition(entryPoints[0].transform.position);
    levelController.SetHero(driver);

    Off();

    driver.SetInvulnerable(2f);
    driver.GetGameObject().SetActive(true);
    driver.NeedOn();
    driver = null;

    if (wheels != null)
      wheels.Stop();

    AfterOut();
    return true;
  }

  protected virtual void AfterOut()
  {

  }

  public int Use()
  {
    return 0;
  }

  protected virtual void UseObject()
  {
    Out();
  }

  public override bool IsTarget()
  {
    return true;
  }

  public int FuelIn(int value)
  {
    if (IsFull() || value == 0)
      return value;

    //Debug.Log("Transport.FuelIn() + " + value);

    fuel += value;

    int fuelReturn = 0;
    if (fuel > FUEL_MAX)
    {
      fuelReturn = fuel - FUEL_MAX;
      fuel = FUEL_MAX;
    }

    fuelInMarker.Show();
    AfterFuelIn();

    return fuelReturn;
  }

  public int FuelReturn(int value)
  {
    if (IsFull() || value == 0)
      return value;

    //Debug.Log("Transport.FuelReturn() + " + value);

    fuel += value;

    int fuelReturn = 0;
    if (fuel > FUEL_MAX)
    {
      fuelReturn = fuel - FUEL_MAX;
      fuel = FUEL_MAX;
    }

    AfterFuelIn();

    return fuelReturn;
  }

  public int FuelOut()
  {
    //Debug.Log("Transport.FuelOut()");
    if (fuel <= 0)
    {
      fuel = 0;
      return 0;
    }

    fuelTimer += Time.deltaTime;
    if (fuelTimer >= 1f)
    {
      fuelTimer = 0;
      fuel = fuel > 0 ? fuel - 1 : 0;
      AfterFuelOut();
      return 1;
    }
    return 0;
  }

  public int FuelOut(int value)
  {
    //Debug.Log("Transport::FuelOut(int value) = " + value);
    if (fuel <= 0)
    {
      fuel = 0;
      return 0;
    }

    fuel = fuel > 0 ? fuel - value : 0;
    if (fuel < 0)
    {
      value += fuel;
      fuel = 0;
    }

    AfterFuelOut();
    return value;
  }

  public void FillToMax()
  {
    fuel = FUEL_MAX;
  }

  public bool IsFull()
  {
    return fuel == FUEL_MAX;
  }

  public bool IsEmpty()
  {
    return fuel == 0;
  }

  public float GetFuelLeft()
  {
    return fuel / (FUEL_MAX / 100f);
  }

  public virtual void AfterFuelIn()
  {
    repaintFuelMarker();
  }

  public virtual void AfterFuelOut()
  {
    repaintFuelMarker();
    if (IsEmpty() && driver != null)
      Out();
  }

  protected void repaintFuelMarker()
  {
    fuelMarker.Repaint(fuel / (float)FUEL_MAX);
    fuelMarker.SetBlinking(IsEmpty());
  }

  public bool IsMovingNow()
  {
    return characterController.velocity != Vector3.zero;
  }

  public void BackTo()
  {
    if (isBackTo)
      return;

    isBackTo = true;
    isBackToDirection = transform.forward * -1f;
    isBackToTimer = 0.4f;
  }

  protected bool CheckBackTo()
  {
    if (isBackToTimer > 0)
    {
      isBackToTimer -= Time.deltaTime;
      if (isBackToTimer <= 0)
      {
        isBackTo = false;
        return false;
      }
      return true;
    }
    return false;
  }

  protected virtual bool CheckDead()
  {
    if (deadTimer > 0)
    {
      deadTimer -= Time.deltaTime;
      if (deadTimer <= 0)
      {
        deadTimer = 0;
        IAmDead();
        return false;
      }
      return true;
    }
    return false;
  }

  public float GetCost(EMapObjectType enemyType)
  {
    return variableManager.GetTargetCost(EMapObjectType.TYPE_TRANSPORT, enemyType);
  }

  public void Attack(Vector3 enemyPosition, int damage)
  {
    //Debug.Log("Transport.Attack()=" + damage);
    if (IsEmpty())
    {
      die();
      //Debug.Log("Transport.Attack() >>> die()");
    }
    else
    {
      FuelOut(damage);
      shield.Play(enemyPosition, GetObjectRadius());
      if (IsEmpty())
        die();
        //Debug.Log("Transport.Attack() >>> FuelOut()");
      }
  }

  public override void TrueKill()
  {
    if (driver != null)
    {
      driver.SetPosition(entryPoints[0].transform.position);
      driver.GetGameObject().SetActive(true);
      driver.On();
      driver.TrueKill();
    }
    Attack(Vector3.zero, FUEL_MAX);
  }

  protected virtual void beforeDie()
  {

  }

  protected virtual void die()
  {
    beforeDie();
    if (wheels != null)
      wheels.Stop();
    isDead = true;
    SetEnabledForAllColliders(false);
    SetEnabledForRenderers(Main, false);
    bricks.Play();
    deadTimer = DEAD_DURATION;
    afterDie();
  }

  protected virtual void afterDie()
  {

  }

  private void OnParticleCollision(GameObject other)
  {
    //Debug.Log("Transport.OnParticleCollision()");

    if (IsDead())
      return;

    Attack(other.transform.position, GetDamage4Units(other));
  }

  public override void Reset()
  {
    base.Reset();
    fuel = FUEL_MAX;

    SetEnabledForAllColliders(true);
    SetEnabledForRenderers(Main, true);

    repaintFuelMarker();
  }

  public override SavedWorldObject GetSavedWorldObject()
  {
    if (driver != null)
      driver.SetPosition(entryPoints[0].transform.position);

    SavedWorldObject savedLevelObject = base.GetSavedWorldObject();
    savedLevelObject.fuel = this.fuel;
    return savedLevelObject;
  }

  public override SavedWorldObject SetSavedWorldObject(SavedWorldObject savedLevelObject)
  {
    savedLevelObject = base.SetSavedWorldObject(savedLevelObject);
    if (savedLevelObject.fuel >= 0)
      this.fuel = savedLevelObject.fuel;
    return savedLevelObject;
  }

  public void Connect()
  {
    isConnected = true;
  }

  public void Disconnect()
  {
    isConnected = false;
  }

  protected override void OnUpdatedEffects()
  {
    effectMarker.SetValue(effects[Terraformer.EFFECT_SHIELD]);
  }
}
