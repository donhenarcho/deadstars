﻿using UnityEngine;
using System.Collections;

public class Fighter : Transport
{
  public Rotating[] wings;

  FighterGuns guns;

  public override void EndedObjectInit()
  {
    TransportInit();
    wheels = GetComponentInChildren<IWheels>();
    guns = GetComponentInChildren<FighterGuns>();

    rotateSpeed = 6.0f;
    currentMoveSpeed = walkSpeed;
    rps = 80;
    repaintFuelMarker();

    WingsOff();
    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    if (MainCheck())
      return;

    isFiring = false;

    StandartButtonsControll();
    StandartMoveControll();
  }

  private void LateUpdate()
  {
    if (!isReady)
      return;

    if (isPaused)
    {
      if (guns != null && !guns.IsPaused())
        guns.SetPause(true);
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      if (audioObject.IsPlaying(1))
        audioObject.Stop(1);
      return;
    }

    if (guns != null && guns.IsPaused())
    {
      guns.SetPause(false);
      if (!audioObject.IsPlaying(1))
        audioObject.Play(1, true);
    }

    Firing();

    if (isDead || !isOn || IsEmpty())
    {
      if (guns != null)
        guns.Fire(false);
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      if (audioObject.IsPlaying(1))
        audioObject.Stop(1);
      return;
    }

    if (!audioObject.IsPlaying(0))
      audioObject.Play(0, true);

    if (wheels != null)
    {
      if (moving && !wheels.IsGo())
      {
        wheels.Go(rps);
      }
      else
      {
        wheels.Stop();
      }
    }
  }

  protected override void ActionDown()
  {
    isFiring = true;
  }

  protected override void ActionHold()
  {
    isFiring = true;
  }

  protected void Firing()
  {
    if (isFiring)
    {
      if (!IsEmpty() && !guns.IsFiring())
      {
        guns.Fire(true);
        if (!audioObject.IsPlaying(1))
        {
          audioObject.Play(1, true);
        }
      }

      firingFuelTimer += Time.deltaTime;
      if (firingFuelTimer >= 0.5f)
      {
        firingFuelTimer = 0;
        FuelOut(10);
        if (IsEmpty())
        {
          guns.Fire(false);
        }
        repaintFuelMarker();
      }

      if (guns.IsPaused())
      {
        guns.SetPause(false);
        if (!audioObject.IsPlaying(1))
        {
          audioObject.Play(1, true);
        }
      }

      if (IsEmpty())
      {
        audioObject.Stop(1);
      }
    }
    else
    {
      guns.Fire(false);
      audioObject.Stop(1);
    }
  }

  private void WingsOn()
  {
    foreach (Rotating wing in wings)
    {
      wing.On();
    }
  }

  private void WingsOff()
  {
    foreach (Rotating wing in wings)
    {
      wing.Off();
    }
  }

  protected override void AfterIn()
  {
    WingsOn();
    withGravity = false;
    isFlyingObject = true;
  }

  protected override void AfterOut()
  {
    WingsOff();
  }

  public override bool Out()
  {
    isContollBlocked = true;
    withGravity = true;
    isFlyingObject = false;
    StartCoroutine(OutWithDelay(2f));
    return true;
  }

  protected IEnumerator OutWithDelay(float delay)
  {
    yield return new WaitForSeconds(delay);
    OutDefault();
    isContollBlocked = false;
  }
}