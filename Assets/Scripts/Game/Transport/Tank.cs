﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : Transport
{
  public Cabine cabine;
  public Transform tower;
  public Transform ShellPoint;
  public GameObject ShellPrefab;
  private GameObject currentShell;

  public override void EndedObjectInit()
  {
    TransportInit();
    wheels = GetComponentInChildren<IWheels>();

    rotateSpeed = 6.0f;
    currentMoveSpeed = walkSpeed;

    rps = 80;
    repaintFuelMarker();

    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    if (MainCheck())
      return;

    RaycastHit hit;
    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    if (Physics.Raycast(ray, out hit, 1000, Const.LAYER_MASK_TERRAIN_ONLY))
    {
      Vector3 point = hit.point;
      point.y = cabine.transform.position.y;
      Vector3 direction = point - cabine.transform.position;
      cabine.transform.forward = direction;
    }

    StandartButtonsControll();
    StandartMoveControll();
  }

  private void LateUpdate()
  {
    if (!isReady)
      return;

    if (isPaused)
    {
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      if (audioObject.IsPlaying(1))
        audioObject.Stop(1);
      return;
    }

    if (isDead || !isOn || IsEmpty())
    {
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      if (audioObject.IsPlaying(1))
        audioObject.Stop(1);
      return;
    }

    if (moving)
    {
      if (!audioObject.IsPlaying(0))
        audioObject.Play(0, true);
    }
    else
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
    }

    if (wheels != null)
    {
      if (moving && !wheels.IsGo())
      {
        wheels.Go(rps);
      }
      else
      {
        wheels.Stop();
      }
    }
  }

  protected override void ActionDown()
  {
    if (IsEmpty())
      return;

    currentShell = Instantiate(ShellPrefab, ShellPoint.position, Quaternion.identity);
    currentShell.GetComponent<TankShell>().Shot(cabine.transform.forward);
    
    FuelOut(50);
    StartCoroutine(TowerMove());
    audioObject.Play(1);
  }

  protected IEnumerator TowerMove()
  {
    tower.localPosition = new Vector3(0f, 0f, -2f);
    yield return new WaitForSeconds(0.1f);
    tower.localPosition = new Vector3(0f, 0f, 0f);
  }

  public override void Kick()
  {
    //Debug.Log("Tank.Kick()");
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    if (currentShell != null)
      currentShell.GetComponent<TankShell>().SetPause(value);
  }
}
