﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class SpaceshipEngine : MonoBehaviour
{
  public static int STATE_DEFAULT = 0;
  public static int STATE_UP = 1;
  public static int STATE_DOWN = 2;
  public static int STATE_ERROR = 3;

  private float speed = 20f;
  private float angleLimit = 0.1f;
  private float currentSpeed;
  private int state = 0;

  private Quaternion startRotation;
  private Quaternion toRotation;

  public ParticleSystem fire;
  public ParticleSystem smoke;
  public bool IsLeft = false;

  EmissionModule fireEmission;
  EmissionModule smokeEmission;

  private float smokeRateOverTime = 200;
  private float currentSmokeRateOverTime = 200;

  void Start()
  {
    currentSpeed = speed;
    fireEmission = fire.emission;
    smokeEmission = smoke.emission;
    fire.Stop();
    smoke.Stop();
    startRotation = transform.rotation;
  }

  void Update()
  {
    if (state == STATE_ERROR)
    {
      transform.Rotate(currentSpeed * Time.deltaTime, 0, 0);
      if (Mathf.Abs(transform.rotation.x) >= angleLimit)
      {
        currentSpeed *= -1;
      }
    }
    else if (state == STATE_DOWN)
    {
      transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, Time.deltaTime * speed * .5f);
      
      currentSmokeRateOverTime -= 40 * Time.deltaTime;
      smokeEmission.rateOverTime = new ParticleSystem.MinMaxCurve(currentSmokeRateOverTime, currentSmokeRateOverTime);
    }
  }

  public void Default()
  {
    state = STATE_DEFAULT;
    fire.Stop();
    smoke.Stop();
    smokeEmission.rateOverTime = new ParticleSystem.MinMaxCurve(smokeRateOverTime, smokeRateOverTime);
  }

  public void Up()
  {
    state = STATE_UP;
    fire.Play();
    smoke.Stop();
  }

  public void Down()
  {
    state = STATE_DOWN;
    fire.Play();
    if (IsLeft)
    {
      smoke.Play();
      currentSmokeRateOverTime = smokeRateOverTime;
    }
    else
    {
      smoke.Stop();
    }
    toRotation = startRotation;
  }

  public void Error()
  {
    state = STATE_ERROR;
    fire.Play();
    if (IsLeft)
    {
      smoke.Play();
    }
    else
    {
      smoke.Stop();
    }
  }

  public void SetPause(bool value)
  {
    if (state == STATE_DEFAULT)
      return;

    if (value)
    {
      fire.Pause();
    }
    else
    {
      fire.Play();
    }
  }
}
