﻿using UnityEngine;
using System.Collections;

public class StormShell : MonoBehaviour
{
  public ParticleSystem tail;
  public ParticleSystem explosion;

  protected AudioObject audioObject;

  Vector3 shotDirection;
  bool isShoted = false;
  bool isBoomed = false;
  float timer = 4f;
  bool isPaused = false;
  IShellOwner owner;

  void Awake()
  {
    audioObject = GetComponent<AudioObject>();
    Utils.SetCollision4Particles(explosion, Const.TAG_FIRE);
  }

  void Update()
  {
    if (isPaused || isBoomed)
      return;

    if (isShoted)
    {
      Vector3 newPosition = transform.position;
      newPosition += shotDirection * 2;
      transform.position = newPosition;

      Collider[] hitColliders = Physics.OverlapSphere(transform.position, 4f, Const.LAYER_MASK_ENEMY_ONLY);
      if (hitColliders.Length > 0)
      {
        Boom();
        StartCoroutine(Die(2f));
        return;
      }

      timer -= Time.deltaTime;
      if (timer <= 0)
      {
        StartCoroutine(Die(0f));
      }
    }
  }

  void Boom()
  {
    isBoomed = true;
    GetComponent<Renderer>().enabled = false;
    tail.Stop();
    explosion.Play();
    audioObject.Play(1);
  }

  IEnumerator Die(float delay)
  {
    yield return new WaitForSeconds(delay);

    tail.Stop();
    GetComponent<Renderer>().enabled = false;
    isShoted = false;
    if (owner != null)
      owner.OnShellDead(gameObject);
    Destroy(gameObject);
  }

  public void Shot(Vector3 direction, IShellOwner owner)
  {
    this.owner = owner;
    shotDirection = direction;
    isShoted = true;
    audioObject.Play(0);
  }

  public void SetPause(bool value)
  {
    isPaused = value;
    if (value)
    {
      tail.Pause();
      explosion.Pause();
    }
    else
    {
      tail.Play();
      explosion.Play();
    }
    audioObject.SetPause4All(value);
  }
}