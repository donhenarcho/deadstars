﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Storm : Transport, IShellOwner
{
  public Transform Tower;
  public Transform ShellPoints;
  public GameObject ShellPrefab;
  private List<Transform> points;
  private List<GameObject> shells;
  Cabine cabine;

  public override void EndedObjectInit()
  {
    TransportInit();
    wheels = GetComponentInChildren<IWheels>();
    cabine = GetComponentInChildren<Cabine>();

    points = new List<Transform>();
    foreach (Transform shellPoint in ShellPoints)
    {
      points.Add(shellPoint);
    }

    shells = new List<GameObject>();

    rotateSpeed = 6.0f;
    currentMoveSpeed = walkSpeed;
    rps = 80;
    repaintFuelMarker();
    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    if (MainCheck())
      return;

    isFiring = false;

    RaycastHit hit;
    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    if (Physics.Raycast(ray, out hit, 1000, Const.LAYER_MASK_TERRAIN_ONLY))
    {
      Vector3 point = hit.point;
      point.y = cabine.transform.position.y;
      Vector3 direction = point - cabine.transform.position;
      cabine.transform.forward = direction;
    }

    StandartButtonsControll();
    StandartMoveControll();
  }

  private void LateUpdate()
  {
    if (!isReady)
      return;

    if (isPaused)
    {
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return;
    }

    if (isDead || !isOn || IsEmpty())
    {
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return;
    }

    if (moving)
    {
      if (!audioObject.IsPlaying(0))
        audioObject.Play(0, true);
    }
    else
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
    }

    if (wheels != null)
    {
      if (moving && !wheels.IsGo())
      {
        wheels.Go(rps);
      }
      else
      {
        wheels.Stop();
      }
    }
  }

  protected override void ActionDown()
  {
    if (IsEmpty())
      return;

    Transform point = points[Random.Range(0, points.Count)];
    GameObject gameObject = Instantiate(ShellPrefab, point.position, Quaternion.identity);
    gameObject.GetComponent<StormShell>().Shot(Tower.transform.forward, this);
    shells.Add(gameObject);

    FuelOut(5);
    StartCoroutine(TowerMove());
  }

  protected IEnumerator TowerMove()
  {
    Tower.localPosition = new Vector3(0f, -1f, -1f);
    yield return new WaitForSeconds(0.1f);
    Tower.localPosition = new Vector3(0f, 0f, 0f);
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    foreach (GameObject gameObject in shells)
    {
      if (gameObject != null)
        gameObject.GetComponent<StormShell>().SetPause(value);
    }
    audioObject.SetPause4All(value);
  }

  public void OnShellDead(GameObject shell)
  {
    shells.Remove(shell);
  }
}