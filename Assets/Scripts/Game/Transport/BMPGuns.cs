﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class BMPGuns : MonoBehaviour
{
  public ParticleSystem fire1;
  public ParticleSystem fire2;

  bool isFiring = false;
  bool isPaused = false;

  void Awake()
  {
    Utils.SetCollision4Particles(fire1, Const.TAG_FIRE);
    Utils.SetCollision4Particles(fire2, Const.TAG_FIRE);
  }

  void Start()
  {
    fire1.enableEmission = false;
    fire2.enableEmission = false;
  }

  public void Fire(bool isFiring)
  {
    if (this.isFiring == isFiring)
      return;

    this.isFiring = isFiring;
    fire1.enableEmission = isFiring;
    fire2.enableEmission = isFiring;
  }

  public bool IsFiring()
  {
    return fire1.enableEmission && fire2.enableEmission;
  }

  public void SetPause(bool value)
  {
    if (value)
    {
      fire1.Pause();
      fire2.Pause();
    }
    else
    {
      fire1.Play();
      fire2.Play();
    }
  }

  public bool IsPaused()
  {
    return fire1.isPaused && fire2.isPaused;
  }
}
