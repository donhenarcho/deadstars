﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpacetruckContainer : MapObject
{
  protected CharacterController characterController;
  protected bool isBoomed = false;

  void Start()
  {
    characterController = GetComponent<CharacterController>();
  }

  public override void EndedObjectInit()
  {
    Init();
    isInit = true;
  }

  void Update()
  {
    if (!isReady)
      return;

    Vector3 movement = Vector3.zero;
    movement.y = Const.WORLD_GRAVITY * 50 * Time.deltaTime;
    characterController.Move(movement);

    if (!isBoomed)
    {
      isBoomed = true;
      StartCoroutine(Boom(0.1f));
    }
  }

  private IEnumerator Boom(float waitTime)
  {
    yield return new WaitForSeconds(waitTime);
    audioObject.Play(0);
  }

  public override void Reset()
  {
    base.Reset();
    isBoomed = false;
  }
}
