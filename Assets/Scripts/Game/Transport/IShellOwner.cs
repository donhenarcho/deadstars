﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IShellOwner
{
  void OnShellDead(GameObject shell);
}
