﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class CannonGuns : MonoBehaviour
{
  static int FUEL_MAX = 100;
  int fuel = FUEL_MAX;
  float delta = 0;

  public Transform gun1;
  public Transform gun2;
  public Transform gun3;
  public Transform gun4;

  public ParticleSystem fire1;
  EmissionModule emission1;
  public ParticleSystem fire2;
  EmissionModule emission2;
  public ParticleSystem fire3;
  EmissionModule emission3;
  public ParticleSystem fire4;
  EmissionModule emission4;
  public GameObject fuelMarker;

  bool isFiring = false;

  void Start()
  {
    emission1 = fire1.emission;
    fire1.enableEmission = false;
    emission2 = fire2.emission;
    fire2.enableEmission = false;
    emission3 = fire3.emission;
    fire3.enableEmission = false;
    emission4 = fire4.emission;
    fire4.enableEmission = false;
  }

  void Update()
  {
    if (isFiring)
    {
      Use();
    }
  }

  public void Fire(bool isEnabled)
  {
    if (isEnabled && fuel == 0)
      return;

    isFiring = isEnabled;
    fire1.enableEmission = isEnabled;
    fire2.enableEmission = isEnabled;
    fire3.enableEmission = isEnabled;
    fire4.enableEmission = isEnabled;
  }

  public int Use()
  {
    if (fuel == 0)
      return 0;

    delta += Time.deltaTime;
    if (delta >= 1)
    {
      delta = 0;
      fuel = fuel > 0 ? fuel - 1 : 0;
      if (fuel == 0)
      {
        Fire(false);
      }
      RepaintFuelMarker();
      return 1;
    }
    return 0;
  }

  private void RepaintFuelMarker()
  {
    Vector3 localScale = fuelMarker.transform.localScale;
    localScale.x = fuel / (float)FUEL_MAX;
    fuelMarker.transform.localScale = localScale;
    fuelMarker.GetComponentInChildren<Renderer>().enabled = fuel > 0;
  }

  public int Fill(int fuel)
  {
    int delta = 0;
    this.fuel = fuel;
    if (this.fuel > FUEL_MAX)
    {
      delta = this.fuel - FUEL_MAX;
    }

    RepaintFuelMarker();

    return delta;
  }
}
