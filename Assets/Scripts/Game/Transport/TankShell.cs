﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShell : MonoBehaviour
{
  public GameObject TankShellFirePrefab;
  public ParticleSystem fire;

  protected AudioObject audioObject;

  Vector3 shotDirection;
  bool isShoted = false;
  float timer = 4f;
  bool isPaused = false;

  void Awake()
  {
    audioObject = GetComponent<AudioObject>();
  }

  void Update()
  {
    if (isPaused)
      return;

    if (isShoted)
    {
      Vector3 newPosition = transform.position;
      newPosition += shotDirection * 2;
      transform.position = newPosition;

      Collider[] hitColliders = Physics.OverlapSphere(transform.position, 4f, Const.LAYER_MASK_ENEMY_ONLY);
      if (hitColliders.Length > 0)
      {
        Boom();
        Die();
        return;
      }

      timer -= Time.deltaTime;
      if (timer <= 0)
      {
        Die();
      }
    }
  }

  void Boom()
  {
    Vector3 firePosition = transform.position;
    firePosition.y = 0.1f;
    Instantiate(TankShellFirePrefab, firePosition, Quaternion.identity);
    audioObject.Play(0);
  }

  void Die()
  {
    fire.Stop();
    GetComponent<Renderer>().enabled = false;
    isShoted = false;
    Destroy(this.gameObject);
  }

  public void Shot(Vector3 direction)
  {
    shotDirection = direction;
    isShoted = true;
  }

  public void SetPause(bool value)
  {
    isPaused = value;
    if (value)
    {
      fire.Pause();
    }
    else
    {
      fire.Play();
    }
    audioObject.SetPause4All(value);
  }
}
