﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ark : MapObject, IHaveFuel, AI__AttackTarget
{
  public SpaceshipEngine engineControllerR1;
  public SpaceshipEngine engineControllerL1;
  public SpaceshipEngine engineControllerR2;
  public SpaceshipEngine engineControllerL2;

  public Transform Main;
  public ParticleSystem bricks;

  protected float DEAD_DURATION = 10f;

  protected Shield shield;
  protected UI__FuelInMarker fuelInMarker;
  protected FuelMarker fuelMarker;
  protected int FUEL_MAX = 0;
  protected int fuel = 0;
  protected float fuelTimer = 0;
  protected float deadTimer = 0;

  protected CharacterController characterController;

  private static float DURATION_ANIMATION_DOWN = 5f;

  private float verticalSpeed = 8f;

  private float timerDown = 0;

  public override void EndedObjectInit()
  {
    Init();

    levelController = FindObjectOfType<WorldController>();
    characterController = GetComponent<CharacterController>();
    fuelMarker = GetComponentInChildren<FuelMarker>();
    shield = GetComponentInChildren<Shield>();
    fuelInMarker = GetComponentInChildren<UI__FuelInMarker>();
    fuelInMarker.Off();
    
    transform.gameObject.layer = Const.LAYER_TRANSPORT;
    Main.gameObject.layer = Const.LAYER_INVISIBLE_OBJECT;
    
    FUEL_MAX = variableManager.GetHP(GetMapObjectType());

    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckBehavior();

    if (timerDown > 0)
    {
      Vector3 movement = Vector3.zero;
      movement.y = -1f;
      timerDown -= Time.deltaTime;
      
      if (timerDown <= 0)
      {
        timerDown = 0;
        Vector3 position = transform.position;
        position.y = 0f;
        levelController.KillAllObjectsHere(position, 12f);
        transform.position = position;
        StopEngines();
        return;
      }

      movement *= Time.deltaTime * verticalSpeed;
      characterController.Move(movement);
    }
  }

  private IEnumerator go()
  {
    yield return new WaitForSeconds(0.1f);

    timerDown = DURATION_ANIMATION_DOWN;
    RunEngines();
  }

  private void RunEngines()
  {
    engineControllerR1.Up();
    engineControllerL1.Up();
    engineControllerR2.Up();
    engineControllerL2.Up();
    audioObject.Play(0, true);
  }

  private void StopEngines()
  {
    engineControllerR1.Default();
    engineControllerL1.Default();
    engineControllerR2.Default();
    engineControllerL2.Default();
    audioObject.Stop(0);
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    engineControllerL1.SetPause(value);
    engineControllerR1.SetPause(value);
    engineControllerL2.SetPause(value);
    engineControllerR2.SetPause(value);
    if (value)
    {
      if (audioObject.IsPlaying(0))
      {
        audioObject.Pause(0);
      }
    }
    else
    {
      if (audioObject.IsPaused(0))
      {
        audioObject.UnPause(0);
      }
    }
  }

  public int FuelIn(int value)
  {
    if (IsFull() || value == 0)
      return value;

    //Debug.Log("Transport.FuelIn() + " + value);

    fuel += value;

    int fuelReturn = 0;
    if (fuel > FUEL_MAX)
    {
      fuelReturn = fuel - FUEL_MAX;
      fuel = FUEL_MAX;
    }

    fuelInMarker.Show();
    AfterFuelIn();

    return fuelReturn;
  }

  public int FuelReturn(int value)
  {
    if (IsFull() || value == 0)
      return value;

    //Debug.Log("Transport.FuelReturn() + " + value);

    fuel += value;

    int fuelReturn = 0;
    if (fuel > FUEL_MAX)
    {
      fuelReturn = fuel - FUEL_MAX;
      fuel = FUEL_MAX;
    }

    AfterFuelIn();

    return fuelReturn;
  }

  public int FuelOut()
  {
    //Debug.Log("Transport.FuelOut()");
    if (fuel <= 0)
    {
      fuel = 0;
      return 0;
    }

    fuelTimer += Time.deltaTime;
    if (fuelTimer >= 1f)
    {
      fuelTimer = 0;
      fuel = fuel > 0 ? fuel - 1 : 0;
      AfterFuelOut();
      return 1;
    }
    return 0;
  }

  public int FuelOut(int value)
  {
    //Debug.Log("Transport::FuelOut(int value) = " + value);
    if (fuel <= 0)
    {
      fuel = 0;
      return 0;
    }

    fuel = fuel > 0 ? fuel - value : 0;
    if (fuel < 0)
    {
      value += fuel;
      fuel = 0;
    }

    AfterFuelOut();
    return value;
  }

  public void FillToMax()
  {
    fuel = FUEL_MAX;
  }

  public bool IsFull()
  {
    return fuel == FUEL_MAX;
  }

  public bool IsEmpty()
  {
    return fuel == 0;
  }

  public float GetFuelLeft()
  {
    return fuel / (FUEL_MAX / 100f);
  }

  public virtual void AfterFuelIn()
  {
    repaintFuelMarker();
  }

  public virtual void AfterFuelOut()
  {
    repaintFuelMarker();
  }

  protected void repaintFuelMarker()
  {
    fuelMarker.Repaint(fuel / (float)FUEL_MAX);
    fuelMarker.SetBlinking(IsEmpty());
  }

  public float GetCost(EMapObjectType enemyType)
  {
    return variableManager.GetTargetCost(EMapObjectType.TYPE_ARK, enemyType);
  }

  public virtual bool IsAvailableForAttack()
  {
    return !IsDead();
  }

  public void Attack(Vector3 enemyPosition, int damage)
  {
    Debug.Log("ARK.Attack()=" + damage);
    if (IsEmpty())
    {
      die();
      //Debug.Log("Transport.Attack() >>> die()");
    }
    else
    {
      FuelOut(damage);
      shield.Play(enemyPosition, GetObjectRadius());
      if (IsEmpty())
        die();
      //Debug.Log("Transport.Attack() >>> FuelOut()");
    }
  }

  protected virtual void beforeDie()
  {

  }

  protected virtual void die()
  {
    beforeDie();
    isDead = true;
    SetEnabledForAllColliders(false);
    SetEnabledForRenderers(Main, false);
    bricks.Play();
    deadTimer = DEAD_DURATION;
    IAmWaitingDeath();
    afterDie();
  }

  protected virtual void afterDie()
  {

  }

  private void OnParticleCollision(GameObject other)
  {
    //Debug.Log("Transport.OnParticleCollision()");

    if (IsDead())
      return;

    Attack(other.transform.position, GetDamage4Units(other));
  }

  public override void Reset()
  {
    base.Reset();
    fuel = FUEL_MAX;

    SetEnabledForAllColliders(true);
    SetEnabledForRenderers(Main, true);

    repaintFuelMarker();
  }

  protected void CheckBehavior()
  {
    Debug.Log("CheckBehavior()");
    Debug.Log(behavior);

    if (waitNextObjectActionTimer > 0)
    {
      waitNextObjectActionTimer -= Time.deltaTime;
      return;
    }

    if (behavior != null && behavior.Count > 0)
    {
      ObjectAction objectAction = behavior.Dequeue();
      objectAction.Print();

      switch (objectAction.Type)
      {
        case ObjectAction.TYPE_WAIT:
          waitNextObjectActionTimer = objectAction.GetDelay();
          break;
        case ObjectAction.TYPE_DOIT:
          DoIt(objectAction.GetOperationName());
          break;
      }
    }
  }

  protected override void DoIt(string operationName)
  {
    Debug.Log("DoIt >>> " + operationName);

    Vector3 position = transform.position;
    switch (operationName)
    {
      case "up":
        position.y = 4000f;
        SetPosition(position);
        break;
      case "down":
        position.y = 44f;
        SetPosition(position);
        StartCoroutine(go());
        break;
    }
  }
}
