﻿public interface ITransport
{
  bool In(Girl girl);
  bool Out();
  bool IsMovingNow();
}
