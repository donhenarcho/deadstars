﻿using UnityEngine;
using System.Collections;

public class FighterGuns : MonoBehaviour
{
  public ParticleSystem fire1;

  bool isFiring = false;
  bool isPaused = false;

  void Awake()
  {
    Utils.SetCollision4Particles(fire1, Const.TAG_PLASMA);
  }

  void Start()
  {
    fire1.enableEmission = false;
  }

  public void Fire(bool isFiring)
  {
    if (this.isFiring == isFiring)
      return;

    this.isFiring = isFiring;
    fire1.enableEmission = isFiring;
  }

  public bool IsFiring()
  {
    return fire1.enableEmission;
  }

  public void SetPause(bool value)
  {
    if (value)
    {
      fire1.Pause();
    }
    else
    {
      fire1.Play();
    }
  }

  public bool IsPaused()
  {
    return fire1.isPaused;
  }
}