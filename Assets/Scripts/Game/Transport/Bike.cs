﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bike : Transport
{
  AnimationObject bike;

  public override void EndedObjectInit()
  {
    TransportInit();

    bike = GetComponentInChildren<AnimationObject>();
    bike.SetState(0);
    wheels = GetComponentInChildren<IWheels>();
    rotateSpeed = 12.0f;
    rps = 120;
    currentMoveSpeed = walkSpeed;
    repaintFuelMarker();
    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    if (MainCheck())
      return;

    StandartButtonsControll();
    StandartMoveControll();
  }

  private void LateUpdate()
  {
    if (!isReady)
      return;

    if (isPaused)
    {
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return;
    }

    if (isDead || !isOn || IsEmpty())
    {
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return;
    }

    if (moving)
    {
      if (!audioObject.IsPlaying(0))
        audioObject.Play(0, true);
    }
    else
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
    }

    if (wheels != null)
    {
      if (moving && !wheels.IsGo())
      {
        wheels.Go(rps);
      }
      else
      {
        wheels.Stop();
      }
    }
  }

  protected override void AfterIn()
  {
    bike.SetState(1);
  }

  protected override void AfterOut()
  {
    bike.SetState(0);
  }

  public override void Kick()
  {
    //Debug.Log("Bike.Kick()");
  }
}
