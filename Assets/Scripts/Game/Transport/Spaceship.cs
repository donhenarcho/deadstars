﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : Transport
{
  public GameObject hull1;
  public GameObject hull2;

  public SpaceshipEngine engineController1;
  public SpaceshipEngine engineController2;

  private static float DURATION_ANIMATION_UP = 4f;
  private static float DURATION_ANIMATION_ERROR = 4f;
  private static float DURATION_ANIMATION_DOWN = 4f;

  private float verticalSpeed = 1f;

  private float timerUp = 0;
  private float timerError = 0;
  private float timerDown = 0;

  private bool isAnimationError = false;
  private bool isBlocked = false;

  void Start()
  {
    SpaceshipInit();
  }

  protected void SpaceshipInit()
  {
    base.Init();

    walkSpeed = 0f;
    rotateSpeed = 0f;
    currentMoveSpeed = walkSpeed;
  }

  void Update()
  {
    if (!isReady)
      return;

    if (MainCheck())
      return;

    if (isAnimationError)
    {
      Vector3 movement = Vector3.zero;
      if (timerUp > 0)
      {
        movement.y = 1f;
        timerUp -= Time.deltaTime;
        if (timerUp <= 0)
        {
          timerUp = 0;
          timerError = DURATION_ANIMATION_ERROR;
          engineController1.Error();
          engineController2.Error();
        }
      }
      else if (timerError > 0)
      {
        movement.y = 0f;
        timerError -= Time.deltaTime;
        if (timerError <= 0)
        {
          timerError = 0;
          timerDown = DURATION_ANIMATION_DOWN;
          engineController1.Down();
          engineController2.Down();
        }
      }
      else if (timerDown > 0)
      {
        movement.y = -1f;
        timerDown -= Time.deltaTime;
        if (timerDown <= 0)
        {
          timerDown = 0;
          engineController1.Default();
          engineController2.Default();
          UseObject();
          isAnimationError = false;
          isBlocked = false;
        }
      }

      movement *= Time.deltaTime * verticalSpeed;
      characterController.Move(movement);
    }

    if (isBlocked)
      return;

    StandartButtonsControll();
  }

  private void startAnimationError()
  {
    timerUp = DURATION_ANIMATION_UP;
    engineController1.Up();
    engineController2.Up();
    isAnimationError = true;
  }

  protected override void AfterIn()
  {
    isBlocked = true;
    hull1.SetActive(false);
    hull2.SetActive(true);
    startAnimationError();
  }

  protected override void AfterOut()
  {
    hull1.SetActive(true);
    hull2.SetActive(false);
  }
}
