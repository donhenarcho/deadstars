﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BMP : Transport
{
  Cabine cabine;
  BMPGuns bmpGuns;

  public override void EndedObjectInit()
  {
    TransportInit();
    wheels = GetComponentInChildren<IWheels>();
    cabine = GetComponentInChildren<Cabine>();
    bmpGuns = GetComponentInChildren<BMPGuns>();
    
    rotateSpeed = 6.0f;
    currentMoveSpeed = walkSpeed;
    rps = 80;
    repaintFuelMarker();
    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    if (MainCheck())
      return;

    isFiring = false;

    RaycastHit hit;
    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    if (Physics.Raycast(ray, out hit, 1000, Const.LAYER_MASK_TERRAIN_ONLY))
    {
      Vector3 point = hit.point;
      point.y = cabine.transform.position.y;
      Vector3 direction = point - cabine.transform.position;
      cabine.transform.forward = direction;
    }

    StandartButtonsControll();
    StandartMoveControll();
  }

  private void LateUpdate()
  {
    if (!isReady)
      return;

    if (isPaused)
    {
      if (bmpGuns != null && !bmpGuns.IsPaused())
        bmpGuns.SetPause(true);
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      if (audioObject.IsPlaying(1))
        audioObject.Stop(1);
      return;
    }

    if (bmpGuns != null && bmpGuns.IsPaused())
    {
      bmpGuns.SetPause(false);
      if (!audioObject.IsPlaying(1))
        audioObject.Play(1, true);
    }

    Firing();

    if (isDead || !isOn || IsEmpty())
    {
      if (bmpGuns != null)
        bmpGuns.Fire(false);
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      if (audioObject.IsPlaying(1))
        audioObject.Stop(1);
      return;
    }

    if (moving)
    {
      if (!audioObject.IsPlaying(0))
        audioObject.Play(0, true);
    }
    else
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
    }

    if (wheels != null)
    {
      if (moving && !wheels.IsGo())
      {
        wheels.Go(rps);
      }
      else
      {
        wheels.Stop();
      }
    }
  }

  protected override void ActionDown()
  {
    isFiring = true;
  }

  protected override void ActionHold()
  {
    isFiring = true;
  }

  protected void Firing()
  {
    if (isFiring)
    {
      if (!IsEmpty() && !bmpGuns.IsFiring())
      {
        bmpGuns.Fire(true);
        if (!audioObject.IsPlaying(1))
        {
          audioObject.Play(1, true);
        }
      }

      firingFuelTimer += Time.deltaTime;
      if (firingFuelTimer >= 0.5f)
      {
        firingFuelTimer = 0;
        FuelOut(1);
        if (IsEmpty())
        {
          bmpGuns.Fire(false);
        }
        repaintFuelMarker();
      }

      if (bmpGuns.IsPaused())
      {
        bmpGuns.SetPause(false);
        if (!audioObject.IsPlaying(1))
        {
          audioObject.Play(1, true);
        }
      }

      if (IsEmpty())
      {
        audioObject.Stop(1);
      }
    }
    else
    {
      bmpGuns.Fire(false);
      audioObject.Stop(1);
    }
  }
}
