﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniSpacetruck : MapObject
{
  public SpaceshipEngine engineControllerR1;
  public SpaceshipEngine engineControllerL1;

  public GameObject container;

  protected CharacterController characterController;

  private static float DURATION_ANIMATION_DOWN = 4f;
  private static float DURATION_ANIMATION_UNLOADING = 1f;
  private static float DURATION_ANIMATION_UP = 4f;

  private float verticalSpeed = 8f;

  private float timerDown = 0;
  private float timerUnloading = 0;
  private float timerUp = 0;

  private bool isStarted = false;

  void Start()
  {
    characterController = GetComponent<CharacterController>();
    Init();
  }

  void Update()
  {
    if (!isReady)
      return;

    if (!isStarted)
      return;

    if (isPaused)
      return;

    Vector3 movement = Vector3.zero;
    if (timerDown > 0)
    {
      movement.y = -1f;
      timerDown -= Time.deltaTime;
      if (timerDown <= 0)
      {
        timerDown = 0;

        Vector3 position = transform.position;
        position.y -= 10f;
        StartCoroutine(addContainer(0f, container, position));

        timerUnloading = DURATION_ANIMATION_UNLOADING;
      }
    }
    else if (timerUnloading > 0)
    {
      timerUnloading -= Time.deltaTime;
      if (timerUnloading <= 0)
      {
        timerUnloading = 0;
        timerUp = DURATION_ANIMATION_UP;
      }
    }
    else if (timerUp > 0)
    {
      movement.y = 1f;
      timerUp -= Time.deltaTime;
      if (timerUp <= 0)
      {
        timerUp = 0;
        StopEngines();
        isStarted = false;
        Vector3 position = transform.position;
        position.y = 1000f;
        transform.position = position;
        levelController.MiniSpacetruckFinish();
        return;
      }
    }

    movement *= Time.deltaTime * verticalSpeed;
    characterController.Move(movement);
  }

  private IEnumerator addContainer(float wait, GameObject container, Vector3 position)
  {
    yield return new WaitForSeconds(wait);

    levelController.AddSpacetruckContainer(position);
    container.SetActive(false);
    levelController.KillAllObjectsHere(position, 6f);
  }

  public bool GoTo(Vector3 position)
  {
    if (isStarted)
      return false;

    container.SetActive(true);

    position.y = 44f;
    transform.position = position;

    StartCoroutine(go());
    return true;
  }

  private IEnumerator go()
  {
    yield return new WaitForSeconds(0.1f);

    timerDown = DURATION_ANIMATION_DOWN;
    RunEngines();
    isStarted = true;
  }

  private void RunEngines()
  {
    engineControllerR1.Up();
    engineControllerL1.Up();
    audioObject.Play(0, true);
  }

  private void StopEngines()
  {
    engineControllerR1.Default();
    engineControllerL1.Default();
    audioObject.Stop(0);
  }

  public bool IsStarted()
  {
    return isStarted;
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    engineControllerL1.SetPause(value);
    engineControllerR1.SetPause(value);
    audioObject.SetPause4All(value);
  }
}