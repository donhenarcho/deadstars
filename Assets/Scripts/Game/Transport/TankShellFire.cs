﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShellFire : MapObject
{
  public const float DELAY_DIE_1 = 10f;
  public const float DELAY_DIE_2 = 10f;

  public ParticleSystem fire;
  public ParticleSystem smoke;
  public ParticleSystem bricks;

  float timerDie1 = 0f;
  float timerDie2 = 0f;

  void Awake()
  {
    timerDie1 = DELAY_DIE_1;
    audioObject = GetComponent<AudioObject>();
  }

  void Start()
  {
    audioObject.Play(0, true);
  }

  void Update()
  {
    if (isPaused)
      return;

    if (timerDie1 > 0)
    {
      timerDie1 -= Time.deltaTime;
      if (timerDie1 <= 0)
      {
        timerDie1 = 0f;
        Die1();
      }
    }

    if (timerDie2 > 0)
    {
      timerDie2 -= Time.deltaTime;
      if (timerDie2 <= 0)
      {
        timerDie2 = 0f;
        Die2();
      }
    }
  }

  private void Die1()
  {
    fire.Stop();
    smoke.Stop();
    audioObject.Stop(0);
    timerDie2 = DELAY_DIE_2;
  }

  private void Die2()
  {
    Destroy(this.gameObject);
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    if (value)
    {
      if (fire.isPlaying)
      {
        fire.Pause();
        smoke.Pause();
      }
      bricks.Pause();
    }
    else
    {
      if (fire.IsAlive())
      {
        fire.Play();
        smoke.Play();
      }
      
      bricks.Play();
    }
    audioObject.SetPause4All(value);
  }
}
