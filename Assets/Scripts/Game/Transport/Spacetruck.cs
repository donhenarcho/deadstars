﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spacetruck : MapObject
{
  public SpaceshipEngine engineControllerR1;
  public SpaceshipEngine engineControllerR2;
  public SpaceshipEngine engineControllerL1;
  public SpaceshipEngine engineControllerL2;

  public GameObject container1;
  public GameObject container2;
  public GameObject container3;
  public GameObject container4;

  protected CharacterController characterController;

  private static float DURATION_ANIMATION_DOWN = 10f;
  private static float DURATION_ANIMATION_UNLOADING = 4f;
  private static float DURATION_ANIMATION_UP = 10f;

  private float verticalSpeed = 3f;

  private float timerDown = 0;
  private float timerUnloading = 0;
  private float timerUp = 0;

  private bool isStarted = false;

  void Start()
  {
    characterController = GetComponent<CharacterController>();
    Init();
  }

  void Update()
  {
    if (!isReady)
      return;

    if (!isStarted)
      return;
    
    Vector3 movement = Vector3.zero;
    if (timerDown > 0)
    {
      movement.y = -1f;
      timerDown -= Time.deltaTime;
      if (timerDown <= 0)
      {
        timerDown = 0;

        StartCoroutine(addContainer(0f, container1, getPositionForContainer(1)));
        StartCoroutine(addContainer(1f, container2, getPositionForContainer(2)));
        StartCoroutine(addContainer(2f, container3, getPositionForContainer(3)));
        StartCoroutine(addContainer(3f, container4, getPositionForContainer(4)));

        timerUnloading = DURATION_ANIMATION_UNLOADING;
      }
    }
    else if (timerUnloading > 0)
    {
      timerUnloading -= Time.deltaTime;
      if (timerUnloading <= 0)
      {
        timerUnloading = 0;
        timerUp = DURATION_ANIMATION_UP;
      }
    }
    else if (timerUp > 0)
    {
      movement.y = 1f;
      timerUp -= Time.deltaTime;
      if (timerUp <= 0)
      {
        timerUp = 0;
        StopEngines();
        isStarted = false;
        Vector3 position = transform.position;
        position.y = 1000f;
        transform.position = position;
        //levelController.SpacetruckFinish();
        return;
      }
    }
      
    movement *= Time.deltaTime * verticalSpeed;
    characterController.Move(movement);
  }

  private Vector3 getPositionForContainer(int num)
  {
    Vector3 position = Vector3.zero;

    switch (num)
    {
      case 1:
        position.x = transform.position.x - 10;
        position.z = transform.position.z + 10;
        break;
      case 2:
        position.x = transform.position.x + 10;
        position.z = transform.position.z + 10;
        break;
      case 3:
        position.x = transform.position.x - 10;
        position.z = transform.position.z - 10;
        break;
      case 4:
        position.x = transform.position.x + 10;
        position.z = transform.position.z - 10;
        break;
    }

    return position;
  }

  private IEnumerator addContainer(float wait, GameObject container, Vector3 position)
  {
    yield return new WaitForSeconds(wait);

    levelController.AddSpacetruckContainer(position);
    container.SetActive(false);
    levelController.KillAllObjectsHere(position, 6f);
  }

  public bool GoTo(Vector3 position)
  {
    if (isStarted)
      return false;

    container1.SetActive(true);
    container2.SetActive(true);
    container3.SetActive(true);
    container4.SetActive(true);

    position.y = 40f;
    transform.position = position;

    StartCoroutine(go());
    return true;
  }

  private IEnumerator go()
  {
    yield return new WaitForSeconds(0.1f);

    timerDown = DURATION_ANIMATION_DOWN;
    RunEngines();
    isStarted = true;
  }

  private void RunEngines()
  {
    engineControllerR1.Up();
    engineControllerR2.Up();
    engineControllerL1.Up();
    engineControllerL2.Up();
  }

  private void StopEngines()
  {
    engineControllerR1.Default();
    engineControllerR2.Default();
    engineControllerL1.Default();
    engineControllerL2.Default();
  }

  public bool IsStarted()
  {
    return isStarted;
  }
}
