﻿public interface IWheel
{
  void Go(float rps, bool isBack);  // revolutions per second
  void Stop();
  bool IsLeft();
}
