﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHaveConnect
{
  void Connect();
  void Disconnect();
}
