﻿using System.Collections;
using UnityEngine;

public class Tree : MapObject
{
  public Transform Main;

  bool isDying = false;

  void Start()
  {
    transform.gameObject.layer = Const.LAYER_TREES;
    Ready();
  }

  public void Boom(Vector3 direction)
  {
    if (isDying)
      return;

    isDying = true;

    SetEnabledForAllColliders(false);
    Main.Rotate(new Vector3(90, 0, 0));
    direction.y = 0;
    transform.forward = direction;
    Main.position += direction;

    StartCoroutine(Die());
  }

  private IEnumerator Die()
  {
    yield return new WaitForSeconds(4f);
    SetEnabledForRenderers(Main, false);
    isDead = true;
  }

  private void OnParticleCollision(GameObject other)
  {
    if (IsDead())
      return;

    if (GetDamage4Enemy(other) == 0)
      return;

    SetEnabledForRenderers(Main, false);
    isDead = true;
  }
}
