﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioController : MonoBehaviour
{
  public AudioSource music;

  private float musicVolume = 1f;
  private float soundVolume = 1f;

  public float GetMusicVolume()
  {
    return musicVolume;
  }

  public float GetSoundVolume()
  {
    return soundVolume;
  }

  void OnEnable()
  {
    LoadSettings();
    musicPlay();
  }

  void OnDisable()
  {
    musicStop();
  }

  private void LoadSettings()
  {
    if (PlayerPrefs.HasKey(Const.PLAYER_PREFS_MUSIC_VOLUME))
    {
      musicVolume = PlayerPrefs.GetFloat(Const.PLAYER_PREFS_MUSIC_VOLUME) / 100f;
    }

    if (PlayerPrefs.HasKey(Const.PLAYER_PREFS_SOUND_VOLUME))
    {
      soundVolume = PlayerPrefs.GetFloat(Const.PLAYER_PREFS_SOUND_VOLUME) / 100f;
    }
  }

  public void OnSliderMusicChanged(float volume)
  {
    music.volume = volume / 100f;
  }

  public void OnSliderSoundChanged(float volume)
  {

  }

  private void musicPlay()
  {
    music.loop = true;
    music.volume = musicVolume;
    music.Play();
  }

  private void musicStop()
  {
    if (music.isPlaying)
    {
      music.Stop();
    }
  }
}
