﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MapObject
{
  public Transform Main;
  public ParticleSystem bricks;

  protected Renderer[] renderers;
  protected Color partDefaultColor;

  protected float DEAD_DURATION = 10f;
  protected int hp = 100;
  protected float deadTimer = 0;

  protected bool isGetDefaultColor = false;

  void Awake()
  {
    renderers = Main.GetComponentsInChildren<Renderer>();
  }

  public override void EndedObjectInit()
  {
    base.Init();
    partDefaultColor = renderers[0].material.color;
    transform.gameObject.layer = Const.LAYER_STONES;
    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit)
      return;

    CheckDead();
  }

  void LateUpdate()
  {
    if (!isGetDefaultColor)
    {
      isGetDefaultColor = true;
      partDefaultColor = renderers[0].material.color;
      bricks.GetComponent<Renderer>().material.color = partDefaultColor;
    }
  }

  private void OnParticleCollision(GameObject other)
  {
    if (isDead)
      return;

    if (hp <= 0)
      return;

    SetDamage(GetDamage4Enemy(other));
  }

  public void Boom()
  {
    SetDamage(hp);
  }

  protected virtual void SetDamage(int damage)
  {
    if (hp == 0)
      return;

    hp -= damage;
    if (hp < 0)
      hp = 0;

    if (hp > 0)
    {
      StartCoroutine(blink());
    }
    else
    {
      die();
    }
  }

  private IEnumerator blink()
  {
    foreach (Renderer renderer in renderers)
      renderer.material.color = Color.red;
    yield return new WaitForSeconds(0.1f);
    foreach (Renderer renderer in renderers)
      renderer.material.color = partDefaultColor;
  }

  protected virtual void die()
  {
    isDead = true;
    SetEnabledForAllColliders(false);
    SetEnabledForRenderers(Main, false);
    bricks.Play();
    audioObject.Play(0);
    deadTimer = DEAD_DURATION;
  }

  protected virtual bool CheckDead()
  {
    if (deadTimer > 0)
    {
      deadTimer -= Time.deltaTime;
      if (deadTimer <= 0)
      {
        deadTimer = 0;
        IAmDead();
        return false;
      }
      return true;
    }
    return false;
  }

  public override void Ready()
  {
    base.Ready();
    renderers = Main.GetComponentsInChildren<Renderer>();
    partDefaultColor = renderers[0].material.color;
  }

  public override void Reset()
  {
    base.Reset();
    hp = 100;
    SetEnabledForAllColliders(true);
    SetEnabledForRenderers(Main, true);
  }
}
