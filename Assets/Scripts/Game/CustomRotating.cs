﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomRotating : MonoBehaviour
{
  public float speedFrom = 80f;
  public float speedTo = 40f;
  public float angleFrom = 290f;
  public float angleTo = 350f;
  public float currentSpeed;
  public float startAngle;

  void Start()
  {
    transform.localEulerAngles = new Vector3(0, 0, startAngle);
    //Debug.Log("START transform.rotation.z=" + transform.localEulerAngles.z);
  }

  void Update()
  {
    transform.Rotate(0, 0, currentSpeed * Time.deltaTime);
    //Debug.Log("transform.rotation.z=" + transform.localEulerAngles.z);
    if (transform.localEulerAngles.z <= angleFrom)
    {
      transform.localEulerAngles = new Vector3(0, 0, angleFrom);
      currentSpeed = speedFrom;
    }
    else if (transform.localEulerAngles.z >= angleTo)
    {
      transform.localEulerAngles = new Vector3(0, 0, angleTo);
      currentSpeed = speedTo;
    }
  }
}
