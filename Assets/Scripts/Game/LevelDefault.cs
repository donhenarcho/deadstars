﻿using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

[XmlRoot("Default")]
public class LevelDefault : Level
{
  public int ResourceMax = 0;
  public int ResourceOnePartSize = 0;
  public int ResourceOnePartTime = 0;

  public float SateliteStrikeRecoveryTime = 0;
  public float SateliteStrikePrepareTime = 0;
  public float SateliteStrikeFiringTime = 0;

  public int SpeedHarvesterCleaner = 0;
  public int SpeedHarvesterFuelOut = 0;
  public int SpeedTurretFuelOut = 0;

  [XmlArray("ResourceCosts"), XmlArrayItem("ObjectValue")]
  public ObjectValue[] ResourceCosts;
  private Dictionary<int, int> ResourceCostsDict;

  public override void InitAfterLoad()
  {
    //Debug.Log("LevelDefault::InitAfterLoad()");
    base.InitAfterLoad();

    ResourceCostsDict = new Dictionary<int, int>();
    foreach (ObjectValue objectValue in ResourceCosts)
      ResourceCostsDict.Add(objectValue.ObjectType, objectValue.Value);
  }

  public Dictionary<int, int> GetResourceCostsDict()
  {
    return ResourceCostsDict;
  }

  public void Print()
  {
    string temp = "";
    temp += ToString();
    
    temp += "\n SpeedHarvesterCleaner=" + SpeedHarvesterCleaner;
    temp += "\n SpeedHarvesterFuelOut=" + SpeedHarvesterFuelOut;
    temp += "\n SpeedTurretFuelOut=" + SpeedTurretFuelOut;
    
    Debug.Log(temp);
  }
}