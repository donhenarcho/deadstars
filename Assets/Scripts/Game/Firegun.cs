﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class Firegun : MonoBehaviour
{
  public GameObject owner;
  public Transform ShellPoint;
  public GameObject FiregunShellPrefab;
  private FiregunShell currentShell;

  private bool isFiring = false;
  bool isPaused = false;

  void Start()
  {
    On();
  }

  void Update()
  {
    if (isPaused)
      return;

    if (!isFiring)
      return;

    if (currentShell == null)
    {
      GameObject shell = Instantiate(FiregunShellPrefab, ShellPoint.position, owner.transform.rotation);
      currentShell = shell.GetComponent<FiregunShell>();
      currentShell.SetFiregun(this);
      currentShell.Init();
    }

    isFiring = false;
  }

  public void Fire()
  {
    isFiring = true;
  }

  public bool IsFiring()
  {
    return currentShell != null;
  }

  public bool Use(float fuelLeft)
  {
    return fuelLeft > 0;
  }

  public void Off()
  {
    this.gameObject.SetActive(false);
  }

  public void On()
  {
    this.gameObject.SetActive(true);
  }

  public void SetActive(bool isActive)
  {
    this.gameObject.SetActive(isActive);
  }

  public void SetPause(bool value)
  {
    isPaused = value;
    if (currentShell != null)
      currentShell.SetPause(value);
  }

  public bool IsPaused()
  {
    return isPaused;
  }

  public void OnShellDead(FiregunShell shell) 
  {
    currentShell = null;
  }
}
