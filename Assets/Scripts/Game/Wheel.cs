﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : AnimationObject, IWheel
{
  protected float speed = 1.0f;
  protected float timer = 0;

  public bool isLeft;
  protected bool isRun = false;
  protected bool isBack = false;

  void Start()
  {
    SetState(0);
  }

  void Update()
  {
    if (!isRun)
      return;

    CheckGo();
  }

  protected void CheckGo()
  {
    if (timer > 0)
    {
      timer -= Time.deltaTime;
      if (timer <= 0)
      {
        timer = speed;
        if (isBack)
          Prev();
        else
          Next();
      }
    }
    return;
  }

  public void Go(float rps, bool isBack)
  {
    this.isBack = isBack;
    speed = 1 / rps;
    timer = speed;
    isRun = true;
  }

  public void Stop()
  {
    isRun = false;
    isBack = false;
    timer = 0;
  }

  public bool IsLeft()
  {
    return isLeft;
  }
}
