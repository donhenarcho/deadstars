﻿public interface IWheels
{
  void Go();
  void Go(float rps);
  void Stop();
  bool IsGo();
}
