﻿public enum EnemyPartTagEnum
{
  Leg,
  LegLeftBottom, 
  LegLeftTop, 
  LegRightBottom, 
  LegRightTop, 
  HandLeftBottom, 
  HandLeftTop,
  HandRightBottom, 
  HandRightTop, 
  Body, 
  Head
}