﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelMarker : MonoBehaviour
{
  public GameObject fuelMarker;
  public GameObject fuelNoneMarker;

  private Material material;

  private float blinkingTimer = 0f;
  private bool emit = false;
  private bool isBlinking = false;

  private void Start()
  {
    material = fuelNoneMarker.GetComponentInChildren<Renderer>().material;
  }

  private void Update()
  {
    if (isBlinking)
      blinking();
  }

  private void blinking()
  {
    if (blinkingTimer >= 1f)
    {
      emit = !emit;
      if (emit)
        material.EnableKeyword("_EMISSION");
      else
        material.DisableKeyword("_EMISSION");
      blinkingTimer = 0f;
    }

    blinkingTimer += Time.deltaTime;
  }

  public void Repaint(float value)
  {
    Vector3 localScale = fuelMarker.transform.localScale;
    localScale.x = value;
    fuelMarker.transform.localScale = localScale;
    fuelMarker.GetComponentInChildren<Renderer>().enabled = value > 0;
    fuelNoneMarker.GetComponentInChildren<Renderer>().enabled = value <= 0;
  }

  public void SetBlinking(bool isBlinking)
  {
    this.isBlinking = isBlinking;
  }
}
