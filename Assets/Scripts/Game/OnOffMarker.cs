﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnOffMarker : MonoBehaviour
{
  public GameObject onMarker;
  public GameObject offMarker;

  private Material material;

  private float blinkingTimer = 0f;
  private bool emit = false;
  private bool isBlinking = false;

  private void Start()
  {
    material = offMarker.GetComponentInChildren<Renderer>().material;
  }

  private void Update()
  {
    if (isBlinking)
      blinking();
  }

  private void blinking()
  {
    if (blinkingTimer >= 1f)
    {
      emit = !emit;
      if (emit)
        material.EnableKeyword("_EMISSION");
      else
        material.DisableKeyword("_EMISSION");
      blinkingTimer = 0f;
    }

    blinkingTimer += Time.deltaTime;
  }

  public void On()
  {
    onMarker.GetComponentInChildren<Renderer>().enabled = true;
    offMarker.GetComponentInChildren<Renderer>().enabled = false;
    SetBlinking(false);
  }

  public void Off()
  {
    onMarker.GetComponentInChildren<Renderer>().enabled = false;
    offMarker.GetComponentInChildren<Renderer>().enabled = true;
    SetBlinking(true);
  }

  public void SetBlinking(bool isBlinking)
  {
    this.isBlinking = isBlinking;
  }
}
