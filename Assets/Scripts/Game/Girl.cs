﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Girl : MapObject, IHero, IHaveFuel, IMovingObject, IHaveConnect, AI__AttackTarget
{
  protected Firegun gunController;
  
  protected Shield shield;
  protected UI__FuelInMarker fuelInMarker;

  protected const string ANIMATOR_PARAM_SPEED = "speed";
  protected const string ANIMATOR_PARAM_FALL = "fall";
  protected const string ANIMATOR_PARAM_DEAD = "dead";
  protected const string ANIMATOR_PARAM_LIFTING = "lifting";

  protected const float KEEPING_WARM_DELAY = 60f;

  protected const float ANIMATION_LENGTH_FALL = 1.533f;
  protected const float ANIMATION_LENGTH_STAND_UP = 7.433f;
  protected const float ANIMATION_LENGTH_LIFTING = 0.5f;

  protected float walkSpeed = 6.0f;
  protected float rotateSpeed = 10.0f;
  protected float currentMoveSpeed = 0;

  protected bool moving = false;
  protected bool prevMoving = true;
  protected bool isOn = false;

  protected float timerOn = 0;
  protected float timerOff = 0;

  protected CharacterController characterController;

  protected Vector2 nextMapCoords = Vector2.zero;
  protected Vector3 nextWorldCoords = Vector3.zero;
  protected List<Vector2> pathToNextMapCoords = new List<Vector2>();

  protected bool needDeactivate = false;

  protected float fallTimer = 0;
  protected float standUpTimer = 0;
  protected float liftingTimer = 0;

  protected Animator animator;

  protected FuelMarker fuelMarker;
  static int FUEL_MAX = 0;
  int fuel = 0;
  float fuelTimer = 0;
  float keepingWarmTimer = 0;

  protected bool isBackTo = false;
  protected Vector3 isBackToDirection;
  protected float isBackToTimer = 0;

  protected bool isConnected = false;

  protected bool isInvulnerable = false;
  protected float isInvulnerableTimer = 0f;

  protected UI__EffectMarker effectMarker;

  public void SetInvulnerable(float duration)
  {
    if (IsEmpty())
      return;

    isInvulnerableTimer = duration;
    isInvulnerable = true;
  }

  protected void CheckIsInvulnerable()
  {
    if (isInvulnerableTimer > 0)
    {
      isInvulnerableTimer -= Time.deltaTime;
      if (isInvulnerableTimer <= 0)
      {
        isInvulnerableTimer = 0;
        isInvulnerable = false;
      }
    }
  }

  protected void GirlInit()
  {
    Init();

    effectMarker = GetComponentInChildren<UI__EffectMarker>();
    effectMarker.SetValue(0);

    shield = GetComponentInChildren<Shield>();
    characterController = GetComponent<CharacterController>();
    animator = GetComponent<Animator>();
    fuelMarker = GetComponentInChildren<FuelMarker>();
    gunController = GetComponentInChildren<Firegun>();
    fuelInMarker = GetComponentInChildren<UI__FuelInMarker>();
    fuelInMarker.Off();

    animator.SetBool(ANIMATOR_PARAM_FALL, false);
    animator.SetBool(ANIMATOR_PARAM_DEAD, false);
    animator.SetBool(ANIMATOR_PARAM_LIFTING, false);

    walkSpeed = variableManager.GetSpeed(GetMapObjectType());
    rotateSpeed = 10.0f;
    currentMoveSpeed = walkSpeed;

    FUEL_MAX = variableManager.GetHP(GetMapObjectType());
    fuel = FUEL_MAX;
    repaintFuelMarker();

    transform.gameObject.layer = Const.LAYER_HERO;

    On();
  }

  void Update()
  {
    if (!isInit)
    {
      isInit = true;
      GirlInit();
    }

    if (isPaused)
      return;

    CheckUpdateEffects();

    Vector3 movement = Vector3.zero;
    Vector3 distance = Vector3.zero;
    moving = false;

    CheckIsInvulnerable();

    if (levelController == null)
    {
      ApplyMovement(movement);
      return;
    }

    /*if (isConnected)
    {
      ApplyMovement(movement, firing);
      return;
    }*/

    CheckOnOff();

    if (needDeactivate)
    {
      needDeactivate = false;
      ApplyMovement(movement);
      checkSoundStates();
      this.gameObject.SetActive(false);
      return;
    }

    if (!isOn)
    {
      ApplyMovement(movement);
      return;
    }

    if (CheckFall())
    {
      ApplyMovement(movement);
      return;
    }

    if (CheckLifting())
    {
      ApplyMovement(movement);
      return;
    }

    if (isDead)
    {
      ApplyMovement(movement);
      return;
    }

    if (CheckBackTo())
    {
      movement = GetMovementFromDirection(movement, isBackToDirection);
      ApplyMovement(movement);
      return;
    }

    /*if (!isConnected 
      && Input.GetKeyDown(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_FIRE)))
    {
      if (!gunController.IsFiring())
      {
        StartLifting();
      }
    }*/

    if (!isConnected
      && Input.GetKeyDown(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_USE)))
    {
      UseObject();
    }

    float dltH = Input.GetAxis("Horizontal");
    float dltV = Input.GetAxis("Vertical");
    distance = Quaternion.Euler(0, 45, 0) * (new Vector3(dltH, 0, dltV));

    /*RaycastHit hit;
    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    if (Physics.Raycast(ray, out hit, 1000))
    {
      distance = hit.point - transform.position;
    }
    currentMoveSpeed = 0f;
    if (Input.GetKey(KeyCode.W))
    {
      currentMoveSpeed = walkSpeed;
    }*/

    movement = GetMovementFromDirection(movement, distance);
    ApplyMovement(movement);
  }

  private void LateUpdate()
  {
    if (!isReady)
      return;

    if (isPaused)
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      if (audioObject.IsPlaying(1))
        audioObject.Stop(1);
      if (!gunController.IsPaused())
        gunController.SetPause(true);
      return;
    }

    if (gunController != null && gunController.IsPaused())
      gunController.SetPause(false);

    //checkKeepingWarm();
    checkSoundStates();
  }

  private Vector3 GetMovementFromDirection(Vector3 movement, Vector3 direction)
  {
    direction.y = 0;
    if (direction != Vector3.zero)
      RotateTo(direction);
    direction.Normalize();
    movement = direction * currentMoveSpeed;
    movement = Vector3.ClampMagnitude(movement, currentMoveSpeed);
    return movement;
  }

  private void checkSoundStates()
  {
    if (characterController == null)
      return;

    if (moving)
    {
      if (!audioObject.IsPlaying(0))
        audioObject.Play(0, true);
    }
    else
    {
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
    }
  }

  protected bool CheckFall()
  {
    if (fallTimer > 0)
    {
      fallTimer -= Time.deltaTime;
      if (fallTimer <= 0)
      {
        StopFall();

        return false;
      }
      return true;
    }
    return false;
  }

  protected void StartFall()
  {
    if (fallTimer > 0)
      return;

    //Debug.Log("Fall Start");
    fallTimer = ANIMATION_LENGTH_FALL;
    animator.SetBool(ANIMATOR_PARAM_FALL, true);

    die();
  }

  protected void StopFall()
  {
    fallTimer = 0;
    animator.SetBool(ANIMATOR_PARAM_FALL, false);
    IAmDead();
    //Debug.Log("Fall Stop");
  }

  protected bool CheckLifting()
  {
    if (liftingTimer > 0)
    {
      liftingTimer -= Time.deltaTime;
      if (liftingTimer <= 0)
      {
        StopLifting();
        return false;
      }
      return true;
    }
    return false;
  }

  protected void StartLifting()
  {
    if (liftingTimer > 0)
      return;

    liftingTimer = ANIMATION_LENGTH_LIFTING;
    animator.SetBool(ANIMATOR_PARAM_LIFTING, true);
  }

  protected void StopLifting()
  {
    liftingTimer = 0;
    animator.SetBool(ANIMATOR_PARAM_LIFTING, false);
    gunController.Fire();
  }

  IEnumerator StopMove(
      float _waitTime
    )
  {
    yield return new WaitForSeconds(_waitTime);
    moving = false;
  }

  public override void Beside(
      GameObject _monster
    )
  {
    /*
    IsMonsterBeside = true;
    currentMoveSpeed = 0;
    FastRotateToObject(_monster);
    StartCoroutine(Hit(1.0f));*/
  }

  IEnumerator Die(
      float _waitTime
    )
  {
    yield return new WaitForSeconds(_waitTime);
    //animator.SetBool(ANIMATOR_PARAM_DEAD, true);
  }

  protected List<IUsable> GetBesideUsableObjects()
  {
    List<IUsable> objects = new List<IUsable>();
    Collider[] hitColliders = Physics.OverlapSphere(transform.position, Const.BESIDE_OBJECTS_FOR_USE_DISTANCE);
    foreach (var hitCollider in hitColliders)
    {
      if (hitCollider.gameObject.Equals(this.gameObject))
        continue;

      IUsable iUsable = hitCollider.GetComponent<IUsable>();
      if (iUsable != null)
      {
        objects.Add(iUsable);
      }
    }
    return objects;
  }

  protected List<IHaveFuel> GetBesideHaveFuelObjects()
  {
    List<IHaveFuel> objects = new List<IHaveFuel>();
    Collider[] hitColliders = Physics.OverlapSphere(transform.position, Const.BESIDE_OBJECTS_FOR_USE_DISTANCE);
    foreach (var hitCollider in hitColliders)
    {
      if (hitCollider.gameObject.Equals(this.gameObject))
        continue;

      IHaveFuel iHaveFuel = hitCollider.GetComponent<IHaveFuel>();
      if (iHaveFuel != null)
      {
        objects.Add(iHaveFuel);
      }
    }
    return objects;
  }

  protected void UseObject()
  {
    List<IUsable> objects = GetBesideUsableObjects();

    /*Debug.Log("===");
    foreach (IUsable obj in objects)
      Debug.Log("---->" + ((MapObject)obj).transform.position + "/" + ((MapObject)obj).GetMapObjectType());
    Debug.Log("===");*/

    if (objects.Count > 0)
    {
      UseObject(objects[0]);
    }
  }

  protected void UseObject(
      IUsable _object
    )
  {
    if (_object == null)
      return;

    GameObject useObject = ((MapObject)_object).gameObject;

    FastRotateToObject(useObject);

    ITransport iTransport = useObject.GetComponent<ITransport>();
    if (iTransport != null)
    {
      //Debug.Log("Try IN");
      iTransport.In(this);
      return;
    }

    _object.Use();
  }

  private void OnParticleCollision(GameObject other)
  {
    if (IsDead())
      return;

    if (isInvulnerable)
      return;

    if (GetDamage4Units(other) > 0)
    {
      StartFall();
      FuelOut(fuel);
    }
  }

  public bool IsAvailableForAttack()
  {
    return fallTimer == 0;
  }

  private void die()
  {
    isDead = true;
    animator.SetBool(ANIMATOR_PARAM_DEAD, true);
  }

  public override bool IsTarget()
  {
    return true;
  }

  public bool IsFull()
  {
    return fuel == FUEL_MAX;
  }

  public bool IsEmpty()
  {
    return fuel == 0;
  }

  public float GetFuelLeft()
  {
    return fuel / (FUEL_MAX / 100f);
  }

  public int FuelIn(int value)
  {
    if (IsFull() || value == 0)
      return value;

    fuel += value;

    int fuelReturn = 0;
    if (fuel > FUEL_MAX)
    {
      fuelReturn = fuel - FUEL_MAX;
      fuel = FUEL_MAX;
    }

    repaintFuelMarker();
    fuelInMarker.Show();

    return fuelReturn;
  }

  public int FuelReturn(int value)
  {
    if (IsFull() || value == 0)
      return value;

    fuel += value;

    int fuelReturn = 0;
    if (fuel > FUEL_MAX)
    {
      fuelReturn = fuel - FUEL_MAX;
      fuel = FUEL_MAX;
    }

    repaintFuelMarker();

    return fuelReturn;
  }

  public int FuelOut()
  {
    if (isInvulnerable)
      return 0;

    //Debug.Log("Girl::FuelOut()");
    if (fuel <= 0)
    {
      fuel = 0;
      return 0;
    }

    fuelTimer += Time.deltaTime;
    if (fuelTimer >= 1)
    {
      fuelTimer = 0;
      fuel = fuel > 0 ? fuel - 1 : 0;
      repaintFuelMarker();
      return 1;
    }
    return 0;
  }

  public int FuelOut(int value)
  {
    if (isInvulnerable)
      return 0;

    //Debug.Log("Girl::FuelOut(int value)");
    if (fuel <= 0)
    {
      fuel = 0;
      return 0;
    }

    fuel = fuel > 0 ? fuel - value : 0;
    if (fuel < 0)
    {
      value += fuel;
      fuel = 0;
    }

    repaintFuelMarker();
    return value;
  }

  public void FillToMax()
  {
    fuel = FUEL_MAX;
    repaintFuelMarker();
  }

  protected void repaintFuelMarker()
  {
    fuelMarker.Repaint(fuel / (float)FUEL_MAX);
    fuelMarker.SetBlinking(IsEmpty());
  }

  protected void ApplyMovement(Vector3 movement)
  {
    if (movement.sqrMagnitude > 0.1f)
    {
      moving = true;
    }

    animator.SetFloat(ANIMATOR_PARAM_SPEED, movement.sqrMagnitude);

    movement.y = Const.WORLD_GRAVITY * 50 * Time.deltaTime;
    movement *= Time.deltaTime;
    characterController.Move(movement);
  }

  public int Use()
  {
    return 0;
  }

  protected void RotateTo(
      Vector3 _direction
    )
  {
    Quaternion direction = Quaternion.LookRotation(_direction);
    transform.rotation = Quaternion.Lerp(
      transform.rotation,
      direction,
      rotateSpeed * Time.deltaTime);
  }

  protected void FastRotateToObject(
      GameObject _object
    )
  {
    Vector3 direction = _object.transform.position - transform.position;
    direction.y = 0;
    transform.rotation = Quaternion.LookRotation(direction);
  }

  public void NeedOn()
  {
    timerOn = 5;
  }

  public void NeedOff()
  {
    timerOff = 5;
  }

  public virtual void On()
  {
    isOn = true;
  }

  public void Off()
  {
    isOn = false;
    animator.SetFloat(ANIMATOR_PARAM_SPEED, 0);
  }

  protected void CheckOnOff()
  {
    if (timerOn > 0)
    {
      timerOn -= 1;
      if (timerOn == 0)
        On();
    }
    else if (timerOff > 0)
    {
      timerOff -= 1;
      if (timerOff == 0)
        Off();
    }
  }

  public EMapObjectType GetHeroType()
  {
    return GetMapObjectType();
  }

  public void NeedDeactivate()
  {
    needDeactivate = true;
  }

  public void SetPosition(Vector3 position)
  {
    transform.position = position;
  }

  public void BackTo()
  {
    if (isBackTo)
      return;
    
    isBackTo = true;
    isBackToDirection = transform.forward * -1f;
    isBackToTimer = 0.4f;
  }

  protected bool CheckBackTo()
  {
    if (isBackToTimer > 0)
    {
      isBackToTimer -= Time.deltaTime;
      if (isBackToTimer <= 0)
      {
        isBackTo = false;
        return false;
      }
      return true;
    }
    return false;
  }

  public void Connect()
  {
    isConnected = true;
  }

  public void Disconnect()
  {
    isConnected = false;
  }

  public override bool IsVisibleOnGameMap()
  {
    return false;
  }

  public float GetCost(EMapObjectType enemyType)
  {
    return variableManager.GetTargetCost(EMapObjectType.TYPE_HERO, enemyType);
  }

  public void Attack(Vector3 enemyPosition, int damage)
  {
    if (isInvulnerable)
      return;

    if (IsEmpty())
    {
      StartFall();
    }
    else
    {
      FuelOut(damage);
      shield.Play(enemyPosition, GetObjectRadius());
      if (IsEmpty())
        StartFall();
    }
  }

  public override void TrueKill()
  {
    isInvulnerable = false;
    Attack(Vector3.zero, FUEL_MAX);
  }

  public override bool IsHero()
  {
    return true;
  }

  void OnControllerColliderHit(ControllerColliderHit hit)
  {
    /*MapObject mapObject = hit.collider.gameObject.GetComponent<MapObject>();
    if (mapObject != null)
    {
      switch (mapObject.GetMapObjectType())
      {
        case Const.TYPE_TREE:
          Tree tree = mapObject.gameObject.GetComponent<Tree>();
          tree.Boom();
          break;
      }
    }*/
  }

  public override SavedWorldObject GetSavedWorldObject()
  {
    SavedWorldObject savedLevelObject = base.GetSavedWorldObject();
    savedLevelObject.fuel = this.fuel;
    return savedLevelObject;
  }

  public override SavedWorldObject SetSavedWorldObject(SavedWorldObject savedLevelObject)
  {
    savedLevelObject = base.SetSavedWorldObject(savedLevelObject);
    if (savedLevelObject.fuel >= 0)
      this.fuel = savedLevelObject.fuel;
    return savedLevelObject;
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    animator.enabled = !value;
  }

  protected override void OnUpdatedEffects()
  {
    effectMarker.SetValue(effects[Terraformer.EFFECT_SHIELD]);
  }
}
