﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class Blood : MonoBehaviour
{
  ParticleSystem blood;
  EmissionModule emission;
  IHero hero;

  void Start()
  {
    blood = GetComponentInChildren<ParticleSystem>();
    emission = blood.emission;
  }

  void Update()
  {
    
  }

  public void SetHero(IHero hero)
  {
    this.hero = hero;
  }

  public void Off()
  {
    blood.Stop();
  }

  public void On(int power)
  {
    emission.rateOverTime = new ParticleSystem.MinMaxCurve(power * 10.0f, power * 10.0f);
    blood.Play();
  }
}
