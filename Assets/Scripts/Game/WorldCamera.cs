﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WorldCamera : MonoBehaviour
{
  public const float CAMERA_DISTANCE = 66.0f;
  public const float CAMERA_ROTATE_SPEED = 20.0f;

  private WorldController levelController;

  public const float CAMERA_SIZE_DEFAULT = 24.0f;

  private IHero target = null;

  public float rotSpeed = 1.5f;

  private Camera camera;

  private float shakeTimer = 0;

  private float prevSize = 0f;
  private float nextSize = 0f;
  public float duration = 0.4f;
  private float elapsed = 0.0f;
  private bool transition = false;

  void Start()
  {
    levelController = FindObjectOfType<WorldController>();
    camera = GetComponentInChildren<Camera>();
    camera.orthographicSize = CAMERA_SIZE_DEFAULT;
  }

  void LateUpdate()
  {
    if (!levelController.IsLevelReady())
      return;

    if (target == null)
      return;

    if (transition)
    {
      elapsed += Time.deltaTime / duration;
      Camera.main.orthographicSize = Mathf.Lerp(prevSize, nextSize, elapsed);
      if (elapsed > 1.0f)
      {
        elapsed = 0;
        transition = false;
      }
    }

    Vector3 position = target.GetPosition();
    position.y = target.GetPosition().y;
    transform.position = position;
  }

  public void SetTarget(
      IHero _target
    )
  {
    target = _target;
  }

  public Vector3 GetCameraPosition()
  {
    return camera.transform.position;
  }
}
