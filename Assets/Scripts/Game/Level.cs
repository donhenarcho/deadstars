﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class Level
{
  [XmlAttribute("id")]
  public int Id;
  [XmlAttribute("filename")]
  public string Filename = "";
  public string NameCode;
  public string DescriptionCode;

  public float HeroPositionX = 0;
  public float HeroPositionY = 0;

  public int HiddenMenu = 0;
  [XmlArray("HiddenButtons"), XmlArrayItem("HiddenButton")]
  public int[] MenuHiddenButtons;

  [XmlArray("NewButtons"), XmlArrayItem("NewButton")]
  public int[] MenuNewButtons;

  [XmlArray("TargetCosts"), XmlArrayItem("ObjectValue")]
  public ObjectValue[] TargetCosts;
  private Dictionary<int, int> TargetCostsDict;

  [XmlArray("TargetCosts4Golem"), XmlArrayItem("ObjectValue")]
  public ObjectValue[] TargetCosts4Golem;
  private Dictionary<int, int> TargetCosts4GolemDict;

  [XmlArray("FactoryLimits"), XmlArrayItem("ObjectValue")]
  public ObjectValue[] FactoryLimits;
  private Dictionary<int, int> FactoryLimitsDict;

  [XmlArray("ObjectSettingsList"), XmlArrayItem("ObjectSettings")]
  public ObjectSettings[] ObjectSettingsList;
  private Dictionary<int, ObjectSettings> ObjectSettingsDict;

  [XmlArray("Objects"), XmlArrayItem("Object")]
  public LevelObject[] ObjectList;

  [XmlArray("EnemyWaves"), XmlArrayItem("EnemyWave")]
  public EnemyWave[] EnemyWaveList;
  private Queue<EnemyWave> EnemyWaveQueue;
  private int PrevEnemyWaveId = 0;
  private int LastEnemyWaveId = 0;
  private int LastLevelTaskId = 0;
  
  public int TargetOnlyHero = 0;
  public int EnemyWavesLoop = 0;

  [XmlArray("Tasks"), XmlArrayItem("Task")]
  public LevelTask[] TaskList;
  private Queue<LevelTask> TaskQueue;

  [XmlArray("Dialogues"), XmlArrayItem("Dialog")]
  public LevelDialog[] DialogList;

  [XmlArray("Behavior"), XmlArrayItem("ObjectActions")]
  public ObjectActions[] ObjectActionsList;
  private Dictionary<int, ObjectActions> ObjectActionsDict;

  private LevelTask currentTask;
  private bool isEnded = false;
  private bool result = false;

  public virtual void InitAfterLoad()
  {
    //Debug.Log("Level::InitAfterLoad()");

    TargetCostsDict = new Dictionary<int, int>();
    foreach (ObjectValue objectValue in TargetCosts)
      TargetCostsDict.Add(objectValue.ObjectType, objectValue.Value);

    TargetCosts4GolemDict = new Dictionary<int, int>();
    foreach (ObjectValue objectValue in TargetCosts4Golem)
      TargetCosts4GolemDict.Add(objectValue.ObjectType, objectValue.Value);

    FactoryLimitsDict = new Dictionary<int, int>();
    foreach (ObjectValue objectValue in FactoryLimits)
      FactoryLimitsDict.Add(objectValue.ObjectType, objectValue.Value);

    ObjectSettingsDict = new Dictionary<int, ObjectSettings>();
    foreach (ObjectSettings objectSettings in ObjectSettingsList)
      ObjectSettingsDict.Add(objectSettings.ObjectType, objectSettings);

    EnemyWaveQueue = new Queue<EnemyWave>();
    foreach (EnemyWave settings in EnemyWaveList)
    {
      settings.InitAfterLoad();
      EnemyWaveQueue.Enqueue(settings);
    }

    TaskQueue = new Queue<LevelTask>();
    foreach (LevelTask task in TaskList)
      TaskQueue.Enqueue(task);

    foreach (LevelDialog dialog in DialogList)
      dialog.InitAfterLoad();

    ObjectActionsDict = new Dictionary<int, ObjectActions>();
    foreach (ObjectActions objectActions in ObjectActionsList)
    {
      objectActions.InitAfterLoad();
      ObjectActionsDict.Add(objectActions.Id, objectActions);
    }
  }

  public EnemyWave GetEnemyWave()
  {
    if (EnemyWaveQueue == null)
      return null;

    if (EnemyWaveQueue.Count == 0)
    {
      if (IsEnemyWavesLoop())
      {
        EnemyWaveQueue = new Queue<EnemyWave>();
        foreach (EnemyWave settings in EnemyWaveList)
        {
          EnemyWaveQueue.Enqueue(settings);
        }
      }
      else
      {
        return null;
      }
    }

    EnemyWave wave = EnemyWaveQueue.Dequeue();
    PrevEnemyWaveId = LastEnemyWaveId;
    LastEnemyWaveId = wave.Id;
    return wave;
  }

  public void ReturnEnemyWave(EnemyWave wave)
  {
    var items = EnemyWaveQueue.ToArray();
    EnemyWaveQueue.Clear();
    EnemyWaveQueue.Enqueue(wave);
    foreach (var item in items)
      EnemyWaveQueue.Enqueue(item);

    if (LastEnemyWaveId == wave.Id)
      LastEnemyWaveId = PrevEnemyWaveId;
  }

  public LevelTask GetNextTask()
  {
    //Debug.Log("GetNextTask() = " + currentTask);
    if (currentTask != null && !currentTask.IsEnded())
      return null;

    if (TaskQueue == null || TaskQueue.Count == 0)
      return null;

    currentTask = TaskQueue.Dequeue();
    LastLevelTaskId = currentTask.Id;

    Debug.Log("GetNextTask() >>> ");
    Debug.Log(GetCurrentTask());

    return currentTask;
  }

  public LevelTask GetCurrentTask()
  {
    return currentTask;
  }

  public void ApplySavedWorld(SavedWorld savedWorld)
  {
    Debug.Log("ApplySavedWorld()");
    Debug.Log(savedWorld);
    if (savedWorld.lastLevelTaskId > 0)
      while (LastLevelTaskId != savedWorld.lastLevelTaskId)
      {
        if (currentTask != null)
          currentTask.End();
        GetNextTask();
      }

    if (savedWorld.lastEnemyWaveId > 0)
      while (LastEnemyWaveId != savedWorld.lastEnemyWaveId)
        GetEnemyWave();

    Debug.Log("LastEnemyWaveId >>> " + LastEnemyWaveId);
  }

  public void EndTask(bool result)
  {
    currentTask.End();
    if (result)
    {
      if (TaskQueue.Count == 0)
      {
        isEnded = true;
        this.result = true;
      }
    }
    else
    {
      isEnded = true;
      this.result = false;
    }
  }

  public bool IsEnded()
  {
    return isEnded;
  }

  public bool Result()
  {
    return result;
  }

  public LevelDialog GetStartDialog()
  {
    LevelTask task = GetCurrentTask();
    return GetDialogById(task.StartDialogId);
  }

  public LevelDialog GetWinDialog()
  {
    LevelTask task = GetCurrentTask();
    return GetDialogById(task.WinDialogId);
  }

  public LevelDialog GetLoseDialog()
  {
    LevelTask task = GetCurrentTask();
    return GetDialogById(task.LoseDialogId);
  }

  public LevelDialog GetHeroDeadDialog()
  {
    LevelTask task = GetCurrentTask();
    return GetDialogById(task.HeroDeadDialogId);
  }

  private LevelDialog GetDialogById(int id)
  {
    foreach (LevelDialog dialog in DialogList)
      if (dialog.Id == id)
        return dialog;
    return null;
  }

  public string GetProgress(WorldController world)
  {
    if (currentTask == null)
      return "";

    return currentTask.GetProgress(world);
  }

  public int GetLastEnemyWaveId()
  {
    return LastEnemyWaveId;
  }

  public int GetLastLevelTaskId()
  {
    return LastLevelTaskId;
  }

  public Dictionary<int, int> GetTargetCostsDict()
  {
    return TargetCostsDict;
  }

  public Dictionary<int, int> GetTargetCosts4GolemDict()
  {
    return TargetCosts4GolemDict;
  }

  public Dictionary<int, int> GetFactoryLimitsDict()
  {
    return FactoryLimitsDict;
  }

  public Dictionary<int, ObjectSettings> GetObjectSettingsDict()
  {
    return ObjectSettingsDict;
  }

  public ObjectActions GetObjectActions(int id)
  {
    return ObjectActionsDict[id];
  }

  public bool IsEnemyWavesLoop()
  {
    return EnemyWavesLoop == 1;
  }

  public bool IsTargetOnlyHero()
  {
    return TargetOnlyHero == 1;
  }

  public override string ToString()
  {
    string temp = "";
    temp += "\n *****LEVEL [" + Id + "]*****";
    temp += "\n Filename=" + Filename;
    temp += "\n NameCode=" + NameCode;
    temp += "\n DescriptionCode=" + DescriptionCode;
    temp += "\n HeroPositionX=" + HeroPositionX;
    temp += "\n HeroPositionY=" + HeroPositionY;
    temp += "\n HiddenMenu=" + HiddenMenu;
    temp += "\n MenuBlockedButtons:";
    foreach (int buttonId in MenuHiddenButtons)
      temp += "\n -> MenuHiddenButtonId=" + buttonId;

    foreach(KeyValuePair<int, ObjectSettings> keyValuePair in ObjectSettingsDict)
    {
      temp += keyValuePair.Value.ToString();
    }

    foreach (LevelObject levelObject in ObjectList)
      temp += levelObject.ToString();
    foreach (EnemyWave enemyWave in EnemyWaveList)
      temp += enemyWave.ToString();
    foreach (LevelTask task in TaskList)
      temp += task.ToString();
    foreach (LevelDialog dialog in DialogList)
      temp += dialog.ToString();
    return temp;
  }

  public void Print()
  {
    Debug.Log(ToString());
  }
}
