﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
  ParticleSystem particleSystem;
  Vector3 startLocalPosition;
  void Start()
  {
    particleSystem = GetComponent<ParticleSystem>();
    startLocalPosition = transform.localPosition;
  }

  void Update()
  {
  }

  public void Play(Vector3 enemyPosition, float radius)
  {
    transform.localPosition = startLocalPosition;
    Vector3 direction = enemyPosition - transform.position;
    transform.position = transform.position
      + direction.normalized * radius;
    transform.forward = direction;
    particleSystem.Play();
  }
}
