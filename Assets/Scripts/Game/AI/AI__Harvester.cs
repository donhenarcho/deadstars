﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI__Harvester : AI__Transport
{
  public int cleanerSpeed = 1;
  public override void EndedObjectInit()
  {
    AI__TransportInit();

    ROTATE_SPEED = 4f;
    currentMoveSpeed = WALK_SPEED;

    cleanerSpeed = variableManager.GetSpeedHarvesterCleaner();

    On();
    isInit = true;
    isForgetTarget = true;
  }

  void Update()
  {
    if (!isReady || !isInit)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    if (CheckDead())
      return;

    if (isDead)
      return;

    Vector3 movement = Vector3.zero;

    if (!isOn)
      return;

    movement = TransportDefaultBehavior(movement);
    ApplyMovement(movement);
  }

  public bool AddFuelFromCleaner()
  {
    if (fuel >= FUEL_MAX)
      return false;

    fuel += cleanerSpeed;
    if (fuel > FUEL_MAX)
      fuel = FUEL_MAX;

    repaintFuelMarker();

    return true;
  }

  public override void Reset()
  {
    base.Reset();
    fuel = (int)(FUEL_MAX * 0.2f);
    repaintFuelMarker();
  }

  public override float GetCost(EMapObjectType enemyType)
  {
    return variableManager.GetTargetCost(GetMapObjectType(), enemyType);
  }

  protected override void DoIt(string operationName)
  {
    Debug.Log("DoIt >>> " + operationName);

    Vector3 position = transform.position;
    switch (operationName)
    {
      case "fuelfull":
        fuel = FUEL_MAX;
        repaintFuelMarker();
        break;
      case "finish":
        levelController.IAmFinished(this);
        isDead = true;
        gameObject.SetActive(false);
        break;
    }
  }
}
