﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI__Golem : AI__Enemy
{
  public SnowWave snowWave;
  public SnowWave snowWaveLine;
  protected GolemBlood bloodController;

  protected float attackKickMomentTimer = 0f;

  public override void EndedObjectInit()
  {
    EnemyInit();

    SHOW_PART_DELAY = 0.5f;
    ANIMATION_LENGTH_ATTACK = 9.233f;
    ANIMATION_LENGTH_DEAD = 7.400f;
    ANIMATION_LENGTH_DEAD_KICK_MOMENT = 5.200f;
    ANIMATION_LENGTH_ATTACK_KICK_MOMENT = 3.0f;
    PAUSE_BETWEEN_ATTACK = 1.0f;
    DEAD_DURATION = 4.0f;
    ATTACK_DISTANCE = 20.0f;
    SEE_ANGLE = 360.0f;
    ATTACK_ANGLE = 20.0f;
    ROTATE_SPEED = 4.0f;
    DEATH_COUNT = 2;

    currentMoveSpeed = WALK_SPEED;

    bloodController = GetComponentInChildren<GolemBlood>();
    snowWave.ChangePosition(new Vector3(0, 0.045f, 0.231f));

    animator.SetBool(ANIMATOR_PARAM_DEAD, false);

    isInit = true;

    parts = GetComponentsInChildren<EnemyPart>();
    foreach (EnemyPart part in parts)
    {
      part.SetEnemyController(this);
      part.SetState(EnemyPart.ENEMY_PART_STATE_100);
    }

    //UpdateEnemyPartStates();
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    if (attackKickMomentTimer > 0f)
    {
      attackKickMomentTimer -= Time.deltaTime;
      if (attackKickMomentTimer <= 0f)
      {
        attackKickMomentTimer = 0f;
        KickMoment();
      }
    }

    CheckHeroDamaging();
    Vector3 movement = Vector3.zero;
    movement = AttackingDefaultBehavior(movement);
    ApplyMovement(movement);
  }

  protected override Vector3 AttackingDefaultBehavior(Vector3 movement)
  {
    CheckAttackTarget();
    CheckSearchTarget();

    if (IsAttackTargetDead())
    {
      StopAttack();
      return movement;
    }

    if (CheckDying() || CheckDead() || CheckAttack() || CheckPauseBetweenAttack())
    {
      return movement;
    }

    movement = MovingDefaultBehavior(movement);

    if (IsReadyToAttack())
    {
      Attack();
    }

    return movement;
  }

  protected override void Attack()
  {
    //Debug.Log("AI__Golem.Attack()");
    attackTimer = ANIMATION_LENGTH_ATTACK;
    animator.SetBool(ANIMATOR_PARAM_ATTACK, true);
    animator.SetFloat(ANIMATOR_PARAM_SPEED, 0);
    attackKickMomentTimer = ANIMATION_LENGTH_ATTACK_KICK_MOMENT;
    //StartCoroutine(KickMoment(ANIMATION_LENGTH_ATTACK_KICK_MOMENT));
  }

  protected void KickMoment()
  {
    if (attackTimer > 0)
    {
      if ((attackTarget != null && attackTarget.GetObjectRadius() >= ATTACK_DISTANCE) 
        || GetDistanceToMoveTarget() > ATTACK_DISTANCE / 2)
        snowWaveLine.Run();
      else
        snowWave.Run();
      audioObject.Play(0);
    }
  }

  protected override void DyingKick()
  {
    /*foreach (IEnemyPart part in parts)
      part.Hide();*/
    //bloodController.Play();
  }

  protected override bool CheckDead()
  {
    if (deadTimer > 0)
    {
      deadTimer -= Time.deltaTime;
      if (deadTimer <= 0)
      {
        isDead = true;
        deadTimer = 0;
        IAmDead();
        return false;
      }
      Vector3 position = this.transform.position;
      position.y -= Time.deltaTime;
      this.transform.position = position;
      return true;
    }
    return false;
  }

  public override void Damage(GameObject other, EnemyPartTagEnum partTag, int damage, int partHp)
  {
    if (hp == 0)
      return;

    hp -= damage;
    if (hp < 0)
      hp = 0;

    //UpdateEnemyPartStates();

    if (hp == 0)
    {
      die();
    }
    else
    {
      CheckSourceDamage(other);
    }
  }

  public override void Ready()
  {
    base.Ready();

    animator.SetBool(ANIMATOR_PARAM_DEAD, isDead);

    currentPartsState = EnemyPart.ENEMY_PART_STATE_100;
    foreach (EnemyPart part in parts)
    {
      part.SetState(EnemyPart.ENEMY_PART_STATE_100);
    }
  }

  public override void Reset()
  {
    base.Reset();
    animator.SetBool(ANIMATOR_PARAM_DEAD, isDead);
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    snowWave.SetPause(value);
    snowWaveLine.SetPause(value);
  }
}
