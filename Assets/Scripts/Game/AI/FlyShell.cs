﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlyShell : MonoBehaviour
{
  public ParticleSystem explosion;

  protected AudioObject audioObject;

  bool isShoted = false;
  bool isBoomed = false;
  bool isPaused = false;
  IShellOwner owner;

  void Awake()
  {
    audioObject = GetComponent<AudioObject>();
    Utils.SetCollision4Particles(explosion, Const.TAG_SNOW_WAVE_SMALL);
  }

  void Update()
  {
    if (isPaused || isBoomed)
      return;

    if (isShoted)
    {
      Vector3 newPosition = transform.position;
      newPosition += Vector3.down * 2;
      transform.position = newPosition;

      if (transform.position.y <= 0.5f)
      {
        Boom();
        StartCoroutine(Die(2f));
        return;
      }
    }
  }

  void Boom()
  {
    isBoomed = true;
    GetComponent<Renderer>().enabled = false;
    explosion.Play();
    audioObject.Play(0);
  }

  IEnumerator Die(float delay)
  {
    yield return new WaitForSeconds(delay);

    GetComponent<Renderer>().enabled = false;
    isShoted = false;
    if (owner != null)
      owner.OnShellDead(gameObject);
    Destroy(gameObject);
  }

  public void Shot(IShellOwner owner)
  {
    isShoted = true;
  }

  public void SetPause(bool value)
  {
    isPaused = value;
    if (value)
    {
      explosion.Pause();
    }
    else
    {
      explosion.Play();
    }
    audioObject.SetPause4All(value);
  }
}