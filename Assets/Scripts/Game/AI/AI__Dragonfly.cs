﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI__Dragonfly : AI__Enemy
{
  public Transform Body;
  public Transform Wings;
  public ParticleSystem Arrow;
  public ParticleSystem Bricks;

  private Color partDefaultColor;

  void Awake()
  {
    Utils.SetCollision4Particles(Arrow, Const.TAG_DRAGONFLY_ARROW);
  }

  public override void EndedObjectInit()
  {
    EnemyInit();
    renderers = Body.GetComponentsInChildren<Renderer>();

    ANIMATION_LENGTH_ATTACK = 1f;
    ANIMATION_LENGTH_DEAD = 1f;
    ANIMATION_LENGTH_DEAD_KICK_MOMENT = 0f;
    ANIMATION_LENGTH_ATTACK_KICK_MOMENT = 0f;
    PAUSE_BETWEEN_ATTACK = 0f;
    DEAD_DURATION = 4.0f;
    ATTACK_DISTANCE = 10.0f;
    SEE_ANGLE = 360.0f;
    ATTACK_ANGLE = 360.0f;
    ROTATE_SPEED = 10f;

    partDefaultColor = renderers[0].material.color;
    currentMoveSpeed = WALK_SPEED;

    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    Vector3 movement = Vector3.zero;
    movement = AttackingDefaultBehavior(movement);
    ApplyMovement(movement);
  }

  private void LateUpdate()
  {
    if (!isReady || !isInit || isDying || isDead)
      return;

    if (isPaused)
      return;
  }

  protected override Vector3 AttackingDefaultBehavior(Vector3 movement)
  {
    CheckAttackTarget();
    CheckSearchTarget();

    if (IsAttackTargetDead())
    {
      StopAttack();
      return movement;
    }

    if (CheckDying() || CheckDead() || CheckAttack())
    {
      return movement;
    }

    movement = MovingDefaultBehavior(movement);

    if (IsReadyToAttack())
    {
      Attack();
    }

    return movement;
  }

  protected override void Attack()
  {
    attackTimer = ANIMATION_LENGTH_ATTACK;
    Arrow.transform.LookAt(attackTarget.GetGameObject().transform);
    Arrow.Play();
    audioObject.Play(1);
  }

  public override void Ready()
  {
    base.Ready();
    renderers = Body.GetComponentsInChildren<Renderer>();
    audioObject.Play(0, true);
  }

  public override void Reset()
  {
    base.Reset();
    Body.gameObject.SetActive(true);
    Wings.gameObject.SetActive(true);
  }

  protected override void die()
  {
    if (isDying)
      return;

    hp = 0;

    isDying = true;
    StopAttack();
    dyingTimer = ANIMATION_LENGTH_DEAD;
    SetEnabledForAllColliders(false);
    Body.gameObject.SetActive(false);
    Wings.gameObject.SetActive(false);

    characterController.enabled = false;
    audioObject.Stop(0);
    Bricks.Play();
  }

  protected override void StopAttack()
  {
    attackTimer = 0;
  }

  private void OnParticleCollision(GameObject other)
  {
    if (isDying)
      return;

    if (hp <= 0)
      return;

    if (other.tag.Equals(Const.TAG_FIRE)
       || other.tag.Equals(Const.TAG_PLASMA))
    {
      CheckSourceDamage(other);
    }

    SetDamage(GetDamage4Enemy(other));
  }

  protected virtual void SetDamage(int damage)
  {
    if (hp == 0)
      return;

    hp -= damage;
    if (hp < 0)
      hp = 0;

    //Debug.Log(">>> damage=" + damage);

    if (hp > 0)
    {
      StartCoroutine(blink());
    }
    else
    {
      die();
    }
  }

  private IEnumerator blink()
  {
    foreach (Renderer renderer in renderers)
      renderer.material.color = Color.red;
    yield return new WaitForSeconds(0.1f);
    foreach (Renderer renderer in renderers)
      renderer.material.color = partDefaultColor;
  }

  protected override void HitWithTransport(ITransport iTransport)
  {
    //Debug.Log("HitWithTransport!");
    if (iTransport.IsMovingNow())
    {
      SetDamage(10);
    }
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    animator.enabled = !value;
    FireSetPause(value);
  }

  public void FireSetPause(bool value)
  {
    if (!Arrow.IsAlive())
      return;

    if (value)
    {
      Arrow.Pause();
    }
    else
    {
      Arrow.Play();
    }

    audioObject.SetPause4All(value);
  }
}
