﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI__Centaur : AI__Enemy
{
  public Transform Main;
  public Animator handsAnimator;

  public SnowWave snowWave;

  public ParticleSystem Fire;

  private Color partDefaultColor;

  void Awake()
  {
    renderers = GetComponentsInChildren<Renderer>();
    Utils.SetCollision4Particles(Fire, Const.TAG_CENTAUR_ARROW);
  }

  public override void EndedObjectInit()
  {
    EnemyInit();

    SHOW_PART_DELAY = 0.1f;
    ANIMATION_LENGTH_ATTACK = 0.417f;
    ANIMATION_LENGTH_ATTACK_KICK_MOMENT = 0.200f;
    ANIMATION_LENGTH_DEAD = 0.583f;
    PAUSE_BETWEEN_ATTACK = 1.0f;
    DEAD_DURATION = 4.0f;
    ATTACK_DISTANCE = 24f;
    SEE_ANGLE = 360.0f;
    ATTACK_ANGLE = 45.0f;
    ROTATE_SPEED = 4.0f;

    currentMoveSpeed = WALK_SPEED;

    partDefaultColor = renderers[0].material.color;

    isInit = true;

    parts = GetComponentsInChildren<EnemyPart>();
    foreach (EnemyPart part in parts)
    {
      part.SetEnemyController(this);
      part.SetState(EnemyPart.ENEMY_PART_STATE_100);
    }

    //UpdateEnemyPartStates();
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    CheckHeroDamaging();
    Vector3 movement = Vector3.zero;
    movement = AttackingDefaultBehavior(movement);
    ApplyMovement(movement);
  }

  protected override Vector3 AttackingDefaultBehavior(Vector3 movement)
  {
    CheckAttackTarget();
    CheckSearchTarget();

    if (IsAttackTargetDead())
    {
      StopAttack();
      return movement;
    }

    if (CheckDying() || CheckDead() || CheckAttack() || CheckPauseBetweenAttack())
    {
      CheckKick();
      return movement;
    }

    movement = MovingDefaultBehavior(movement);

    if (IsReadyToAttack())
    {
      Attack();
    }

    return movement;
  }

  protected bool CheckKick()
  {
    if (kickTimer > 0)
    {
      kickTimer -= Time.deltaTime;
      if (kickTimer <= 0)
      {
        kickTimer = 0;
        //Debug.Log("AI__Scorpion.CheckKick()-1");
        if (!attackTarget.IsDead())
        {
          //Debug.Log("AI__Scorpion.CheckKick()-2");
          Vector3 position = transform.position;
          Vector3 targetPosition = attackTarget.GetPosition();
          float targetDist = GetDistanceToMoveTarget();
          Vector3 directionToTarget = targetPosition - position;
          float angleToTarget = Vector3.Angle(transform.forward, directionToTarget);
          if (targetDist <= ATTACK_DISTANCE && angleToTarget < ATTACK_ANGLE)
          {
            //Debug.Log("AI__Scorpion.CheckKick()-3");
            attackTarget.Attack(transform.position, variableManager.GetDamage(GetMapObjectType()));
          }
        }
        return false;
      }
      return true;
    }
    return false;
  }

  protected override void Attack()
  {
    //Debug.Log("AI__Scorpion.Attack()");
    if (handsAnimator.GetBool(ANIMATOR_PARAM_ATTACK))
      return;

    FastRotateToPosition(attackTarget.GetPosition());
    handsAnimator.SetBool(ANIMATOR_PARAM_ATTACK, true);
    attackTimer = ANIMATION_LENGTH_ATTACK;
    kickTimer = ANIMATION_LENGTH_ATTACK_KICK_MOMENT;
    audioObject.Play(0);
    Fire.Play();
  }

  protected override void StopAttack()
  {
    handsAnimator.SetBool(ANIMATOR_PARAM_ATTACK, false);
    //Debug.Log("AI__Enemy.StopAttack()");
  }

  private void OnParticleCollision(GameObject other)
  {
    if (isDying)
      return;

    if (hp <= 0)
      return;

    if (other.tag.Equals(Const.TAG_FIRE)
      || other.tag.Equals(Const.TAG_PLASMA))
    {
      CheckSourceDamage(other);
    }

    SetDamage(GetDamage4Enemy(other));
  }

  protected virtual void SetDamage(int damage)
  {
    if (hp == 0)
      return;

    hp -= damage;
    if (hp < 0)
      hp = 0;

    //UpdateEnemyPartStates();

    if (hp > 0)
    {
      StartCoroutine(blink());
    }
    else
    {
      die();
    }
  }

  protected override void die()
  {
    if (isDying)
      return;

    hp = 0;

    isDying = true;
    StopAttack();
    dyingTimer = ANIMATION_LENGTH_DEAD;
    SetEnabledForAllColliders(false);
    SetEnabledForRenderers(Main, false);
    characterController.enabled = false;
    snowWave.Run();
    audioObject.Play(1);
  }

  private IEnumerator blink()
  {
    foreach (Renderer renderer in renderers)
      renderer.material.color = Color.red;
    yield return new WaitForSeconds(0.1f);
    foreach (Renderer renderer in renderers)
      renderer.material.color = partDefaultColor;
  }

  protected override void HitWithTransport(ITransport iTransport)
  {
    //Debug.Log("HitWithTransport!");
    if (iTransport.IsMovingNow())
    {
      SetDamage(10);
    }
  }

  public override void Ready()
  {
    base.Ready();

    currentPartsState = EnemyPart.ENEMY_PART_STATE_100;
    foreach (EnemyPart part in parts)
    {
      part.SetState(EnemyPart.ENEMY_PART_STATE_100);
    }
  }

  public override void Reset()
  {
    base.Reset();
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    if (handsAnimator != null)
      handsAnimator.enabled = !value;
    FireSetPause(value);
    snowWave.SetPause(value);
  }

  public void FireSetPause(bool value)
  {
    if (!Fire.IsAlive())
      return;

    if (value)
    {
      Fire.Pause();
    }
    else
    {
      Fire.Play();
    }

    audioObject.SetPause4All(value);
  }
}
