﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTarget : AI__MoveTarget
{
  protected Vector3 position;

  public MoveTarget(Vector3 position)
  {
    this.position = position;
  }

  public int GetId()
  {
    return 0;
  }

  public int GetObjectRadius()
  {
    return 1;
  }

  public int GetMapObjectType()
  {
    return 0;
  }

  public Vector3 GetPosition()
  {
    return position;
  }

  public GameObject GetGameObject()
  {
    return null;
  }

  public bool IsFlyingObject()
  {
    return false;
  }
}
