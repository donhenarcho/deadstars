﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MapObject;

public interface AI__AttackTarget : AI__MoveTarget
{
  float GetCost(EMapObjectType enemyType);
  bool IsAvailableForAttack();
  void Attack(Vector3 enemyPosition, int damage);
  bool IsDead();
  bool IsHero();
}
