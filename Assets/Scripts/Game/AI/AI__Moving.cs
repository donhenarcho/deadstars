﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AI__Moving : MapObject, IMovingObject
{
  protected const int DEVIATION_DIRECTION_COUNT_MAX = 5;
  protected float WALK_SPEED = 0f;
  protected float ROTATE_SPEED = 0f;

  protected CharacterController characterController;

  protected bool isOn = false;

  protected float currentMoveSpeed = 0f;

  protected bool isStalker = false;
  protected AI__MoveTarget moveTarget;
  protected float moveTargetDistance = 0f;

  protected bool moving = false;
  protected float moveStopTimer = 0;

  protected Vector3 directionToTarget = Vector3.zero;
  protected Vector3 lastDirectionToTarget = Vector3.zero;

  protected bool isBackTo = false;
  protected Vector3 isBackToDirection;
  protected float isBackToTimer = 0;

  protected float whatToDoNextTimer = 0;

  protected bool deviationRequired = false;
  protected Vector3 deviationDirection = Vector3.zero;
  protected int deviationDirectionCoefficient = 1;
  protected int deviationDirectionCount = 0;
  protected int deviationValue = 0;
  protected float deviationTimer = 0;

  protected Queue<Vector3> path;

  protected bool isWaiting = false;
  protected float waitingTimer = 0f;

  protected bool isForgetTarget = false;

  protected float deadTimer = 0;

  protected void AI__MovingInit()
  {
    Init();
    characterController = GetComponentInChildren<CharacterController>();
    path = new Queue<Vector3>();
  }

  protected virtual void OnControllerColliderHitMovingDefault(ControllerColliderHit hit)
  {
    //Debug.Log("AI__Moving.OnControllerColliderHitMovingDefault()");

    MapObject mapObject = hit.collider.gameObject.GetComponentInChildren<MapObject>();
    if (mapObject == null)
      mapObject = hit.collider.gameObject.GetComponentInParent<MapObject>();

    if (mapObject == null)
      return;

    //Debug.Log("HIT >> " + mapObject.GetMapObjectType());

    switch (mapObject.GetMapObjectType())
    {
      case EMapObjectType.TYPE_TREE:
        Tree tree = mapObject.gameObject.GetComponent<Tree>();
        tree.Boom(transform.forward);
        return;
    }

    if (!deviationRequired)
    {
      /*AI__Moving aIMoving = mapObject.GetComponent<AI__Moving>();
      if (aIMoving != null && !aIMoving.IsWaiting())
      {
        PleaseWait();
        return;
      }*/

      deviationValue = mapObject.GetObjectRadius() + GetObjectRadius();
      Vector3 dirPerp = new Vector3(hit.normal.z, 0, -hit.normal.x);
      deviationDirection = Vector3.Project(transform.forward, dirPerp);

      deviationDirectionCount++;
      if (deviationDirectionCount >= DEVIATION_DIRECTION_COUNT_MAX)
      {
        deviationDirectionCount = 0;
        deviationDirectionCoefficient *= -1;
      }
      deviationDirection *= deviationDirectionCoefficient;

      deviationTimer = 4 / (WALK_SPEED / 2);
      deviationRequired = true;
    }
  }

  protected virtual Vector3 MovingDefaultBehavior(Vector3 movement)
  {
    if (CheckDead())
    {
      //Debug.Log("AI__Moving.CheckDead()");
      return movement;
    }

    if (isDead)
      return movement;

    if (CheckWaiting())
    {
      //Debug.Log("AI__Moving.CheckWaiting()");
      return movement;
    }

    CheckBehavior();

    if (CheckBackTo())
    {
      movement = GetMovementFromDirection(movement, isBackToDirection);
      //Debug.Log("AI__Moving.CheckBackTo()");
      return movement;
    }

    if (CheckDeviation() && deviationRequired)
    {
      movement = GetMovementFromDirection(movement, deviationDirection);
      //Debug.Log("AI__Moving.CheckDeviation()");
      return movement;
    }

    //Debug.Log("moveTarget=" + moveTarget.GetPosition());
    //Debug.Log("GetDistanceToMoveTarget()=" + GetDistanceToMoveTarget());
    
    if (moveTarget != null)
    {
      if (GetDistanceToMoveTarget() > moveTargetDistance)
      {
        Vector3 direction = GetMoveTargetCorrectPosition() - transform.position;
        movement = GetMovementFromDirection(movement, direction);
        RotateTo(direction);
        //Debug.Log("AI__Moving.Moving()");
        return movement;
      }
      else
      {
        ReachMoveTargetEvent(moveTarget);

        if (path.Count > 0)
        {
          moveTarget = new MoveTarget(path.Dequeue());
        }
        else if (isForgetTarget)
        {
          moveTarget = null;
        }
      }
    }
    else if (path.Count > 0)
    {
      moveTarget = new MoveTarget(path.Dequeue());
    }

    return movement;
  }

  private Vector3 GetMovementFromDirection(Vector3 movement, Vector3 direction)
  {
    direction.y = 0;
    if (direction != Vector3.zero)
      RotateTo(direction);
    direction.Normalize();
    movement = direction * currentMoveSpeed;
    movement = Vector3.ClampMagnitude(movement, currentMoveSpeed);
    return movement;
  }

  protected void ApplyMovement(Vector3 movement)
  {
    if (withGravity)
    {
      movement.y = Const.WORLD_GRAVITY * 50 * Time.deltaTime;
    }
    
    movement *= Time.deltaTime;
    characterController.Move(movement);

    if (characterController.velocity != Vector3.zero)
    {
      moving = true;
      moveStopTimer = .2f;
    }

    if (moveStopTimer > 0)
    {
      moveStopTimer -= Time.deltaTime;
      if (moveStopTimer <= 0)
      {
        moving = false;
        moveStopTimer = 0;
      }
    }
  }

  protected void RotateTo(
      Vector3 _direction
    )
  {
    Quaternion direction = Quaternion.LookRotation(_direction);
    transform.rotation = Quaternion.Lerp(
      transform.rotation,
      direction,
      ROTATE_SPEED * Time.deltaTime);
  }

  protected void RotateToObject(
      GameObject _object
    )
  {
    Quaternion direction = Quaternion.LookRotation(_object.transform.position - transform.position);
    direction.y = 0;
    transform.rotation = Quaternion.Lerp(
      transform.rotation,
      direction,
      ROTATE_SPEED * Time.deltaTime);
  }

  protected void FastRotateToObject(
      GameObject _object
    )
  {
    Vector3 direction = _object.transform.position - transform.position;
    direction.y = 0;
    transform.rotation = Quaternion.LookRotation(direction);
  }

  protected void FastRotateToPosition(
      Vector3 _position
    )
  {
    Vector3 direction = _position - transform.position;
    direction.y = 0;
    transform.rotation = Quaternion.LookRotation(direction);
  }

  protected bool CheckDeviation()
  {
    if (deviationTimer > 0)
    {
      deviationTimer -= Time.deltaTime;
      if (deviationTimer <= 0)
      {
        deviationTimer = 0;
        deviationRequired = false;
        return false;
      }
      return true;
    }
    return false;
  }

  protected bool CheckWaiting()
  {
    if (waitingTimer > 0)
    {
      waitingTimer -= Time.deltaTime;
      if (waitingTimer <= 0)
      {
        waitingTimer = 0;
        isWaiting = false;
        return false;
      }
      return true;
    }
    return false;
  }

  public void BackTo()
  {
    if (isBackTo)
      return;

    isBackTo = true;
    isBackToDirection = transform.forward * -1f;
    isBackToTimer = 0.4f;
  }

  protected bool CheckBackTo()
  {
    if (isBackToTimer > 0)
    {
      isBackToTimer -= Time.deltaTime;
      if (isBackToTimer <= 0)
      {
        isBackTo = false;
        return false;
      }
      return true;
    }
    return false;
  }

  public void SetMoveTarget(AI__MoveTarget moveTarget, float distance)
  {
    this.moveTarget = moveTarget;
    moveTargetDistance = distance;
  }

  public bool HaveMoveTarget()
  {
    return moveTarget != null;
  }

  public virtual void ReachMoveTargetEvent(AI__MoveTarget moveTarget)
  {
    //Debug.Log("AI__Moving.ReachMoveTargetEvent()");
  }

  public bool ReachedMoveTarget()
  {
    return GetDistanceToMoveTarget() <= moveTargetDistance;
  }

  protected float GetDistanceToMoveTarget()
  {
    if (moveTarget == null)
      return -1f;

    float distance = Vector3.Distance(transform.position, GetMoveTargetCorrectPosition()) - moveTarget.GetObjectRadius();
    //Debug.Log(">>> dist=" + distance);
    return distance;
  }

  protected Vector3 GetMoveTargetCorrectPosition()
  {
    Vector3 position = moveTarget.GetPosition();
    if (isFlyingObject)
    {
      position.y += Const.FLYHEIGHT;
    }
    return position;
  }

  public virtual void On()
  {
    isOn = true;
  }

  public virtual void Off()
  {
    isOn = false;
  }

  public virtual bool Switch()
  {
    if (isOn)
      Off();
    else
      On();
    return IsOn();
  }

  public bool IsOn()
  {
    return isOn;
  }

  public void SetPath(Queue<Vector3> path)
  {
    /*if (path != null)
    {
      Debug.Log("=========");
      Debug.Log("SetPath()=" + path.Count);
      foreach (Vector3 point in path)
        Debug.Log("point=" + point);
      Debug.Log("=========");
    }*/
    this.path = path;
    moveTargetDistance = 0.5f;
  }

  public void ResetPath()
  {
    path = new Queue<Vector3>();
  }

  public bool IsPathEnded()
  {
    return path.Count == 0;
  }

  public bool IsWaiting()
  {
    return isWaiting;
  }

  public void PleaseWait()
  {
    if (isWaiting)
      return;

    waitingTimer = 5f;
    isWaiting = true;
  }

  public void SetStalkerMode(bool isStalker)
  {
    this.isStalker = isStalker;
  }

  protected virtual bool CheckDead()
  {
    if (deadTimer > 0)
    {
      deadTimer -= Time.deltaTime;
      if (deadTimer <= 0)
      {
        deadTimer = 0;
        IAmDead();
        return false;
      }
      return true;
    }
    return false;
  }

  public override void Reset()
  {
    base.Reset();
    if (characterController != null)
      characterController.enabled = false;
  }

  public override void Ready()
  {
    base.Ready();
    if (characterController != null)
      characterController.enabled = true;
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
  }

  protected void CheckBehavior()
  {
    if (waitNextObjectActionTimer > 0)
    {
      waitNextObjectActionTimer -= Time.deltaTime;
      return;
    }

    if (moveTarget != null)
      return;

    if (behavior != null && behavior.Count > 0)
    {
      ObjectAction objectAction = behavior.Dequeue();
      objectAction.Print();

      switch (objectAction.Type)
      {
        case ObjectAction.TYPE_GOTO:
          moveTarget = new MoveTarget(objectAction.GetPoint());
          break;
        case ObjectAction.TYPE_WAIT:
          waitNextObjectActionTimer = objectAction.GetDelay();
          break;
        case ObjectAction.TYPE_DOIT:
          DoIt(objectAction.GetOperationName());
          break;
        case ObjectAction.TYPE_PUTO:
          SetPosition(objectAction.GetPoint());
          break;
      }
    }
  }
}
