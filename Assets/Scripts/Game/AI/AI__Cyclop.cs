﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI__Cyclop : AI__Enemy
{
  public Transform Head;
  public ParticleSystem Fire;
  public ParticleSystem Bricks;
  bool isFiring = false;

  void Awake()
  {
    Utils.SetCollision4Particles(Fire, Const.TAG_CYCLOP_FIRE);
    renderers = GetComponentsInChildren<Renderer>();
  }

  public override void EndedObjectInit()
  {
    EnemyInit();

    SHOW_PART_DELAY = 0.5f;
    ANIMATION_LENGTH_ATTACK = 9.233f;
    ANIMATION_LENGTH_DEAD = 7.400f;
    ANIMATION_LENGTH_DEAD_KICK_MOMENT = 5.200f;
    ANIMATION_LENGTH_ATTACK_KICK_MOMENT = 3.0f;
    PAUSE_BETWEEN_ATTACK = 1.0f;
    DEAD_DURATION = 4.0f;
    ATTACK_DISTANCE = 20.0f;
    SEE_ANGLE = 360.0f;
    ATTACK_ANGLE = 360.0f;
    ROTATE_SPEED = 4.0f;
    DEATH_COUNT = 2;

    currentMoveSpeed = WALK_SPEED;

    parts = GetComponentsInChildren<EnemyPart>();
    foreach (EnemyPart part in parts)
    {
      part.SetEnemyController(this);
      part.SetState(EnemyPart.ENEMY_PART_STATE_100);
    }

    animator.SetBool(ANIMATOR_PARAM_DEAD, false);

    FireOff();
    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    CheckHeroDamaging();
    Vector3 movement = Vector3.zero;
    movement = AttackingDefaultBehavior(movement);
    ApplyMovement(movement);
  }

  protected override void AfterLateUpdate()
  {
    if (attackTarget != null)
    {
      Head.LookAt(attackTarget.GetGameObject().transform);
    }
  }

  protected override Vector3 AttackingDefaultBehavior(Vector3 movement)
  {
    CheckAttackTarget();
    CheckSearchTarget();

    if (IsAttackTargetDead())
    {
      StopAttack();
      return movement;
    }

    if (CheckDying() || CheckDead())
    {
      return movement;
    }

    movement = MovingDefaultBehavior(movement);

    if (IsReadyToAttack())
    {
      Attack();
    }
    else
    {
      StopAttack();
    }

    return movement;
  }

  protected override void Attack()
  {
    FireOn();
  }

  protected override void StopAttack()
  {
    attackTimer = 0;
    animator.SetBool(ANIMATOR_PARAM_ATTACK, false);
    FireOff();
  }

  public void FireOn()
  {
    isFiring = true;
    Fire.enableEmission = true;
    audioObject.Play(0, true);
  }

  public void FireOff()
  {
    isFiring = false;
    Fire.enableEmission = false;
    audioObject.Stop(0);
  }

  public bool IsFiring()
  {
    return Fire.enableEmission;
  }

  protected virtual void die()
  {
    if (isDying)
      return;

    MyHpIsZero();
    isDying = true;
    StopAttack();
    animator.SetBool(ANIMATOR_PARAM_DEAD, true);
    dyingTimer = ANIMATION_LENGTH_DEAD;
    dyingKickTimer = ANIMATION_LENGTH_DEAD_KICK_MOMENT;
    SetEnabledForAllColliders(false);
    characterController.enabled = false;
    Head.gameObject.SetActive(false);
    Bricks.Play();
  }

  protected override bool CheckDead()
  {
    if (deadTimer > 0)
    {
      deadTimer -= Time.deltaTime;
      if (deadTimer <= 0)
      {
        isDead = true;
        deadTimer = 0;
        IAmDead();
        return false;
      }
      Vector3 position = this.transform.position;
      position.y -= Time.deltaTime;
      this.transform.position = position;
      return true;
    }
    return false;
  }

  public override void Damage(GameObject other, EnemyPartTagEnum partTag, int damage, int partHp)
  {
    if (hp == 0)
      return;

    hp -= damage;
    if (hp < 0)
      hp = 0;

    //UpdateEnemyPartStates();

    if (hp == 0)
    {
      die();
    }
    else
    {
      CheckSourceDamage(other);
    }
  }

  public override void Ready()
  {
    base.Ready();

    animator.SetBool(ANIMATOR_PARAM_DEAD, isDead);

    Head.gameObject.SetActive(true);

    currentPartsState = EnemyPart.ENEMY_PART_STATE_100;
    foreach (EnemyPart part in parts)
    {
      part.SetState(EnemyPart.ENEMY_PART_STATE_100);
    }
  }

  public override void Reset()
  {
    base.Reset();
    animator.SetBool(ANIMATOR_PARAM_DEAD, isDead);
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    if (value)
    {
      Fire.Pause();
    }
    else
    {
      Fire.Play();
    }
    audioObject.SetPause4All(value);
  }
}
