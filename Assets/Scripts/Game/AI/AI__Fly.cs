﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AI__Fly : AI__Enemy, IShellOwner
{
  public Transform Body;
  public Transform Wings;
  public ParticleSystem Bricks;
  public GameObject ShellPrefab;

  List<GameObject> shells;

  private Color partDefaultColor;

  void Awake()
  {
    //Utils.SetCollision4Particles(Arrow, Const.TAG_CENTAUR_ARROW);
  }

  public override void EndedObjectInit()
  {
    EnemyInit();
    renderers = Body.GetComponentsInChildren<Renderer>();

    ANIMATION_LENGTH_ATTACK = 4f;
    ANIMATION_LENGTH_DEAD = 1f;
    ANIMATION_LENGTH_DEAD_KICK_MOMENT = 0f;
    ANIMATION_LENGTH_ATTACK_KICK_MOMENT = 0f;
    PAUSE_BETWEEN_ATTACK = 0f;
    DEAD_DURATION = 4.0f;
    ATTACK_DISTANCE = 6.0f;
    SEE_ANGLE = 360.0f;
    ATTACK_ANGLE = 360.0f;
    ROTATE_SPEED = 10f;

    shells = new List<GameObject>();

    partDefaultColor = renderers[0].material.color;
    currentMoveSpeed = WALK_SPEED;

    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    Vector3 movement = Vector3.zero;
    movement = AttackingDefaultBehavior(movement);
    ApplyMovement(movement);
  }

  private void LateUpdate()
  {
    if (!isReady || !isInit || isDying || isDead)
      return;

    if (isPaused)
      return;
  }

  protected override Vector3 AttackingDefaultBehavior(Vector3 movement)
  {
    CheckAttackTarget();
    CheckSearchTarget();

    if (IsAttackTargetDead())
    {
      StopAttack();
      return movement;
    }

    if (CheckDying() || CheckDead() || CheckAttack())
    {
      return movement;
    }

    movement = MovingDefaultBehavior(movement);

    if (IsReadyToAttack())
    {
      Attack();
    }

    return movement;
  }

  protected override void Attack()
  {
    attackTimer = ANIMATION_LENGTH_ATTACK;
    GameObject gameObject = Instantiate(ShellPrefab, transform.position, Quaternion.identity);
    gameObject.GetComponent<FlyShell>().Shot(this);
    shells.Add(gameObject);
  }

  public override void Ready()
  {
    base.Ready();
    renderers = Body.GetComponentsInChildren<Renderer>();
    audioObject.Play(0, true);
  }

  public override void Reset()
  {
    base.Reset();
    Body.gameObject.SetActive(true);
    Wings.gameObject.SetActive(true);
  }

  protected override void die()
  {
    if (isDying)
      return;

    hp = 0;

    isDying = true;
    StopAttack();
    dyingTimer = ANIMATION_LENGTH_DEAD;
    SetEnabledForAllColliders(false);
    Body.gameObject.SetActive(false);
    Wings.gameObject.SetActive(false);

    characterController.enabled = false;
    audioObject.Stop(0);
    Bricks.Play();
  }

  protected override void StopAttack()
  {
    attackTimer = 0;
  }

  private void OnParticleCollision(GameObject other)
  {
    if (isDying)
      return;

    if (hp <= 0)
      return;

    if (other.tag.Equals(Const.TAG_FIRE)
       || other.tag.Equals(Const.TAG_PLASMA))
    {
      CheckSourceDamage(other);
    }

    SetDamage(GetDamage4Enemy(other));
  }

  protected virtual void SetDamage(int damage)
  {
    if (hp == 0)
      return;

    hp -= damage;
    if (hp < 0)
      hp = 0;

    //Debug.Log(">>> damage=" + damage);

    if (hp > 0)
    {
      StartCoroutine(blink());
    }
    else
    {
      die();
    }
  }

  private IEnumerator blink()
  {
    foreach (Renderer renderer in renderers)
      renderer.material.color = Color.red;
    yield return new WaitForSeconds(0.1f);
    foreach (Renderer renderer in renderers)
      renderer.material.color = partDefaultColor;
  }

  protected override void HitWithTransport(ITransport iTransport)
  {
    //Debug.Log("HitWithTransport!");
    if (iTransport.IsMovingNow())
    {
      SetDamage(10);
    }
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    animator.enabled = !value;
    foreach (GameObject gameObject in shells)
    {
      if (gameObject != null)
        gameObject.GetComponent<StormShell>().SetPause(value);
    }
    audioObject.SetPause4All(value);
  }

  public void OnShellDead(GameObject shell)
  {
    shells.Remove(shell);
  }
}