﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AI__Transport : AI__Moving, IHaveFuel, AI__AttackTarget
{
  public Transform Main;
  public ParticleSystem bricks;
  protected Shield shield;
  protected UI__FuelInMarker fuelInMarker;

  protected float FUEL_OUT_FOR_MOVING_DELAY = 10f;
  protected float DEAD_DURATION = 10f;

  protected float rps = 80;
  protected IWheels wheels;

  protected FuelMarker fuelMarker;
  protected int FUEL_MAX = 100;
  protected int fuel = 0;
  protected float fuelTimer = 0;

  protected UI__EffectMarker effectMarker;

  protected void AI__TransportInit()
  {
    AI__MovingInit();

    effectMarker = GetComponentInChildren<UI__EffectMarker>();
    effectMarker.SetValue(0);

    shield = GetComponentInChildren<Shield>();
    fuelMarker = GetComponentInChildren<FuelMarker>();
    wheels = GetComponentInChildren<IWheels>();
    fuelInMarker = GetComponentInChildren<UI__FuelInMarker>();
    fuelInMarker.Off();

    transform.gameObject.layer = Const.LAYER_AI_TRANSPORT;
    Main.gameObject.layer = Const.LAYER_INVISIBLE_OBJECT;

    WALK_SPEED = variableManager.GetSpeed(GetMapObjectType());
    FUEL_MAX = variableManager.GetHP(GetMapObjectType());

    repaintFuelMarker();
  }

  void Update()
  {
    if (!isReady)
      return;
  }

  private void LateUpdate()
  {
    if (!isReady)
      return;

    if (isPaused)
    {
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      if (audioObject.IsPlaying(0))
        audioObject.Stop(0);
      return;
    }

    if (isDead)
    {
      if (wheels != null && wheels.IsGo())
        wheels.Stop();
      return;
    }

    if (wheels != null)
    {
      if (moving && !wheels.IsGo())
      {
        wheels.Go(rps);
      }
      else
      {
        wheels.Stop();
      }
    }

    if (audioObject != null)
    {
      if (!moving)
      {
        if (audioObject.IsPlaying(0))
          audioObject.Stop(0);
      }
      else if (!audioObject.IsPlaying(0))
        audioObject.Play(0, true);
    }
  }

  protected virtual Vector3 TransportDefaultBehavior(Vector3 movement)
  {
    if (IsEmpty())
    {
      return movement;
    }

    return MovingDefaultBehavior(movement);
  }

  private void OnControllerColliderHit(ControllerColliderHit hit)
  {
    MapObject mapObject = hit.collider.gameObject.GetComponentInChildren<MapObject>();
    if (mapObject == null)
      mapObject = hit.collider.gameObject.GetComponentInParent<MapObject>();

    if (mapObject == null)
      return;

    switch (mapObject.GetMapObjectType())
    {
      case EMapObjectType.TYPE_HERO:
        PleaseWait();
        return;
    }

    OnControllerColliderHitMovingDefault(hit);
  }

  protected void repaintFuelMarker()
  {
    fuelMarker.Repaint(fuel / (float)FUEL_MAX);
    fuelMarker.SetBlinking(IsEmpty());
  }

  public int FuelIn(int value)
  {
    if (IsFull() || value == 0)
      return value;

    //Debug.Log("AI__Transport.FuelIn() + " + value);

    fuel += value;

    int fuelReturn = 0;
    if (fuel > FUEL_MAX)
    {
      fuelReturn = fuel - FUEL_MAX;
      fuel = FUEL_MAX;
    }

    AfterFuelIn();
    fuelInMarker.Show();

    return fuelReturn;
  }

  public int FuelReturn(int value)
  {
    if (IsFull() || value == 0)
      return value;

    //Debug.Log("AI__Transport.FuelReturn() + " + value);

    fuel += value;

    int fuelReturn = 0;
    if (fuel > FUEL_MAX)
    {
      fuelReturn = fuel - FUEL_MAX;
      fuel = FUEL_MAX;
    }

    AfterFuelIn();

    return fuelReturn;
  }

  public int FuelOut()
  {
    //Debug.Log("AI__Transport.FuelOut()");
    if (fuel <= 0)
    {
      fuel = 0;
      return 0;
    }

    fuelTimer += Time.deltaTime;
    if (fuelTimer >= 1)
    {
      fuelTimer = 0;
      fuel = fuel > 0 ? fuel - 1 : 0;
      AfterFuelOut();
      return 1;
    }
    return 0;
  }

  public int FuelOut(int value)
  {
    //Debug.Log("AI__Transport::FuelOut(int value)");
    if (fuel <= 0)
    {
      fuel = 0;
      return 0;
    }

    fuel = fuel > 0 ? fuel - value : 0;
    if (fuel < 0)
    {
      value += fuel;
      fuel = 0;
    }

    AfterFuelOut();
    return value;
  }

  public void FillToMax()
  {
    fuel = FUEL_MAX;
  }

  public bool IsFull()
  {
    return fuel == FUEL_MAX;
  }

  public bool IsEmpty()
  {
    return fuel == 0;
  }

  public float GetFuelLeft()
  {
    return fuel / (FUEL_MAX / 100f);
  }

  public float GetCurrentFuelValue()
  {
    return fuel;
  }

  public float GetFuelMax()
  {
    return FUEL_MAX;
  }

  public virtual void AfterFuelIn()
  {
    repaintFuelMarker();
  }

  public virtual void AfterFuelOut()
  {
    repaintFuelMarker();
  }

  public virtual float GetCost(EMapObjectType enemyType)
  {
    return variableManager.GetTargetCost(GetMapObjectType(), enemyType);
  }

  public bool IsAvailableForAttack()
  {
    return !IsDead();
  }

  protected virtual void beforeDie()
  {

  }

  protected virtual void die()
  {
    beforeDie();
    wheels.Stop();
    isDead = true;
    SetEnabledForAllColliders(false);
    SetEnabledForRenderers(Main, false);
    bricks.Play();
    deadTimer = DEAD_DURATION;
    afterDie();
  }

  protected virtual void afterDie()
  {

  }

  public void Attack(Vector3 enemyPosition, int damage)
  {
    Debug.Log("Attack >>> " + damage);
    if (IsEmpty())
    {
      die();
    }
    else
    {
      FuelOut(damage);
      shield.Play(enemyPosition, GetObjectRadius());
      if (IsEmpty())
        die();
    }
  }

  public override void TrueKill()
  {
    Attack(Vector3.zero, FUEL_MAX);
  }

  private void OnParticleCollision(GameObject other)
  {
    if (IsDead())
      return;

    Attack(other.transform.position, GetDamage4Units(other));
  }

  public override SavedWorldObject GetSavedWorldObject()
  {
    SavedWorldObject savedLevelObject = base.GetSavedWorldObject();
    if (moveTarget != null)
      savedLevelObject.moveTargetId = moveTarget.GetId();
    savedLevelObject.fuel = fuel;
    return savedLevelObject;
  }

  public override SavedWorldObject SetSavedWorldObject(SavedWorldObject savedLevelObject)
  {
    savedLevelObject = base.SetSavedWorldObject(savedLevelObject);
    if (savedLevelObject.fuel >= 0)
      this.fuel = savedLevelObject.fuel;
    return savedLevelObject;
  }

  public override bool IsTarget()
  {
    return true;
  }

  public override void Reset()
  {
    base.Reset();
    fuel = FUEL_MAX;
    fuelTimer = 0;
    SetEnabledForAllColliders(true);
    SetEnabledForRenderers(Main, true);
  }

  protected override void OnUpdatedEffects()
  {
    effectMarker.SetValue(effects[Terraformer.EFFECT_SHIELD]);
  }
}
