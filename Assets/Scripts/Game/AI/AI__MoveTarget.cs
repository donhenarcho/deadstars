﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface AI__MoveTarget
{
  Vector3 GetPosition();
  int GetObjectRadius();
  int GetId();
  GameObject GetGameObject();
  bool IsFlyingObject();
}
