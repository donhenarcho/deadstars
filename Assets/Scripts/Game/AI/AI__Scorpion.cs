﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI__Scorpion : AI__Enemy
{
  private Color partDefaultColor;

  public override void EndedObjectInit()
  {
    EnemyInit();

    SHOW_PART_DELAY = 0.1f;
    ANIMATION_LENGTH_ATTACK = 1.033f;
    ANIMATION_LENGTH_ATTACK_KICK_MOMENT = 1.000f;
    ANIMATION_LENGTH_DEAD = 0.583f;
    PAUSE_BETWEEN_ATTACK = 1.0f;
    DEAD_DURATION = 4.0f;
    ATTACK_DISTANCE = 3.6f;
    SEE_ANGLE = 360.0f;
    ATTACK_ANGLE = 45.0f;
    ROTATE_SPEED = 4.0f;

    currentMoveSpeed = WALK_SPEED;

    partDefaultColor = renderers[0].material.color;
    animator.SetBool(ANIMATOR_PARAM_DEAD, false);

    isInit = true;

    parts = GetComponentsInChildren<EnemyPart>();
    foreach (EnemyPart part in parts)
    {
      part.SetEnemyController(this);
      part.SetState(EnemyPart.ENEMY_PART_STATE_100);
    }

    //UpdateEnemyPartStates();
  }

  void Awake()
  {
    renderers = GetComponentsInChildren<Renderer>();
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    CheckHeroDamaging();
    Vector3 movement = Vector3.zero;
    movement = AttackingDefaultBehavior(movement);
    ApplyMovement(movement);
  }

  protected override Vector3 AttackingDefaultBehavior(Vector3 movement)
  {
    CheckAttackTarget();
    CheckSearchTarget();

    if (IsAttackTargetDead())
    {
      StopAttack();
      return movement;
    }

    if (CheckDying() || CheckDead() || CheckAttack() || CheckPauseBetweenAttack())
    {
      CheckKick();
      return movement;
    }

    movement = MovingDefaultBehavior(movement);

    if (IsReadyToAttack())
    {
      Attack();
    }

    return movement;
  }

  protected bool CheckKick()
  {
    if (kickTimer > 0)
    {
      kickTimer -= Time.deltaTime;
      if (kickTimer <= 0)
      {
        kickTimer = 0;
        //Debug.Log("AI__Scorpion.CheckKick()-1");
        if (!attackTarget.IsDead())
        {
          //Debug.Log("AI__Scorpion.CheckKick()-2");
          Vector3 position = transform.position;
          Vector3 targetPosition = attackTarget.GetPosition();
          float targetDist = GetDistanceToMoveTarget();
          Vector3 directionToTarget = targetPosition - position;
          float angleToTarget = Vector3.Angle(transform.forward, directionToTarget);
          if (targetDist <= ATTACK_DISTANCE && angleToTarget < ATTACK_ANGLE)
          {
            //Debug.Log("AI__Scorpion.CheckKick()-3");
            attackTarget.Attack(transform.position, variableManager.GetDamage(GetMapObjectType()));
          }
        }
        return false;
      }
      return true;
    }
    return false;
  }

  protected override void Attack()
  {
    //Debug.Log("AI__Scorpion.Attack()");
    FastRotateToPosition(attackTarget.GetPosition());
    attackTimer = ANIMATION_LENGTH_ATTACK;
    kickTimer = ANIMATION_LENGTH_ATTACK_KICK_MOMENT;
    animator.SetBool(ANIMATOR_PARAM_ATTACK, true);
    animator.SetFloat(ANIMATOR_PARAM_SPEED, 0);

    audioObject.Play(0);
  }

  private void OnParticleCollision(GameObject other)
  {
    if (isDying)
      return;

    if (hp <= 0)
      return;

    if (other.tag.Equals(Const.TAG_FIRE) 
      || other.tag.Equals(Const.TAG_PLASMA))
    {
        CheckSourceDamage(other);
    }

    SetDamage(GetDamage4Enemy(other));
  }

  protected virtual void SetDamage(int damage)
  {
    if (hp == 0)
      return;

    hp -= damage;
    if (hp < 0)
      hp = 0;

    //UpdateEnemyPartStates();

    if (hp > 0)
    {
      StartCoroutine(blink());
    }
    else
    {
      die();
    }
  }

  private IEnumerator blink()
  {
    foreach (Renderer renderer in renderers)
      renderer.material.color = Color.red;
    yield return new WaitForSeconds(0.1f);
    foreach (Renderer renderer in renderers)
      renderer.material.color = partDefaultColor;
  }

  protected override void HitWithTransport(ITransport iTransport)
  {
    //Debug.Log("HitWithTransport!");
    if (iTransport.IsMovingNow())
    {
      SetDamage(10);
    }
  }

  public override void Ready()
  {
    base.Ready();
    animator.SetBool(ANIMATOR_PARAM_DEAD, isDead);

    currentPartsState = EnemyPart.ENEMY_PART_STATE_100;
    foreach (EnemyPart part in parts)
    {
      part.SetState(EnemyPart.ENEMY_PART_STATE_100);
    }

    foreach (Renderer renderer in renderers)
      renderer.material.color = partDefaultColor;
  }

  public override void Reset()
  {
    base.Reset();
    animator.SetBool(ANIMATOR_PARAM_DEAD, isDead);
  }
}
