﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AI__Attacking : AI__Moving
{
  protected float HERO_DAMAGING_DELAY = 1f;
  protected float HERO_NO_DAMAGING_DELAY_MIN = 10f;
  protected float HERO_NO_DAMAGING_DELAY_MAX = 60f;

  protected float ATTACK_ANGLE = 0f;
  protected float ATTACK_DISTANCE = 0f;
  protected float SEE_ANGLE = 0f;
  protected float SEE_DISTANCE = 0f;
  protected float PAUSE_BETWEEN_ATTACK = 0f;

  protected AI__AttackTarget attackTarget;

  protected float searchTargetTimer = 0;
  protected float attackTimer = 0;
  protected float kickTimer = 0;
  protected float betweenAttackTimer = 0;
  protected bool isBetweenAttack = false;
  protected bool isAttacking = false;

  protected bool isHeroDamaging = false;
  protected float heroDamagingTimer = 0f;
  protected float heroNoDamagingTimer = 0f;

  public bool CanAttackFlyingTargets = false;

  protected void AI__AttackingInit()
  {
    AI__MovingInit();
  }

  protected void CheckHeroDamaging()
  {
    if (isHeroDamaging)
    {
      heroDamagingTimer += Time.deltaTime;
      if (heroDamagingTimer >= HERO_DAMAGING_DELAY)
      {
        heroDamagingTimer = 0f;
        SearchAttackTarget(true);
        heroNoDamagingTimer = Random.Range(HERO_NO_DAMAGING_DELAY_MIN, HERO_NO_DAMAGING_DELAY_MAX);
        //Debug.Log("heroNoDamagingTimer=" + heroNoDamagingTimer);
        isHeroDamaging = false;
      }
    }
    else if (heroNoDamagingTimer > 0f)
    {
      heroNoDamagingTimer -= Time.deltaTime;
      if (heroNoDamagingTimer <= 0f)
      {
        heroNoDamagingTimer = 0f;
        SearchAttackTarget(true);
        //Debug.Log("heroNoDamagingTimer=0");
        //Debug.Log("SearchAttackTarget(true)");
      }
    }
  }

  protected void CheckSourceDamage(GameObject source)
  {
    if (source == null)
      return;

    IHero hero = source.GetComponentInParent<IHero>();
    if (attackTarget == null 
      || !attackTarget.Equals(source.GetComponentInParent<AI__AttackTarget>()))
    {
      isHeroDamaging = hero != null;
      if (!isHeroDamaging)
        heroDamagingTimer = 0f;
    }
    else
    {
      isHeroDamaging = false;
    }
  }

  protected List<AI__AttackTarget> GetPotentialTargets()
  {
    List<AI__AttackTarget> objects = new List<AI__AttackTarget>();
    Collider[] hitColliders = Physics.OverlapSphere(transform.position, SEE_DISTANCE, Const.LAYER_MASK_FOR_ENEMY_SEARCH);
    //Debug.Log("hitColliders.Length=" + hitColliders.Length);

    foreach (var hitCollider in hitColliders)
    {
      AI__AttackTarget target = hitCollider.GetComponent<AI__AttackTarget>();
      if (target != null)
      {
        //Debug.Log("---->" + target.GetPosition() + "/" + target.GetCost());
        objects.Add(target);
      }
    }

    //Debug.Log("objects.Count=" + objects.Count);

    return objects;
  }

  protected Dictionary<AI__AttackTarget, float> GetPotentialTargetsWithRatings()
  {
    Dictionary<AI__AttackTarget, float> targetWithRating = new Dictionary<AI__AttackTarget, float>();

    List<AI__AttackTarget> potentialTargets = GetPotentialTargets();

    /*Debug.Log("===");
    foreach (AI__AttackTarget target in potentialTargets)
      Debug.Log("---->" + target.GetPosition() + "/" + target.GetCost());
    Debug.Log("===");*/

    //Debug.Log("potentialTargets.Count=" + potentialTargets.Count);

    foreach (AI__AttackTarget target in potentialTargets)
    {
      if (!CanAttackFlyingTargets && target.IsFlyingObject())
        continue;

      if (variableManager.IsTargetOnlyHero() && !target.IsHero())
        continue;

      float rating = 0;
      
      if (target.IsAvailableForAttack() 
        && target.GetCost(GetMapObjectType()) > 0)
      {
        rating -= Vector3.Distance(transform.position, target.GetPosition());
        rating += target.GetCost(GetMapObjectType());
        
        int randValue = Random.Range(0, 4);
        if (randValue == 0)
        {
          rating += variableManager.GetTargetCost(EMapObjectType.TYPE_RANDOM, GetMapObjectType());
        }

        if (target.IsHero() && isHeroDamaging)
        {
          rating += Const.MAX_TARGET_COST;
        }

        targetWithRating.Add(target, rating);
      }
    }

    return targetWithRating;
  }

  protected AI__AttackTarget GetPotentialTarget()
  {
    Dictionary<AI__AttackTarget, float> potentialTargets = GetPotentialTargetsWithRatings();

    AI__AttackTarget target = null;
    foreach (KeyValuePair<AI__AttackTarget, float> targetWithRating in potentialTargets)
      if (target == null || potentialTargets[target] < targetWithRating.Value)
        target = targetWithRating.Key;

    //Debug.Log("AI__Attacking.GetPotentialTarget() >>> " + target.GetMapObjectType());

    return target;
  }

  protected void SearchAttackTarget()
  {
    SearchAttackTarget(false);
  }

  protected void SearchAttackTarget(bool isForcibly)
  {
    //Debug.Log("AI__Attacking.SearchAttackTarget() >>> " + isForcibly);
    if (isForcibly || attackTarget == null || attackTarget.IsDead())
    {
      AI__AttackTarget target = GetPotentialTarget();
      SetAttackTarget(target);
    }
  }

  public void SetAttackTarget(AI__AttackTarget attackTarget)
  {
    //Debug.Log("AI__Attacking.SetAttackTarget()="+attackTarget);
    if (attackTarget == null)
      return;

    if (!CanAttackFlyingTargets && attackTarget.IsFlyingObject())
      return;

    SetMoveTarget(attackTarget, ATTACK_DISTANCE);
    this.attackTarget = attackTarget;
  }

  public AI__AttackTarget GetAttackTarget()
  {
    return attackTarget;
  }

  public bool CheckAttackTarget(int id)
  {
    return attackTarget != null && attackTarget.GetId() == id;
  }

  protected virtual Vector3 AttackingDefaultBehavior(Vector3 movement)
  {
    CheckSearchTarget();

    if (IsAttackTargetDead())
    {
      StopAttack();
      return movement;
    }

    if (CheckAttack())
    {
      //Debug.Log("AI__Attacking.CheckAttack()");
      isAttacking = true;
      return movement;
    }

    if (CheckPauseBetweenAttack())
    {
      //Debug.Log("AI__Attacking.CheckPauseBetweenAttack()");
      isAttacking = true;
      return movement;
    }

    isAttacking = false;

    movement = MovingDefaultBehavior(movement);

    if (IsReadyToAttack())
    {
      Attack();
    }

    return movement;
  }

  protected float GetAngleToAttackTarget()
  {
    if (attackTarget == null || attackTarget.IsDead())
      return -1f;

    Vector3 directionToTarget = attackTarget.GetPosition() - transform.position;
    float angleToTarget = Vector3.Angle(transform.forward, directionToTarget);
    //Debug.Log(">>> angleToTarget=" + angleToTarget);
    return angleToTarget;
  }

  protected virtual bool IsReadyToAttack()
  {
    if (!attackTarget.IsDead())
      return GetDistanceToMoveTarget() <= ATTACK_DISTANCE && GetAngleToAttackTarget() < ATTACK_ANGLE;
    return false;
  }

  protected bool IsAttackTargetDead()
  {
    if (attackTarget == null)
      return true;
    return attackTarget.IsDead();
  }

  protected bool CheckAttack()
  {
    if (attackTimer > 0)
    {
      attackTimer -= Time.deltaTime;
      if (attackTimer <= 0)
      {
        StopAttack();
        betweenAttackTimer = PAUSE_BETWEEN_ATTACK;
        return false;
      }
      return true;
    }
    return false;
  }

  protected void CheckSearchTarget()
  {
    searchTargetTimer += Time.deltaTime;
    if (searchTargetTimer >= 1.0f)
    {
      searchTargetTimer = 0;
      SearchAttackTarget();
    }
  }

  protected bool CheckPauseBetweenAttack()
  {
    if (betweenAttackTimer > 0)
    {
      if (!isBetweenAttack)
      {
        //Debug.Log("Pause Between Attack Start");
      }
      isBetweenAttack = true;
      betweenAttackTimer -= Time.deltaTime;
      if (betweenAttackTimer <= 0)
      {
        betweenAttackTimer = 0;
        isBetweenAttack = false;
        //Debug.Log("Pause Between Attack Stop");
        return false;
      }
      return true;
    }
    return false;
  }

  protected virtual void Attack()
  {
    //Debug.Log("AI__Attacking.Attack()");
  }

  protected virtual void StopAttack()
  {
    attackTimer = 0;
    //Debug.Log("AI__Attacking.StopAttack()");
  }

  protected virtual void OnControllerColliderHitAttackingDefault(ControllerColliderHit hit)
  {
    //Debug.Log("AI__Attacking.OnControllerColliderHitAttackingDefault()");
  }

  protected void CheckAttackTarget()
  {
    if (attackTarget != null && (attackTarget.IsDead() || (!CanAttackFlyingTargets && attackTarget.IsFlyingObject())))
    {
      attackTarget = null;
      moveTarget = null;
    }
  }
}
