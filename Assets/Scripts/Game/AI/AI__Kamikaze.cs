﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI__Kamikaze : AI__Enemy
{
  public Transform Main;
  public AnimationObject head;
  public AnimationObject legs;

  public SnowWave snowWave;

  private Color partDefaultColor;

  public override void EndedObjectInit()
  {
    EnemyInit();

    ANIMATION_LENGTH_ATTACK = 0f;
    ANIMATION_LENGTH_DEAD = 1f;
    ANIMATION_LENGTH_DEAD_KICK_MOMENT = 0f;
    ANIMATION_LENGTH_ATTACK_KICK_MOMENT = 0f;
    PAUSE_BETWEEN_ATTACK = 0f;
    DEAD_DURATION = 4.0f;
    ATTACK_DISTANCE = 2.0f;
    SEE_ANGLE = 360.0f;
    ATTACK_ANGLE = 360.0f;
    ROTATE_SPEED = 10f;

    partDefaultColor = renderers[0].material.color;
    currentMoveSpeed = WALK_SPEED;

    legs.SetAnimationWithStates(new int[] { 1, 2 });
    legs.SetAnimationDelay(0.1f);

    head.SetAnimationDelay(1f);
    head.Play();

    isInit = true;
  }

  void Update()
  {
    if (!isReady || !isInit || isDead)
      return;

    if (isPaused)
      return;

    CheckUpdateEffects();

    Vector3 movement = Vector3.zero;
    movement = AttackingDefaultBehavior(movement);
    ApplyMovement(movement);
  }

  private void LateUpdate()
  {
    if (!isReady || !isInit || isDying || isDead)
      return;

    if (isPaused)
      return;

    LegsAnimation(moving);
  }

  private void LegsAnimation(bool moving)
  {
    if (moving)
    {
      legs.Play();
    }
    else
    {
      legs.Stop();
      legs.SetState(0);
    }
  }

  protected override Vector3 AttackingDefaultBehavior(Vector3 movement)
  {
    CheckAttackTarget();
    CheckSearchTarget();

    if (IsAttackTargetDead())
    {
      StopAttack();
      return movement;
    }

    if (CheckDying() || CheckDead() || CheckAttack())
    {
      return movement;
    }

    movement = MovingDefaultBehavior(movement);

    if (IsReadyToAttack())
    {
      Attack();
    }

    return movement;
  }

  protected override void Attack()
  {
    //Debug.Log("AI__Kamikaze.Attack()");
    die();
    
  }

  public override void Ready()
  {
    base.Ready();
    renderers = GetComponentsInChildren<Renderer>();
    head.SetAnimationDelay(1f);
    head.Play();
  }

  public override void Reset()
  {
    base.Reset();
    SetEnabledForRenderers(Main, true);
  }

  protected override void die()
  {
    if (isDying)
      return;

    hp = 0;

    isDying = true;
    StopAttack();
    dyingTimer = ANIMATION_LENGTH_DEAD;
    SetEnabledForAllColliders(false);
    
    legs.Stop();
    legs.SetState(0);
    head.Stop();
    head.SetState(0);

    SetEnabledForRenderers(Main, false);
    characterController.enabled = false;
    snowWave.Run();
    audioObject.Play(0);
  }

  protected override void StopAttack()
  {
    attackTimer = 0;
  }

  private void OnParticleCollision(GameObject other)
  {
    if (isDying)
      return;

    if (hp <= 0)
      return;

    if (other.tag.Equals(Const.TAG_FIRE)
      || other.tag.Equals(Const.TAG_PLASMA))
    {
      CheckSourceDamage(other);
    }

    SetDamage(GetDamage4Enemy(other));
  }

  protected virtual void SetDamage(int damage)
  {
    if (hp == 0)
      return;

    hp -= damage;
    if (hp < 0)
      hp = 0;

    //Debug.Log(">>> damage=" + damage);

    if (hp > 0)
    {
      StartCoroutine(blink());
    }
    else
    {
      die();
    }
  }

  private IEnumerator blink()
  {
    foreach (Renderer renderer in renderers)
      renderer.material.color = Color.red;
    yield return new WaitForSeconds(0.1f);
    foreach (Renderer renderer in renderers)
      renderer.material.color = partDefaultColor;
  }

  protected override void HitWithTransport(ITransport iTransport)
  {
    //Debug.Log("HitWithTransport!");
    if (iTransport.IsMovingNow())
    {
      SetDamage(10);
    }
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    snowWave.SetPause(value);
    legs.SetPause(value);
    head.SetPause(value);
  }
}
