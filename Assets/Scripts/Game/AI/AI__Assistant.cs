﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI__Assistant : AI__Transport
{
  public GameObject Radar;
  protected AssistantHead head;
  protected Rotating radarRotating;

  protected float fuelOutTargetTimer = 0;

  protected bool isFinished = false;

  public override void EndedObjectInit()
  {
    AI__TransportInit();

    ROTATE_SPEED = 4f;
    currentMoveSpeed = WALK_SPEED;

    head = GetComponentInChildren<AssistantHead>();
    radarRotating = Radar.GetComponentInChildren<Rotating>();

    RadarOff();
    isInit = true;
    isForgetTarget = true;
    On();
  }

  private void RadarOn()
  {
    Radar.SetActive(true);
    radarRotating.On();
  }

  private void RadarOff()
  {
    radarRotating.Off();
    Radar.SetActive(false);
  }

  void Update()
  {
    if (!isReady || !isInit)
      return;

    if (isPaused)
      return;

    Vector3 movement = Vector3.zero;

    if (isDead)
    {
      ApplyMovement(movement);
      return;
    }

    if (!isOn)
      return;

    movement = TransportDefaultBehavior(movement);
    ApplyMovement(movement);
  }

  protected override void beforeDie()
  {
    Destroy(head.gameObject);
  }

  public override void On()
  {
    base.On();
    head.On();
  }

  public override void Off()
  {
    base.Off();
    head.Off();
  }

  protected override void DoIt(string operationName)
  {
    Debug.Log("DoIt >>> " + operationName);

    Vector3 position = transform.position;
    switch (operationName)
    {
      case "radar_on":
        RadarOn();
        break;
      case "radar_off":
        RadarOff();
        break;
      case "finish":
        isFinished = true;
        break;
    }
  }

  public bool IsFinished()
  {
    return isFinished;
  }

  public override void Reset()
  {
    base.Reset();
    fuel = FUEL_MAX;
    repaintFuelMarker();
  }
}
