﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AI__Enemy : AI__Attacking, IEnemy
{
  protected const string ANIMATOR_PARAM_SPEED = "speed";
  protected const string ANIMATOR_PARAM_ATTACK = "attack";
  protected const string ANIMATOR_PARAM_DEAD = "dead";
  protected const string ANIMATOR_PARAM_DEAD_TYPE = "death_type";

  protected float ANIMATION_LENGTH_ATTACK = 0f;
  protected float ANIMATION_LENGTH_ATTACK_KICK_MOMENT = 0f;
  protected float ANIMATION_LENGTH_DEAD = 0f;
  protected float ANIMATION_LENGTH_DEAD_KICK_MOMENT = 0f;

  protected float SHOW_PART_DELAY = 0.1f;

  protected float DEAD_DURATION = 0f;

  protected int DEATH_COUNT = 1;

  protected Animator animator;

  protected bool isDying = false;

  protected float fallTimer = 0;
  protected float dyingTimer = 0;
  protected float dyingKickTimer = 0;

  protected int HP_MAX = 0;
  protected int hp = 0;

  protected IEnemyPart[] parts;

  protected Renderer[] renderers;

  protected Queue<Renderer> queueRenderers;

  protected int currentPartsState = 0;

  protected UI__EffectMarker effectMarker;

  protected void EnemyInit()
  {
    AI__AttackingInit();

    effectMarker = GetComponentInChildren<UI__EffectMarker>();
    effectMarker.SetValue(0);

    animator = GetComponent<Animator>();
    renderers = GetComponentsInChildren<Renderer>();

    if (animator != null)
      animator.SetInteger(ANIMATOR_PARAM_DEAD_TYPE, Random.Range(0, DEATH_COUNT));

    currentPartsState = EnemyPart.ENEMY_PART_STATE_100;

    SEE_DISTANCE = variableManager.GetSeeDistance(GetMapObjectType());
    WALK_SPEED = variableManager.GetSpeed(GetMapObjectType());
    HP_MAX = variableManager.GetHP(GetMapObjectType());

    transform.gameObject.layer = Const.LAYER_AI_ENEMY;
  }

  private void LateUpdate()
  {
    if (!isReady)
      return;

    if (isPaused)
      return;

    if (animator != null)
    {
      if (moving)
      {
        animator.SetFloat(ANIMATOR_PARAM_SPEED, currentMoveSpeed);
      }
      else
      {
        animator.SetFloat(ANIMATOR_PARAM_SPEED, 0);
      }
    }

    AfterLateUpdate();
  }

  protected virtual void AfterLateUpdate()
  {

  }

  public virtual void Damage(GameObject other, EnemyPartTagEnum partTag, int damage, int partHp)
  {
    throw new System.NotImplementedException();
  }

  protected bool CheckDying()
  {
    if (dyingTimer > 0)
    {
      dyingTimer -= Time.deltaTime;
      if (dyingTimer <= 0)
      {
        dyingTimer = 0;
        deadTimer = DEAD_DURATION;
        IAmWaitingDeath();
        return false;
      }
      return true;
    }
    return false;
  }

  protected bool CheckDyingKick()
  {
    if (dyingKickTimer > 0)
    {
      dyingKickTimer -= Time.deltaTime;
      if (dyingKickTimer <= 0)
      {
        dyingKickTimer = 0;
        DyingKick();
        return false;
      }
      return true;
    }
    return false;
  }

  protected virtual void DyingKick()
  {

  }

  protected virtual void die()
  {
    if (isDying)
      return;

    MyHpIsZero();
    isDying = true;
    StopAttack();
    animator.SetBool(ANIMATOR_PARAM_DEAD, true);
    dyingTimer = ANIMATION_LENGTH_DEAD;
    dyingKickTimer = ANIMATION_LENGTH_DEAD_KICK_MOMENT;
    SetEnabledForAllColliders(false);
    characterController.enabled = false;
  }

  protected override void StopAttack()
  {
    attackTimer = 0;
    animator.SetBool(ANIMATOR_PARAM_ATTACK, false);
    //Debug.Log("AI__Enemy.StopAttack()");
  }

  void OnControllerColliderHit(ControllerColliderHit hit)
  {
    OnControllerColliderHitMovingDefault(hit);
    OnControllerColliderHitAttackingDefault(hit);
  }

  protected virtual void HitWithTransport(ITransport iTransport)
  {

  }

  public bool IsAlive()
  {
    return !isDead && !isDying;
  }

  public override bool IsEnemy()
  {
    return true;
  }

  public bool IsVisibleOnCameraView()
  {
    bool isVisible = true;

    foreach (Renderer renderer in renderers)
      if (!renderer.isVisible)
        isVisible = false;

    return isVisible;
  }

  public override SavedWorldObject GetSavedWorldObject()
  {
    SavedWorldObject savedLevelObject = base.GetSavedWorldObject();
    savedLevelObject.hp = hp;
    if (moveTarget != null)
      savedLevelObject.moveTargetId = moveTarget.GetId();
    if (attackTarget != null)
      savedLevelObject.attackTargetId = attackTarget.GetId();
    return savedLevelObject;
  }

  public override SavedWorldObject SetSavedWorldObject(SavedWorldObject savedLevelObject)
  {
    savedLevelObject = base.SetSavedWorldObject(savedLevelObject);
    if (savedLevelObject.hp >= 0)
      this.hp = savedLevelObject.hp;
    return savedLevelObject;
  }

  public override void Ready()
  {
    base.Ready();
    renderers = GetComponentsInChildren<Renderer>();
    queueRenderers = new Queue<Renderer>();
    Utils.Shuffle(renderers);
    foreach (Renderer renderer in renderers)
    {
      renderer.enabled = false;
      queueRenderers.Enqueue(renderer);
    }
    StartCoroutine(ShowPart());
  }

  private IEnumerator ShowPart()
  {
    yield return new WaitForSeconds(SHOW_PART_DELAY);
    if (queueRenderers.Count > 0)
    {
      queueRenderers.Dequeue().enabled = true;
      if (queueRenderers.Count > 0)
        StartCoroutine(ShowPart());
    }
  }

  public override void Reset()
  {
    base.Reset();

    isDying = false;

    fallTimer = 0;
    dyingTimer = 0;
    dyingKickTimer = 0;
    hp = HP_MAX;

    SetEnabledForAllColliders(true);
  }

  public override void SetPause(bool value)
  {
    base.SetPause(value);
    if (animator != null)
      animator.enabled = !value;
  }

  public override void TrueKill()
  {
    hp = 0;
    die();
  }

  /*protected void UpdateEnemyPartStates()
  {
    if (hp <= (HP_MAX * 0.1f) && currentPartsState == EnemyPart.ENEMY_PART_STATE_20)
    {
      currentPartsState = EnemyPart.ENEMY_PART_STATE_0;
      foreach (EnemyPart part in parts)
      {
        part.SetState(EnemyPart.ENEMY_PART_STATE_0);
      }
    }
    else if (hp <= (HP_MAX * 0.2f) && currentPartsState == EnemyPart.ENEMY_PART_STATE_40)
    {
      currentPartsState = EnemyPart.ENEMY_PART_STATE_20;
      foreach (EnemyPart part in parts)
      {
        part.SetState(EnemyPart.ENEMY_PART_STATE_20);
      }
    }
    else if (hp <= (HP_MAX * 0.4f) && currentPartsState == EnemyPart.ENEMY_PART_STATE_60)
    {
      currentPartsState = EnemyPart.ENEMY_PART_STATE_40;
      foreach (EnemyPart part in parts)
      {
        part.SetState(EnemyPart.ENEMY_PART_STATE_40);
      }
    }
    else if (hp <= (HP_MAX * 0.6f) && currentPartsState == EnemyPart.ENEMY_PART_STATE_80)
    {
      currentPartsState = EnemyPart.ENEMY_PART_STATE_60;
      foreach (EnemyPart part in parts)
      {
        part.SetState(EnemyPart.ENEMY_PART_STATE_60);
      }
    }
    else if (hp <= (HP_MAX * 0.8f) && currentPartsState == EnemyPart.ENEMY_PART_STATE_100)
    {
      currentPartsState = EnemyPart.ENEMY_PART_STATE_80;
      foreach (EnemyPart part in parts)
      {
        part.SetState(EnemyPart.ENEMY_PART_STATE_80);
      }
    }
  }*/

  public int GetHP()
  {
    return hp;
  }

  protected override void OnUpdatedEffects()
  {
    effectMarker.SetValue(effects[Terraformer.EFFECT_WEAPON]);
  }
}
