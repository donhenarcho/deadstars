﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUI__Marker
{
  void On();
  void Off();
  void GoTo(Vector3 vector3);
  void Rotate(float angle);
  void SetForvard(Vector3 vector3);
  void PlayAnimation();
  void StopAnimation();
  void SetMarkerState(int state);
}
