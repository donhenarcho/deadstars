﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI__Loading : MonoBehaviour
{
  public Text Hint;

  public void Set(string hint)
  {
    this.Hint.text = hint;
  }

  public void Show()
  {
    gameObject.SetActive(true);
  }

  public void Hide()
  {
    gameObject.SetActive(false);
  }

  public bool IsShow()
  {
    return gameObject.activeSelf;
  }
}
