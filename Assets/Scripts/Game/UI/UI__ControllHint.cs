﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI__ControllHint : MonoBehaviour
{
  public const int STATE_DEFAULT = 0;

  protected AnimationObject animation;

  protected int currentAnimationState = 0;
  protected float animationTimer = 0;

  void Start()
  {
    animation = GetComponentInChildren<AnimationObject>();
    Hide();
  }

  void Update()
  {
    if (currentAnimationState > STATE_DEFAULT)
    {
      animationTimer += Time.deltaTime;
      if (animationTimer > 0.5f)
      {
        animationTimer = 0;
        if (animation.GetState() == STATE_DEFAULT)
          animation.SetState(currentAnimationState);
        else
          animation.SetState(STATE_DEFAULT);
      }
    }
  }

  public void Show(int state)
  {
    animation.gameObject.SetActive(true);
    currentAnimationState = state;
  }

  public void Hide()
  {
    currentAnimationState = STATE_DEFAULT;
    animation.SetState(STATE_DEFAULT);
    animation.gameObject.SetActive(false);
  }

  public int GetCurrentAnimationState()
  {
    return currentAnimationState;
  }

  public bool IsShow()
  {
    return animation.gameObject.activeSelf;
  }
}
