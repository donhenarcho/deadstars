﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MapObject;

public class UI__EnemyPointers : MonoBehaviour
{
  private float radius = 6f;
  public Transform[] pointers;
  public Transform background;
  void Start()
  {
    HidePointers();
  }

  void Update()
  {
        
  }

  public void MoveTo(Vector3 ownerPosition)
  {
    ownerPosition.y = 0.1f;
    transform.position = ownerPosition;
  }

  public void ShowPointers(List<Vector3> directions, EMapObjectType objectType)
  {
    switch (objectType)
    {
      case EMapObjectType.TYPE_HERO:
        radius = 4;
        background.localScale = new Vector3(1f, 1f, 1f);
        break;
      case EMapObjectType.TYPE_TANK:
      case EMapObjectType.TYPE_STORM:
      case EMapObjectType.TYPE_CANNON:
        radius = 8;
        background.localScale = new Vector3(2f, 2f, 1f);
        break;
      case EMapObjectType.TYPE_BMP:
        radius = 4;
        background.localScale = new Vector3(1f, 1f, 1f);
        break;
      case EMapObjectType.TYPE_BIKE:
        radius = 4;
        background.localScale = new Vector3(1f, 1f, 1f);
        break;
    }

    HidePointers();
    int index = 0;
    foreach (Vector3 direction in directions)
    {
      Vector3 newlocalPosition = new Vector3(direction.x, direction.z * -1f, 0) * radius;
      pointers[index].localPosition = newlocalPosition;
      pointers[index].gameObject.SetActive(true);

      index++;
      if (index >= pointers.Length)
        break;
    }
  }

  public void HidePointers()
  {
    foreach (Transform pointer in pointers)
    {
      pointer.gameObject.SetActive(false);
    }
  }

  private Vector3 getCoords(float angle)
  {
    Vector3 coords = Vector3.zero;
    coords.x = radius * Mathf.Cos(angle);
    coords.z = radius * Mathf.Sin(angle);
    return coords;
  }
}
