﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI__Toast : MonoBehaviour
{
  private const float LIVE_MAX = 4f;

  private Image image;
  private Text text;

  private float alpha = 0.8f;

  private float timer = 0f;

  void Awake()
  {
    image = GetComponent<Image>();
    text = GetComponentInChildren<Text>();
  }

  void Start()
  {
  }

  void Update()
  {
    if (timer > 0)
    {
      timer -= Time.deltaTime;
      if (timer <= 0)
      {
        timer = 0;
        Destroy(this.gameObject);
      }
    }
  }

  public void Info(string text)
  {
    Color color = Color.black;
    color.a = alpha;
    this.image.color = color;
    this.text.color = Color.white;
    this.text.text = text;
    timer = LIVE_MAX;
  }

  public void Error(string text)
  {
    Color color = Color.white;
    color.a = alpha;
    this.image.color = color;
    this.text.color = Color.black;
    this.text.text = text;
    timer = LIVE_MAX;
  }
}
