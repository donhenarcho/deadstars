﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI__ObjectMenu : MonoBehaviour
{
  public Transform Backgrounds;
  public Transform Progress;
  public Transform Selects;

  public Sprite[] progressSprites;

  public Transform AcceptDialog;
  public Text AcceptDialogText;
  public Text AcceptDialogBtnYes;
  public Text AcceptDialogBtnNo;

  protected GameController gameController;
  protected WorldController levelController;
  protected VariableManager variableManager;
  protected LocalizationManager localizationManager;
  protected UI__Controller uiController;

  protected bool isInit = false;
  protected bool isOn = true;

  protected int currentSelect = -1;
  protected int currentButtonId = 0;
  protected int currentSubMenu = 0;
  private int currentButtonsNum = 8;

  protected bool isHelpMode = false;

  protected Dictionary<int, List<UI__ObjectMenuButton>> buttons;

  protected List<int> hiddenButtons = new List<int>();
  protected List<int> newButtons = new List<int>();

  protected int currentDialogAcceptType = 0;

  protected float updateButtonStateTimer = 0;

  void Start()
  {
    uiController = FindObjectOfType<UI__Controller>();
    gameController = FindObjectOfType<GameController>();
    levelController = FindObjectOfType<WorldController>();
    variableManager = FindObjectOfType<VariableManager>();
    localizationManager = FindObjectOfType<LocalizationManager>();

    AcceptDialog.gameObject.SetActive(false);

    if (variableManager.DoNeedToHideMenuButtons())
    {
      hiddenButtons = new List<int>();
      foreach (int btnId in variableManager.GetHiddenMenuButtons())
        hiddenButtons.Add(btnId);
    }

    newButtons = new List<int>();
    foreach (int btnId in variableManager.GetNewMenuButtons())
      newButtons.Add(btnId);

    List<int> submenuWithNewButton = new List<int>();
    buttons = new Dictionary<int, List<UI__ObjectMenuButton>>();
    foreach (UI__ObjectMenuButton button in GetComponentsInChildren<UI__ObjectMenuButton>())
    {
      button.SetState(getButtonState(button.id));
      if (!buttons.ContainsKey(button.submenuIndex))
        buttons.Add(button.submenuIndex, new List<UI__ObjectMenuButton>());
      buttons[button.submenuIndex].Add(button);

      if (hiddenButtons.Contains(button.id))
        button.Hide();

      if (newButtons.Contains(button.id))
      {
        button.New();
        if (button.submenuIndex > 0)
        {
          submenuWithNewButton.Add(button.submenuIndex);
        }
      }
    }

    foreach (UI__ObjectMenuButton button in GetComponentsInChildren<UI__ObjectMenuButton>())
    {
      if (button.nextSubmenuIndex > 0 && submenuWithNewButton.Contains(button.nextSubmenuIndex))
        button.HasNew();
    }
  }

  void Update()
  {
    if (!isOn)
      return;

    if (updateButtonStateTimer > 0)
    {
      updateButtonStateTimer -= Time.deltaTime;
      if (updateButtonStateTimer <= 0)
      {
        updateButtonStateTimer = 1f;
        UpdateAllButtonStates();
      }
    }


    /*float dltH = Input.GetAxis("Horizontal");
    float dltV = Input.GetAxis("Vertical");
    Vector3 direction = new Vector3(dltH, 0, dltV);
    if (direction != Vector3.zero)
    {
      float angle = Vector3.SignedAngle(direction, transform.forward, Vector3.up);

      //Debug.Log("menu-angle=" + angle);

      if (angle < 22.5f && angle > -22.5f)
        Select(1);
      else if (angle > 22.5f && angle < 67.5f)
        Select(0);
      else if (angle > 67.5f && angle < 112.5f)
        Select(3);
      else if (angle > 112.5f && angle < 157.5f)
        Select(5);
      else if (angle > 157.5f && angle <= 180f)
        Select(6);
      else if (angle < -22.5f && angle >= -67.5f)
        Select(2);
      else if (angle < -67.5f && angle >= -112.5f)
        Select(4);
      else if (angle < -112.5f && angle >= -157.5f)
        Select(7);
      else if (angle < -157.5f && angle >= -180f)
        Select(6);
    }

    if (Input.GetKeyDown(gameController.GetKeyCodeForAction(Const.CONTROL_ACTION_FIRE)))
    {
      OnTap();
    }*/
  }

  private void LateUpdate()
  {
    if (!isInit)
    {
      isInit = true;
      Off();
    }
  }

  protected virtual int getButtonState(int buttonId)
  {
    return 0;
  }

  protected virtual string getButtonTopLabelValue(int buttonId)
  {
    return "";
  }

  protected virtual string getButtonBottomLabelValue(int buttonId)
  {
    return "";
  }

  protected virtual string getButtonHintCode(int buttonId)
  {
    return "";
  }

  protected virtual int[] getButtonProgresses()
  {
    return new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
  }

  public void UpgateProgresses()
  {
    //Debug.Log("UpgateProgress");
    int[] progresses = getButtonProgresses();

    int index = 0;
    foreach (Transform progress in Progress)
    {
      Image image = progress.GetComponent<Image>();

      if (progresses[index] == -1)
        image.enabled = false;
      else
      {
        image.enabled = true;
        image.sprite = progressSprites[progresses[index]];
      }

      index++;
    }
  }

  protected void SetSubmenu(int index)
  {
    int num = 0;
    foreach (KeyValuePair<int, List<UI__ObjectMenuButton>> entry in buttons)
    {
      foreach (UI__ObjectMenuButton button in entry.Value)
      {
        button.gameObject.SetActive(entry.Key == index || button.isAlwaysVisible);
        if (entry.Key == index)
        {
          button.SetState(getButtonState(button.id));
          button.SetTopLabel(getButtonTopLabelValue(button.id));
          button.SetBottomLabel(getButtonBottomLabelValue(button.id));
          num++;
        }
      }
    }
    currentButtonsNum = num;
    currentSubMenu = index;

    SubMenuChangedEvent();
    UpgateProgresses();
    UpdateBackgrounds();
  }

  protected void UpdateAllButtonStates()
  {
    foreach (KeyValuePair<int, List<UI__ObjectMenuButton>> entry in buttons)
    {
      foreach (UI__ObjectMenuButton button in entry.Value)
      {
        if (button.gameObject.activeSelf)
        {
          button.SetState(getButtonState(button.id));
          button.SetTopLabel(getButtonTopLabelValue(button.id));
          button.SetBottomLabel(getButtonBottomLabelValue(button.id));
        }
      }
    }
  }

  protected virtual void SubMenuChangedEvent()
  {

  }

  protected void UpdateBackgrounds()
  {
    //Debug.Log("UpdateBackgrounds");
    int index = 0;
    foreach (Transform background in Backgrounds)
    {
      Image image = background.GetComponent<Image>();
      if (image == null)
        continue;
      index++;
    }
  }

  public void Select(int index)
  {
    InnerSelect(index);
  }

  protected void InnerSelect(int index)
  {
    if (index >= currentButtonsNum)
      return;

    if (currentSelect == index)
      return;

    int num = 0;
    foreach (Transform background in Selects)
    {
      Image image = background.GetComponent<Image>();
      if (image == null)
        continue;

      image.enabled = num == index;
      num++;
    }

    currentSelect = index;
    HintUpdate();
  }

  protected virtual void HintUpdate()
  {
    currentButtonId = buttons[currentSubMenu][currentSelect].id;
    uiController.SetBottomHint(getButtonHintCode(currentButtonId));
  }

  protected virtual bool OnTap()
  {
    return OnTapDefault();
  }

  public virtual void OnMouseClick(int index)
  {
    return;
  }

  protected bool OnTapDefault()
  {
    UI__ObjectMenuButton button = buttons[currentSubMenu][currentSelect];
    if (button.isBack)
    {
      SetSubmenu(button.backSubmenuIndex);
      HintUpdate();
      return true;
    }

    if (button.isNext)
    {
      SetSubmenu(button.nextSubmenuIndex);
      HintUpdate();
      return true;
    }

    return false;
  }

  public void On()
  {
    isOn = true;
    gameObject.SetActive(isOn);
    SetSubmenu(0);
    Select(0);
    updateButtonStateTimer = 1f;
  }

  public void Off()
  {
    isOn = false;
    gameObject.SetActive(isOn);
    updateButtonStateTimer = 0f;
  }

  public int Switch()
  {
    if (isOn)
    {
      Off();
      return 0;
    }
    else
    {
      On();
      return 1;
    }
  }

  public bool IsShow()
  {
    return isOn;
  }

  public bool IsHelpMode()
  {
    return isHelpMode;
  }

  public void RunHelpMode()
  {
    isHelpMode = true;
  }

  public void StopHelpMode()
  {
    isHelpMode = false;
  }

  protected virtual void ShowAcceptDialog(int type)
  {

  }

  protected virtual void HideAcceptDialog()
  {

  }

  protected virtual void OnAcceptDialogClickYes()
  {

  }

  protected virtual void OnAcceptDialogClickNo()
  {

  }
}
