﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI__FuelInMarker : MonoBehaviour
{
  Rotating marker;
  float timerOff = 0;

  void Update()
  {
    if (timerOff > 0)
    {
      timerOff -= Time.deltaTime;
      if (timerOff <= 0)
      {
        timerOff = 0;
        Off();
      }
    }
  }

  public void Show()
  {
    On();
    timerOff = 2f;
  }

  public void On()
  {
    gameObject.SetActive(true);
    if (marker == null)
      marker = GetComponentInChildren<Rotating>();
    marker.On();
  }

  public void Off()
  {
    if (marker == null)
      marker = GetComponentInChildren<Rotating>();
    marker.Off();
    gameObject.SetActive(false);
  }
}
