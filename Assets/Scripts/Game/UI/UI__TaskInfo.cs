﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI__TaskInfo : MonoBehaviour
{
  public Image background;
  public Text text;

  private WorldController worldController;
  private UI__Chat Chat;
  private LevelTask task;
  private string messageRender = "";
  private Color backgroundColor;
  private string lastProgress;
  private float blinkTimer = 0f;
  private int blinkCounter = 0;

  void Start()
  {
    worldController = FindObjectOfType<WorldController>();
    Chat = FindObjectOfType<UI__Chat>();
    backgroundColor = background.color;
    Hide();
  }

  void Update()
  {
    if (!IsShow())
      return;

    Update(worldController.GetTaskProgress());

    if (blinkTimer > 0f)
    {
      blinkTimer -= Time.deltaTime;
      if (blinkTimer <= 0f)
      {
        blinkCounter++;
        StartCoroutine(blink());
        if (blinkCounter < 2)
        {
          blinkTimer = 0.2f;
        }
      }
    }
  }

  public void Show(LevelTask levelTask)
  {
    if (levelTask == null)
      return;

    if (!levelTask.Equals(task))
    {
      StartBlink();
    }

    task = levelTask;
    messageRender = Chat.GetMessageRender(1, levelTask.text);
    background.color = backgroundColor;
    gameObject.SetActive(true);
  }

  private void Update(string progress)
  {
    if (task.GetTaskType() != LevelTask.TYPE_COLLECTION && lastProgress != "" && !progress.Equals(lastProgress) 
      && (task.GetTaskType() != LevelTask.TYPE_DEFEND_OBJECTS || progress.Contains("[00:00]")))
    {
      StartBlink();
    }

    lastProgress = progress;
    text.text = messageRender + " <b>" + progress + "</b>";
  }

  public void Hide()
  {
    gameObject.SetActive(false);
  }

  public bool IsShow()
  {
    return gameObject.activeSelf;
  }

  private void StartBlink()
  {
    blinkTimer = 0.2f;
    blinkCounter = 0;
  }

  private IEnumerator blink()
  {
    background.color = Color.white;
    yield return new WaitForSeconds(0.1f);
    background.color = backgroundColor;
  }
}
