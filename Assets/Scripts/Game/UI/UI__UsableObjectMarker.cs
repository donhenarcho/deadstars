﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI__UsableObjectMarker : MonoBehaviour
{
  public Transform Marker;

  protected bool isInit = false;
  protected bool isOn = false;
  protected int direction = -1;

  void Update()
  {
    if (!isInit)
    {
      isInit = true;
      Off();
    }

    Vector3 position = Marker.position;
    position.y += 0.08f * direction;

    if (position.y > 10)
    {
      position.y = 10;
      direction = -1;
    }

    if (position.y < 8)
    {
      position.y = 8;
      direction = 1;
    }

    Marker.position = position;
  }

  public void On(Vector3 position)
  {
    transform.position = position;
    isOn = true;
    gameObject.SetActive(isOn);
  }

  public void Off()
  {
    isOn = false;
    gameObject.SetActive(isOn);
  }
}
