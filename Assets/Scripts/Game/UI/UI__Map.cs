﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static MapObject;

public class UI__Map : MonoBehaviour
{
  public GameObject PointsWrapper;
  public Sprite HeroSprite;
  public Sprite BaseSprite;
  public Sprite TransportSprite;
  public Sprite EnemySprite;
  public Sprite PyramidSprite;

  protected List<GameObject> points;

  protected bool isInit = false;
  protected bool isOn = false;

  protected float mapSize = 0f;
  protected float factScale = 0f;

  protected Dictionary<IMapObject, RectTransform> objectsWithPoints;

  void Start()
  {
    points = new List<GameObject>();
    mapSize = GetComponent<RectTransform>().rect.width;

    objectsWithPoints = new Dictionary<IMapObject, RectTransform>();
  }

  void Update()
  {
    if (!isInit)
    {
      isInit = true;
      Hide();
    }

    if (IsShow())
    {
      UpdatePoints();
    }
  }

  public void Show(List<IMapObject> mapObjects, float worldSize)
  {
    factScale = mapSize / worldSize;
    isOn = true;
    gameObject.SetActive(isOn);
    SetObjects(mapObjects);
  }

  public void Refresh(List<IMapObject> mapObjects)
  {
    if (!isOn)
      return;


    List<IMapObject> newMapObjects = new List<IMapObject>();
    foreach (MapObject mapObject in mapObjects)
    {
      if (!objectsWithPoints.ContainsKey(mapObject))
      {
        newMapObjects.Add(mapObject);
      }
    }

    SetObjects(newMapObjects);
  }

  private void UpdatePoints()
  {
    foreach (KeyValuePair<IMapObject, RectTransform> pair in objectsWithPoints)
    {
      pair.Value.anchoredPosition = GetAnchoredPosition(pair.Key);
    }
  }

  public void Hide()
  {
    isOn = false;
    foreach (GameObject point in points)
    {
      Destroy(point);
    }
    points.Clear();
    objectsWithPoints.Clear();
    gameObject.SetActive(isOn);
  }

  public void SetObjects(List<IMapObject> mapObjects)
  {
    foreach (IMapObject mapObject in mapObjects)
    {
      addPoint(mapObject);
    }
  }

  protected void addPoint(IMapObject mapObject)
  {
    GameObject point;
    Image image;
    RectTransform rectTransform;

    point = new GameObject();

    rectTransform = point.AddComponent<RectTransform>();
    rectTransform.parent = PointsWrapper.transform;
    rectTransform.anchorMin = new Vector2(0, 0);
    rectTransform.anchorMax = new Vector2(0, 0);

    image = point.AddComponent<Image>();
    switch (mapObject.GetMapObjectType())
    {
      case EMapObjectType.TYPE_HERO:
        image.sprite = HeroSprite;
        image.color = Color.white;
        break;
      case EMapObjectType.TYPE_TERRAFORMER:
        image.sprite = BaseSprite;
        image.color = Color.white;
        break;
      case EMapObjectType.TYPE_GOLEM:
      case EMapObjectType.TYPE_SCORPION:
      case EMapObjectType.TYPE_KAMIKAZE:
      case EMapObjectType.TYPE_CYCLOP:
      case EMapObjectType.TYPE_CENTAUR:
      case EMapObjectType.TYPE_DRAGONFLY:
      case EMapObjectType.TYPE_FLY:
      case EMapObjectType.TYPE_BIG_GOLEM:
        image.sprite = EnemySprite;
        image.color = Color.black;
        break;
      case EMapObjectType.TYPE_PYRAMID:
      case EMapObjectType.TYPE_BLACK_BOX:
        image.sprite = PyramidSprite;
        image.color = Color.white;
        break;
      default:
        if (mapObject.IsHero())
          image.sprite = HeroSprite;
        else
          image.sprite = TransportSprite;
        image.color = Color.white;
        break;
    }

    //float distance = Vector3.Distance(transform.position, mapObject.transform.position);

    rectTransform.anchoredPosition = GetAnchoredPosition(mapObject);
    rectTransform.sizeDelta = new Vector2(image.sprite.texture.width, image.sprite.texture.height);

    points.Add(point);
    objectsWithPoints.Add(mapObject, rectTransform);
  }

  private Vector2 GetAnchoredPosition(IMapObject mapObject)
  {
    Vector3 position = mapObject.GetPosition();
    position = position * factScale;
    return new Vector2(-position.z, position.x);
  }

  public bool IsShow()
  {
    return isOn;
  }
}
