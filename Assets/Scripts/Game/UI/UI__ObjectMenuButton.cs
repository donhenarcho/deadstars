﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI__ObjectMenuButton : MonoBehaviour
{
  public Text TopLabel;
  public Text BottomLabel;
  public Image marker;
  public Sprite[] sprites;
  public int id = 0;
  public int parentId = 0;
  public int submenuIndex = 0;
  public bool isBack = false;
  public int backSubmenuIndex = 0;
  public bool isNext = false;
  public int nextSubmenuIndex = 0;
  public bool isAlwaysVisible = false;

  protected int state = -1;
  protected int numStates = 0;
  protected bool isInit = false;

  private Image icon;

  void Awake()
  {
    icon = GetComponent<Image>();
    marker.enabled = false;
    marker.color = Const.MENU_COLOR_NEW_BUTTON;
  }

  void Update()
  {
    if (!isInit)
    {
      isInit = true;
      numStates = sprites.Length;
      if (numStates > 0)
        SetState(0);
    }
  }

  public void SetState(
      int _state
    )
  {
    if (!isInit)
      return;

    if (numStates == 0)
      return;

    if (state == _state)
      return;

    //Debug.Log("state=" + _state);

    state = _state;
    icon.sprite = sprites[state];
  }

  private void OnMouseEnter()
  {
    //Debug.Log(">>> " + id);
  }

  public void ClearLabels()
  {
    TopLabel.text = "";
    BottomLabel.text = "";
  }

  public void SetTopLabel(string value)
  {
    TopLabel.text = value;
  }

  public void SetBottomLabel(string value)
  {
    BottomLabel.text = value;
  }

  public void New()
  {
    if (icon != null)
      icon.color = Const.MENU_COLOR_NEW_BUTTON;
  }

  public void HasNew()
  {
    marker.enabled = true;
  }

  public void Hide()
  {
    if (icon != null)
      icon.color = Color.clear;
    TopLabel.enabled = false;
    BottomLabel.enabled = false;
  }

  public void Show()
  {
    if (icon != null)
      icon.color = Color.white;
    TopLabel.enabled = true;
    BottomLabel.enabled = true;
  }

  public bool IsShow()
  {
    return TopLabel.enabled;
  }
}
