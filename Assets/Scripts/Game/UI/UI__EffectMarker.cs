﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI__EffectMarker : MonoBehaviour
{
  public void SetValue(float value)
  {
    gameObject.SetActive(value > 0);
  }
}
