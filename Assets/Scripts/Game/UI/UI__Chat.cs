﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI__Chat : MonoBehaviour
{
  public RectTransform Content;
  public GameObject ChatMessagePrefab;

  private UI__Controller uiController;
  private WorldController worldController;
  private LocalizationManager localizationManager;

  private Dictionary<int, string> authors;

  private ScrollRect scrollRect;

  private LevelDialog dialog;

  void Start()
  {
    scrollRect = GetComponentInChildren<ScrollRect>();
    uiController = FindObjectOfType<UI__Controller>();
    worldController = FindObjectOfType<WorldController>();
    localizationManager = FindObjectOfType<LocalizationManager>();

    authors = new Dictionary<int, string>();
    authors.Add(0, "<b><color=#ffffffff>[" + localizationManager.GetWord("DIALOG_AUTHOR_0") + "]:</color></b>\n");
    authors.Add(1, "<b><color=#993366ff>[" + localizationManager.GetWord("DIALOG_AUTHOR_1") + "]:</color></b>\n");
    authors.Add(2, "<b><color=#336699ff>[" + localizationManager.GetWord("DIALOG_AUTHOR_2") + "]:</color></b>\n");

    Hide();
  }

  void Update()
  {
    if (dialog.IsEnd())
    {
      if (Input.GetKeyDown(KeyCode.Escape))
        Next();
      return;
    }
      
    if (Input.GetKeyDown(KeyCode.Space))
      Next();
  }

  public void Show(LevelDialog dialog)
  {
    uiController.SetBottomHint("HINT_DIALOG_CONTROL_NEXT");

    foreach (Transform child in Content)
    {
      GameObject.Destroy(child.gameObject);
    }

    gameObject.SetActive(true);
    this.dialog = dialog;
    NextMessage();
  }

  public void Hide()
  {
    gameObject.SetActive(false);
    uiController.ClearHints();
  }

  public bool IsShow()
  {
    return gameObject.activeSelf;
  }

  private bool NextMessage()
  {
    if (dialog.IsEnd())
      return false;

    LevelDialogMessage message = dialog.GetMessage();
    GameObject gameObject = Instantiate(ChatMessagePrefab, Vector3.zero, Quaternion.identity);
    gameObject.transform.SetParent(Content, false);
    gameObject.GetComponentInChildren<Text>().text = GetMessageRender(message);
    StartCoroutine(ScrollToBottom());

    if (dialog.IsEnd())
    {
      uiController.SetBottomHint("HINT_DIALOG_CONTROL_END");
    }

    return true;
  }

  public string GetMessageRender(LevelDialogMessage message)
  {
    return authors[message.AuthorId] + localizationManager.GetWord(message.GetTextCode(dialog.LevelId, dialog.Id));
  }

  public string GetMessageRender(int authorId, string textCode)
  {
    return authors[authorId] + localizationManager.GetWord(textCode);
  }

  public void Next()
  {
    if (!NextMessage())
    {
      worldController.HideDialog();
    }
  }

  private IEnumerator ScrollToBottom()
  {
    yield return new WaitForEndOfFrame();
    scrollRect.normalizedPosition = new Vector2(0, 0);
  }
}
