﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static MapObject;

public class UI__AssistantMenu : UI__ObjectMenu
{
  public const int ACCEPT_DIALOG_TYPE_NONE = 0;
  public const int ACCEPT_DIALOG_TYPE_LOAD = 1;
  public const int ACCEPT_DIALOG_TYPE_EXIT = 2;

  protected override int getButtonState(int buttonId)
  {
    return base.getButtonState(buttonId);
  }

  protected override string getButtonTopLabelValue(int buttonId)
  {
    string value = "";

    switch (buttonId)
    {
      case 101: // terraformer
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_TERRAFORMER);
        break;
      case 106: // resources value
        KeyValuePair<int, int> pair = levelController.GetResourcesInOrbitValue();
        value += String.Format(localizationManager.GetWord("ASSISTANT_MENU_BUTTON_TEXT_106"), pair.Key, pair.Value);
      break;
      case 402: // terraformer upgrade
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_TURRET);
        break;
      case 403: // terraformer upgrade
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_TERRAFORMER_UPGRADE_SHIELD);
        break;
      case 405: // terraformer upgrade
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_TERRAFORMER_UPGRADE_WEAPON);
        break;
      case 202: // harvester
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_HARVESTER);
        break;
      case 204: // bike
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_BIKE);
        break;
      case 205: // bmp
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_BMP);
        break;
      case 206: // tank
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_TANK);
        break;
      case 207: // storm
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_STORM);
        break;
      case 208: // fighter
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_FIGHTER);
        break;
      case 209: // cannon
        value += variableManager.GetResourceCost(EMapObjectType.TYPE_CANNON);
        break;
    }

    return value;
  }

  protected override string getButtonBottomLabelValue(int buttonId)
  {
    string value = "";

    switch (buttonId)
    {
      case 106: // resources timer
        value += levelController.GetResourcesInOrbitTimer();
        break;
      case 110: // satelite strike recovery timer
        value += levelController.GetSateliteStrikeRecoveryTime();
        break;
    }

    return value;
  }

  protected override string getButtonHintCode(int buttonId)
  {
    if (isHelpMode || newButtons.Contains(buttonId))
      return "ASSISTANT_MENU_BUTTON_HINT_FOR_HELP_MODE_" + buttonId;
    return "ASSISTANT_MENU_BUTTON_HINT_" + buttonId;
  }

  protected override int[] getButtonProgresses()
  {
    int[] progresses = new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1 };
    
    if (currentSubMenu == 2)
    {
      Terraformer baseManager = levelController.GetCurrentBase();
      if (baseManager != null)
      {
        if (!hiddenButtons.Contains(402))
          progresses[1] = baseManager.GetCurrentTurretUpgradeLevel();
        if (!hiddenButtons.Contains(403))
          progresses[2] = baseManager.GetCurrentShieldUpgradeLevel();
        if (!hiddenButtons.Contains(405))
          progresses[5] = baseManager.GetCurrentWeaponUpgradeLevel();
      }
    }

    return progresses;
  }

  protected override bool OnTap()
  {
    if (gameController.IsJoystickControl())
      OnMouseClick(currentSelect);
    return false;
  }

  public override void OnMouseClick(int index)
  {
    currentButtonId = buttons[currentSubMenu][index].id;

    if (hiddenButtons.Contains(currentButtonId))
      return;

    if (OnTapDefault())
      return;

    switch (currentButtonId)
    {
      case 101:
        if (!levelController.OneSpacetruckIsStarted() 
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_TERRAFORMER)))
        {
          levelController.HideTerminalMenu();
          levelController.AddBaseTo();
        }
        break;
      case 109:
        levelController.HideTerminalMenu();
        break;
      case 110:
        if (levelController.Strike())
        {
          levelController.HideTerminalMenu();
        }
        break;
      case 202:
        if (!levelController.OneSpacetruckIsStarted() 
          && levelController.СheckAvailabilityOfBaseNearby()
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_HARVESTER)))
        {
          levelController.HideTerminalMenu();
          levelController.AddObjectToCurrentBase(EMapObjectType.TYPE_HARVESTER);
        }
        break;
      case 402:
        if (!levelController.OneSpacetruckIsStarted() 
          && levelController.СheckAvailabilityOfBaseNearby()
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_TURRET)))
        {
          levelController.HideTerminalMenu();
          levelController.AddObjectToCurrentBase(EMapObjectType.TYPE_TURRET);
        }
        break;
      case 403:
        if (!levelController.OneSpacetruckIsStarted()
          && levelController.СheckAvailabilityOfBaseNearby()
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_TERRAFORMER_UPGRADE_SHIELD)))
        {
          levelController.HideTerminalMenu();
          levelController.AddObjectToCurrentBase(EMapObjectType.TYPE_TERRAFORMER_UPGRADE_SHIELD);
        }
        break;
      case 405:
        if (!levelController.OneSpacetruckIsStarted()
          && levelController.СheckAvailabilityOfBaseNearby()
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_TERRAFORMER_UPGRADE_WEAPON)))
        {
          levelController.HideTerminalMenu();
          levelController.AddObjectToCurrentBase(EMapObjectType.TYPE_TERRAFORMER_UPGRADE_WEAPON);
        }
        break;
      case 204:
        if (!levelController.OneSpacetruckIsStarted()
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_BIKE)))
        {
          levelController.HideTerminalMenu();
          levelController.AddObjectToHero(EMapObjectType.TYPE_BIKE);
        }
        break;
      case 205:
        if (!levelController.OneSpacetruckIsStarted()
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_BMP)))
        {
          levelController.HideTerminalMenu();
          levelController.AddObjectToHero(EMapObjectType.TYPE_BMP);
        }
        break;
      case 206:
        if (!levelController.OneSpacetruckIsStarted()
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_TANK)))
        {
          levelController.HideTerminalMenu();
          levelController.AddObjectToHero(EMapObjectType.TYPE_TANK);
        }
        break;
      case 207:
        if (!levelController.OneSpacetruckIsStarted()
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_STORM)))
        {
          levelController.HideTerminalMenu();
          levelController.AddObjectToHero(EMapObjectType.TYPE_STORM);
        }
        break;
      case 208:
        if (!levelController.OneSpacetruckIsStarted()
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_FIGHTER)))
        {
          levelController.HideTerminalMenu();
          levelController.AddObjectToHero(EMapObjectType.TYPE_FIGHTER);
        }
        break;
      case 209:
        if (!levelController.OneSpacetruckIsStarted()
          && levelController.СheckResourcesInOrbit(variableManager.GetResourceCost(EMapObjectType.TYPE_CANNON)))
        {
          levelController.HideTerminalMenu();
          levelController.AddObjectToHero(EMapObjectType.TYPE_CANNON);
        }
        break;
      case 302:
        //levelController.HideTerminalMenu();
        //levelController.SaveWorld();
        break;
      case 303:
        //ShowAcceptDialog(ACCEPT_DIALOG_TYPE_LOAD);
        break;
      case 304:
        ShowAcceptDialog(ACCEPT_DIALOG_TYPE_EXIT);
        break;
    }

    return;
  }

  protected override void HintUpdate()
  {
    currentButtonId = buttons[currentSubMenu][currentSelect].id;

    if (hiddenButtons.Contains(currentButtonId))
    {
      uiController.SetBottomHint(getButtonHintCode(0));
      return;
    }

    switch (currentButtonId)
    {
      case 106:
        uiController.SetBottomHint(getButtonHintCode(currentButtonId), levelController.GetResourcesInOrbitTimer());
        break;
      /*case 108:
        uiController.SetBottomHint(getButtonHintCode(currentButtonId), levelController.GetTerraformersCount(), levelController.GetTerraformersNeedForWin());
        break;*/
      default:
        uiController.SetBottomHint(getButtonHintCode(currentButtonId));
        break;
    }
  }

  protected override void SubMenuChangedEvent()
  {
    /*if (currentSubMenu == 1)
    {
      levelController.СheckAvailabilityOfBaseNearby();
    }*/
  }

  protected override void ShowAcceptDialog(int type)
  {
    currentDialogAcceptType = type;
    switch (currentDialogAcceptType)
    {
      case ACCEPT_DIALOG_TYPE_LOAD:
        AcceptDialog.gameObject.SetActive(true);
        AcceptDialogText.text = localizationManager.GetWord("ASSISTANT_MENU_ACCEPT_DIALOG_LOAD");
        AcceptDialogBtnYes.text = localizationManager.GetWord("BUTTON_LABEL_YES");
        AcceptDialogBtnNo.text = localizationManager.GetWord("BUTTON_LABEL_NO");
        break;
      case ACCEPT_DIALOG_TYPE_EXIT:
        AcceptDialog.gameObject.SetActive(true);
        AcceptDialogText.text = localizationManager.GetWord("ASSISTANT_MENU_ACCEPT_DIALOG_EXIT");
        AcceptDialogBtnYes.text = localizationManager.GetWord("BUTTON_LABEL_YES");
        AcceptDialogBtnNo.text = localizationManager.GetWord("BUTTON_LABEL_NO");
        break;
    }
  }

  protected override void HideAcceptDialog()
  {
    currentDialogAcceptType = ACCEPT_DIALOG_TYPE_NONE;
    AcceptDialog.gameObject.SetActive(false);
  }

  protected override void OnAcceptDialogClickYes()
  {
    switch (currentDialogAcceptType)
    {
      case ACCEPT_DIALOG_TYPE_LOAD:
        HideAcceptDialog();
        levelController.HideTerminalMenu();
        levelController.LoadWorld();
        break;
      case ACCEPT_DIALOG_TYPE_EXIT:
        HideAcceptDialog();
        levelController.HideTerminalMenu();
        levelController.GoToMainMenu();
        break;
    }
  }

  protected override void OnAcceptDialogClickNo()
  {
    switch (currentDialogAcceptType)
    {
      case ACCEPT_DIALOG_TYPE_LOAD:
      case ACCEPT_DIALOG_TYPE_EXIT:
        HideAcceptDialog();
        break;
    }
  }
}
