﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static MapObject;

public class UI__Controller : MonoBehaviour
{
  private const float UI_HEIGHT = 5f;

  public Text DebugLine;
  public Text TopHint;
  public Text BottomHint;
  public Text MiddleHint;

  public UI__UsableObjectMarker usableObjectMarker;
  public UI__AssistantMenu assistantMenu;
  public UI__Map map;
  public UI__JoystickHint joystickHint;
  public UI__MouseHint mouseHint;
  public UI__WasdHint wasdHint;
  public UI__KeyboardHint keyboardHint;
  public UI__EnemyPointers enemyPointers;
  public UI__Loading Loading;
  public UI__Chat Chat;
  public UI__TaskInfo TaskInfo;
  public UI__ToastList ToastList;

  private LocalizationManager localizationManager;

  private bool isInit = false;

  private void Start()
  {
    localizationManager = FindObjectOfType<LocalizationManager>();
    ClearBottomHint();
  }

  private void LateUpdate()
  {
    if (!isInit)
    {
      isInit = true;
      if (PlayerPrefs.HasKey(Const.PLAYER_PREFS_LANGUAGE))
      {
        localizationManager.SetLang(PlayerPrefs.GetString(Const.PLAYER_PREFS_LANGUAGE));
      }
      else
      {
        localizationManager.SetLang(Const.PLAYER_PREFS_VALUE_LANGUAGE_EN);
      }
      LoadingShow();
    }
  }

  public void LoadingShow()
  {
    Loading.Set(localizationManager.GetWord("LOADING"));
    Loading.Show();
  }

  public void LoadingHide()
  {
    Loading.Hide();
  }

  public int SwitchAssistantMenu()
  {
    ClearBottomHint();
    HideMap();
    return assistantMenu.Switch();
  }

  public void ShowAssistantMenu()
  {
    ClearBottomHint();
    assistantMenu.On();
  }

  public void HideAssistantMenu()
  {
    ClearBottomHint();
    assistantMenu.Off();
  }

  public bool IsShowAssistantMenu()
  {
    return assistantMenu.IsShow();
  }

  public void RunAssistantMenuHelpMode()
  {
    assistantMenu.RunHelpMode();
  }

  public bool IsAssistantMenuHelpMode()
  {
    return assistantMenu.IsHelpMode();
  }

  public void SetDebugLine(string message)
  {
    DebugLine.color = Color.red;
    DebugLine.alignment = TextAnchor.MiddleCenter;
    DebugLine.text = message;
  }

  public void SetTopHintToRight(string code)
  {
    TopHint.color = Color.white;
    TopHint.alignment = TextAnchor.MiddleRight;
    TopHint.text = Utils.GetString4UI(localizationManager.GetWord(code));
  }

  public void SetBottomHint(string code)
  {
    BottomHint.color = Color.white;
    BottomHint.alignment = TextAnchor.MiddleCenter;
    BottomHint.text = Utils.GetString4UI(localizationManager.GetWord(code));
  }

  public void SetBottomHint(string code, string value1)
  {
    BottomHint.color = Color.white;
    BottomHint.alignment = TextAnchor.MiddleCenter;
    BottomHint.text = Utils.GetString4UI(String.Format(localizationManager.GetWord(code), value1));
  }

  public void SetBottomHint(string code, string value1, string value2)
  {
    BottomHint.color = Color.white;
    BottomHint.alignment = TextAnchor.MiddleCenter;
    BottomHint.text = Utils.GetString4UI(String.Format(localizationManager.GetWord(code), value1, value2));
  }

  public void SetBottomHint(string code, int value1, int value2)
  {
    MiddleHint.color = Color.white;
    BottomHint.alignment = TextAnchor.MiddleCenter;
    MiddleHint.text = Utils.GetString4UI(String.Format(localizationManager.GetWord(code), value1, value2));
  }

  public void SetMiddleHint(string code)
  {
    MiddleHint.color = Color.white;
    MiddleHint.alignment = TextAnchor.MiddleCenter;
    MiddleHint.text = Utils.GetString4UI(localizationManager.GetWord(code));
  }

  public void SetMiddleHint(string code, string value1)
  {
    MiddleHint.color = Color.white;
    MiddleHint.alignment = TextAnchor.MiddleCenter;
    MiddleHint.text = Utils.GetString4UI(String.Format(localizationManager.GetWord(code), value1));
  }

  public void SetMiddleHint(string code, string value1, string value2)
  {
    MiddleHint.color = Color.white;
    MiddleHint.alignment = TextAnchor.MiddleCenter;
    MiddleHint.text = Utils.GetString4UI(String.Format(localizationManager.GetWord(code), value1, value2));
  }

  public void SetMiddleHint(string code, int value1, int value2)
  {
    MiddleHint.color = Color.white;
    MiddleHint.alignment = TextAnchor.MiddleCenter;
    MiddleHint.text = Utils.GetString4UI(String.Format(localizationManager.GetWord(code), value1, value2));
  }

  public void ClearBottomHint()
  {
    BottomHint.text = "";
  }

  public void ClearTopHint()
  {
    TopHint.text = "";
  }

  public void ClearMiddleHint()
  {
    MiddleHint.text = "";
  }

  public void ClearHints()
  {
    ClearTopHint();
    ClearBottomHint();
    ClearMiddleHint();
  }

  public void UpdateAssistanMenuProgresses()
  {
    assistantMenu.UpgateProgresses();
  }

  public void UsableObjectMarkerShow(Vector3 position)
  {
    usableObjectMarker.On(position);
  }

  public void UsableObjectMarkerHide()
  {
    usableObjectMarker.Off();
  }

  public bool JoystickHintShow(int state)
  {
    if (joystickHint.GetCurrentAnimationState() != state)
    {
      joystickHint.Show(state);
      return true;
    }
    return false;
  }

  public bool JoystickHintHide(int state)
  {
    if (joystickHint.GetCurrentAnimationState() == state)
    {
      JoystickHintHide();
      return true;
    }
    return false;
  }

  public void JoystickHintHide()
  {
    joystickHint.Hide();
  }

  public bool IsJoystickHintShow()
  {
    return joystickHint.IsShow();
  }

  public bool MouseHintShow(int state)
  {
    if (mouseHint.GetCurrentAnimationState() != state)
    {
      mouseHint.Show(state);
      return true;
    }
    return false;
  }

  public bool MouseHintHide(int state)
  {
    if (mouseHint.GetCurrentAnimationState() == state)
    {
      MouseHintHide();
      return true;
    }
    return false;
  }

  public void MouseHintHide()
  {
    mouseHint.Hide();
  }

  public bool IsMouseHintShow()
  {
    return mouseHint.IsShow();
  }

  public bool WasdHintShow(int state)
  {
    if (wasdHint.GetCurrentAnimationState() != state)
    {
      wasdHint.Show(state);
      return true;
    }
    return false;
  }

  public bool WasdHintHide(int state)
  {
    if (wasdHint.GetCurrentAnimationState() == state)
    {
      WasdHintHide();
      return true;
    }
    return false;
  }

  public void WasdHintHide()
  {
    wasdHint.Hide();
  }

  public bool IsWasdHintShow()
  {
    return wasdHint.IsShow();
  }

  public bool KeyboardHintShow(int state)
  {
    if (keyboardHint.GetCurrentAnimationState() != state)
    {
      keyboardHint.Show(state);
      return true;
    }
    return false;
  }

  public bool KeyboardHintHide(int state)
  {
    if (keyboardHint.GetCurrentAnimationState() == state)
    {
      KeyboardHintHide();
      return true;
    }
    return false;
  }

  public void KeyboardHintHide()
  {
    keyboardHint.Hide();
  }

  public bool IsKeyboardHintShow()
  {
    return keyboardHint.IsShow();
  }

  public void MovePointers(Vector3 ownerPosition)
  {
    enemyPointers.MoveTo(ownerPosition);
  }

  public void ShowPointers(List<Vector3> directions, EMapObjectType objectType)
  {
    enemyPointers.gameObject.SetActive(true);
    enemyPointers.ShowPointers(directions, objectType);
  }

  public void HidePointers()
  {
    enemyPointers.gameObject.SetActive(false);
    enemyPointers.HidePointers();
  }

  public bool IsShowMap()
  {
    return map.IsShow();
  }

  public void ShowMap(List<IMapObject> mapObjects, float worldSize)
  {
    map.Show(mapObjects, worldSize);
  }

  public void UpdateMap(List<IMapObject> mapObjects)
  {
    map.Refresh(mapObjects);
  }

  public void HideMap()
  {
    map.Hide();
  }

  public void ShowDialog(LevelDialog dialog)
  {
    Chat.Show(dialog);
  }

  public void HideDialog()
  {
    Chat.Hide();
  }

  public bool IsShowDialog()
  {
    return Chat.IsShow();
  }

  public void ShowTaskInfo(LevelTask levelTask)
  {
    TaskInfo.Show(levelTask);
  }

  public void HideTaskInfo()
  {
    TaskInfo.Hide();
  }

  public void ToastInfo(string code)
  {
    ToastList.Info(Utils.GetString4UI(localizationManager.GetWord(code)));
  }

  public void ToastInfo(string code, int value1)
  {
    ToastList.Info(Utils.GetString4UI(String.Format(localizationManager.GetWord(code), value1)));
  }

  public void ToastError(string code)
  {
    ToastList.Error(Utils.GetString4UI(localizationManager.GetWord(code)));
  }

  public void ToastError(string code, int value1)
  {
    ToastList.Error(Utils.GetString4UI(String.Format(localizationManager.GetWord(code), value1)));
  }
}
