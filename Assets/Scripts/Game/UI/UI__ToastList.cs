﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI__ToastList : MonoBehaviour
{
  public RectTransform List;
  public GameObject ToastPrefab;

  public void Info(string text)
  {
    GameObject gameObject = Instantiate(ToastPrefab, Vector3.zero, Quaternion.identity);
    gameObject.transform.SetParent(List, false);
    gameObject.GetComponent<UI__Toast>().Info(text);
  }

  public void Error(string text)
  {
    GameObject gameObject = Instantiate(ToastPrefab, Vector3.zero, Quaternion.identity);
    gameObject.transform.SetParent(List, false);
    gameObject.GetComponent<UI__Toast>().Error(text);
  }
}
