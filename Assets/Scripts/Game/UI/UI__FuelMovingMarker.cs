﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI__FuelMovingMarker : MonoBehaviour, IUI__Marker
{
  AnimationObject animation;
  protected bool isOn = false;
  protected bool isOnAnimation = false;

  void Start()
  {
    animation = GetComponent<AnimationObject>();
    animation.SetAnimationDelay(0.1f);
    Off();
  }

  void Update()
  {
    if (!isOn)
      return;

    if (isOnAnimation)
      animation.CheckAnimation();
  }

  public void GoTo(Vector3 vector3)
  {
    this.transform.position = vector3;
  }

  public void Rotate(float angle)
  {
    this.transform.eulerAngles = new Vector3(0, angle, 0);
  }

  public void PlayAnimation()
  {
    isOnAnimation = true;
  }

  public void StopAnimation()
  {
    isOnAnimation = false;
  }

  public void On()
  {
    isOn = true;
    animation.SetState(0);
    gameObject.SetActive(true);
    PlayAnimation();
  }

  public void Off()
  {
    isOn = false;
    animation.SetState(0);
    gameObject.SetActive(false);
    StopAnimation();
  }

  public void SetForvard(Vector3 vector3)
  {
    this.transform.forward = vector3;
  }

  public void SetMarkerState(int state)
  {
    if (state == Const.FUEL_MOVING_START)
      PlayAnimation();
    else
      StopAnimation();
  }
}
