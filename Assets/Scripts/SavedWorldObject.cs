﻿using System;
using UnityEngine;

[Serializable]
public class SavedWorldObject : WorldObject
{
  public int id = 0;
  public int ownerId = 0;

  public int fuel = -1;
  public int hp = -1;

  public int moveTargetId = 0;
  public int attackTargetId = 0;

  public void Print()
  {
    Debug.Log("=========");
    Debug.Log("type=" + type);
    Debug.Log("subType=" + subType);
    Debug.Log("id=" + id);
    Debug.Log("ownerId=" + ownerId);
    Debug.Log("fuel=" + fuel);
    Debug.Log("hp=" + hp);
    Debug.Log("moveTargetId=" + moveTargetId);
    Debug.Log("attackTargetId=" + attackTargetId);
    Debug.Log("position=" + GetPosition());
    Debug.Log("=========");
  }

  public static SavedWorldObject GetSavedWorldObject(WorldObject worldObject)
  {
    SavedWorldObject savedWorldObject = new SavedWorldObject();
    savedWorldObject.SetPosition(worldObject.GetPosition());
    savedWorldObject.type = worldObject.type;
    savedWorldObject.subType = worldObject.subType;
    savedWorldObject.state = worldObject.state;
    savedWorldObject.angle = worldObject.angle;

    savedWorldObject.groupId = worldObject.groupId;
    savedWorldObject.SetGroupPosition(worldObject.GetGroupPosition());

    savedWorldObject.scaleXYZ = worldObject.scaleXYZ;
    savedWorldObject.scaleX = worldObject.scaleX;
    savedWorldObject.scaleZ = worldObject.scaleZ;
    savedWorldObject.scaleXZ = worldObject.scaleXZ;
    savedWorldObject.scaleY = worldObject.scaleY;

    savedWorldObject.behaviorId = worldObject.behaviorId;
    return savedWorldObject;
  }
}
