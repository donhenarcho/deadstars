﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
  public Terrain terrain;

  public GameObject PanelDebug;
  public Dropdown DropdownLevels;
  public InputField InputFieldSizeMap;
  public Toggle ToggleConfirmGenerate;
  public Button ButtonGenerate;

  public GameObject PanelStart;
  public GameObject PanelSettings;
  public GameObject PanelLanguage;
  public GameObject PanelDialogAccept;
  public GameObject PanelLevel;
  public GameObject PanelDemo;

  public GameObject Space1;
  public GameObject Space2;

  public GameObject ButtonContinue;

  public Text VersionLabel;
  public Text BtnNewGameLabel;
  public Text BtnContinueLabel;
  public Text BtnSettingsLabel;
  public Text BtnExitLabel;

  public Text MusicLabel;
  public Text SoundLabel;
  public Text JoystickLabel;
  public Text BtnLanguageLabel;
  public Text BtnBackLabel;

  public Slider SliderMusic;
  public Slider SliderSound;

  public Toggle ToggleJoystick;

  public Text DialogAcceptText;
  public Text BtnDialogAcceptYes;
  public Text BtnDialogAcceptNo;

  public Text PanelLevelText;
  public Text BtnPanelLevelNext;
  public Text BtnPanelLevelBack;

  public Text PanelDemoText;
  public Text BtnPanelDemoSteam;
  public Text BtnPanelDemoBack;

  public UI__Loading Loading;

  private AudioController audioController;
  private LocalizationManager localizationManager;
  private VariableManager variableManager;

  private bool isInit = false;

  void Start()
  {
    terrain.terrainData.size = new Vector3(100, 100, 100);
    terrain.gameObject.transform.position = new Vector3(-50, 0, -50);

    audioController = FindObjectOfType<AudioController>();
    localizationManager = FindObjectOfType<LocalizationManager>();
    variableManager = FindObjectOfType<VariableManager>();

    PanelDebug.SetActive(Const.TEST_MODE);
    if (Const.TEST_MODE)
    {
      if (PlayerPrefs.HasKey(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID)
        && PlayerPrefs.GetInt(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID) > 1)
      {
        DropdownLevels.value = PlayerPrefs.GetInt(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID) - 1;
      }
    }

    InputFieldSizeMap.gameObject.SetActive(Const.DEBUG_MODE);
    ToggleConfirmGenerate.gameObject.SetActive(Const.DEBUG_MODE);
    ButtonGenerate.gameObject.SetActive(Const.DEBUG_MODE);

    VersionLabel.text = Const.VERSION;

    PanelStart.SetActive(true);
    PanelSettings.SetActive(false);
    PanelLanguage.SetActive(false);
    PanelDialogAccept.SetActive(false);
    PanelLevel.SetActive(false);
    PanelDemo.SetActive(false);

    if (CheckGameProgress())
    {
      ButtonContinue.SetActive(true);
      Space1.SetActive(false);
      Space2.SetActive(true);
    }
    else
    {
      ButtonContinue.SetActive(false);
      Space1.SetActive(true);
      Space2.SetActive(false);
    }

    if (PlayerPrefs.HasKey(Const.PLAYER_PREFS_AFTER_WIN) 
      && PlayerPrefs.GetInt(Const.PLAYER_PREFS_AFTER_WIN) == 1)
    {
      PlayerPrefs.SetInt(Const.PLAYER_PREFS_AFTER_WIN, 0);
      Continue();
    }

    Loading.Hide();
  }

  void Update()
  {
    if (!isInit)
    {
      isInit = true;
      if (PlayerPrefs.HasKey(Const.PLAYER_PREFS_LANGUAGE))
      {
        localizationManager.SetLang(PlayerPrefs.GetString(Const.PLAYER_PREFS_LANGUAGE));
      }
      else
      {
        localizationManager.SetLang(Const.PLAYER_PREFS_VALUE_LANGUAGE_EN);
      }
      UpdateMenuLabels();
    }
  }

  private void UpdateMenuLabels()
  {
    BtnNewGameLabel.text = localizationManager.GetWord("MAIN_MENU_LABEL_NEW_GAME");
    BtnContinueLabel.text = localizationManager.GetWord("MAIN_MENU_LABEL_CONTINUE");
    BtnSettingsLabel.text = localizationManager.GetWord("MAIN_MENU_LABEL_SETTINGS");
    BtnExitLabel.text = localizationManager.GetWord("MAIN_MENU_LABEL_EXIT");

    MusicLabel.text = localizationManager.GetWord("MAIN_MENU_LABEL_MUSIC");
    SoundLabel.text = localizationManager.GetWord("MAIN_MENU_LABEL_SOUND");
    JoystickLabel.text = localizationManager.GetWord("MAIN_MENU_LABEL_JOYSTICK");
    BtnLanguageLabel.text = localizationManager.GetWord("MAIN_MENU_LABEL_LANGUAGE");
    BtnBackLabel.text = localizationManager.GetWord("MAIN_MENU_LABEL_BACK");

    DialogAcceptText.text = localizationManager.GetWord("MAIN_MENU_LABEL_DIALOG_ACCEPT_NEW_GAME");
    BtnDialogAcceptYes.text = localizationManager.GetWord("BUTTON_LABEL_YES");
    BtnDialogAcceptNo.text = localizationManager.GetWord("BUTTON_LABEL_NO");

    //PanelLevelText.text = "";
    BtnPanelLevelNext.text = localizationManager.GetWord("BUTTON_LABEL_PLAY");
    BtnPanelLevelBack.text = localizationManager.GetWord("MAIN_MENU_LABEL_BACK");
    
    //PanelDemoText.text = localizationManager.GetWord("MAIN_MENU_LABEL_DEMO");
    PanelDemoText.text = localizationManager.GetWord("MAIN_MENU_LABEL_END");
    BtnPanelDemoSteam.text = localizationManager.GetWord("BUTTON_LABEL_STEAM");
    BtnPanelDemoBack.text = localizationManager.GetWord("MAIN_MENU_LABEL_BACK");
  }

  private void LoadingShow()
  {
    Loading.Set(localizationManager.GetWord("LOADING"));
    Loading.Show();
  }

  private bool CheckGameProgress()
  {
    if (PlayerPrefs.HasKey(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID)
      && PlayerPrefs.GetInt(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID) > 1)
    {
      return true;
    }

    return false;
  }

  public void NewGame()
  {
    if (CheckGameProgress())
    {
      ShowDialogAcceptNewGame();
      return;
    }

    ShowPanelLevel();
  }

  private void ShowDialogAcceptNewGame()
  {
    DialogAcceptText.text = localizationManager.GetWord("MAIN_MENU_LABEL_DIALOG_ACCEPT_NEW_GAME");
    BtnDialogAcceptYes.text = localizationManager.GetWord("BUTTON_LABEL_YES");
    BtnDialogAcceptNo.text = localizationManager.GetWord("BUTTON_LABEL_NO");

    PanelStart.SetActive(false);
    PanelDialogAccept.SetActive(true);
  }

  public void CloseDialogAcceptNewGame()
  {
    PanelStart.SetActive(true);
    PanelDialogAccept.SetActive(false);
  }

  public void AcceptNewGame()
  {
    PlayerPrefs.SetInt(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID, 1);
    PlayerPrefs.Save();
    Play();
  }

  public void Continue()
  {
    if (PlayerPrefs.HasKey(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID))
    {
      if (PlayerPrefs.GetInt(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID) > Const.LAST_LEVEL_ID)
        ShowPanelDemo();
      else
        ShowPanelLevel();
    }
  }

  public void ShowPanelDemo()
  {
    PanelStart.SetActive(false);
    PanelDemo.SetActive(true);
  }

  public void BackFromDemo()
  {
    PanelStart.SetActive(true);
    PanelDemo.SetActive(false);
  }

  public void GoToSteam()
  {
    Application.OpenURL("https://store.steampowered.com/app/1425030/Cyberwinter/");
  }

  public void GoToDiscord()
  {
    Application.OpenURL("https://discord.gg/MfJQGZ3tQY");
  }

  private void ShowPanelLevel()
  {
    PanelLevelText.text = GetLevelInfo();
    PanelStart.SetActive(false);
    PanelLevel.SetActive(true);
  }

  private string GetLevelInfo()
  {
    Level level = variableManager.GetCurrentLevel();

    //Debug.Log("GetLevelInfo >>> " + level);

    string name = localizationManager.GetWord(level.NameCode);
    string description = localizationManager.GetWord(level.DescriptionCode);

    return Utils.GetString4UI(String.Format(localizationManager.GetWord("LEVEL_INFO"), level.Id, Const.LAST_LEVEL_ID, name, description));
  }

  public void Play()
  {
    LoadingShow();
    StartCoroutine(GoToGame());
  }

  public void BackFromPanelLevel()
  {
    PanelStart.SetActive(true);
    PanelLevel.SetActive(false);
  }

  private IEnumerator GoToGame()
  {
    yield return new WaitForSeconds(1f);
    SceneManager.LoadScene("Game", LoadSceneMode.Single);
  }

  private void LoadSettings()
  {
    SliderMusic.value = 100;
    if (PlayerPrefs.HasKey(Const.PLAYER_PREFS_MUSIC_VOLUME))
    {
      SliderMusic.value = PlayerPrefs.GetFloat(Const.PLAYER_PREFS_MUSIC_VOLUME);
    }

    SliderSound.value = 100;
    if (PlayerPrefs.HasKey(Const.PLAYER_PREFS_SOUND_VOLUME))
    {
      SliderSound.value = PlayerPrefs.GetFloat(Const.PLAYER_PREFS_SOUND_VOLUME);
    }

    ToggleJoystick.isOn = false;
    if (PlayerPrefs.HasKey(Const.PLAYER_PREFS_JOYSTICK_IS_ON))
    {
      ToggleJoystick.isOn = PlayerPrefs.GetInt(Const.PLAYER_PREFS_JOYSTICK_IS_ON) == 1;
    }
  }

  private void SaveSettings()
  {
    PlayerPrefs.SetFloat(Const.PLAYER_PREFS_MUSIC_VOLUME, SliderMusic.value);
    PlayerPrefs.SetFloat(Const.PLAYER_PREFS_SOUND_VOLUME, SliderSound.value);
    PlayerPrefs.SetInt(Const.PLAYER_PREFS_JOYSTICK_IS_ON, ToggleJoystick.isOn ? 1 : 0);
    PlayerPrefs.Save();
  }

  public void Settings()
  {
    LoadSettings();

    PanelStart.SetActive(false);
    PanelSettings.SetActive(true);
  }

  public void Exit()
  {
    Application.Quit();
  }

  public void Back()
  {
    SaveSettings();
    PanelStart.SetActive(true);
    PanelSettings.SetActive(false);
  }

  public void OnSliderMusicChanged()
  {
    audioController.OnSliderMusicChanged(SliderMusic.value);
  }

  public void OnSliderSoundChanged()
  {
    audioController.OnSliderSoundChanged(SliderSound.value);
  }

  public void SetLanguage(string lang)
  {
    localizationManager.SetLang(lang);
    PlayerPrefs.SetString(Const.PLAYER_PREFS_LANGUAGE, lang);
    PlayerPrefs.Save();
    UpdateMenuLabels();
    PanelLanguage.SetActive(false);
    PanelSettings.SetActive(true);
  }

  public void Language()
  {
    PanelSettings.SetActive(false);
    PanelLanguage.SetActive(true);
  }

  public void Generate()
  {
    if (!ToggleConfirmGenerate.isOn)
    {
      Debug.LogError("You need to confirm the operation!");
      return;
    }

    int levelIndex = DropdownLevels.value;
    WorldGenerator mapGenerator = new WorldGenerator();
    mapGenerator.SetMapSize(Int16.Parse(InputFieldSizeMap.text), Int16.Parse(InputFieldSizeMap.text), 50);
    mapGenerator.SetBlocksRatio(0.5f);
    //mapGenerator.SetFuelRatio(0.1f);
    mapGenerator.SetTreeScale(0.5f, 1.5f);
    mapGenerator.Start("world_" + (levelIndex + 1));
  }

  public void DebugPlay()
  {
    int levelIndex = DropdownLevels.value;
    PlayerPrefs.SetInt(Const.PLAYER_MEMORY_CURRENT_LEVEL_ID, levelIndex + 1);
    PlayerPrefs.Save();
    StartCoroutine(GoToGame());
  }
}
