﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowFPS : MonoBehaviour
{
  public static float fps;
  public Texture2D background;
  private int speed = 100;

  void Start()
  {
  }

  void OnGUI()
  {
    speed += 1;
    if (speed > 100)
    {
      fps = 1.0f / Time.deltaTime;
      speed = 0;
    }

    GUI.DrawTexture(new Rect(0, 0, 60, 20), background);
    GUI.Label(new Rect(2, 0, 100, 20), "FPS: " + (int)fps);
  }
}
