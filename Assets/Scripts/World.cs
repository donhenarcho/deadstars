﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class World
{
  public int sizeX = 10;
  public int sizeY = 10;
  public List<WorldObject> objects = new List<WorldObject>();

  public static void Write(World world, string filename)
  {
    using (Stream stream = File.Open(filename + ".clear", FileMode.Create))
    {
      var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
      binaryFormatter.Serialize(stream, world);
    }
  }

  public static World Read(string filename)
  {
    try
    {
      using (Stream stream = File.Open(filename + ".clear", FileMode.Open))
      {
        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        return (World)binaryFormatter.Deserialize(stream);
      }
    }
    catch (Exception e)
    {
      return new World();
    }
  }

  public static World Read(TextAsset textAsset)
  {
    try
    {
      Stream stream = new MemoryStream(textAsset.bytes);
      BinaryFormatter formatter = new BinaryFormatter();
      World data = formatter.Deserialize(stream) as World;
      stream.Close();
      return data;
    }
    catch (Exception e)
    {
      return new World();
    }
  }
}
