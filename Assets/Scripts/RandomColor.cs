﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColor : MonoBehaviour
{
  public Color[] colors;

  void OnEnable()
  {
    Renderer[] renderers = GetComponentsInChildren<Renderer>();
    int index = Random.Range(0, colors.Length);
    foreach (Renderer renderer in renderers)
      renderer.material.color = colors[index];
  }
}
