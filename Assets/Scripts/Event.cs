﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Event
{
  public const string MUSIC_BATTLE_START            = "music_battle_start";
  public const string MUSIC_BATTLE_STOP             = "music_battle_stop";
  public const string SOUND_FIRE_START              = "sound_fire_start";
  public const string SOUND_FIRE_STOP               = "sound_fire_stop";
  public const string SOUND_RUN_ON_SNOW_START       = "sound_run_on_snow_start";
  public const string SOUND_RUN_ON_SNOW_STOP        = "sound_run_on_snow_stop";
  public const string SOUND_HISS_START              = "sound_hiss_start";
  public const string SOUND_SPACESHIP_ENGINE_START  = "sound_spaceship_engine_start";
  public const string SOUND_SPACESHIP_ENGINE_STOP   = "sound_spaceship_engine_stop";
  public const string SOUND_SIRENA_START            = "sound_sirena_start";
  public const string SOUND_SIRENA_STOP             = "sound_sirena_stop";
}
