﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
  public static void Shuffle<T>(T[] array)
  {
    System.Random rng = new System.Random();
    int n = array.Length;
    while (n > 1)
    {
      int k = rng.Next(n--);
      T temp = array[n];
      array[n] = array[k];
      array[k] = temp;
    }
  }

  public static void Reverse<T>(T[] array)
  {
    int n = array.Length;
    for (int i = 0, j = n - 1; i < j; i++, j--)
    {
      T t = array[i];
      array[i] = array[j];
      array[j] = t;
    }
  }

  public static void Shuffle<T>(List<T> ts)
  {
    var count = ts.Count;
    var last = count - 1;
    for (var i = 0; i < last; ++i)
    {
      var r = UnityEngine.Random.Range(i, count);
      var tmp = ts[i];
      ts[i] = ts[r];
      ts[r] = tmp;
    }
  }

  public static void Print(int[,] _layer)
  {
    if (_layer == null)
    {
      return;
    }

    string logs = "";
    for (int y = 0; y < _layer.GetLength(1); ++y)
    {
      for (int x = 0; x < _layer.GetLength(0); ++x)
      {
        logs += "[" + _layer[x, y] + "]";
      }
      logs += "\n";
    }
    Debug.Log(logs);
  }

  public static Queue<Vector3> GetPathRandom(Vector3 center, float radius, float objectSize)
  {
    int variant = UnityEngine.Random.Range(0, 6);
    switch (variant)
    {
      case 0:
        return GetPathSpiral1(center, radius, objectSize);
      case 1:
        return GetPathSpiral2(center, radius, objectSize);
      case 2:
        return GetPathSnake1(center, radius, objectSize);
      case 3:
        return GetPathSnake2(center, radius, objectSize);
      case 4:
        return GetPathSnake3(center, radius, objectSize);
      case 5:
        return GetPathSnake4(center, radius, objectSize);
    }

    return GetPathSpiral2(center, radius, objectSize);
  }

  public static Queue<Vector3> GetPathSpiral1(Vector3 center, float radius, float objectSize)
  {
    Queue<Vector3> path = new Queue<Vector3>();

    float currentRadius = radius;
    float doubleObjectSize = objectSize * 2;

    while (currentRadius > doubleObjectSize)
    {
      getNextPoint(center, currentRadius, currentRadius, path);
      getNextPoint(center, -currentRadius, currentRadius, path);
      getNextPoint(center, -currentRadius, -currentRadius, path);
      getNextPoint(center, currentRadius, -currentRadius, path);
      getNextPoint(center, currentRadius, currentRadius, path);

      currentRadius -= doubleObjectSize;
    }

    return path;
  }

  public static Queue<Vector3> GetPathSpiral2(Vector3 center, float radius, float objectSize)
  {
    Queue<Vector3> path = new Queue<Vector3>();

    float doubleObjectSize = objectSize * 2;
    float currentRadius = doubleObjectSize;

    while (currentRadius < radius)
    {
      getNextPoint(center, currentRadius, currentRadius, path);
      getNextPoint(center, -currentRadius, currentRadius, path);
      getNextPoint(center, -currentRadius, -currentRadius, path);
      getNextPoint(center, currentRadius, -currentRadius, path);
      getNextPoint(center, currentRadius, currentRadius, path);

      currentRadius += doubleObjectSize;
    }

    return path;
  }

  public static Queue<Vector3> GetPathSnake1(Vector3 center, float radius, float objectSize)
  {
    Queue<Vector3> path = new Queue<Vector3>();

    float currentRadius = radius;
    float doubleObjectSize = objectSize * 2;

    while (currentRadius > -radius)
    {
      getNextPoint(center, radius, currentRadius, path);
      getNextPoint(center, -radius, currentRadius, path);

      currentRadius -= doubleObjectSize;
    }

    return path;
  }

  public static Queue<Vector3> GetPathSnake2(Vector3 center, float radius, float objectSize)
  {
    Queue<Vector3> path = new Queue<Vector3>();

    float currentRadius = radius;
    float doubleObjectSize = objectSize * 2;

    while (currentRadius > -radius)
    {
      getNextPoint(center, currentRadius, radius, path);
      getNextPoint(center, currentRadius, -radius, path);

      currentRadius -= doubleObjectSize;
    }

    return path;
  }

  public static Queue<Vector3> GetPathSnake3(Vector3 center, float radius, float objectSize)
  {
    Queue<Vector3> path = new Queue<Vector3>();

    float currentRadius = -radius;
    float doubleObjectSize = objectSize * 2;

    while (currentRadius < radius)
    {
      getNextPoint(center, -radius, currentRadius, path);
      getNextPoint(center, radius, currentRadius, path);

      currentRadius += doubleObjectSize;
    }

    return path;
  }

  public static Queue<Vector3> GetPathSnake4(Vector3 center, float radius, float objectSize)
  {
    Queue<Vector3> path = new Queue<Vector3>();

    float currentRadius = -radius;
    float doubleObjectSize = objectSize * 2;

    while (currentRadius < radius)
    {
      getNextPoint(center, currentRadius, -radius, path);
      getNextPoint(center, currentRadius, radius, path);

      currentRadius += doubleObjectSize;
    }

    return path;
  }

  private static Vector3 getNextPoint(Vector3 prevPoint, float dltX, float dltZ, Queue<Vector3> path)
  {
    Vector3 nextPoint = new Vector3(prevPoint.x + dltX, 0, prevPoint.z + dltZ);
    path.Enqueue(nextPoint);
    return nextPoint;
  }

  public static string SecToMinWithSec(float seconds)
  {
    TimeSpan time = TimeSpan.FromSeconds(seconds);
    return time.ToString(@"mm\:ss");
  }

  public static float CalculateAngle360(Vector3 from, Vector3 to)
  {
    return Quaternion.FromToRotation(from, to - from).eulerAngles.y;
  }

  public static string GetString4UI(string text)
  {
    return text.Replace("[n]", "\n")
      .Replace("[b]", "<b>")
      .Replace("[/b]", "</b>");
  }

  public static void SetCollision4Particles(ParticleSystem particleSystem, string tag)
  {
    ParticleSystem.CollisionModule collisionModule = particleSystem.collision;
    switch (tag)
    {
      case Const.TAG_FIRE:
        collisionModule.collidesWith = Const.LAYER_MASK_FOR_FIRE_GUN;
        break;
      case Const.TAG_PLASMA:
        collisionModule.collidesWith = Const.LAYER_MASK_FOR_PLASMA;
        break;
      case Const.TAG_SNOW_WAVE:
      case Const.TAG_SNOW_WAVE_SMALL:
      case Const.TAG_CYCLOP_FIRE:
      case Const.TAG_CENTAUR_ARROW:
      case Const.TAG_DRAGONFLY_ARROW:
        collisionModule.collidesWith = Const.LAYER_MASK_FOR_ENEMY_SEARCH;
        break;
      case Const.TAG_SNOW_BLOOD:
        collisionModule.collidesWith = Const.LAYER_MASK_FOR_ENEMY_SEARCH_WITH_TERRAIN;
        break;

    }
  }
}
