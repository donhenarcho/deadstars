﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;

public class LocalizationManager : MonoBehaviour
{
  public static LocalizationManager instance;
  public string[] tags;
  public TextAsset file;
  private string lang;
  private string Lang
  {
    get
    {
      return lang;
    }
    set
    {
      PlayerPrefs.SetString("Language", value);
      lang = value;
    }
  }

  private Dictionary<string, Dictionary<string, string>> languages;
  private XmlDocument xmlDoc = new XmlDocument();
  private XmlReader reader;

  public string GenLang()
  {
    return lang;
  }

  public void SetLang(string _lang)
  {
    PlayerPrefs.SetString("Language", _lang);
    lang = _lang;
    //Messenger.Broadcast(GameEvent.CHANGE_LANGUAGE);
  }

  void Awake()
  {
    instance = this;

    if (!PlayerPrefs.HasKey("Language"))
    {
      Lang = tags[0];
    }
    else
    {
      Lang = PlayerPrefs.GetString("Language");
    }

    languages = new Dictionary<string, Dictionary<string, string>>();
    reader = XmlReader.Create(new StringReader(file.text));
    xmlDoc.Load(reader);

    for (int i = 0; i < tags.Length; i++)
    {
      languages.Add(tags[i], new Dictionary<string, string>());
      XmlNodeList langs = xmlDoc["Data"].GetElementsByTagName(tags[i]);
      for (int j = 0; j < langs.Count; j++)
      {
        languages[tags[i]].Add(langs[j].Attributes["Key"].Value, langs[j].Attributes["Word"].Value);
      }
    }
  }

  void Start()
  {
  }

  void Update()
  {
    lang = PlayerPrefs.GetString("Language");
  }

  public string GetWord(string _lang, string _key)
  {
    return languages[_lang][_key];
  }

  public string GetWord(string _key)
  {
    if (languages[lang].ContainsKey(_key))
      return languages[lang][_key];
    else
      return "???";
  }
}
