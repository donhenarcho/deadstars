﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Const
{
  public const string VERSION = "v. 1.02";

  public const bool TEST_MODE = false;
  public const bool DEBUG_MODE = false;
  public const bool WITH_TIMER_MODE = false;

  public const int LAST_LEVEL_ID = 12;

  public const int TYPE_CONTROL_KEYBOARD_WITH_MOUSE = 0;
  public const int TYPE_CONTROL_JOYSTICK = 1;

  public const int MAX_BASE_UPGRADE_LEVEL = 5;

  public const int MAX_TARGET_COST = 99999999;

  public const float WORLD_RADIUS = 8.0f;
  public const float WORLD_GRAVITY = -9.8f;

  public const float FLYHEIGHT = 16f;

  public static readonly Vector3 WORLD_CENTER = new Vector3(0, 0, 0);

  public const int LEFT = 0;
  public const int TOP = 1;
  public const int RIGHT = 2;
  public const int BOTTOM = 3;

  public const int TOP_LEFT = 0;
  public const int TOP_RIGHT = 1;
  public const int BOTTOM_RIGHT = 2;
  public const int BOTTOM_LEFT = 3;

  public const int FUEL_MOVING_STOP = 0;
  public const int FUEL_MOVING_START = 1;
  public const int FUEL_MOVING_ERROR = 2;

  public static readonly int[] EDITOR_TYPES = new int[] { 2, 3, 4, 5, 6 };

  public const string TAG_FIRE = "Fire";
  public const string TAG_PLASMA = "Plasma";
  public const string TAG_MONSTERS = "Monsters";
  public const string TAG_SNOW_WAVE = "SnowWave";
  public const string TAG_SNOW_BLOOD = "SnowBlood";
  public const string TAG_SNOW_WAVE_SMALL = "SnowWaveSmall";
  public const string TAG_SNOW_WAVE_BIG = "SnowWaveBig";
  public const string TAG_FUEL = "Fuel";
  public const string TAG_CYCLOP_FIRE = "CyclopFire";
  public const string TAG_CENTAUR_ARROW = "CentaurArrow";
  public const string TAG_DRAGONFLY_ARROW = "DragonflyArrow";

  public const int LAYER_TERRAIN = 8;
  public const int LAYER_AFRAID_OF_FIRE = 9;
  public const int LAYER_AFRAID_OF_SNOW_WAVE = 10;
  public const int LAYER_HERO = 11;
  public const int LAYER_TRANSPORT = 13;
  public const int LAYER_BUILDING = 16;
  public const int LAYER_AI_TRANSPORT = 15;
  public const int LAYER_AI_ENEMY = 17;
  public const int LAYER_FUEL_CLEANER = 20;
  public const int LAYER_FUEL_SPAWN_POINT = 21;
  public const int LAYER_ENEMY_SPAWN_POINT = 22;
  public const int LAYER_STONES = 23;
  public const int LAYER_TREES = 25;
  public const int LAYER_FOUND_OBJECT = 28;
  public const int LAYER_INVISIBLE_OBJECT = 29;

  public const int LAYER_MASK_ENEMY_ONLY = 1 << Const.LAYER_AI_ENEMY;
  public const int LAYER_MASK_BUILDING_ONLY = 1 << Const.LAYER_BUILDING;
  public const int LAYER_MASK_FOUND_OBJECT_ONLY = 1 << Const.LAYER_FOUND_OBJECT;
  public const int LAYER_MASK_FOR_HIDDEN_OBJECTS = ((1 << Const.LAYER_HERO) | (1 << Const.LAYER_INVISIBLE_OBJECT));
  public const int LAYER_MASK_TRANSPORT_ONLY = 1 << Const.LAYER_TRANSPORT;
  public const int LAYER_MASK_FUEL_SPAWN_POINT_ONLY = 1 << Const.LAYER_FUEL_SPAWN_POINT;
  public const int LAYER_MASK_ENEMY_SPAWN_POINT_ONLY = 1 << Const.LAYER_ENEMY_SPAWN_POINT;
  public const int LAYER_MASK_TERRAIN_ONLY = 1 << Const.LAYER_TERRAIN;
  public const int LAYER_MASK_ALL_WITHOUT_HERO = ~(1 << Const.LAYER_HERO);

  public const int LAYER_MASK_FOR_ENEMY_SEARCH = ((1 << Const.LAYER_HERO) | (1 << Const.LAYER_BUILDING) | (1 << Const.LAYER_AI_TRANSPORT) | (1 << Const.LAYER_TRANSPORT) | (1 << Const.LAYER_FOUND_OBJECT));
  public const int LAYER_MASK_FOR_ENEMY_SEARCH_WITH_TERRAIN = ((1 << Const.LAYER_TERRAIN) | (1 << Const.LAYER_HERO) | (1 << Const.LAYER_BUILDING) | (1 << Const.LAYER_AI_TRANSPORT) | (1 << Const.LAYER_TRANSPORT) | (1 << Const.LAYER_FOUND_OBJECT));
  public const int LAYER_MASK_FOR_FIRE_GUN = ((1 << Const.LAYER_AFRAID_OF_FIRE) | (1 << Const.LAYER_AI_ENEMY) | (1 << Const.LAYER_STONES) | (1 << Const.LAYER_FOUND_OBJECT));
  public const int LAYER_MASK_FOR_PLASMA = ((1 << Const.LAYER_HERO) | (1 << Const.LAYER_BUILDING) | (1 << Const.LAYER_AI_TRANSPORT) | (1 << Const.LAYER_TRANSPORT) | (1 << Const.LAYER_AFRAID_OF_FIRE) | (1 << Const.LAYER_AI_ENEMY) | (1 << Const.LAYER_STONES) | (1 << Const.LAYER_TREES) | (1 << Const.LAYER_FOUND_OBJECT));

  public const float TERRAIN_HEIGHT = 100;

  public const int CONTROL_ACTION_FIRE = 101;
  public const int CONTROL_ACTION_USE = 102;
  public const int CONTROL_ACTION_MENU = 103;
  public const int CONTROL_ACTION_MAP = 104;
  public const int CONTROL_ACTION_ESCAPE = 105;
  public const int CONTROL_ACTION_RESTART = 106;
  public const int CONTROL_ACTION_PAUSE = 107;

  public const float BESIDE_OBJECTS_FOR_USE_DISTANCE = 6f;
  public const float BESIDE_OBJECTS_FOR_FIND_DISTANCE = 6f;
  public const float BESIDE_ENEMIES_DISTANCE = 100f;

  public const int MAX_HINT_SHOW_TIMES = 1;
  public const int JOYSTICK_HINT_TERMINAL_MENU = 100;
  public const int JOYSTICK_HINT_USE_TRANSPORT = 101;
  public const int JOYSTICK_HINT_FIRE = 102;
  public const int KEYBOARD_HINT_TERMINAL_MENU = 200;
  public const int MOUSE_HINT_USE_TRANSPORT = 201;
  public const int MOUSE_HINT_FIRE = 202;
  public const int KEYBOARD_HINT_WASD = 203;
  public const int KEYBOARD_HINT_MAP = 204;

  public const string PLAYER_PREFS_MUSIC_VOLUME = "PLAYER_PREFS_MUSIC_VOLUME";
  public const string PLAYER_PREFS_SOUND_VOLUME = "PLAYER_PREFS_SOUND_VOLUME";
  public const string PLAYER_PREFS_JOYSTICK_IS_ON = "PLAYER_PREFS_JOYSTICK_IS_ON";
  public const string PLAYER_PREFS_LANGUAGE = "PLAYER_PREFS_LANGUAGE";
  public const string PLAYER_PREFS_VALUE_LANGUAGE_EN = "EN";
  public const string PLAYER_PREFS_VALUE_LANGUAGE_RU = "RU";
  public const string PLAYER_MEMORY_CURRENT_LEVEL_ID = "PLAYER_MEMORY_CURRENT_LEVEL_ID";
  public const string PLAYER_PREFS_AFTER_WIN = "PLAYER_PREFS_AFTER_WIN";

  public static Vector3 OUT_OF_WORLD_POSITION = Vector3.one * 10000;

  public static Color MENU_COLOR_NEW_BUTTON = new Color(255f / 255f, 255f / 255f, 100f / 255f);
}
