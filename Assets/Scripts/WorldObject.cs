﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WorldObject
{
  public int groupId = 0;
  public float groupX = 0f;
  public float groupY = 0f;
  public float groupZ = 0f;

  public float x = 0f;
  public float y = 0f;
  public float z = 0f;

  public int type = 0;
  public int subType = 0;
  public int state = 0;
  public float angle = 0;

  public float scaleXYZ = 1f;
  public float scaleX = 1f;
  public float scaleZ = 1f;
  public float scaleXZ = 1f;
  public float scaleY = 1f;

  public int behaviorId = 0;

  public WorldObject()
  {

  }

  public WorldObject(LevelObject levelObject)
  {
    type = levelObject.Type;
    x = levelObject.PosX;
    y = levelObject.PosY;
    z = levelObject.PosZ;
    angle = levelObject.Angle;
    behaviorId = levelObject.BehaviorId;
  }

  public Vector3 GetPosition()
  {
    return new Vector3(x, y, z);
  }

  public bool IsZeroPosition()
  {
    return x == 0 && y == 0 && z == 0;
  }

  public Vector3 GetGroupPosition()
  {
    return new Vector3(groupX, groupY, groupZ);
  }

  public void SetPosition(Vector3 position)
  {
    x = position.x;
    y = position.y;
    z = position.z;
  }

  public void SetGroupPosition(Vector3 position)
  {
    groupX = position.x;
    groupY = position.y;
    groupZ = position.z;
  }
}
